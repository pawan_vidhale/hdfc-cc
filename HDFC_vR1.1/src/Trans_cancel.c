/*******************************************************************/
//				HDFC CC
//		            Receipt_Cancel.c
/*******************************************************************/
/**
 *
 * @author FTLIT003
**/
#include <header.h>
#include "sqlite3.h"
#include "logo.h"
/*******************************************************************/
//Declaration

/*******************************************************************/

Receipt_Cancel(char* Receipt_Cancel_Transaction,char* Receipt_Cancel_CC_no,char* Reprint_Cancel_Print_Count,char* PickPayment_Flag_show)
{
	int ret=0;	
	int Receipt_Cancel_key,OTP_Not_Key,Remark_cancel_key=0,No_Of_Copy_Canl,Battery_Cancell_key;	
	resultset Transaction_Status,Cancellation_Ip_Hwid,OTP_Passwd,OTP_Count,hwid_Exec,Time_Status,Get_allocation_customer_no,Get_pickup_customer_no;
	char otp_password[10],Transaction_Details[200],Transaction_RSA_b64[500],Cancellation_date[50],Cancellation_time[50],Cancelltimenow[20],Cancelldatenow[20],Print_Date_Time[100],Print_Cancell_Date_Time[100],Print_Cancell_Executive_code[100],Print_Cancell_Total_Amt[100],Cancellation_OTP_Count[30],Battery_return[20],Remark_cancel[15],Machine_ID[50],Print_Machine_ID[100],Print_Machine[100],Print_Transaction_No[100],Executive_Code[50],Print_Cancell_loan_no[100],Print_Cancell_date[50],Remark_Loan_No_amount[100],Print_Payment_Mode[50],Print_Payment_Amount[50],Customer_ID[30],Branch_Id[30];
	char MAX_NO_CANCELLATION=0;
	resultset wallet_hour_set;

	memset(&wallet_hour_set,0,sizeof(wallet_hour_set));

	lk_dispclr();
	lk_disptext(0,4,"   HDFC Bank CC",0);
	lk_disptext(1,0,"Payment Cancellation",0);	
	lk_disphlight(1);
	lk_disptext(2,0,"  Would you like to",0);
	lk_disptext(3,0,"    Cancel this ",0);
	lk_disptext(4,0,"   Transaction?",0);	
	lk_disptext(5,0,"  1=>YES   2=>NO",0);
	Receipt_Cancel_key=lk_getkey();	
	if(Receipt_Cancel_key==1)
	{
			
Remark_cancel:
		chk_rtc();
		lk_dispclr(); 
		lk_disptext(0,1,"     HDFC Bank CC",0);
		lk_disptext(1,1,"Cancellation Remark",0);
		lk_disphlight(1);
		lk_disptext(2,0," Please Press Key:",0);		
		lk_disptext(3,0,"1=> CC Number",0);
		lk_disptext(4,0,"2=> Amount Entered",0);
		lk_disptext(5,0,"3=> Invalid Chq Details",0);		
		Remark_cancel_key=lk_getkey();	
		memset(Remark_cancel,0,sizeof(Remark_cancel));		
		if(Remark_cancel_key==1) strcpy(Remark_cancel,"1");
		else if(Remark_cancel_key==2) strcpy(Remark_cancel,"2");
		else if(Remark_cancel_key==3) strcpy(Remark_cancel,"3");
	        else Receipt_Cancel(Receipt_Cancel_Transaction,Receipt_Cancel_CC_no,Reprint_Cancel_Print_Count,PickPayment_Flag_show);
		printf("\nIn Receipt_Cancel");
OTP:
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		OTP_Count = get_result("select OTP_Count,OTP from cancellationotps where OTP!='0';");	
		if(OTP_Count.rows>0)
		{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank CC",0);
		lk_disptext(1,2,"   Cancellation",0);
		lk_disphlight(1); 
		memset(Cancellation_OTP_Count,0,sizeof(Cancellation_OTP_Count));
		sprintf(Cancellation_OTP_Count,"Enter OTP %s",OTP_Count.recordset[0][0]);		
		lk_disptext(2,0,Cancellation_OTP_Count,0);	
		memset(otp_password,0,sizeof(otp_password));
		ret=0;
		ret=lk_getpassword(otp_password,5,6);
		otp_password[ret]='\0';
		printf("ret=%d\n",ret);			
		if(strlen(otp_password)>0&&strlen(otp_password)<7&&(Check_Numeric(otp_password)==1))
		{			
			if((strlen(otp_password)>0)&&(strcmp(OTP_Count.recordset[0][1],otp_password)==0)&&(Check_Numeric(otp_password)==1))			
			{	
			execute("Update CancellationOTPs SET OTP='0' where OTP='%s';",otp_password);
			close_sqlite();			
			open_sqlite("/mnt/jffs2/hdfccctrans.db");	
			execute("Update unuploadedtransactiondetails SET status='1' WHERE Transaction_ID='%s';",Receipt_Cancel_Transaction);
			close_sqlite();
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			memset(Machine_ID,0,sizeof(Machine_ID));
			memset(Executive_Code,0,sizeof(Executive_Code));	
			hwid_Exec=get_result("select hwid,Executive_id,last_transaction_no,Executive_Name from operationalstatus");
			memset(Print_Transaction_No,0,sizeof(Print_Transaction_No));			
			sprintf(Print_Transaction_No,"Receipt Number :     %s",Receipt_Cancel_Transaction);			
			memset(Cancelltimenow,0,sizeof(Cancelltimenow));
			sprintf(Cancelltimenow,"%02d:%02d:%02d",curt.tm_hour,curt.tm_min,curt.tm_sec);			
			memset(Cancelldatenow,0,sizeof(Cancelldatenow));
			sprintf(Cancelldatenow,"%02d-%02d-%02d",(curt.tm_mday),(curt.tm_mon+1),(curt.tm_year-100));
			memset(Print_Cancell_Date_Time,0,sizeof(Print_Cancell_Date_Time));			
			sprintf(Print_Cancell_Date_Time,"Date:%s         ",Cancelldatenow);	
			memset(Print_Cancell_date,0,sizeof(Print_Cancell_date));
			sprintf(Print_Cancell_date,"dated %s",Cancelldatenow);				
			if(Remark_cancel_key==1)
			{
			memset(Remark_Loan_No_amount,0,sizeof(Remark_Loan_No_amount)); 
			strcpy(Remark_Loan_No_amount,"Incorrect CC number");
			}
			else if(Remark_cancel_key==2)
			{
			memset(Remark_Loan_No_amount,0,sizeof(Remark_Loan_No_amount));
			strcpy(Remark_Loan_No_amount,"Incorrect amount entered");		 
			}
			else if(Remark_cancel_key==3)
			{
			memset(Remark_Loan_No_amount,0,sizeof(Remark_Loan_No_amount));
			strcpy(Remark_Loan_No_amount,"Invalid Chq Details");		 
			}

/***************************************CUST_ID***********************************/
if(strcmp(PickPayment_Flag_show,"P")==0)
{
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccc.db");
	Get_pickup_customer_no=get_result("SELECT Filler2,CARD_NUMBER,BRANCHID from PICKUP_DETAILS WHERE Filler3='%s';",Receipt_Cancel_CC_no);
	memset(Print_Cancell_loan_no,0,sizeof(Print_Cancell_loan_no));			
	sprintf(Print_Cancell_loan_no,"%s",Get_pickup_customer_no.recordset[0][1]);
	memset(Branch_Id,0,sizeof(Branch_Id));
	sprintf(Branch_Id,"%s",Get_pickup_customer_no.recordset[0][2]);
	memset(Customer_ID,0,sizeof(Customer_ID));
	sprintf(Customer_ID,"%s",Get_pickup_customer_no.recordset[0][0]);
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccc.db");
	execute("delete from PICKUP_DETAILS where Filler3='%s';",Receipt_Cancel_CC_no);					
}
	
else
{
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccc.db");
	Get_allocation_customer_no= get_result("SELECT filler1,CARD_NUMBER,BRANCH_CODE from allocation_details WHERE ALT_ACCOUNT_NO='%s';",Receipt_Cancel_CC_no);
	memset(Print_Cancell_loan_no,0,sizeof(Print_Cancell_loan_no));			
	sprintf(Print_Cancell_loan_no,"%s",Get_allocation_customer_no.recordset[0][1]);
	memset(Branch_Id,0,sizeof(Branch_Id));
	sprintf(Branch_Id,"%s",Get_allocation_customer_no.recordset[0][2]);
	memset(Customer_ID,0,sizeof(Customer_ID));
	sprintf(Customer_ID,"%s",Get_allocation_customer_no.recordset[0][0]);

}

/*************************************************************************************************/
printf("Oout concatenate string");
			memset(Battery_return,0,sizeof(Battery_return));
			sprintf(Battery_return,"%s",Battery_Print_test());
			printf("\nBattery_return:%0.2f",atof(Battery_return));
			if(atof(Battery_return)<(6.8))
			{
Low_Battery_Cancell:
			lk_dispclr();
			lk_disptext(1,5,"Battery Low!",1);
			lk_disptext(3,0,"Can't Do Cancellation.",1);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			printf("\nBattery is Now lower than 15%");
			Battery_Cancell_key = lk_getkey();
			if(Battery_Cancell_key==ENTER)
			{
			close_sqlite();			
			open_sqlite("/mnt/jffs2/hdfccctrans.db");	
			execute("Update unuploadedtransactiondetails SET status='0' WHERE Transaction_ID='%s';",Receipt_Cancel_Transaction);			
			Dra_Menu();
			}
			else goto Low_Battery_Cancell; 
			}
			else if(atof(Battery_return)<(6.92))
			{
			lk_dispclr();
			lk_disptext(1,5,"Battery Low!",1);
			lk_disptext(3,0,"  Plug Power Cord",0);//plug Power code cable
			lk_disptext(4,0," Cable & Press ENTER",0);
			lk_disptext(5,0,"  Key To continue",0);
			lk_getkey();
			}				
////////////////////////////
			memset(Cancellation_time,0,sizeof(Cancellation_time));
			sprintf(Cancellation_time,"%02d=%02d=%02d",curt.tm_hour,curt.tm_min,curt.tm_sec);	
			memset(Cancellation_date,0,sizeof(Cancellation_date));
			sprintf(Cancellation_date,"%02d=%02d=%04d",(curt.tm_mon+1),(curt.tm_mday),(curt.tm_year+1900));	
			close_sqlite();			
			open_sqlite("/mnt/jffs2/hdfccctrans.db");				
			Transaction_Status=get_result("Select Executive_Name,Executive_Code,Loan_No,Transaction_ID,Total_Amt,Payee_Type,Place_Contacted,Instument_Type,Bank_Name,chq_DD_No,chq_DD_Date,PAN_No,Payee_Mobile_No,Micr_number,Transaction_Date,Transaction_Time,Trn_cancel,Reprint_count,Branch_Id,Alt_Account_No From transactiondetails where Transaction_ID='%s';",Receipt_Cancel_Transaction);
			memset(Transaction_Details,0,sizeof(Transaction_Details));			
			sprintf(Transaction_Details,"Validate,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",Transaction_Status.recordset[0][1],Transaction_Status.recordset[0][2],Transaction_Status.recordset[0][3],Transaction_Status.recordset[0][4],Transaction_Status.recordset[0][5],Transaction_Status.recordset[0][6],Transaction_Status.recordset[0][7],Transaction_Status.recordset[0][8],Transaction_Status.recordset[0][9],Transaction_Status.recordset[0][10],Transaction_Status.recordset[0][11],Transaction_Status.recordset[0][12],Transaction_Status.recordset[0][13],Cancellation_date,Cancellation_time,Remark_cancel,Transaction_Status.recordset[0][17],PickPayment_Flag_show,Customer_ID,Transaction_Status.recordset[0][18],Transaction_Status.recordset[0][19]);
#if PRINTF 					
			printf("\nTransaction_Details:%s\n",Transaction_Details);
#endif			
			memset(Transaction_RSA_b64,0,sizeof(Transaction_RSA_b64));
			sprintf(Transaction_RSA_b64,"%s",RSAEncryption_Base64(Transaction_Details,strlen(Transaction_Details)));
			
			execute("INSERT INTO unuploadedtransactiondetails(Transaction_ID,Transaction_Details,status) VALUES('%s','%s','0');",Transaction_Status.recordset[0][3],Transaction_RSA_b64);//Need to changes			
						
			execute("INSERT INTO transactiondetails(Executive_Name,Executive_Code,Loan_No,Transaction_ID,Total_Amt,Payee_Type,Place_Contacted,Instument_Type,Bank_Name,chq_DD_No,chq_DD_Date,PAN_No,Payee_Mobile_No,Micr_number,Transaction_Date,Transaction_Time,Trn_cancel,Reprint_count,Customer_no,Branch_Id,Alt_Account_No) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",Transaction_Status.recordset[0][0],Transaction_Status.recordset[0][1],Transaction_Status.recordset[0][2],Transaction_Status.recordset[0][3],Transaction_Status.recordset[0][4],Transaction_Status.recordset[0][5],Transaction_Status.recordset[0][6],Transaction_Status.recordset[0][7],Transaction_Status.recordset[0][8],Transaction_Status.recordset[0][9],Transaction_Status.recordset[0][10],Transaction_Status.recordset[0][11],Transaction_Status.recordset[0][12],Transaction_Status.recordset[0][13],Cancellation_date,Cancellation_time,Remark_cancel,Transaction_Status.recordset[0][17],Customer_ID,Transaction_Status.recordset[0][18],Transaction_Status.recordset[0][19]);

			close_sqlite();								
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			if(atoi(Transaction_Status.recordset[0][5])==1)
				execute("Update operationalstatus SET WalletBalance=WalletBalance+%.2f;",atof(Transaction_Status.recordset[0][4]));
				
			memset(Print_Payment_Mode,0,sizeof(Print_Payment_Mode));
			sprintf(Print_Payment_Mode,"Payment Mode     : %s",Transaction_Status.recordset[0][7]);		
			memset(Print_Payment_Amount,0,sizeof(Print_Payment_Amount));
			sprintf(Print_Payment_Amount,"Payment Amount   : Rs.%s/-",Transaction_Status.recordset[0][4]);

			prn_open();

			for(No_Of_Copy_Canl=0;No_Of_Copy_Canl<2;No_Of_Copy_Canl++)
			{			
			if(prn_paperstatus()!=0)
			{
			lk_dispclr();
			lk_disptext(1,5,"No Paper !",1);
			lk_disptext(3,0,"Insert Paper & Press",0);
			lk_disptext(4,0," Any Key To continue",0);
			lk_getkey();
			}
			lk_dispclr();
			lk_disptext(2,2,"Printing..",1);
			ret=prn_write_bmp(HDFC_Logo,sizeof(HDFC_Logo));				
			ret=prn_write_text("Cancellation Receipt",20,2);
			ret=prn_write_text("----------------------------------------",40,1);
			ret=prn_write_text("Please note that the receipt #          ",40,1);
			ret=prn_write_text(Print_Transaction_No,strlen(Print_Transaction_No),2);
			ret=prn_write_text("Issued towards yours credit card number ",40,1);
			ret=prn_write_text(Print_Cancell_loan_no,strlen(Print_Cancell_loan_no),1);
			ret=prn_write_text(Print_Cancell_date,strlen(Print_Cancell_date),1);
			ret=prn_write_text("is being cancelled for the reason       ",40,1);
			ret=prn_write_text(Remark_Loan_No_amount,strlen(Remark_Loan_No_amount),1);
			ret=prn_write_text("----------------------------------------",40,1);
			ret=prn_write_text(Print_Payment_Mode,strlen(Print_Payment_Mode),1);
			ret=prn_write_text(Print_Payment_Amount,strlen(Print_Payment_Amount),1);
			ret=prn_write_text("----------------------------------------",40,1);			
			ret=prn_write_text("   Any overwriting",22,2);
			ret=prn_write_text("     is invalid",22,2);
			ret=prn_write_text("----------------------------------------",40,1);
			ret=prn_write_text("With regard to any discrepancy perta-   ",40,1);			
			ret=prn_write_text("ining to this receipt, please contact   ",40,1);
			ret=prn_write_text("National Manager,                       ",40,1);
			ret=prn_write_text("Retail Portfolio Management,            ",40,1);
			ret=prn_write_text("HDFC Bank Cards Division,               ",40,1);
			ret=prn_write_text("No.8, Lattice Bridge Road,              ",40,1);
			ret=prn_write_text("Thiruvanmiyur, Chennai 600041.          ",40,1);
			ret=prn_write_text("Please make the payment to an authorized",40,1);
			ret=prn_write_text("representative carrying a valid         ",40,1);
			ret=prn_write_text("identity card.                          ",40,1);
			prn_paper_feed(1);
			ret=prn_write_text("** Please store this receipt properly.  ",40,1);
			ret=prn_write_text("It is suggested that a photocopy should ",40,1);
			ret=prn_write_text("be taken for your prolonged record      ",40,1);				
			prn_paper_feed(2);
			if(No_Of_Copy_Canl==0)	sleep(10);
			}			
			prn_close();			
				
			Upload_Transaction_Request();
			close_sqlite();					
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			wallet_hour_set=get_result("SELECT   DueDays,DueHours FROM operationalstatus;");
#if PRINTF 
			printf("\nSet DueDays:%d",atol(wallet_hour_set.recordset[0][0]));
			printf("\nSet DueHours:%ld",atol(wallet_hour_set.recordset[0][1]));
			printf("\nCurrent Time:%ld",(curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365));
			printf("\nTime Differance:%d",((curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365)-atol(wallet_hour_set.recordset[0][1])));
#endif
			if((wallet_hour_set.rows>0)&&(((curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365)-atol(wallet_hour_set.recordset[0][1]))>(atol(wallet_hour_set.recordset[0][0])*24+1)))
			{
#if PRINTF 
				printf("\nSet DueDays:%d",atol(wallet_hour_set.recordset[0][0]));
				printf("\nSet DueHours:%ld",atol(wallet_hour_set.recordset[0][1]));
				printf("\nCurrent Time:%ld",(curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365));
#endif
				lk_dispclr();
				lk_disptext(2,0,"   Transaction",0);
				lk_disptext(3,0,"    Is Locked",0);
				lk_disptext(4,0," Please contact HO",0);
				sleep(10);
				close_sqlite();			
				open_sqlite("/mnt/jffs2/hdfcccoperational.db");
				execute("UPDATE operationalstatus SET ExecutiveStatus='0';");
				Dra_Menu();
			}
			else Dra_Menu();				
			}
			else
			{
				if(MAX_NO_CANCELLATION <= 1)
				{
					printf("\n\nMAX_NO_CANCELLATION%d",MAX_NO_CANCELLATION);
					lk_dispclr();	
					lk_disptext(2,2,"  Please Enter",0);
					lk_disptext(4,2,"   Valid OTP",0);
					sleep(3);
					MAX_NO_CANCELLATION++;		
					goto OTP;
				}
				else
				{
					lk_dispclr();	
					lk_disptext(2,0,"   OTP Attempt",0);
					lk_disptext(3,0,"Exceeded, Committing",0);
					lk_disptext(4,0,"   Transaction",0);
					sleep(3);
					printf("\nReprint_Cancel_Print_Count:%s",Reprint_Cancel_Print_Count);
					if(strcmp(Reprint_Cancel_Print_Count,"0")==0) Upload_Transaction_Request();
					else
					{
					memset(Cancellation_time,0,sizeof(Cancellation_time));
					sprintf(Cancellation_time,"%02d=%02d=%02d",curt.tm_hour,curt.tm_min,curt.tm_sec);	
					memset(Cancellation_date,0,sizeof(Cancellation_date));
					sprintf(Cancellation_date,"%02d=%02d=%04d",(curt.tm_mon+1),(curt.tm_mday),(curt.tm_year+1900));	
							
					printf("\nCancellation_date:%s\n",Cancellation_date);
					printf("\nCancellation_time:%s\n",Cancellation_time);
					close_sqlite();			
					open_sqlite("/mnt/jffs2/hdfccctrans.db");	
					execute("Update unuploadedtransactiondetails SET status='1' WHERE Transaction_ID='%s';",Receipt_Cancel_Transaction);
					close_sqlite();			
					open_sqlite("/mnt/jffs2/hdfccctrans.db");				
					Transaction_Status=get_result("Select Executive_Name,Executive_Code,Loan_No,Transaction_ID,Total_Amt,Payee_Type,Place_Contacted,Instument_Type,Bank_Name,chq_DD_No,chq_DD_Date,PAN_No,Payee_Mobile_No,Micr_number,Transaction_Date,Transaction_Time,Trn_cancel,Reprint_count,Customer_no,Branch_Id,Alt_Account_No From transactiondetails where Transaction_ID='%s';",Receipt_Cancel_Transaction);
					memset(Transaction_Details,0,sizeof(Transaction_Details));
					sprintf(Transaction_Details,"Validate,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,0,%s,%s,%s,%s,%s",Transaction_Status.recordset[0][1],Transaction_Status.recordset[0][2],Transaction_Status.recordset[0][3],Transaction_Status.recordset[0][4],Transaction_Status.recordset[0][5],Transaction_Status.recordset[0][6],Transaction_Status.recordset[0][7],Transaction_Status.recordset[0][8],Transaction_Status.recordset[0][9],Transaction_Status.recordset[0][10],Transaction_Status.recordset[0][11],Transaction_Status.recordset[0][12],Transaction_Status.recordset[0][13],Cancellation_date,Cancellation_time,Transaction_Status.recordset[0][17],PickPayment_Flag_show,Transaction_Status.recordset[0][18],Transaction_Status.recordset[0][19],Transaction_Status.recordset[0][20]);
#if PRINTF
					printf("\nTransaction_Details:%s\n",Transaction_Details);	
#endif
					memset(Transaction_RSA_b64,0,sizeof(Transaction_RSA_b64));
					sprintf(Transaction_RSA_b64,"%s",RSAEncryption_Base64(Transaction_Details,strlen(Transaction_Details)));
					execute("INSERT INTO unuploadedtransactiondetails(Transaction_ID,Transaction_Details,status) VALUES('%s','%s','0');",Transaction_Status.recordset[0][3],Transaction_RSA_b64);//Need to changes
					Upload_Transaction_Request();
					}					
					close_sqlite();					
					open_sqlite("/mnt/jffs2/hdfcccoperational.db");
					wallet_hour_set=get_result("SELECT DueDays,DueHours FROM operationalstatus;");
#if PRINTF
					printf("\nSet DueDays:%d",atol(wallet_hour_set.recordset[0][0]));
					printf("\nSet DueHours:%ld",atol(wallet_hour_set.recordset[0][1]));
					printf("\nCurrent Time:%ld",(curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365));
					printf("\nTime Differance:%d",((curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365)-atol(wallet_hour_set.recordset[0][1])));
#endif
					if((wallet_hour_set.rows>0)&&(((curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365)-atol(wallet_hour_set.recordset[0][1]))>(atol(wallet_hour_set.recordset[0][0])*24+1)))
					{
#if PRINTF 
						printf("\nSet DueDays:%d",atol(wallet_hour_set.recordset[0][0]));
						printf("\nSet DueHours:%ld",atol(wallet_hour_set.recordset[0][1]));
						printf("\nCurrent Time:%ld",(curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365));
#endif
						lk_dispclr();
						lk_disptext(2,0,"   Transaction",0);
						lk_disptext(3,0,"    Is Locked",0);
						lk_disptext(4,0," Please contact HO",0);
						sleep(10);
						close_sqlite();			
						open_sqlite("/mnt/jffs2/hdfcccoperational.db");
						execute("UPDATE operationalstatus SET ExecutiveStatus='0';");
						Dra_Menu();
					}
					else Dra_Menu();
				}
			}
		}
		else if(ret==-1) goto Remark_cancel;
		else goto OTP;  
		}
		else
		{
OTP_Not_Found:
			lk_dispclr();
			lk_disptext(1,2,"  Number of OTP",0);	
			lk_disptext(2,2,"  Exceeded, ",0);			
			lk_disptext(3,2,"  Do online Login",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);						
			OTP_Not_Key=lk_getkey();
			if(OTP_Not_Key==ENTER)
			{			
			Receipt_Cancel(Receipt_Cancel_Transaction,Receipt_Cancel_CC_no,Reprint_Cancel_Print_Count,PickPayment_Flag_show);
			}
			else goto OTP_Not_Found;			
		}
	}
	else if(Receipt_Cancel_key==2)
	{	
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		execute("delete from PICKUP_DETAILS where Filler3='%s';",Receipt_Cancel_CC_no);
		printf("\nReprint_Cancel_Print_Count:%s",Reprint_Cancel_Print_Count);
		if(strcmp(Reprint_Cancel_Print_Count,"0")==0) Upload_Transaction_Request();
		else
		{
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfccctrans.db");	
		execute("Update unuploadedtransactiondetails SET status='1' WHERE Transaction_ID='%s';",Receipt_Cancel_Transaction);
		
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfccctrans.db");				
		Transaction_Status=get_result("Select Executive_Name,Executive_Code,Loan_No,Transaction_ID,Total_Amt,Payee_Type,Place_Contacted,Instument_Type,Bank_Name,chq_DD_No,chq_DD_Date,PAN_No,Payee_Mobile_No,Micr_number,Transaction_Date,Transaction_Time,Trn_cancel,Reprint_count,Customer_no,Branch_Id,Alt_Account_No From transactiondetails where Transaction_ID='%s';",Receipt_Cancel_Transaction);
		memset(Transaction_Details,0,sizeof(Transaction_Details));
		sprintf(Transaction_Details,"Validate,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,0,%s,%s,%s,%s,%s",Transaction_Status.recordset[0][1],Transaction_Status.recordset[0][2],Transaction_Status.recordset[0][3],Transaction_Status.recordset[0][4],Transaction_Status.recordset[0][5],Transaction_Status.recordset[0][6],Transaction_Status.recordset[0][7],Transaction_Status.recordset[0][8],Transaction_Status.recordset[0][9],Transaction_Status.recordset[0][10],Transaction_Status.recordset[0][11],Transaction_Status.recordset[0][12],Transaction_Status.recordset[0][13],Transaction_Status.recordset[0][14],Transaction_Status.recordset[0][15],Transaction_Status.recordset[0][17],PickPayment_Flag_show,Transaction_Status.recordset[0][18],Transaction_Status.recordset[0][19],Transaction_Status.recordset[0][20]);
				
#if PRINTF
		printf("\nTransaction_Details:%s\n",Transaction_Details);	
#endif
		memset(Transaction_RSA_b64,0,sizeof(Transaction_RSA_b64));
		sprintf(Transaction_RSA_b64,"%s",RSAEncryption_Base64(Transaction_Details,strlen(Transaction_Details)));
		execute("INSERT INTO unuploadedtransactiondetails(Transaction_ID,Transaction_Details,status) VALUES('%s','%s','0');",Transaction_Status.recordset[0][3],Transaction_RSA_b64);//Need to changes
		Upload_Transaction_Request();			
		}


		close_sqlite();					
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		wallet_hour_set=get_result("SELECT DueDays,DueHours FROM operationalstatus;");
#if PRINTF
		printf("\nSet DueDays:%d",atol(wallet_hour_set.recordset[0][0]));
		printf("\nSet DueHours:%ld",atol(wallet_hour_set.recordset[0][1]));
		printf("\nCurrent Time:%ld",(curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365));
		printf("\nTime Differance:%d",((curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365)-atol(wallet_hour_set.recordset[0][1])));
#endif
		if((wallet_hour_set.rows>0)&&(((curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365)-atol(wallet_hour_set.recordset[0][1]))>(atol(wallet_hour_set.recordset[0][0])*24+1)))
		{
#if PRINTF 
			printf("\nSet DueDays:%d",atol(wallet_hour_set.recordset[0][0]));
			printf("\nSet DueHours:%ld",atol(wallet_hour_set.recordset[0][1]));
			printf("\nCurrent Time:%ld",(curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365));
#endif
			lk_dispclr();
			lk_disptext(2,0,"   Transaction",0);
			lk_disptext(3,0,"    Is Locked",0);
			lk_disptext(4,0," Please contact HO",0);
			sleep(10);
			close_sqlite();			
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			execute("UPDATE operationalstatus SET ExecutiveStatus='0';");
			Dra_Menu();
		}
		else Dra_Menu();
	}
	else Receipt_Cancel(Receipt_Cancel_Transaction,Receipt_Cancel_CC_no,Reprint_Cancel_Print_Count,PickPayment_Flag_show);
}
/******************************************************************/

/******************************************************************/
