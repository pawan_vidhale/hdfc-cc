/*******************************************************************
			   HDFC Bank CC
		         change_password.c
/*******************************************************************/
#include <header.h>


int Dra_Change_pwd(char* Dra_old_password)
{
	char change_Pwd1[20],change_Pwd2[20];
	int retval=0,Valid_key=0,Change_key=0,Six_key=0;


	lk_dispclr();
	lk_disptext(0,1,"   HDFC Bank CC",0);	
    	lk_disptext(1,0,"Change Password",0);
	lk_disphlight(1);
    	lk_disptext(3,0,"Enter New Password",0);	
	memset(change_Pwd1,0,sizeof(change_Pwd1));
	retval=lk_getpassword(change_Pwd1,5,6);
	printf("retval=%d\n",retval);		
	change_Pwd1[retval]='\0';
    	if(strlen(change_Pwd1)>0&&(Check_Numeric(change_Pwd1)==1)&&(strcmp("123456",change_Pwd1)!=0))
	{
		if(strlen(change_Pwd1)==6)
		{
		lk_dispclr();
		lk_disptext(0,1,"   HDFC Bank CC",0);
		lk_disptext(1,0,"Change Password",0);			
		lk_disphlight(1);
		lk_disptext(3,0,"Confirm Password",0);			
		retval = 0;
		memset(change_Pwd2,0,sizeof(change_Pwd2));
		retval=lk_getpassword(change_Pwd2,5,6);
		printf("retval=%d\n",retval);		
		change_Pwd2[retval]='\0';
		if(strlen(change_Pwd2)>0&&Check_Numeric(change_Pwd2)==1)
		{
			if(strcmp(change_Pwd1,change_Pwd2)==0)
			{ 
				Dra_PWD_Request(change_Pwd2);
			}
			else
			{
Change_confirm_password:
				lk_dispclr();				
	    			lk_disptext(2,0,"Change Enter Password",0);
				lk_disptext(3,0,"& Confirm Password",0);
				lk_disptext(4,0,"   Is Not Match",0);
				lk_disptext(5,0,"Press Enter to Cont.",0);
				Change_key = lk_getkey();
				if(Change_key==ENTER)
				{	
				Dra_Change_pwd(Dra_old_password);
				}
				else goto Change_confirm_password; 				
			}		
		}
		else Dra_Change_pwd(Dra_old_password);	
		}
		else
		{
Six_digit:	
			lk_dispclr();
			lk_disptext(2,1,"Please Enter",0);
			lk_disptext(3,2,"Six Digit password",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			Six_key = lk_getkey();
			if(Six_key==ENTER)			
			{
			Dra_Change_pwd(Dra_old_password);
			}
			else goto Six_digit;

		}
		
	}
	else
	{
Valid_password:
	lk_dispclr();	
	lk_disptext(2,1,"  Please Enter",0);
	lk_disptext(3,2," Valid password",0);
	lk_disptext(5,0,"Press Enter to Cont.",0);
	Valid_key = lk_getkey();
	if(Valid_key==ENTER)
	{	
	Dra_Change_pwd(Dra_old_password);
	}
	else goto Valid_password; 
	}

}

/******************************************************************/
int SIM_Auth()
{
	resultset ICCID_Ret;

	memset(&ICCID_Ret,0,sizeof(ICCID_Ret));

	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");	
	ICCID_Ret=get_result("SELECT SIM_ICCID,SIM_ICCID_Server FROM operationalstatus;");
	//if((atoi(ICCID_Ret.recordset[0][0])!=0)&&(atoi(ICCID_Ret.recordset[0][1])!=0))
	//{
		if(strncmp(ICCID_Ret.recordset[0][0],ICCID_Ret.recordset[0][1],17)!=0) 
		{
			printf("Not Match1\n");
			lk_dispclr();
			lk_disptext(1,7,"  HDFC",0);
			lk_disphlight(1);
			lk_disptext(2,0," SIM Authentication",0);
			lk_disptext(3,0,"     Fail Pls ",0);
			lk_disptext(4,0,"Contact Head Office",0);
			lk_getkey();
			lk_dispclr();
			lk_disptext(3,1,"  Please Wait...",1);
			close_sqlite();
			lk_close();
			system("poweroff"); 	
			sleep(10);
			return -1;
		}
	//}

return 0;
}
