/*******************************************************************
			   HDFC Bank CC
		            Dra_login.c
/*******************************************************************/
#include <header.h>

/******************************************************************/

/*******************************************************************/
int Date_Validation()
{	
	int Syn_cont,Customer_Verify_key,ret_not_logout; 
	char Del_datebase[25],current_day[25],current_date[50],perv_year[25],curr_month[25],curr_year[25],perv_month[25],Negative[20],Negativecheck[20];		                    
	resultset Date_Diff,Pervieous_date;
	
	memset(&Date_Diff,0,sizeof(Date_Diff));	
	memset(&Pervieous_date,0,sizeof(Pervieous_date));	

	chk_rtc();
	close_sqlite();
	create_transactiondatabase();
	close_sqlite();
	Day_transactiondatabase();
	memset(current_date,0,sizeof(current_date));
	sprintf(current_date,"%4d-%02d-%02d",(curt.tm_year+1900),(curt.tm_mon+1),(curt.tm_mday));
	printf("\ncurrent_date:%s",current_date);
	memset(current_day,0,sizeof(current_day));
	sprintf(current_day,"%02d",(curt.tm_mday));
	printf("\ncurrent_day:%s",current_day);	
	memset(curr_month,0,sizeof(curr_month));
	sprintf(curr_month,"%02d",(curt.tm_mon+1));
	printf("\ncurr_month:%s",curr_month);	
	memset(curr_year,0,sizeof(curr_year));
	sprintf(curr_year,"%02d",(curt.tm_year-100));
	printf("\ncurr_year:%s",curr_year);

	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Pervieous_date=get_result("select Previous_date from operationalstatus");
	Date_Diff=get_result("select (julianday('%s')) - (julianday('%s'))",current_date,Pervieous_date.recordset[0][0]);
	memset(Negative,0,sizeof(Negative));	 	
	sprintf(Negative,"%s",Date_Diff.recordset[0][0]);	
	memset(Negativecheck,0,sizeof(Negativecheck));
	sprintf(Negativecheck,"%c",Negative[0]);

	memset(Del_datebase,0,sizeof(Del_datebase));	 	
	sprintf(Del_datebase,"%s",Pervieous_date.recordset[0][0]);	
	printf("\nDel_datebase:%s",Del_datebase);
	memset(perv_month,0,sizeof(perv_month));
	sprintf(perv_month,"%c%c",Del_datebase[5],Del_datebase[6]);
	printf("\nperv_month:%s",perv_month);
	memset(perv_year,0,sizeof(perv_year));
	sprintf(perv_year,"%c%c",Del_datebase[2],Del_datebase[3]);
	printf("\nperv_year:%s",perv_year);

	if(atoi(Date_Diff.recordset[0][0])>0||(strcmp(Negativecheck,"-")==0))
	{		
		ret_not_logout=Not_Yet_Logout_day();
		if(ret_not_logout==1) After_main();
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfcccoperational.db"); // open Data
		execute("Update operationalstatus SET last_transaction_no='1' WHERE last_transaction_no='9999';");	
		execute("Update operationalstatus SET Previous_date='%s';",current_date);		
		close_sqlite();
		system("rm /mnt/jffs2/hdfccctransday.db");
		close_sqlite();
		Day_transactiondatabase();		
		/************************** 1st date delete database********************************/
		lk_getrtc(&curt);
		printf("\n 1st Date delete database :%02d/%02d/%d",(curt.tm_mday),(curt.tm_mon+1),(curt.tm_year+1900));
		if(curt.tm_mday==01)
		{	
			printf("\nRemove DB mday");
			system ("rm -rf /mnt/jffs2/hdfccc.db");
		}
		else if(atoi(perv_month)<atoi(curr_month))
		{
			printf("\nRemove DB File-1");
			system("rm -rf /mnt/jffs2/hdfccc.db");
		}
		else if(atoi(perv_year)<atoi(curr_year))
		{
			printf("\nRemove DB File-2");
			system("rm -rf /mnt/jffs2/hdfccc.db");
		}
		else Date_Validation();
		/*************************************************************************************/
	}

return SUCCESS;		
}

/*************************************************************************************/
int Dra_login()
{
	int retval=0,chng_pwd_return=0;
	int Password_Wrong_key=0,DRA_Wrong_key=0;
	char DRA_ID[20],Dra_password[20],dec_dra_login[20],dec_dra_pass[20];	
	
	
	Date_Validation();	
	lk_dispclr();
        lk_disptext(0,1,"   HDFC Bank CC",0);
        lk_disptext(1,0,"Executive Login",0);
        lk_disphlight(1);
        lk_disptext(2,0,"Enter Executive ID:",0);
        memset(DRA_ID,0,sizeof(DRA_ID));
        if((retval=lk_getalpha(3,0,DRA_ID,10,strlen(DRA_ID),0))<0) After_main();
        DRA_ID[retval]='\0';
	if((strlen(DRA_ID)>0)&&(Check_Alpha_Numeric(DRA_ID)==1))
        {		
		retval=0;				
                lk_disptext(4,0,"Enter Password:",0);
                memset(Dra_password,0,sizeof(Dra_password));
                retval=lk_getpassword(Dra_password,5,6);
                printf("retval=%d\n",retval);           
                Dra_password[retval]='\0';		
		if((Check_Numeric(Dra_password)==1)&&(strlen(DRA_ID)==10)&&(strlen(Dra_password)==6))
                {			
			Png_Status_Without_Reconnect();
			if(gprsconnection==1)	
			{
			printf("\nDRA_login_online");
			BOD_Online(DRA_ID,Dra_password);						
			}
			else
			{
			printf("\nDRA_login_offline");
			if(strcmp(Dra_password,"123456")==0) Dra_login();
			else if(strcmp(DRA_ID,"HDFC@1234")==0) Dra_login();
			else BOD_Offline(DRA_ID,Dra_password);
			}
		}
		else
		{
DRA_Password:
	        lk_dispclr();		
	        lk_disptext(2,0,"  Please Enter Valid",0);
	        lk_disptext(3,0,"   Executive ID ",0);
		lk_disptext(4,0,"   OR Password ",0);	        
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Password_Wrong_key = lk_getkey();
		if(Password_Wrong_key==ENTER) Dra_login();
		else goto DRA_Password;
		}		
	}
	else
	{
DRA_ID_Key:
                lk_dispclr();		
                lk_disptext(2,0,"  Please Enter Valid",0);
                lk_disptext(3,0,"   Executive ID",0);                
                lk_disptext(5,0,"Press Enter to Cont.",0);
		DRA_Wrong_key = lk_getkey();
		if(DRA_Wrong_key==ENTER) Dra_login();		
		else goto DRA_ID_Key;  
	}
}

/******************************************************************/
int BOD_Online(char *Dra_Exec_ID,char *Dra_Exec_password)
{
	int retval_Info_Sysc,chng_pwd_return;
	char dec_draexecutive_login[25],dec_draexecutive_pass[25];

	resultset Exec_login,password_status_count,password_update,Lockpassword;

	memset(&Exec_login,0,sizeof(Exec_login));	
	memset(&password_update,0,sizeof(password_update));	

	retval_Info_Sysc=Executive_Info_Sync(Dra_Exec_ID);
	if(retval_Info_Sysc==0)
	{
Dra_Authentication:
	close_sqlite();
        open_sqlite("/mnt/jffs2/hdfcccoperational.db");
        Exec_login=get_result("SELECT Executive_id,Executive_Password,ChangePwdDay,password_status FROM operationalstatus;");
	memset(dec_draexecutive_login,0,sizeof(dec_draexecutive_login));
	sprintf(dec_draexecutive_login,"%s",Decrypted_sqlite_DB(Exec_login.recordset[0][0]));
	memset(dec_draexecutive_pass,0,sizeof(dec_draexecutive_pass));
	sprintf(dec_draexecutive_pass,"%s",Decrypted_sqlite_DB(Exec_login.recordset[0][1]));	
		
	if((Exec_login.rows>0)&&(strncmp(dec_draexecutive_login,Dra_Exec_ID,10)==0&&strcmp(dec_draexecutive_pass,Dra_Exec_password)==0))
	{		
		if((atoi(Exec_login.recordset[0][2])<1)||(strncmp(dec_draexecutive_pass,"123456",6)==0))
		{	
			chng_pwd_return=Dra_Change_pwd(Dra_Exec_password);
			printf("chng_pwd_return:%d\n",chng_pwd_return);	
			if(chng_pwd_return==1)	Sys_Cont(Dra_Exec_ID);
			else Dra_login();				
		}
		else Sys_Cont(Dra_Exec_ID);		
	}
	else
	{
		Wrong_Pin_Authentication();
		Dra_login();		
	}	
	}
	else goto Dra_Authentication;
}

/******************************************************************/
int BOD_Offline(char *Dra_Exec_ID,char *Dra_Exec_password)
{
	int Customer_Verify_key;	
	char password_status[10];
	char dec_draexecutive_pass[10],dec_draexecutive_login[10];
	resultset Dra_login_ID,Wrong_password_status_count,password_update,Existing_PickupDetails,Existing_CustomerDetails;

	
	memset(&Dra_login_ID,0,sizeof(Dra_login_ID));
	memset(&Wrong_password_status_count,0,sizeof(Wrong_password_status_count));
	memset(&password_update,0,sizeof(password_update));
	memset(&Existing_PickupDetails,0,sizeof(Existing_PickupDetails));
	memset(&Existing_CustomerDetails,0,sizeof(Existing_CustomerDetails));

	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Dra_login_ID=get_result("SELECT Executive_id,Executive_Password,loginstatus FROM operationalstatus;");		
	memset(dec_draexecutive_login,0,sizeof(dec_draexecutive_login));
	sprintf(dec_draexecutive_login,"%s",Decrypted_sqlite_DB(Dra_login_ID.recordset[0][0]));		
	memset(dec_draexecutive_pass,0,sizeof(dec_draexecutive_pass));
	sprintf(dec_draexecutive_pass,"%s",Decrypted_sqlite_DB(Dra_login_ID.recordset[0][1]));

	if((Dra_login_ID.rows>0)&&(strncmp(dec_draexecutive_login,Dra_Exec_ID,10)==0&&strcmp(dec_draexecutive_pass,Dra_Exec_password)==0))
	{
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		Existing_CustomerDetails=get_result("SELECT CARD_NUMBER FROM ALLOCATION_DETAILS;");
		Existing_PickupDetails=get_result("SELECT CARD_NUMBER FROM PICKUP_DETAILS;");
		if((Existing_CustomerDetails.rows>0)||(Existing_PickupDetails.rows>0))
		{	
		Dra_Menu();
		}
		else
		{
Customer_INFO:
		lk_dispclr();
		lk_disptext(2,0,"Customer Information",0);
		lk_disptext(3,0,"      Not Found     ",0);
		lk_disptext(4,0,"  Please contact HO",0);
		lk_disptext(5,0,"Enter->Continue",0);					
		Customer_Verify_key = lk_getkey();
		if(Customer_Verify_key==ENTER)
		{
			After_main();
		}
                else goto Customer_INFO;	 		
		}		
	}
	else
	{	
		Wrong_Pin_Authentication();
		After_main();
	}
	
}

/**********************************************************/
int Wrong_Pin_Authentication()
{
	int Auth_Verify_key,Number_Wrong_key;
	char password_status[20],Lockpassword_info[50],Lockpassword_RSA_b64[500],dec_executive_lock[20];
	resultset password_status_count,password_update,Lockpassword;

	memset(&password_status_count,0,sizeof(password_status_count));
	memset(&password_update,0,sizeof(password_update));
	memset(&Lockpassword,0,sizeof(Lockpassword));
	
	
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	password_status_count = get_result("SELECT noofpin_allow,password_status,password_status_count FROM operationalstatus");
	if((atoi(password_status_count.recordset[0][2]))>0)
	{
Authent_Failed_online:
		lk_dispclr();
		lk_disptext(2,0,"   Authentication",0);
		lk_disptext(3,0,"       Failed",0);
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Auth_Verify_key = lk_getkey();
		if(Auth_Verify_key==ENTER)
		{
		memset(password_status,0,sizeof(password_status));
		sprintf(password_status,"%d",atoi(password_status_count.recordset[0][2])-1);
		printf("\npassword_status:%s",password_status);
		execute("Update operationalstatus SET password_status_count='%s';",password_status);
		return 0;										
		}
		else goto Authent_Failed_online;
	}
	else
	{
Number_Wrong_pin_online:
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		execute("Update operationalstatus SET password_status='0';");
		password_update=get_result("Select noofpin_allow from operationalstatus"); 	
		execute("UPDATE operationalstatus SET password_status_count='%s';",password_update.recordset[0][0]);
		lk_dispclr();	
		lk_disptext(2,0,"Number of Wrong PIN",0);
		lk_disptext(3,0," Allowed Exceeded",0);
		lk_disptext(4,0,"Machine is Locked",0);	
		lk_disptext(5,0,"Press Enter to Cont.",0);				
		Number_Wrong_key = lk_getkey();
		if(Number_Wrong_key==ENTER)
		{
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		Lockpassword=get_result("Select httpip,Localhttpip,hwid,Executive_id,password_status From operationalstatus;");	
		memset(dec_executive_lock,0,sizeof(dec_executive_lock));
		sprintf(dec_executive_lock,"%s",Decrypted_sqlite_DB(Lockpassword.recordset[0][3]));
		memset(Lockpassword_info,0,sizeof(Lockpassword_info));
		sprintf(Lockpassword_info,"Validate,%s,%s,%s",dec_executive_lock,Lockpassword.recordset[0][2],Lockpassword.recordset[0][4]);
		memset(Lockpassword_RSA_b64,0,sizeof(Lockpassword_RSA_b64));
		sprintf(Lockpassword_RSA_b64,"%s",RSAEncryption_Base64(Lockpassword_info,strlen(Lockpassword_info)));
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfccctrans.db");
		execute("INSERT INTO lockpassword_details(lockpassword_details,lockpassword_Status) VALUES('%s','0');",Lockpassword_RSA_b64);
		Lock_password_Request();
		return 1;		
		}
		else goto Number_Wrong_pin_online;
	}		        

}


/******************************************************************/
int Sys_Cont(char* Syn_Dra_Exec)
{
	int Syn_cont,Customer_Verify_key,ret_customer;
	resultset Customer_Details,Existing_PickupDetails;
	
	memset(&Customer_Details,0,sizeof(Customer_Details));
	memset(&Existing_PickupDetails,0,sizeof(Existing_PickupDetails));

	lk_dispclr();
	lk_disptext(0,1,"   HDFC Bank CC",0);	
	lk_disptext(1,2,"  Please Select",0);
	lk_disphlight(1); 			
	lk_disptext(3,0,"Press 1->Sync.",0);
	lk_disptext(4,0,"Press 2->Cont.",0);

	while(1)
    	{
		Syn_cont = lk_getkey();
		if((Syn_cont==CANCEL)||(Syn_cont==1)||(Syn_cont==2)) break;
	}

	if(Syn_cont == CANCEL)
	{
		Dra_login();
	}
	else if(Syn_cont == 1)
	{		
		printf("\nDRA customer infor:%s",Syn_Dra_Exec);
		ret_customer=Customer_Info_Sync(Syn_Dra_Exec);
		if(ret_customer==0)
		{
		BOD_EOD_Request_Status();
		}
		else Sys_Cont(Syn_Dra_Exec);						
	}
	else if(Syn_cont == 2)
	{				
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		Customer_Details= get_result("SELECT CARD_NUMBER FROM ALLOCATION_DETAILS;");
		Existing_PickupDetails=get_result("SELECT CARD_NUMBER FROM PICKUP_DETAILS;");	
		if(Customer_Details.rows>0||Existing_PickupDetails.rows>0)
		{			
			BOD_EOD_Request_Status();					
		}
		else
		{
Customer_Info:
			lk_dispclr();	
			lk_disptext(0,1,"    HDFC Bank CC",0);
			lk_disptext(2,0,"Customer Information",0);
			lk_disptext(3,0,"      Not Found     ",0);
			lk_disptext(4,0,"  Please contact HO",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);				
			Customer_Verify_key = lk_getkey();
			if(Customer_Verify_key==ENTER)
			{
			Dra_login();			
			}
			else goto Customer_Info;
		}
	}	
	else Sys_Cont(Syn_Dra_Exec);
}



/******************************************************************/
int BOD_EOD_Request_Status()
{
	char dec_executive_login[20],dec_executive_bod[20],BOD_Data[100],BOD_EOD_RSA_b64[500];
	resultset Exec_login,Existing_CustomerDetails,password_update,get_BOD,Customer_Details;

	memset(&Existing_CustomerDetails,0,sizeof(Existing_CustomerDetails));
	memset(&Exec_login,0,sizeof(Exec_login));
	memset(&password_update,0,sizeof(password_update));		
		

	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");	
	Exec_login=get_result("SELECT Executive_id,hwid,loginstatus,noofpin_allow FROM operationalstatus;");
	execute("UPDATE operationalstatus SET password_status_count='%s';",Exec_login.recordset[0][3]);	
	if(atoi(Exec_login.recordset[0][2])==0)
	{
	memset(dec_executive_login,0,sizeof(dec_executive_login));
	sprintf(dec_executive_login,"%s",Decrypted_sqlite_DB(Exec_login.recordset[0][0]));	
	memset(BOD_Data,0,sizeof(BOD_Data));
	sprintf(BOD_Data,"Validate,%s,%s,%02d=%02d=%04d,%02d=%02d=%02d,B",dec_executive_login,Exec_login.recordset[0][1],(curt.tm_mon+1),(curt.tm_mday),(curt.tm_year+1900),curt.tm_hour,curt.tm_min,curt.tm_sec);	
	memset(BOD_EOD_RSA_b64,0,sizeof(BOD_EOD_RSA_b64));
	sprintf(BOD_EOD_RSA_b64,"%s",RSAEncryption_Base64(BOD_Data,strlen(BOD_Data)));	
	close_sqlite();	
	open_sqlite("/mnt/jffs2/hdfccctrans.db"); // open trans   
	execute("INSERT INTO unuploaded_BOD_EOD_details(BOD_EOD_details,BOD_EOD_Status) VALUES('%s','0');",BOD_EOD_RSA_b64);
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");	
	execute("UPDATE operationalstatus SET loginstatus='1';");	
	Upload_BOD_EOD_Request();
	Dra_Menu();
	}
	else
	{
	Dra_Menu();
	}

}

/******************************************************************/
int Not_Yet_Logout_day()
{
	
	int EOD_key;
	resultset Check_EOD,get_EOD,unuploaded_trans1,unuploaded_followUp1,unuploaded_BOD_EOD1;  


	memset(&Check_EOD,0,sizeof(Check_EOD));
	memset(&get_EOD,0,sizeof(get_EOD));
	memset(&unuploaded_trans1,0,sizeof(unuploaded_trans1));
	memset(&unuploaded_followUp1,0,sizeof(unuploaded_followUp1));
	memset(&unuploaded_BOD_EOD1,0,sizeof(unuploaded_BOD_EOD1));

	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	get_EOD=get_result("select loginstatus from operationalstatus");
    	
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccctrans.db");
	Check_EOD = get_result("SELECT * from transactiondetails;");
	unuploaded_trans1=get_result("SELECT * FROM unuploadedtransactiondetails;");
	unuploaded_followUp1=get_result("SELECT * FROM unuploadedfollowupdetails;");
	unuploaded_BOD_EOD1=get_result("SELECT * FROM unuploaded_BOD_EOD_details;");	
	if(atoi(get_EOD.recordset[0][0])==1)  
	{
LogOut_key:
		lk_dispclr();
		lk_disptext(2,0," You have Not Yet",0);
		lk_disptext(3,0,"   logged out ",0);		
		lk_disptext(5,0,"Press Enter to Logout.",0);
		EOD_key = lk_getkey();
		if(EOD_key==ENTER)
		{			
			End_Of_Day("1");
		}
		else goto LogOut_key;			        	
	}
	else if((unuploaded_trans1.rows==0)&&(unuploaded_followUp1.rows==0)&&atoi(get_EOD.recordset[0][0])==0)
	{
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		execute("UPDATE operationalstatus SET loginstatus='0';");
		return 0;		
	}
	else
	{
		return 1;
	}	

return 1;			
}


