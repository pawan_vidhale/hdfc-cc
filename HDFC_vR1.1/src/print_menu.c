/*******************************************************************/
//				HDFC
//		               Print.c
/*******************************************************************/
/**
 *
 * @author FTLIT003
**/
#include <header.h>
#include "sqlite3.h"
#include "logo.h"
/*******************************************************************/
//Declaration
/*******************************************************************/
struct tm curt;
static int MAX_NO_REPRINT_NO=0;
char* pw(long,char[]);
char* Amount_Conversion();
char* Conversion();
char Amount_In_Char[200],amt_char[20];
char Conv_Amount[200];
char *one[]={" "," One"," Two"," Three"," Four"," Five"," Six"," Seven"," Eight"," Nine"," Ten"," Eleven"," Twelve"," Thirteen"," Fourteen"," Fifteen"," Sixteen"," Seventeen"," Eighteen"," Nineteen"};
char *ten[]={" "," "," Twenty"," Thirty"," Forty"," Fifty"," Sixty"," Seventy"," Eighty"," Ninety"};

/*******************************************************************/
/*******************************************************************/
Print_Display(char* Instument_Type_show,char* Total_Amount_show,char* Mode_show,char* Mobile_show,char* CC_no_show,char* PAN_show,char* Chq_DD_show,char* Chq_DD_Date_show,char* Chq_DD_Bank_show,char* Payee_Type_show,char* Place_Cont_show, char* Micr_Number_show,char* PickPayment_Flag_show)
{
		
	char Executive_Code[50],Machine_ID[50],Print_Machine_ID[100],Print_Transaction_No[100],timenow[100],datenow[100],Date_Time_print[100],Print_Pay_type[100],Print_Inst_Type[100],Print_Machine[100],Print_Customer_Name[100],Print_Amount[100],Print_Amount_word[100],Print_Loan_No[100],Transaction_No[50],Print_Date_Time[100],Sign_Customer_Name[100],Print_Product_Name[100],Print_Agent_Name[100],Print_Agent_Code[100],Print_Exe_Card[100],Print_PAN_No[100],Print_Chq_DD[100],Print_Bucket_desc[100],Print_card_outstanding[100],Print_amount_overdue[100];
	char Print_Chq_DD_Date[100],Print_Chq_DD_Bank[100],Print_PAN[100],Transaction_timenow[20],Transaction_datenow[20],Tran_Payee_Type[20],Tran_Place_Cont[20],Amount_In_char[200],Transaction_Details[500],Transaction_RSA_b64[500],Cheq_DD_Date_segregat[20],Print_Executive_Name[200],Julian[50],Julianreturn[50],Executive_N[100],last_transaction[20],printing_datenow[15],Print_Micr_No[100],Payee_Name_show[15],Customer_ID[30],Credit_Card_no[30],Branch_Id[30];
	resultset hwid_Exec,Get_contract_no,Tran_id_verify,get_transcation_check,Get_pickup_contract_no,Tran_id_day,Get_allocation_contact_no;	
	char bmpbuff1[30000],Battery_return[10];
	int len,No_Of_Copy,Battery_Blocked_key,ret=0;
	int ret_update_transcation=0,ret_update=0,check_count_tra=0;	

	memset(&hwid_Exec,0,sizeof(hwid_Exec));
	memset(&Get_contract_no,0,sizeof(Get_contract_no));
	memset(&Tran_id_verify,0,sizeof(Tran_id_verify));
	memset(&get_transcation_check,0,sizeof(get_transcation_check));
	memset(&Get_pickup_contract_no,0,sizeof(Get_pickup_contract_no));
	memset(&Get_allocation_contact_no,0,sizeof(Get_allocation_contact_no));
	memset(&Tran_id_day,0,sizeof(Tran_id_day));
#if PRINTF
	printf("\nInstument_Type_show:%s",Instument_Type_show);	
	printf("\nPayment_Total_show:%s",Total_Amount_show);
	printf("\nMode_show:%s",Mode_show);				
	printf("\nMobile_show:%s",Mobile_show);
	printf("\nCC_no_show:%s",CC_no_show);
	printf("\nPAN_show:%s",PAN_show);	
	printf("\nChq_DD_show:%s",Chq_DD_show);	
	printf("\nChq_DD_Date_show:%s",Chq_DD_Date_show);
	printf("\nChq_DD_Bank_show:%s",Chq_DD_Bank_show);	
	printf("\nPayee_Type_show:%s",Payee_Type_show);
	printf("\nPlace_Cont_show:%s",Place_Cont_show);
	printf("\nMicr_Number_show:%s",Micr_Number_show);
	printf("\nPickPayment_flag%s",PickPayment_Flag_show);	
#endif	
	chk_rtc();	
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");	
	memset(Machine_ID,0,sizeof(Machine_ID));
	memset(Executive_Code,0,sizeof(Executive_Code));	
	hwid_Exec=get_result("select hwid,Executive_id,last_transaction_no,Executive_Name from operationalstatus");	
	sprintf(Machine_ID,"%s",hwid_Exec.recordset[0][0]);
	sprintf(Executive_Code,"%s",Decrypted_sqlite_DB(hwid_Exec.recordset[0][1]));	

	//close_sqlite();
	//open_sqlite("/mnt/jffs2/hdfccc.db");	
	//Get_contract_no= get_result("SELECT CUST_ID,BRANCH_CODE,ADDRESS_P_LMARK,BRANCH_CODE,AGENCE_CODE,AGENCE_NAME,CARD_NUMBER from ALLOCATION_DETAILS WHERE ALT_ACCOUNT_NO='%s';",CC_no_show);	

	memset(Print_Machine_ID,0,sizeof(Print_Machine));
	sprintf(Print_Machine_ID,"%s",hwid_Exec.recordset[0][0]);	
	memset(Julianreturn,0,sizeof(Julianreturn));
	memset(Julian,0,sizeof(Julian));
	sprintf(Julian,"%s",jday(Julianreturn));
	printf("\nJulian:%s",Julian);	

	memset(Print_Transaction_No,0,sizeof(Print_Transaction_No));
	sprintf(Print_Transaction_No,"Receipt Number :     %c%c%c%c%c%02d%s%04d",Print_Machine_ID[3],Print_Machine_ID[4],Print_Machine_ID[5],Print_Machine_ID[6],Print_Machine_ID[7],(curt.tm_year-100),Julian,atoi(hwid_Exec.recordset[0][2]));
#if PRINTF
	print("\nPrint_Transaction_No:%s",Print_Transaction_No);
#endif
	memset(Transaction_No,0,sizeof(Transaction_No));
	sprintf(Transaction_No,"%c%c%c%c%c%02d%s%04d",Print_Machine_ID[3],Print_Machine_ID[4],Print_Machine_ID[5],Print_Machine_ID[6],Print_Machine_ID[7],(curt.tm_year-100),Julian,atoi(hwid_Exec.recordset[0][2]));

	memset(last_transaction,0,sizeof(last_transaction));
	sprintf(last_transaction,"%04d",atoi(hwid_Exec.recordset[0][2]));

	memset(printing_datenow,0,sizeof(printing_datenow));
	sprintf(printing_datenow,"%4d-%02d-%02d",(curt.tm_year+1900),(curt.tm_mon+1),(curt.tm_mday));
	
	memset(Payee_Name_show,0,sizeof(Payee_Name_show));
	if(strcmp(Payee_Type_show,"1")==0) strcpy(Payee_Name_show,"Customer");
	else if(strcmp(Payee_Type_show,"2")==0) strcpy(Payee_Name_show,"Relative");
	else if(strcmp(Payee_Type_show,"3")==0) strcpy(Payee_Name_show,"Others");
	else Dra_Menu();	
	
	if(strcmp(Place_Cont_show,"1")==0) print("\nPlace_Cont_show:%s",Place_Cont_show);
	else if(strcmp(Place_Cont_show,"2")==0) print("\nPlace_Cont_show:%s",Place_Cont_show);
	else if(strcmp(Place_Cont_show,"3")==0) print("\nPlace_Cont_show:%s",Place_Cont_show);
	else Dra_Menu();
	
	memset(Transaction_timenow,0,sizeof(Transaction_timenow));
	sprintf(Transaction_timenow,"%02d=%02d=%02d",curt.tm_hour,curt.tm_min,curt.tm_sec);	
	memset(Transaction_datenow,0,sizeof(Transaction_datenow));
	sprintf(Transaction_datenow,"%02d=%02d=%04d",(curt.tm_mon+1),(curt.tm_mday),(curt.tm_year+1900));
	memset(Executive_N,0,sizeof(Executive_N));
	sprintf(Executive_N,"%s",hwid_Exec.recordset[0][3]);
	memset(timenow,0,sizeof(timenow));
	sprintf(timenow,"%02d:%02d:%02d",curt.tm_hour,curt.tm_min,curt.tm_sec);
	memset(datenow,0,sizeof(datenow));
	sprintf(datenow,"%02d-%02d-%02d",(curt.tm_mday),(curt.tm_mon+1),(curt.tm_year-100));
	memset(Print_Date_Time,0,sizeof(Print_Date_Time));
	sprintf(Print_Date_Time,"Date:%s         ",datenow);
	memset(Print_Amount,0,sizeof(Print_Amount));
	sprintf(Print_Amount,"Amount          : Rs.%s/-",Total_Amount_show);
	memset(Amount_In_char,0,sizeof(Amount_In_char));	
	sprintf(Amount_In_char,"Amount(in words): Rs.%s",Conversion(Total_Amount_show));	
	memset(Print_Inst_Type,0,sizeof(Print_Inst_Type));
	sprintf(Print_Inst_Type,"Mode of Payment : %s", Instument_Type_show);	
	memset(Print_Pay_type,0,sizeof(Print_Pay_type));
	sprintf(Print_Pay_type,"Payee Type      : %s",Payee_Name_show);
	memset(Print_PAN,0,sizeof(Print_PAN));
	sprintf(Print_PAN_No,"PAN No.         : %s",PAN_show);	
	memset(Print_Chq_DD,0,sizeof(Print_Chq_DD));
	sprintf(Print_Chq_DD,"Chq/DD No.      : %s",Chq_DD_show);
	memset(Cheq_DD_Date_segregat,0,sizeof(Cheq_DD_Date_segregat));
	sprintf(Cheq_DD_Date_segregat,"%s",Chq_DD_Date_show);
	memset(Print_Chq_DD_Date,0,sizeof(Print_Chq_DD_Date));
	sprintf(Print_Chq_DD_Date,"Chq/DD Date     : %c%c-%c%c-%c%c",Cheq_DD_Date_segregat[0],Cheq_DD_Date_segregat[1],Cheq_DD_Date_segregat[2],Cheq_DD_Date_segregat[3],Cheq_DD_Date_segregat[4],Cheq_DD_Date_segregat[5]);
	memset(Print_Chq_DD_Bank,0,sizeof(Print_Chq_DD_Bank));
	sprintf(Print_Chq_DD_Bank,"Bank Name       : %s",Chq_DD_Bank_show);
	memset(Print_Product_Name,0,sizeof(Print_Product_Name));
	sprintf(Print_Product_Name,"Product Name    : %s","Credit Card");
	memset(Print_Exe_Card,0,sizeof(Print_Exe_Card));
	sprintf(Print_Exe_Card,"FE ID Card #    : %s",Executive_Code);	
	memset(Print_Executive_Name,0,sizeof(Print_Executive_Name));
	sprintf(Print_Executive_Name,"FE Name         : %s",hwid_Exec.recordset[0][3]);
	memset(Print_Micr_No,0,sizeof(Print_Micr_No));
	sprintf(Print_Micr_No,"MICR No.        : %s",Micr_Number_show);


	if(strcmp(PickPayment_Flag_show,"D")==0)
	{
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccc.db");
	Get_allocation_contact_no= get_result("SELECT BUCKET,BUCKET_DEST,LOADED_BAL,TOTAL_AMT_DUE,CURRENT_BL,filler1,CARD_NUMBER,BRANCH_CODE,CUST_ID,AGENCE_CODE,AGENCE_NAME from allocation_details WHERE ALT_ACCOUNT_NO='%s';",CC_no_show);
	
	memset(Print_Customer_Name,0,sizeof(Print_Customer_Name));
	snprintf(Print_Customer_Name,38,"Customer Name   : %s",Get_allocation_contact_no.recordset[0][8]);	
	memset(Print_Loan_No,0,sizeof(Print_Loan_No));
	sprintf(Print_Loan_No,"Card #          : %s",Get_allocation_contact_no.recordset[0][6]);
	memset(Print_Agent_Code,0,sizeof(Print_Agent_Code));
	sprintf(Print_Agent_Code,"Agency Code     : %s",Get_allocation_contact_no.recordset[0][9]);
	memset(Print_Agent_Name,0,sizeof(Print_Agent_Name));
	sprintf(Print_Agent_Name,"Agency Name     : %s",Get_allocation_contact_no.recordset[0][10]);
	memset(Credit_Card_no,0,sizeof(Credit_Card_no));
	sprintf(Credit_Card_no,"%s",Get_allocation_contact_no.recordset[0][6]);
	memset(Branch_Id,0,sizeof(Branch_Id));
	sprintf(Branch_Id,"%s",Get_allocation_contact_no.recordset[0][7]);
	memset(Print_Bucket_desc,0,sizeof(Print_Bucket_desc));
	sprintf(Print_Bucket_desc,"%s",Get_allocation_contact_no.recordset[0][1]);
#if PRINTF
	printf("\n%s",Print_Customer_Name);
	printf("\n%s",Print_Loan_No);
	printf("\n%s",Print_Agent_Name);
	printf("\n%s",Print_Agent_Code);	

#endif

		if(strcmp(Print_Bucket_desc,"RECOVERIES")==0)
		{
		memset(Print_card_outstanding,0,sizeof(Print_card_outstanding));
		sprintf(Print_card_outstanding,"Card Outstanding: %s",Get_allocation_contact_no.recordset[0][2]);
		}
		else
		{
		memset(Print_card_outstanding,0,sizeof(Print_card_outstanding));
		sprintf(Print_card_outstanding,"Card Outstanding: %s",Get_allocation_contact_no.recordset[0][4]);
		}
	memset(Print_amount_overdue,0,sizeof(Print_amount_overdue));
	sprintf(Print_amount_overdue,"Amount overdue  : %s",Get_allocation_contact_no.recordset[0][3]);
	memset(Customer_ID,0,sizeof(Customer_ID));
	sprintf(Customer_ID,"%s",Get_allocation_contact_no.recordset[0][5]);
	}	
	else
	{
	
	printf("\nPickPayment_Flag_show%s",PickPayment_Flag_show);	
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccc.db");	
	Get_pickup_contract_no= get_result("SELECT Filler1,AGENCE_NAME,AGENCE_CODE,Filler2,Filler3,CARD_NUMBER,BRANCHID from PICKUP_DETAILS WHERE Filler3='%s';",CC_no_show);
	memset(Print_Customer_Name,0,sizeof(Print_Customer_Name));
	snprintf(Print_Customer_Name,38,"Customer Name   : %s",Get_pickup_contract_no.recordset[0][0]);
	memset(Print_Loan_No,0,sizeof(Print_Loan_No));
	sprintf(Print_Loan_No,"Card #          : %s",Get_pickup_contract_no.recordset[0][5]);
	memset(Print_Agent_Name,0,sizeof(Print_Agent_Name));
	sprintf(Print_Agent_Name,"Agency Name     : %s",Get_pickup_contract_no.recordset[0][1]);
	memset(Print_Agent_Code,0,sizeof(Print_Agent_Code));
	sprintf(Print_Agent_Code,"Agency Code     : %s",Get_pickup_contract_no.recordset[0][2]);
	memset(Print_card_outstanding,0,sizeof(Print_card_outstanding));
	memset(Credit_Card_no,0,sizeof(Credit_Card_no));
	sprintf(Credit_Card_no,"%s",Get_pickup_contract_no.recordset[0][5]);
	memset(Branch_Id,0,sizeof(Branch_Id));
	sprintf(Branch_Id,"%s",Get_pickup_contract_no.recordset[0][6]);
	sprintf(Print_card_outstanding,"Card Outstanding: %s","NA");
	memset(Print_amount_overdue,0,sizeof(Print_amount_overdue));
	sprintf(Print_amount_overdue,"Amount overdue  : %s","NA");
	memset(Customer_ID,0,sizeof(Customer_ID));
	sprintf(Customer_ID,"%s",Get_pickup_contract_no.recordset[0][3]);
#if PRINTF	
	printf("\n%s",Print_Customer_Name);
	printf("\n%s",Print_Loan_No);
	printf("\n%s",Print_Agent_Name);
	printf("\n%s",Print_Agent_Code);
#endif	
	}
#if PRINTF 
	printf("\nPrint_Date_Time:%s",Print_Date_Time);	
	printf("\nPrint_Transaction_No:%s",Print_Transaction_No);	
	printf("\nPrint_Customer_Name:%s",Print_Customer_Name);		
	printf("\nPrint_Loan_No:%s",Print_Loan_No);
	printf("\nPrint_Pay_type:%s",Print_Pay_type);
	printf("\nPrint_Inst_Type:%s",Print_Inst_Type);	
	printf("\nPrint_Machine_ID:%s",Print_Machine_ID);	
	printf("\nPrint_Customer_Name:%s",Print_Inst_Type);
	printf("\nPrint_Pay_type:%s",Print_Pay_type);	
	printf("\nPrint_Amount:%s",Print_Amount);
	printf("\nPrint_Product_Name:%s",Print_Product_Name);			
	printf("\nPrint_Agent_Name:%s",Print_Agent_Name);
	printf("\nPrint_Exe_Card:%s",Print_Exe_Card);	
	printf("\nPrint_PAN_No:%s",Print_PAN_No);	
	printf("\nPrint_Chq_DD:%s",Print_Chq_DD);
	printf("\nPrint_Chq_DD_Date:%s",Print_Chq_DD_Date);
	printf("\nPrint_Chq_DD_Bank:%s",Print_Chq_DD_Bank);
	printf("\nPrint_amount_overdue:%s",Print_amount_overdue);
	printf("\nPrint_card_outstanding:%s",Print_card_outstanding);	
#endif
	Check_Jalian_lasttranscation(Transaction_No,last_transaction,printing_datenow);	
	if(strlen(Executive_Code)>0&&strlen(CC_no_show)>0&&strlen(Transaction_No)>0&&strlen(Total_Amount_show)>0&&strlen(Payee_Type_show)>0&&strlen(Place_Cont_show)>0&&strlen(Instument_Type_show)>0&&strlen(Chq_DD_Bank_show)>0&&strlen(Chq_DD_show)>0&&strlen(Chq_DD_Date_show)>0&&strlen(PAN_show)>0&&strlen(Mobile_show)>0&&strlen(Transaction_datenow)>0&&strlen(Transaction_timenow)>0&&strlen(Branch_Id))
	{	
	prn_open();	
	memset(Battery_return,0,sizeof(Battery_return));
	sprintf(Battery_return,"%s",Battery_Print_test());
#if PRINTF
	printf("\nBattery_return:%0.2f",atof(Battery_return));
#endif
	if(atof(Battery_return)<(6.8))
	{		
Low_Battery:
		lk_dispclr();
		lk_disptext(1,5,"Battery Low!",1);
		lk_disptext(3,0," Can't Do Transact.",1);
		lk_disptext(5,0,"Press Enter to Cont.",0);
		printf("\nBattery is Now lower than 15%");
		Battery_Blocked_key = lk_getkey();
		if(Battery_Blocked_key==ENTER)
		{
		prn_close();
		Dra_Menu();
		}
		else goto Low_Battery; 
	}
	if(atof(Battery_return)<(6.92))
	{
	lk_dispclr();
	lk_disptext(1,5,"Battery Low!",1);
	lk_disptext(3,0,"  Plug Power Cord",0);//plug Power code cable
	lk_disptext(4,0," Cable & Press ENTER",0);
	lk_disptext(5,0,"  Key To continue",0);
	lk_getkey();
	}
	if(prn_paperstatus()!=0)
	{
//INSERT:	
	lk_dispclr();
	lk_disptext(1,5,"No Paper !",1);
	lk_disptext(3,0,"Insert Paper & Press",0);
	lk_disptext(4,0," Any Key To continue",0);
	lk_getkey();
	}	
	
	//if(prn_paperstatus()!=0)
	//goto INSERT;
	prn_close();	
	lk_dispclr();
	lk_disptext(2,2,"Printing..",1);	
	printf("\nlast_transaction:%s",last_transaction);
	printf("\nTransaction_No:%s",Transaction_No);

	close_sqlite();					
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	ret_update_transcation=execute("Update operationalstatus SET Update_transaction_no='%s';",Transaction_No);
	ret_update=execute("Update operationalstatus SET last_transaction_no=last_transaction_no+1;");
	printf("\nret_update:%d",ret_update);		

	close_sqlite();					
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Tran_id_verify = get_result("SELECT last_transaction_no FROM operationalstatus");	
	if(strcmp(Tran_id_verify.recordset[0][0],last_transaction)==0)
	{
	Dra_Menu();		
	}
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccctransday.db");
	Tran_id_day = get_result("SELECT Transaction_ID FROM transactionday");	
	for(check_count_tra=0;check_count_tra<Tran_id_day.rows;check_count_tra++)
	{			
		if(strcmp(Tran_id_day.recordset[check_count_tra][0],Transaction_No)==0)
		{
		Dra_Menu();		
		}
	}

	write_file();
	write_file_date();

	if(strcmp(Chq_DD_show,"0")==0)
		execute("Update operationalstatus SET WalletBalance=WalletBalance-%.2f;",atof(Total_Amount_show));
	
	memset(Transaction_Details,0,sizeof(Transaction_Details));	
	sprintf(Transaction_Details,"Validate,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,0,0,%s,%s,%s,%s",Executive_Code,Credit_Card_no,Transaction_No,Total_Amount_show,Payee_Type_show,Place_Cont_show,Instument_Type_show,Chq_DD_Bank_show,Chq_DD_show,Chq_DD_Date_show,PAN_show,Mobile_show,Micr_Number_show,Transaction_datenow,Transaction_timenow,PickPayment_Flag_show,Customer_ID,Branch_Id,CC_no_show);


#if PRINTF
	printf("\nTransaction_Details:%s",Transaction_Details);
#endif	

	memset(Transaction_RSA_b64,0,sizeof(Transaction_RSA_b64));
	sprintf(Transaction_RSA_b64,"%s",RSAEncryption_Base64(Transaction_Details,strlen(Transaction_Details)));

	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccctrans.db");	
	execute("INSERT INTO unuploadedtransactiondetails(Transaction_ID,Transaction_Details,status) VALUES('%s','%s','0');",Transaction_No,Transaction_RSA_b64);

	execute("INSERT INTO transactiondetails(Executive_Name,Executive_Code,Loan_No,Transaction_ID,Total_Amt,Payee_Type,Place_Contacted,Instument_Type,Bank_Name,chq_DD_No,chq_DD_Date,PAN_No,Payee_Mobile_No,Micr_number,Transaction_Date,Transaction_Time,Trn_cancel,Reprint_count,Customer_no,Branch_Id,Alt_Account_No) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','0','0','%s','%s','%s');",Executive_N,Executive_Code,Credit_Card_no,Transaction_No,Total_Amount_show,Payee_Type_show,Place_Cont_show,Instument_Type_show,Chq_DD_Bank_show,Chq_DD_show,Chq_DD_Date_show,PAN_show,Mobile_show,Micr_Number_show,Transaction_datenow,Transaction_timenow,Customer_ID,Branch_Id,CC_no_show);

	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccctransday.db");
	execute("INSERT INTO transactionday(Executive_Name,Executive_Code,Loan_No,Transaction_ID) VALUES('%s','%s','%s','%s');",Executive_N,Executive_Code,Credit_Card_no,Transaction_No);

	
	prn_open();		
	ret=prn_write_bmp(HDFC_Logo,sizeof(HDFC_Logo));		
	ret=prn_write_text("      RECEIPT",22,2);
	ret=prn_write_text("----------------------------------------",40,1);	
	ret=prn_write_text("We acknowledge the receipt of the       ",40,1);
	ret=prn_write_text("following payment on your credit        ",40,1);
	ret=prn_write_text("card account held with HDFC BANK LTD    ",40,1);
	ret=prn_write_text("----------------------------------------",40,1);
	ret=prn_write_text(Print_Date_Time,strlen(Print_Date_Time), 1);
	ret=prn_write_text("----------------------------------------",40,1);
	ret=prn_write_text(Print_Transaction_No,strlen(Print_Transaction_No),2);	
	ret=prn_write_text("----------------------------------------",40,1);
	ret=prn_write_text(Print_Loan_No,strlen(Print_Loan_No), 1);
	ret=prn_write_text(Print_Customer_Name,strlen(Print_Customer_Name), 1);	
	ret=prn_write_text(Print_Pay_type,strlen(Print_Pay_type), 1);			
	ret=prn_write_text(Print_Amount,strlen(Print_Amount), 1);
	ret=prn_write_text(Amount_In_char,strlen(Amount_In_char), 1);
	ret=prn_write_text(Print_Inst_Type,strlen(Print_Inst_Type), 1);
	if(strcmp(PickPayment_Flag_show,"D")==0)
	{
	ret=prn_write_text(Print_card_outstanding,strlen(Print_card_outstanding), 1);	
	ret=prn_write_text(Print_amount_overdue,strlen(Print_amount_overdue), 1);
	}		
	ret=prn_write_text(Print_Agent_Name,strlen(Print_Agent_Name), 1);
	ret=prn_write_text(Print_Agent_Code,strlen(Print_Agent_Code), 1);			
	ret=prn_write_text(Print_Exe_Card,strlen(Print_Exe_Card), 1);	
	ret=prn_write_text(Print_Executive_Name,strlen(Print_Executive_Name),1);			
	
	if(strcmp(PAN_show,"0")==0)
	{
		if(strcmp(Chq_DD_show,"0")==0)
		{	ret=prn_write_text("----------------------------------------",40,1);
			ret=prn_write_text("  Any overwriting",22,2);
			ret=prn_write_text("    is invalid",22,2);
			ret=prn_write_text("----------------------------------------",40,1);
			ret=prn_write_text("With regard to any discrepancy perta-   ",40,1);			
			ret=prn_write_text("ining to this receipt, please contact   ",40,1);
			ret=prn_write_text("National Manager,                       ",40,1);
			ret=prn_write_text("Retail Portfolio Management,            ",40,1);
			ret=prn_write_text("HDFC Bank Cards Division,               ",40,1);
			ret=prn_write_text("No.8, Lattice Bridge Road,              ",40,1);
			ret=prn_write_text("Thiruvanmiyur, Chennai 600041.          ",40,1);
			ret=prn_write_text("Please make the payment to an authorized",40,1);
			ret=prn_write_text("representative carrying a valid         ",40,1);
			ret=prn_write_text("identity card.                          ",40,1);					
			prn_paper_feed(1);
			ret=prn_write_text("** Please store this receipt properly.  ",40,1);
			ret=prn_write_text("It is suggested that a photocopy should ",40,1);
			ret=prn_write_text("be taken for your prolonged record.     ",40,1);			
			prn_paper_feed(2);						
		}
		else
		{
			ret=prn_write_text(Print_Chq_DD,strlen(Print_Chq_DD), 1);			
			ret=prn_write_text(Print_Chq_DD_Date,strlen(Print_Chq_DD_Date), 1);
			ret=prn_write_text(Print_Chq_DD_Bank,strlen(Print_Chq_DD_Bank), 1);
			ret=prn_write_text(Print_Micr_No,strlen(Print_Micr_No),1);				
			ret=prn_write_text("----------------------------------------",40,1);
			ret=prn_write_text("  Any overwriting",22,2);
			ret=prn_write_text("    is invalid",22,2);
			ret=prn_write_text("----------------------------------------",40,1);
			ret=prn_write_text("With regard to any discrepancy perta-   ",40,1);			
			ret=prn_write_text("ining to this receipt, please contact   ",40,1);
			ret=prn_write_text("National Manager,                       ",40,1);
			ret=prn_write_text("Retail Portfolio Management,            ",40,1);
			ret=prn_write_text("HDFC Bank Cards Division,               ",40,1);
			ret=prn_write_text("No.8, Lattice Bridge Road,              ",40,1);
			ret=prn_write_text("Thiruvanmiyur, Chennai 600041.          ",40,1);
			ret=prn_write_text("Please make the payment to an authorized",40,1);
			ret=prn_write_text("representative carrying a valid         ",40,1);
			ret=prn_write_text("identity card.                          ",40,1);
			prn_paper_feed(1);
			ret=prn_write_text("*Cheque/DD subject to Realisation       ",40,1);
			prn_paper_feed(1);
			ret=prn_write_text("** Please store this receipt properly.  ",40,1);
			ret=prn_write_text("It is suggested that a photocopy should ",40,1);
			ret=prn_write_text("be taken for your prolonged record.     ",40,1);	
			prn_paper_feed(2);					
		}
	}
	else
	{		
		//ret=prn_write_text(Print_PAN_No,strlen(Print_PAN_No), 1);
		if(strcmp(Chq_DD_show,"0")==0)
		{			
			ret=prn_write_text("----------------------------------------",40,1);
			ret=prn_write_text("  Any overwriting",22,2);
			ret=prn_write_text("    is invalid",22,2);
			ret=prn_write_text("----------------------------------------",40,1);
			ret=prn_write_text("With regard to any discrepancy perta-   ",40,1);			
			ret=prn_write_text("ining to this receipt, please contact   ",40,1);
			ret=prn_write_text("National Manager,                       ",40,1);
			ret=prn_write_text("Retail Portfolio Management,            ",40,1);
			ret=prn_write_text("HDFC Bank Cards Division,               ",40,1);
			ret=prn_write_text("No.8, Lattice Bridge Road,              ",40,1);
			ret=prn_write_text("Thiruvanmiyur, Chennai 600041.          ",40,1);
			ret=prn_write_text("Please make the payment to an authorized",40,1);
			ret=prn_write_text("representative carrying a valid         ",40,1);
			ret=prn_write_text("identity card.                          ",40,1);			
			prn_paper_feed(1);
			ret=prn_write_text("** Please store this receipt properly.  ",40,1);
			ret=prn_write_text("It is suggested that a photocopy should ",40,1);
			ret=prn_write_text("be taken for your prolonged record.     ",40,1);			
			prn_paper_feed(2);
		}
		else
		{			
			ret=prn_write_text(Print_Chq_DD,strlen(Print_Chq_DD), 1);			
			ret=prn_write_text(Print_Chq_DD_Date,strlen(Print_Chq_DD_Date), 1);
			ret=prn_write_text(Print_Chq_DD_Bank,strlen(Print_Chq_DD_Bank), 1);
			ret=prn_write_text(Print_Micr_No,strlen(Print_Micr_No),1);			
			ret=prn_write_text("----------------------------------------",40,1);
			ret=prn_write_text("  Any overwriting",22,2);
			ret=prn_write_text("    is invalid",22,2);
			ret=prn_write_text("----------------------------------------",40,1);
			ret=prn_write_text("With regard to any discrepancy perta-   ",40,1);			
			ret=prn_write_text("ining to this receipt, please contact   ",40,1);
			ret=prn_write_text("National Manager,                       ",40,1);
			ret=prn_write_text("Retail Portfolio Management,            ",40,1);
			ret=prn_write_text("HDFC Bank Cards Division,               ",40,1);
			ret=prn_write_text("No.8, Lattice Bridge Road,              ",40,1);
			ret=prn_write_text("Thiruvanmiyur, Chennai 600041.          ",40,1);
			ret=prn_write_text("Please make the payment to an authorized",40,1);
			ret=prn_write_text("representative carrying a valid         ",40,1);
			ret=prn_write_text("identity card.                          ",40,1);			
			prn_paper_feed(1);
			ret=prn_write_text("*Cheque/DD subject to Realisation       ",40,1);
			prn_paper_feed(1);
			ret=prn_write_text("** Please store this receipt properly.  ",40,1);
			ret=prn_write_text("It is suggested that a photocopy should ",40,1);
			ret=prn_write_text("be taken for your prolonged record.     ",40,1);				
			prn_paper_feed(2);		
		}
	}	
	prn_close();
	MAX_NO_REPRINT_NO=0;
	Receipt_Reprint(Executive_Code,CC_no_show,Transaction_No,Print_Transaction_No,Total_Amount_show,Instument_Type_show,Chq_DD_Bank_show,Chq_DD_show,Chq_DD_Date_show,PAN_show,Mobile_show,Payee_Type_show,Place_Cont_show,Micr_Number_show,Transaction_datenow,Transaction_timenow,"0","0",PickPayment_Flag_show);
	}
	else 
	{
		lk_dispclr();
		lk_disptext(2,0,"  Please Re-Enter",0);
		lk_disptext(4,0,"Transaction Details",0);
		sleep(2);
		Dra_Menu();
	}

}

/******************************************************************/
Receipt_Reprint(char* Reprint_Executive_Code,char* Reprint_CC_no,char* Transaction_No,char* Reprint_Transaction_No,char*  Reprint_Total_Amount,char* Reprint_Instument_Type,char* Reprint_Chq_DD_Bank,char* Reprint_Chq_DD,char* Reprint_Chq_DD_Date,char* Reprint_PAN,char* Reprint_Payee_Mobile,char* Reprint_Payee_Type,char* Reprint_Place_Cont,char* Reprint_Micr_Number,char* Reprint_Transaction_dateno,char* Reprint_Transaction_timenow,char* Reprint_Remarks,char* Reprint_Print_Count,char* Reprint_PickPayment_Flag_show)
{	
	int Re_print_key;
	char RePrinting_CC_No[100],RePrinting_Date_Time[100],RePrinting_Customer_Name[100],RePrinting_Payee[100],RePrinting_Pay_type[100],RePrinting_Amount[100],RePrinting_In_char[200],RePrinting_Product_Name[100],RePrinting_Agent_Code[100],RePrinting_Agent_Name[100],RePrinting_Exe_Card[100],RePrinting_Inst_Type[100],RePrinting_PAN[100],RePrinting_Chq_DD[100],RePrinting_Chq_DD_Date[100],RePrinting_Chq_DD_Bank[100],RePrinting_Transaction_No[100],Tran_Payee_Type[50],Tran_Place_Cont[50],RePrintingt_Exe_Card[100],RePrinting_Amount_In_char[200],Machine_ID[100],Executive_Code[20],timenow[50],datenow[50],Cheq_DD_Date_segregat[20],RePrint_Executive_Name[100],RePrint_Micr_No[100],RePrint_Bucket_desc[100],RePrint_card_outstanding[100],RePrint_amount_overdue[100],Credit_Card_no[30],Branch_Id[30];
	char reprint_count[30],Battery_return[20];
	char bmpbuff1[30000];
	char RePrint_EMI_Amount[50],RePrint_CBC_Amount[50],RePrint_LPP_Amount[50],RePrint_Other_Amount[50],Reprint_maxcount[5],RePrint_Multiple_Remarks[500];
        resultset RePrint_perday,Get_contract_no,Get_pickup_contract_no,Get_allocation_contact_no;
	int len,No_Of_Copy,ret=0;

	memset(&RePrint_perday,0,sizeof(RePrint_perday));
	memset(&Get_contract_no,0,sizeof(Get_contract_no));
	memset(&Get_pickup_contract_no,0,sizeof(Get_pickup_contract_no));
 	memset(&Get_allocation_contact_no,0,sizeof(Get_allocation_contact_no));      	
	
	chk_rtc();		
	lk_dispclr();
	lk_disptext(0,4,"HDFC Bank CC",0);
	lk_disptext(1,2,"Payment Re-print",0);	
	lk_disphlight(1);
	lk_disptext(2,0,"  Would you like to",0);
	lk_disptext(3,0,"    Re-print this ",0);
	memset(reprint_count,0,sizeof(reprint_count));
	sprintf(reprint_count," Transaction?(Try%d)",MAX_NO_REPRINT_NO+1);
	lk_disptext(4,0,reprint_count,0);	
	lk_disptext(5,0,"  1=>YES   2=>NO",0);
	Re_print_key=lk_getkey();	
	if(Re_print_key==1)
	{
		memset(Reprint_maxcount,0,sizeof(Reprint_maxcount));
		sprintf(Reprint_maxcount,"%d",MAX_NO_REPRINT_NO+1);

		memset(Machine_ID,0,sizeof(Machine_ID));
		memset(Executive_Code,0,sizeof(Executive_Code));
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		RePrint_perday=get_result("Select ReprintperDay,hwid,Executive_id,Executive_Name from operationalstatus");		
		sprintf(Machine_ID,"%s",RePrint_perday.recordset[0][1]);
		sprintf(Executive_Code,"%s",Decrypted_sqlite_DB(RePrint_perday.recordset[0][2]));		
		memset(RePrinting_Transaction_No,0,sizeof(RePrinting_Transaction_No));
		sprintf(RePrinting_Transaction_No,"%s",Reprint_Transaction_No);		
		
	
		
		memset(Tran_Payee_Type,0,sizeof(Tran_Payee_Type));
		if(strcmp(Reprint_Payee_Type,"1")==0) strcpy(Tran_Payee_Type,"Customer");
		else if(strcmp(Reprint_Payee_Type,"2")==0) strcpy(Tran_Payee_Type,"Relative");
		else if(strcmp(Reprint_Payee_Type,"3")==0) strcpy(Tran_Payee_Type,"Third Party");
		
		memset(Tran_Place_Cont,0,sizeof(Tran_Place_Cont));
		if(strcmp(Reprint_Place_Cont,"1")==0) strcpy(Tran_Place_Cont,"Residence");
		else if(strcmp(Reprint_Place_Cont,"2")==0) strcpy(Tran_Place_Cont,"Office");
		else if(strcmp(Reprint_Place_Cont,"3")==0) strcpy(Tran_Place_Cont,"Others");			
		
		memset(timenow,0,sizeof(timenow));
		sprintf(timenow,"%02d:%02d:%02d",curt.tm_hour,curt.tm_min,curt.tm_sec);
		memset(datenow,0,sizeof(datenow));	
		sprintf(datenow,"%02d-%02d-%02d",(curt.tm_mday),(curt.tm_mon+1),(curt.tm_year-100));
		memset(RePrinting_Date_Time,0,sizeof(RePrinting_Date_Time));
		sprintf(RePrinting_Date_Time,"Date:%s         ",datenow);
		
		
		
		
		memset(RePrinting_Amount,0,sizeof(RePrinting_Amount));
		sprintf(RePrinting_Amount,"Amount          : Rs.%s/-",Reprint_Total_Amount);	
		memset(RePrinting_Amount_In_char,0,sizeof(RePrinting_Amount_In_char));				
		sprintf(RePrinting_Amount_In_char,"Amount(in words): Rs.%s",Conversion(Reprint_Total_Amount));
		memset(RePrinting_Inst_Type,0,sizeof(RePrinting_Inst_Type));
		sprintf(RePrinting_Inst_Type,"Mode of Payment : %s",Reprint_Instument_Type);
		memset(RePrinting_Pay_type,0,sizeof(RePrinting_Pay_type));
		sprintf(RePrinting_Payee,"Payee Type      : %s",Tran_Payee_Type);
		memset(RePrinting_PAN,0,sizeof(RePrinting_PAN));
		sprintf(RePrinting_PAN,"PAN No.         : %s",Reprint_PAN);	
		memset(RePrinting_Chq_DD,0,sizeof(RePrinting_Chq_DD));
		sprintf(RePrinting_Chq_DD,"Chq/DD No.      : %s",Reprint_Chq_DD);		
		memset(Cheq_DD_Date_segregat,0,sizeof(Cheq_DD_Date_segregat));
		sprintf(Cheq_DD_Date_segregat,"%s",Reprint_Chq_DD_Date);
		memset(RePrinting_Chq_DD_Date,0,sizeof(RePrinting_Chq_DD_Date));
		sprintf(RePrinting_Chq_DD_Date,"Chq/DD Date     : %c%c-%c%c-%c%c",Cheq_DD_Date_segregat[0],Cheq_DD_Date_segregat[1],Cheq_DD_Date_segregat[2],Cheq_DD_Date_segregat[3],Cheq_DD_Date_segregat[4],Cheq_DD_Date_segregat[5]);			
		memset(RePrinting_Chq_DD_Bank,0,sizeof(RePrinting_Chq_DD_Bank));	
		sprintf(RePrinting_Chq_DD_Bank,"Bank Name       : %s",Reprint_Chq_DD_Bank);
		memset(RePrinting_Product_Name,0,sizeof(RePrinting_Product_Name));
		sprintf(RePrinting_Product_Name,"Product Name    : %s","Credit Card");				
		
		memset(RePrinting_Exe_Card,0,sizeof(RePrinting_Exe_Card));
		sprintf(RePrinting_Exe_Card,"FE ID Card #    : %s",Reprint_Executive_Code);
		memset(RePrint_Executive_Name,0,sizeof(RePrint_Executive_Name));
		sprintf(RePrint_Executive_Name,"FE Name         : %s",RePrint_perday.recordset[0][3]);		
		memset(RePrint_Micr_No,0,sizeof(RePrint_Micr_No));
		sprintf(RePrint_Micr_No,"MICR No.        : %s",Reprint_Micr_Number);
		if(strcmp(Reprint_PickPayment_Flag_show,"D")==0)
		{	
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		Get_allocation_contact_no= get_result("SELECT BUCKET,BUCKET_DEST,LOADED_BAL,TOTAL_AMT_DUE,CURRENT_BL,CARD_NUMBER,BRANCH_CODE,CUST_ID,AGENCE_CODE,AGENCE_NAME from allocation_details WHERE ALT_ACCOUNT_NO='%s';",Reprint_CC_no);
		
		
		memset(RePrinting_Customer_Name,0,sizeof(RePrinting_Customer_Name));
		snprintf(RePrinting_Customer_Name,38,"Customer Name   : %s",Get_allocation_contact_no.recordset[0][7]);
		memset(RePrinting_CC_No,0,sizeof(RePrinting_CC_No));
		sprintf(RePrinting_CC_No,"Card #          : %s",Get_allocation_contact_no.recordset[0][5]);
		memset(RePrinting_Agent_Name,0,sizeof(RePrinting_Agent_Name));
		sprintf(RePrinting_Agent_Name,"Agency Name     : %s",Get_allocation_contact_no.recordset[0][8]);
		memset(RePrinting_Agent_Code,0,sizeof(RePrinting_Agent_Code));
		sprintf(RePrinting_Agent_Code,"Agency Code     : %s",Get_allocation_contact_no.recordset[0][9]);
		memset(Credit_Card_no,0,sizeof(Credit_Card_no));
		sprintf(Credit_Card_no,"%s",Get_allocation_contact_no.recordset[0][5]);
		memset(Branch_Id,0,sizeof(Branch_Id));
		sprintf(Branch_Id,"%s",Get_allocation_contact_no.recordset[0][6]);
		
#if Printf
		
		printf("\n%s",RePrinting_Customer_Name);
		printf("\n%s",RePrinting_CC_No);
		printf("\n%s",RePrinting_Agent_Code);
		printf("\n%s",RePrinting_Agent_Name);
#endif
		
		
		memset(RePrint_Bucket_desc,0,sizeof(RePrint_Bucket_desc));
		sprintf(RePrint_Bucket_desc,"%s",Get_allocation_contact_no.recordset[0][1]);
		if(strcmp(RePrint_Bucket_desc,"RECOVERIES")==0)
		{
		memset(RePrint_card_outstanding,0,sizeof(RePrint_card_outstanding));
		sprintf(RePrint_card_outstanding,"Card Outstanding: %s",Get_allocation_contact_no.recordset[0][2]);
		}
		else
		{
		memset(RePrint_card_outstanding,0,sizeof(RePrint_card_outstanding));
		sprintf(RePrint_card_outstanding,"Card Outstanding: %s",Get_allocation_contact_no.recordset[0][4]);
		}
		memset(RePrint_amount_overdue,0,sizeof(RePrint_amount_overdue));
		sprintf(RePrint_amount_overdue,"Amount overdue  : %s",Get_allocation_contact_no.recordset[0][3]);
		}
		else
		{
			close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");	
		Get_pickup_contract_no= get_result("SELECT Filler1,AGENCE_NAME,AGENCE_CODE,CARD_NUMBER,BRANCHID from PICKUP_DETAILS WHERE Filler3='%s';",Reprint_CC_no);


		memset(RePrinting_CC_No,0,sizeof(RePrinting_CC_No));
		sprintf(RePrinting_CC_No,"Card #          : %s",Get_pickup_contract_no.recordset[0][3]);
		printf("\n%s",RePrinting_CC_No);
		memset(Credit_Card_no,0,sizeof(Credit_Card_no));
		sprintf(Credit_Card_no,"%s",Get_pickup_contract_no.recordset[0][3]);
		memset(Branch_Id,0,sizeof(Branch_Id));
		sprintf(Branch_Id,"%s",Get_pickup_contract_no.recordset[0][4]);
		memset(RePrinting_Customer_Name,0,sizeof(RePrinting_Customer_Name));
		snprintf(RePrinting_Customer_Name,38,"Customer Name   : %s",Get_pickup_contract_no.recordset[0][0]);
		memset(RePrinting_Agent_Name,0,sizeof(RePrinting_Agent_Name));
		sprintf(RePrinting_Agent_Name,"Agency Name     : %s",Get_pickup_contract_no.recordset[0][1]);
		memset(RePrinting_Agent_Code,0,sizeof(RePrinting_Agent_Code));
		sprintf(RePrinting_Agent_Code,"Agency Code     : %s",Get_pickup_contract_no.recordset[0][2]);
		memset(RePrint_card_outstanding,0,sizeof(RePrint_card_outstanding));
		sprintf(RePrint_card_outstanding,"Card Outstanding: %s","NA");
		memset(RePrint_amount_overdue,0,sizeof(RePrint_amount_overdue));
		sprintf(RePrint_amount_overdue,"Amount overdue  : %s","NA");
#if Printf		
		printf("\n%s",RePrinting_Customer_Name);
		printf("\n%s",RePrinting_CC_No);
		printf("\n%s",RePrinting_Agent_Code);
		printf("\n%s",RePrinting_Agent_Name);	
#endif
		}		
	

		prn_open();
		memset(Battery_return,0,sizeof(Battery_return));
		sprintf(Battery_return,"%s",Battery_Print_test());
#if PRINTF
		printf("\nBattery_return:%0.2f",atof(Battery_return));
#endif
		if(atof(Battery_return)<(6.8))
		{
			lk_dispclr();
			lk_disptext(1,5,"Battery Low!",1);
			lk_disptext(3,0,"  Plug Power Cord",0);//plug Power code cable
			lk_disptext(4,0," Cable & Press Any",0);
			lk_disptext(5,0,"  Key To continue",0);
			lk_getkey();
		}
		if(prn_paperstatus()!=0)
		{
			lk_dispclr();
			lk_disptext(1,5,"No Paper !",1);
			lk_disptext(3,0,"Insert Paper & Press",0);
			lk_disptext(4,0," Any Key To continue",0);
			lk_getkey();
		}		
		lk_dispclr();
		lk_disptext(2,2,"Printing..",1);			
		ret=prn_write_bmp(HDFC_Logo,sizeof(HDFC_Logo));				
		ret=prn_write_text("    RECEIPT COPY ",22,2);
		ret=prn_write_text("---------------------------------------",40,1);	
		ret=prn_write_text("We acknowledge the receipt of the       ",40,1);
		ret=prn_write_text("following payment on your credit        ",40,1);
		ret=prn_write_text("card account held with HDFC BANK LTD    ",40,1);
		ret=prn_write_text("----------------------------------------",40,1);
		ret=prn_write_text(RePrinting_Date_Time,strlen(RePrinting_Date_Time), 1);
		ret=prn_write_text("----------------------------------------",40,1);
		ret=prn_write_text(RePrinting_Transaction_No,strlen(RePrinting_Transaction_No),2);	
		ret=prn_write_text("----------------------------------------",40,1);
		ret=prn_write_text(RePrinting_CC_No,strlen(RePrinting_CC_No), 1);
		ret=prn_write_text(RePrinting_Customer_Name,strlen(RePrinting_Customer_Name), 1);
		ret=prn_write_text(RePrinting_Payee,strlen(RePrinting_Payee), 1);					
		ret=prn_write_text(RePrinting_Amount,strlen(RePrinting_Amount), 1);
		ret=prn_write_text(RePrinting_Amount_In_char,strlen(RePrinting_Amount_In_char), 1);
		ret=prn_write_text(RePrinting_Inst_Type,strlen(RePrinting_Inst_Type), 1);
		if(strcmp(Reprint_PickPayment_Flag_show,"D")==0)
		{
		ret=prn_write_text(RePrint_card_outstanding,strlen(RePrint_card_outstanding), 1);		
		ret=prn_write_text(RePrint_amount_overdue,strlen(RePrint_amount_overdue), 1);
		}
		ret=prn_write_text(RePrinting_Agent_Name,strlen(RePrinting_Agent_Name), 1);
		ret=prn_write_text(RePrinting_Agent_Code,strlen(RePrinting_Agent_Code), 1);			
		ret=prn_write_text(RePrinting_Exe_Card,strlen(RePrinting_Exe_Card), 1);	
		ret=prn_write_text(RePrint_Executive_Name,strlen(RePrint_Executive_Name),1);
					
		
		if(strcmp(Reprint_PAN,"0")==0)
		{
			if(strcmp(Reprint_Chq_DD,"0")==0)
			{				
				ret=prn_write_text("----------------------------------------",40,1);
				ret=prn_write_text("  Any overwriting",22,2);
				ret=prn_write_text("    is invalid",22,2);
				ret=prn_write_text("----------------------------------------",40,1);
				ret=prn_write_text("With regard to any discrepancy perta-   ",40,1);			
				ret=prn_write_text("ining to this receipt, please contact   ",40,1);
				ret=prn_write_text("National Manager,                       ",40,1);
				ret=prn_write_text("Retail Portfolio Management,            ",40,1);
				ret=prn_write_text("HDFC Bank Cards Division,               ",40,1);
				ret=prn_write_text("No.8, Lattice Bridge Road,              ",40,1);
				ret=prn_write_text("Thiruvanmiyur, Chennai 600041.          ",40,1);
				ret=prn_write_text("Please make the payment to an authorized",40,1);
				ret=prn_write_text("representative carrying a valid         ",40,1);
				ret=prn_write_text("identity card.",40,1);
				prn_paper_feed(1);
				ret=prn_write_text("** Please store this receipt properly.  ",40,1);
				ret=prn_write_text("It is suggested that a photocopy should ",40,1);
				ret=prn_write_text("be taken for your prolonged record.     ",40,1);			
				prn_paper_feed(2);
			}
			else
			{
				ret=prn_write_text(RePrinting_Chq_DD,strlen(RePrinting_Chq_DD), 1);
				ret=prn_write_text(RePrinting_Chq_DD_Date,strlen(RePrinting_Chq_DD_Date), 1);
				ret=prn_write_text(RePrinting_Chq_DD_Bank,strlen(RePrinting_Chq_DD_Bank), 1);
				ret=prn_write_text(RePrint_Micr_No,strlen(RePrint_Micr_No), 1);		
				ret=prn_write_text("----------------------------------------",40,1);
				ret=prn_write_text("  Any overwriting",22,2);
				ret=prn_write_text("    is invalid",22,2);
				ret=prn_write_text("----------------------------------------",40,1);
				ret=prn_write_text("With regard to any discrepancy perta-   ",40,1);			
				ret=prn_write_text("ining to this receipt, please contact   ",40,1);
				ret=prn_write_text("National Manager,                       ",40,1);
				ret=prn_write_text("Retail Portfolio Management,            ",40,1);
				ret=prn_write_text("HDFC Bank Cards Division,               ",40,1);
				ret=prn_write_text("No.8, Lattice Bridge Road,              ",40,1);
				ret=prn_write_text("Thiruvanmiyur, Chennai 600041.          ",40,1);
				ret=prn_write_text("Please make the payment to an authorized",40,1);
				ret=prn_write_text("representative carrying a valid",40,1);
				ret=prn_write_text("identity card.",40,1);
				prn_paper_feed(1);
				ret=prn_write_text("*Cheque/DD subject to Realisation        ",40,1);
				prn_paper_feed(1);
				ret=prn_write_text("** Please store this receipt properly.  ",40,1);
				ret=prn_write_text("It is suggested that a photocopy should ",40,1);
				ret=prn_write_text("be taken for your prolonged record.     ",40,1);			
				prn_paper_feed(2);			
			}			
			
		}
		else
		{
			//ret=prn_write_text(RePrinting_PAN,strlen(RePrinting_PAN), 1);
			if(strcmp(Reprint_Chq_DD,"0")==0)
			{				
				ret=prn_write_text("----------------------------------------",40,1);
				ret=prn_write_text("  Any overwriting",22,2);
				ret=prn_write_text("    is invalid",22,2);
				ret=prn_write_text("----------------------------------------",40,1);
				ret=prn_write_text("With regard to any discrepancy perta-   ",40,1);			
				ret=prn_write_text("ining to this receipt, please contact   ",40,1);
				ret=prn_write_text("National Manager,                       ",40,1);
				ret=prn_write_text("Retail Portfolio Management,            ",40,1);
				ret=prn_write_text("HDFC Bank Cards Division,               ",40,1);
				ret=prn_write_text("No.8, Lattice Bridge Road,              ",40,1);
				ret=prn_write_text("Thiruvanmiyur, Chennai 600041.          ",40,1);
				ret=prn_write_text("Please make the payment to an authorized",40,1);
				ret=prn_write_text("representative carrying a valid         ",40,1);
				ret=prn_write_text("identity card.",40,1);
				prn_paper_feed(1);
				ret=prn_write_text("** Please store this receipt properly.  ",40,1);
				ret=prn_write_text("It is suggested that a photocopy should ",40,1);
				ret=prn_write_text("be taken for your prolonged record.     ",40,1);		
				prn_paper_feed(2);
			}
			else
			{
			       	ret=prn_write_text(RePrinting_Chq_DD,strlen(RePrinting_Chq_DD), 1);	
				ret=prn_write_text(RePrinting_Chq_DD_Date,strlen(RePrinting_Chq_DD_Date), 1);
				ret=prn_write_text(RePrinting_Chq_DD_Bank,strlen(RePrinting_Chq_DD_Bank), 1);
				ret=prn_write_text(RePrint_Micr_No,strlen(RePrint_Micr_No), 1);			
				ret=prn_write_text("----------------------------------------",40,1);
				ret=prn_write_text("  Any overwriting",22,2);
				ret=prn_write_text("    is invalid",22,2);
				ret=prn_write_text("----------------------------------------",40,1);
				ret=prn_write_text("With regard to any discrepancy perta-   ",40,1);			
				ret=prn_write_text("ining to this receipt, please contact   ",40,1);
				ret=prn_write_text("National Manager,                       ",40,1);
				ret=prn_write_text("Retail Portfolio Management,            ",40,1);
				ret=prn_write_text("HDFC Bank Cards Division,               ",40,1);
				ret=prn_write_text("No.8, Lattice Bridge Road,              ",40,1);
				ret=prn_write_text("Thiruvanmiyur, Chennai 600041.          ",40,1);
				ret=prn_write_text("Please make the payment to an authorized",40,1);
				ret=prn_write_text("representative carrying a valid         ",40,1);
				ret=prn_write_text("identity card.                          ",40,1);
				prn_paper_feed(1);
				ret=prn_write_text("*Cheque/DD subject to Realisation       ",40,1);
				prn_paper_feed(1);
				ret=prn_write_text("** Please store this receipt properly.  ",40,1);
				ret=prn_write_text("It is suggested that a photocopy should ",40,1);
				ret=prn_write_text("be taken for your prolonged record.     ",40,1);
				prn_paper_feed(2);
			}
		}			
		prn_close();	
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccctrans.db");					
		execute("UPDATE transactiondetails SET Reprint_count='%s' where Transaction_ID='%s'",Reprint_maxcount,Transaction_No);	
		if(MAX_NO_REPRINT_NO <= atoi(RePrint_perday.recordset[0][0])-2)
		{
		printf("\n\nMAX_NO_REPRINT_NO%d",MAX_NO_REPRINT_NO);
		MAX_NO_REPRINT_NO++;
		Receipt_Reprint(Reprint_Executive_Code,Reprint_CC_no,Transaction_No,Reprint_Transaction_No,Reprint_Total_Amount,Reprint_Instument_Type,Reprint_Chq_DD_Bank,Reprint_Chq_DD,Reprint_Chq_DD_Date,Reprint_PAN,Reprint_Payee_Mobile,Reprint_Payee_Type, Reprint_Place_Cont,Reprint_Micr_Number,Reprint_Transaction_dateno,Reprint_Transaction_timenow,Reprint_Remarks,Reprint_maxcount,Reprint_PickPayment_Flag_show);
		}
		else
		{		
		Receipt_Cancel(Transaction_No,Reprint_CC_no,Reprint_maxcount,Reprint_PickPayment_Flag_show);			
		}		
	}
	else if(Re_print_key==2)
	{		
	Receipt_Cancel(Transaction_No,Reprint_CC_no,Reprint_Print_Count,Reprint_PickPayment_Flag_show);
	}
	else	
	Receipt_Reprint(Reprint_Executive_Code,Reprint_CC_no,Transaction_No,Reprint_Transaction_No,Reprint_Total_Amount,Reprint_Instument_Type,Reprint_Chq_DD_Bank,Reprint_Chq_DD,Reprint_Chq_DD_Date,Reprint_PAN,Reprint_Payee_Mobile,Reprint_Payee_Type, Reprint_Place_Cont,Reprint_Micr_Number,Reprint_Transaction_dateno,Reprint_Transaction_timenow,Reprint_Remarks,Reprint_Print_Count,Reprint_PickPayment_Flag_show);
}


/******************************************************************/

/******************************************************************/
char* pw(long n,char ch[])
{
//	printf("\nN is0:%ld",n);
	memset(amt_char,0,20);
//(n>19)?printf("%s %s ",ten[n/10],one[n%10]):printf("%s ",one[n]);
	if(n>19)
	{
//		printf("\nN is 1:%ld",n);
		strcat(amt_char,ten[n/10]);
//		printf("\nN is 2:%ld",n);
		strcat(amt_char,one[n%10]);

	}
	else strcat(amt_char,one[n]);
//	printf("\nN is 3:%ld",n);
	if(strlen(amt_char)==1) memset(amt_char,0,sizeof(amt_char));
	if(n)strcat(amt_char,ch);
	return amt_char;
}	
/******************************************************************/

/******************************************************************/
char* Amount_Conversion(char* Entered_Amount,char* Flag_Entered_Amount)
{
 	long n;
	char Amount_In_Char1[20];

	memset(Amount_In_Char,0,200);
	n=atol(Entered_Amount);
	printf("\nAmunt:%ld\n",n);
	if(n<=0) {   printf("Enter numbers greater than 0");strcpy(Amount_In_Char,"Zero Only");}
 	else
 	{
		n=atol(Entered_Amount);
#if PRINTF
		printf("\nAmount:%ld\n",n);
#endif
		  memset(Amount_In_Char1,0,20);
	          sprintf(Amount_In_Char1,"%s",pw((n/10000000)," Crore"));
		  strcat(Amount_In_Char,Amount_In_Char1);
//		printf("\nAmount_In_Char:%s",Amount_In_Char);
		  memset(Amount_In_Char1,0,20);
		n=atol(Entered_Amount);
#if PRINTF
		printf("\nAmount:%ld\n",n);
#endif
                  sprintf(Amount_In_Char1,"%s",pw(((n/100000)%100)," Lakh"));
		  strcat(Amount_In_Char,Amount_In_Char1);
//		printf("\nAmount_In_Char:%s",Amount_In_Char);
                  memset(Amount_In_Char1,0,20);
		n=atol(Entered_Amount);
#if PRINTF
		printf("\nAmount:%ld\n",n);
#endif
		  sprintf(Amount_In_Char1,"%s",pw(((n/1000)%100)," Thousand"));
		  strcat(Amount_In_Char,Amount_In_Char1);
//		printf("\nAmount_In_Char:%s",Amount_In_Char);
		  memset(Amount_In_Char1,0,20);  
		n=atol(Entered_Amount); 
#if PRINTF               
		printf("\nAmount:%ld\n",n);
#endif
		  sprintf(Amount_In_Char1,"%s",pw(((n/100)%10)," Hundred"));
		  strcat(Amount_In_Char,Amount_In_Char1);
//		printf("\nAmount_In_Char:%s",Amount_In_Char);
		  memset(Amount_In_Char1,0,20);
		n=atol(Entered_Amount);
#if PRINTF
		printf("\nAmount:%ld\n",n);
#endif
		  if(atoi(Flag_Entered_Amount)==0)
		  {                
		  //sprintf(Amount_In_Char1,"%s",pw((n%100)," "));
		  sprintf(Amount_In_Char1,"%s",pw((n%100),""));
		  strcat(Amount_In_Char,Amount_In_Char1);
		  }
		  else
		  {
		  //sprintf(Amount_In_Char1,"%s",pw((n%100)," "));
		  sprintf(Amount_In_Char1,"%s",pw((n%100),""));
		  strcat(Amount_In_Char,Amount_In_Char1);
		  }		
//		printf("\nAmount_In_Char:%s",Amount_In_Char);
	}
#if PRINTF 
	printf("\nNumber in words:%s",Amount_In_Char);
#endif
	return Amount_In_Char;
}


/******************************************************************/
/******************************************************************/
char* Conversion(char* Entered_Amount)
{
	int i,k=0;
	char Amount_verify[50],After_decimal[50],Amount_decimal[50],Final_Amount[150],Amount_check[100];	

	memset(Conv_Amount,0,sizeof(Conv_Amount));
		
	for(i=0;i<strlen(Entered_Amount);i++)
	{
		if(Entered_Amount[i]=='.')
		{	
			k=i+1;
			printf("\nk:%d",k);
			memset(After_decimal,0,sizeof(After_decimal));
			sprintf(After_decimal,"%c%c",Entered_Amount[i+1],Entered_Amount[i+2]);
			if(strcmp(After_decimal,"00")==0)
			{
			break;
			}
			else
			{			
			memset(Amount_decimal,0,sizeof(Amount_decimal));
			sprintf(Amount_decimal,"%s paise Only",Amount_Conversion(After_decimal,"0"));		
			}
			memset(Amount_check,0,sizeof(Amount_check));
			sprintf(Amount_check,"%s",Amount_Conversion(Entered_Amount,"1"));				
			memset(Final_Amount,0,sizeof(Final_Amount));
			sprintf(Final_Amount,"%s and%s",Amount_check,Amount_decimal);
			printf("\nFinal_Amount:%s",Final_Amount);				
			strcpy(Conv_Amount,Final_Amount);				
			return Conv_Amount;
		}		
	}	
	memset(Amount_verify,0,sizeof(Amount_verify));
	sprintf(Amount_verify,"%s Only",Amount_Conversion(Entered_Amount,"0"));	
	strcpy(Conv_Amount,Amount_verify);	
	return Conv_Amount;	
}

/*********************************************************************************************/
int Check_Jalian_lasttranscation(char* unique_lasttranscation,char* last_transaction_sum,char* check_printing_datenow)
{
	int check_count=0,EOD_Wrong_key;
	char current_date[15],sqlite3_current_date[15],transcationid[15],Take_last_transcationid[15],status_current_date[15],status_previous_date[15];
	resultset get_julian_transcationid,get_sqlite_date,get_transcation_id;


	memset(&get_julian_transcationid,0,sizeof(get_julian_transcationid));
	memset(&get_sqlite_date,0,sizeof(get_sqlite_date));
	memset(&get_transcation_id,0,sizeof(get_transcation_id));
#if PRINTF
	printf("\nunique_lasttranscation:%s",unique_lasttranscation);		
	printf("\nlast_transaction_sum:%s",last_transaction_sum);
	printf("\ncheck_printing_datenow:%s",check_printing_datenow);	
#endif
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	get_julian_transcationid=get_result("select last_transaction_no,loginstatus,Previous_date from operationalstatus");

	memset(transcationid,0,sizeof(transcationid));
	sprintf(transcationid,"%04d",atoi(get_julian_transcationid.recordset[0][0]));

	memset(Take_last_transcationid,0,sizeof(Take_last_transcationid));
	sprintf(Take_last_transcationid,"%04d",atoi(get_julian_transcationid.recordset[0][0])-1);
	printf("\ntranscationid:%s",transcationid);	
	printf("\nTake_last_transcationid:%s",Take_last_transcationid);
	get_sqlite_date=get_result("SELECT date('now')");	
	
	memset(status_previous_date,0,sizeof(status_previous_date));
	sprintf(status_previous_date,"%s",get_julian_transcationid.recordset[0][2]);
	
        memset(status_current_date,0,sizeof(status_previous_date));
	sprintf(status_current_date,"%s",get_sqlite_date.recordset[0][0]);
#if PRINTF
	printf("status_previous_date:%s",status_previous_date);
	printf("status_current_date:%s",status_current_date);		
#endif
	if(strcmp(status_previous_date,status_current_date)==0)
	{
		if(strcmp(transcationid,last_transaction_sum)==0)
		{
			if(atoi(transcationid)>atoi(Take_last_transcationid))	
			{			
			printf("\ncheck_printing_datenow:%s",check_printing_datenow);				
			memset(sqlite3_current_date,0,sizeof(sqlite3_current_date));
			sprintf(sqlite3_current_date,"%s",get_sqlite_date.recordset[0][0]);
			printf("\nsqlite3_current_date:%s",sqlite3_current_date);		
			if(strcmp(check_printing_datenow,sqlite3_current_date)==0)
			{								
				close_sqlite();
				open_sqlite("/mnt/jffs2/hdfccctrans.db");
				get_transcation_id=get_result("select Transaction_ID from transactiondetails");	
				for(check_count=0;check_count<get_transcation_id.rows;check_count++)
				{
					if(strcmp(get_transcation_id.recordset[check_count][0],unique_lasttranscation)==0)
					{						
						Dra_Menu();	
					}
				}				
			}
			else Dra_Menu(); 
			}
			else  Dra_Menu();				
		}
		else
		{
				Dra_Menu();
		}
	}
	else
	{
DO_EOD:
	        lk_dispclr();		
	        lk_disptext(2,0,"  Please Do EOD",0);	       	        
		lk_disptext(5,0,"Press Enter to Cont.",0);
		EOD_Wrong_key = lk_getkey();
		if(EOD_Wrong_key==ENTER) Dra_Menu();
		else goto DO_EOD;	
	}

return 0;
}


