/*******************************************************************/
//			  HDFC  Bank CC
//		        Customer_Details.c
/*******************************************************************/
#include <header.h>


/*******************************************************************/
int Customer_Details(char* Cust_Details_Contract_no,char* Customer_Details_Flag)
{
	MENU_T menu;
        int opt=0,ret;
        int selItem  = 0;
        int acceptKbdEvents;
        int maxEntries = 0;
        short scroll = 0;
	resultset Customer_Details;

	memset(&Customer_Details,0,sizeof(Customer_Details));

	printf("In Customer details\n");
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	
	printf("In Customer A/C id:%s\n",Cust_Details_Contract_no);

	Customer_Details= get_result("SELECT ALT_ACCOUNT_NO FROM ALLOCATION_DETAILS WHERE ALT_ACCOUNT_NO='%s';",Cust_Details_Contract_no);
	printf("In Customer details\n");
	if(Customer_Details.rows>0)
	{
	
	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       12;
        maxEntries                      =      menu.maxEntries;

	strcpy(menu.title,"    Details"); 
	strcpy(menu.menu[0],"1.Card number");                   
        strcpy(menu.menu[1],"2.Alt Account No.");//	
	strcpy(menu.menu[2],"3.Card holder name");
	strcpy(menu.menu[3],"4.Date of last pay.");
	strcpy(menu.menu[4],"5.Amount last pay.");
	strcpy(menu.menu[5],"6.PAN number");
	strcpy(menu.menu[6],"7.Bucket description");	
	strcpy(menu.menu[7],"8.Charged off date");
	strcpy(menu.menu[8],"9.Residence Address");
	strcpy(menu.menu[9],"10.Alternate Address");
	strcpy(menu.menu[10],"11.Permanent Address");
	strcpy(menu.menu[11],"12.Office Address");	
	
	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case CANCEL:	After_selecting_customer(Cust_Details_Contract_no,Customer_Details_Flag);
				break;

		case ENTER:
			switch (selItem+1)
                    	{
			case 1: Display_Card_No(Cust_Details_Contract_no);
				break;

			case 2: Display_Alt_Account(Cust_Details_Contract_no);
				break;

			case 3: Display_Customer_Name(Cust_Details_Contract_no);
				break;

			case 4: Display_Date_last(Cust_Details_Contract_no);
				break;

			case 5: Display_Amount_last(Cust_Details_Contract_no);
				break;

			case 6: Display_PAN_Number(Cust_Details_Contract_no);
				break;

			case 7: Display_Bucket(Cust_Details_Contract_no);
				break;
			
			case 8: Display_Charged_off(Cust_Details_Contract_no);
				break;

			case 9: Display_Residence_Add(Cust_Details_Contract_no);
				break;

			case 10: Display_Alternate_Add(Cust_Details_Contract_no);
				 break;

			case 11: Display_Permanent_Add(Cust_Details_Contract_no);
				 break;

			case 12: Display_Office_Add(Cust_Details_Contract_no);
				 break;
	
			}		
		default : break;
		}//switch
	}//while
	}
	else Dra_Menu();

	return 0;
}


/******************************************************************/
int Display_Card_No(char* Dis_Sel_Contract_No)
{
	resultset Customer_Product;

	memset(&Customer_Product,0,sizeof(Customer_Product));

	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Customer_Product= get_result("SELECT CARD_NUMBER FROM ALLOCATION_DETAILS WHERE ALT_ACCOUNT_NO='%s';",Dis_Sel_Contract_No);
	if(Customer_Product.rows>0)
	{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,1,"  Customer Detail",0);
		lk_disphlight(1);
		lk_disptext(2,0,"Alt Account No.:",0);
		lk_disptext(3,0,Customer_Product.recordset[0][0],0);
		//sleep(7);
		lk_getkey();
	}
	else Customer_Details(Dis_Sel_Contract_No,"D");
return SUCCESS;
}



/******************************************************************/
int Display_Alt_Account(char* Dis_Sel_Contract_No)
{
	resultset Customer_Product;

	memset(&Customer_Product,0,sizeof(Customer_Product));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Customer_Product= get_result("SELECT ALT_ACCOUNT_NO FROM ALLOCATION_DETAILS WHERE ALT_ACCOUNT_NO='%s';",Dis_Sel_Contract_No);
	if(Customer_Product.rows>0)
	{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,1,"  Customer Detail",0);
		lk_disphlight(1);
		lk_disptext(2,0,"Alt Account No.:",0);
		lk_disptext(3,0,Customer_Product.recordset[0][0],0);		
		lk_getkey();
	}
	else Customer_Details(Dis_Sel_Contract_No,"D");
return SUCCESS;
}


/******************************************************************/
int Display_Customer_Name(char* Dis_Sel_Contract_No)
{
	resultset Customer_Name;

	memset(&Customer_Name,0,sizeof(Customer_Name));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Customer_Name= get_result("SELECT CUST_ID FROM ALLOCATION_DETAILS WHERE ALT_ACCOUNT_NO='%s';",Dis_Sel_Contract_No);
	if(Customer_Name.rows>0)
	{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,1,"  Customer Detail",0);
		lk_disphlight(1);
		lk_disptext(2,0,"  Customer Name:",0);
		lk_disptext(3,0,Customer_Name.recordset[0][0],0);		
		lk_getkey();
	}
	else Customer_Details(Dis_Sel_Contract_No,"D");
return SUCCESS;
}


/******************************************************************/
int Display_Date_last(char* Dis_Sel_Contract_No)
{
	resultset Customer_Name;

	memset(&Customer_Name,0,sizeof(Customer_Name));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Customer_Name= get_result("SELECT DATE_OF_LPAY FROM ALLOCATION_DETAILS WHERE ALT_ACCOUNT_NO='%s';",Dis_Sel_Contract_No);
	if(Customer_Name.rows>0)
	{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,1,"  Customer Detail",0);
		lk_disphlight(1);
		lk_disptext(2,0,"Date of last pay. :",0);
		lk_disptext(3,0,Customer_Name.recordset[0][0],0);		
		lk_getkey();
	}
	else Customer_Details(Dis_Sel_Contract_No,"D");
return SUCCESS;
}


/******************************************************************/
int Display_Amount_last(char* Dis_Sel_Contract_No)
{
	resultset Customer_Name;

	memset(&Customer_Name,0,sizeof(Customer_Name));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Customer_Name= get_result("SELECT AMT_OF_LPAY FROM ALLOCATION_DETAILS WHERE ALT_ACCOUNT_NO='%s';",Dis_Sel_Contract_No);
	if(Customer_Name.rows>0)
	{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,1,"  Customer Detail",0);
		lk_disphlight(1);
		lk_disptext(2,0,"Amount last pay. :",0);
		lk_disptext(3,0,Customer_Name.recordset[0][0],0);		
		lk_getkey();
	}
	else Customer_Details(Dis_Sel_Contract_No,"D");
return SUCCESS;
}




/******************************************************************/
int Display_PAN_Number(char* Dis_Sel_Contract_No)
{
	resultset Customer_Father_Name;

	memset(&Customer_Father_Name,0,sizeof(Customer_Father_Name));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Customer_Father_Name= get_result("SELECT PAN_NO FROM ALLOCATION_DETAILS WHERE ALT_ACCOUNT_NO='%s';",Dis_Sel_Contract_No);
	if(Customer_Father_Name.rows>0)
	{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,1,"  Customer Detail",0);
		lk_disphlight(1);
		lk_disptext(2,0,"PAN Number:",0);
		lk_disptext(3,0,Customer_Father_Name.recordset[0][0],0);		
		lk_getkey();
	}
	else Customer_Details(Dis_Sel_Contract_No,"D");
return SUCCESS;
}


/******************************************************************/
int Display_Bucket(char* Dis_Sel_Contract_No)
{
	resultset Customer_Name;

	memset(&Customer_Name,0,sizeof(Customer_Name));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Customer_Name= get_result("SELECT BUCKET_DEST FROM ALLOCATION_DETAILS WHERE ALT_ACCOUNT_NO='%s';",Dis_Sel_Contract_No);
	if(Customer_Name.rows>0)
	{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,1,"  Customer Detail",0);
		lk_disphlight(1);
		lk_disptext(2,0,"Bucket Descript.:",0);
		lk_disptext(3,0,Customer_Name.recordset[0][0],0);		
		lk_getkey();
	}
	else Customer_Details(Dis_Sel_Contract_No,"D");
return SUCCESS;
}

/******************************************************************/
int Display_Charged_off(char* Dis_Sel_Contract_No)
{
	resultset Customer_Name;

	memset(&Customer_Name,0,sizeof(Customer_Name));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Customer_Name= get_result("SELECT DATE_ACC_CHARGED_OFF FROM ALLOCATION_DETAILS WHERE ALT_ACCOUNT_NO='%s';",Dis_Sel_Contract_No);
	if(Customer_Name.rows>0)
	{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,1,"  Customer Detail",0);
		lk_disphlight(1);
		lk_disptext(2,0,"Amount last pay. :",0);
		lk_disptext(3,0,Customer_Name.recordset[0][0],0);		
		lk_getkey();
	}
	else Customer_Details(Dis_Sel_Contract_No,"D");
return SUCCESS;
}


/******************************************************************/
int Display_Alternate_Add(char* Dis_Sel_Contract_No)
{	
	MENU_T menu;
        int opt=0;
        int selItem  = 0;
        int acceptKbdEvents;
        int maxEntries = 0,i;
        short scroll = 0;
	char Ma_Address_Cust_Name[200],Ma_Address_display[200];
	int lenAddress=0,lendisplay=0,Mail_key;
	resultset Ma_Address_No;
	
	/******************Mail_Addrs*********************/
	memset(&Ma_Address_No,0,sizeof(Ma_Address_No));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data	
	Ma_Address_No = get_result("SELECT ADDRESS_L1,ADDRESS_L2,ADDRESS_L3,ADDRESS_L4,ADDRESS_ALT_CITY,ADDRESS_ALT_STATE,ADDRESS_ALT_ZIP FROM ALLOCATION_DETAILS WHERE ALT_ACCOUNT_NO='%s';",Dis_Sel_Contract_No);

	memset(Ma_Address_Cust_Name,0,sizeof(Ma_Address_Cust_Name));
	sprintf(Ma_Address_Cust_Name,"%s,%s,%s,%s,%s,%s,%s",Ma_Address_No.recordset[0][0],Ma_Address_No.recordset[0][1],Ma_Address_No.recordset[0][2],Ma_Address_No.recordset[0][3],Ma_Address_No.recordset[0][4],Ma_Address_No.recordset[0][5],Ma_Address_No.recordset[0][6]);	
	
	lenAddress = strlen(Ma_Address_Cust_Name);
	printf("\nlenAddress:%d",lenAddress);
	lendisplay = lenAddress/20;
	printf("\nlendisplay:%d",lendisplay);		

	if(Ma_Address_No.rows>0)
	{
		lk_dispclr(); 
       		menu.start                      =       0;
       		menu.maxEntries                 =       lendisplay+1;
        	maxEntries                      =       menu.maxEntries;
		    
	 	strcpy(menu.title,"Alternate Addrs Detail"); 
		
		for(i=0; i<lendisplay;i++)
		{
		memset(Ma_Address_display,0,sizeof(Ma_Address_display));		
		sprintf(Ma_Address_display,"%s",Ma_Address_Cust_Name);					
		strcpy(menu.menu[0],Ma_Address_display);
		}
		
		while(1)
		{	
		      opt = scroll_menu(&menu,&selItem,acceptKbdEvents);
			switch(opt)
			{				
			    case ENTER: printf("sel item = %d\n",selItem);					
					Customer_Details(Dis_Sel_Contract_No,"D");				
			    default: Customer_Details(Dis_Sel_Contract_No,"D");	
			}
		}		
	}
	else
	{
Mail_Addrs_return:
		lk_dispclr();
	    	lk_disptext(2,0," Alternate Address does ",0);
	    	lk_disptext(3,0,"    not exists",0);
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Mail_key = lk_getkey();
		if(Mail_key==ENTER)
		{		
		Customer_Details(Dis_Sel_Contract_No,"D");
		}
		else goto Mail_Addrs_return;
	}	


return SUCCESS;
}



/******************************************************************/
int Display_Residence_Add(char* Dis_Sel_Contract_No)
{
	MENU_T menu;
        int opt=0;
        int selItem  = 0;
        int acceptKbdEvents;
        int maxEntries = 0,i;
        short scroll = 0;
	char Ma_Address_Cust_Name[200],Ma_Address_display[200];
	int lenAddress=0,lendisplay=0,Mail_key;
	resultset Ma_Address_No;
	
	/******************Mail_Addrs*********************/
	memset(&Ma_Address_No,0,sizeof(Ma_Address_No));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data	
	Ma_Address_No = get_result("SELECT ADDRESS_M1,ADDRESS_M2,ADDRESS_M3,ADDRESS_M4,M_ADDRESS_CITY,M_ADDRESS_STATE,M_ADDRESS_ZIP,M_ADDRESS_LM FROM ALLOCATION_DETAILS WHERE ALT_ACCOUNT_NO='%s';",Dis_Sel_Contract_No);

	memset(Ma_Address_Cust_Name,0,sizeof(Ma_Address_Cust_Name));
	sprintf(Ma_Address_Cust_Name,"%s,%s,%s,%s,%s,%s,%s,%s",Ma_Address_No.recordset[0][0],Ma_Address_No.recordset[0][1],Ma_Address_No.recordset[0][2],Ma_Address_No.recordset[0][3],Ma_Address_No.recordset[0][4],Ma_Address_No.recordset[0][5],Ma_Address_No.recordset[0][6],Ma_Address_No.recordset[0][7]);

	
	lenAddress = strlen(Ma_Address_Cust_Name);
	printf("\nlenAddress:%d",lenAddress);
	lendisplay = lenAddress/20;
	printf("\nlendisplay:%d",lendisplay);		

	if(Ma_Address_No.rows>0)
	{
		lk_dispclr(); 
       		menu.start                      =       0;
       		menu.maxEntries                 =       lendisplay+1;
        	maxEntries                      =       menu.maxEntries;
		    
	 	strcpy(menu.title,"Residence Addrs Detail"); 
		
		for(i=0; i<lendisplay;i++)
		{
		memset(Ma_Address_display,0,sizeof(Ma_Address_display));		
		sprintf(Ma_Address_display,"%s",Ma_Address_Cust_Name);					
		strcpy(menu.menu[0],Ma_Address_display);
		}
		
		while(1)
		{	
		      opt = scroll_menu(&menu,&selItem,acceptKbdEvents);
			switch(opt)
			{				
			    case ENTER: printf("sel item = %d\n",selItem);
					Customer_Details(Dis_Sel_Contract_No,"D");				
			    default: Customer_Details(Dis_Sel_Contract_No,"D");	
			}
		}		
	}
	else
	{
Mail_Addrs_return:
		lk_dispclr();
	    	lk_disptext(2,0," Residence Address does ",0);
	    	lk_disptext(3,0,"    not exists",0);
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Mail_key = lk_getkey();
		if(Mail_key==ENTER)
		{		
		Customer_Details(Dis_Sel_Contract_No,"D");
		}
		else goto Mail_Addrs_return;
	}	
	

return SUCCESS;
}

/******************************************************************/
int Display_Permanent_Add(char* Dis_Sel_Contract_No)
{
	MENU_T menu;
        int opt=0;
        int selItem  = 0;
        int acceptKbdEvents;
        int maxEntries = 0,i;
        short scroll = 0;
	char Ma_Address_Cust_Name[200],Ma_Address_display[200];
	int lenAddress=0,lendisplay=0,Mail_key;
	resultset Ma_Address_No;
	
	/******************Mail_Addrs*********************/
	memset(&Ma_Address_No,0,sizeof(Ma_Address_No));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data	
	Ma_Address_No = get_result("SELECT ADDRESS_P1,ADDRESS_P2,ADDRESS_P3,ADDRESS_P_CITY,ADDRESS_P_ZIP,ADDRESS_P_LMARK,ADDRESS_P_TELPNO FROM ALLOCATION_DETAILS WHERE ALT_ACCOUNT_NO='%s';",Dis_Sel_Contract_No);

	memset(Ma_Address_Cust_Name,0,sizeof(Ma_Address_Cust_Name));
	sprintf(Ma_Address_Cust_Name,"%s,%s,%s,%s,%s,%s,%s",Ma_Address_No.recordset[0][0],Ma_Address_No.recordset[0][1],Ma_Address_No.recordset[0][2],Ma_Address_No.recordset[0][3],Ma_Address_No.recordset[0][4],Ma_Address_No.recordset[0][5],Ma_Address_No.recordset[0][6]);  

//	printf("\nMa_Address_Cust_Name%s",Ma_Address_Cust_Name);
	
	lenAddress = strlen(Ma_Address_Cust_Name);
	printf("\nlenAddress:%d",lenAddress);
	lendisplay = lenAddress/20;
	printf("\nlendisplay:%d",lendisplay);		

	if(Ma_Address_No.rows>0)
	{
		lk_dispclr(); 
       		menu.start                      =       0;
       		menu.maxEntries                 =       lendisplay+1;
        	maxEntries                      =       menu.maxEntries;
		    
	 	strcpy(menu.title,"Permanent Addrs Detail"); 
		
		for(i=0; i<lendisplay;i++)
		{
		memset(Ma_Address_display,0,sizeof(Ma_Address_display));		
		sprintf(Ma_Address_display,"%s",Ma_Address_Cust_Name);					
		strcpy(menu.menu[0],Ma_Address_display);
		}
		
		while(1)
		{	
		      opt = scroll_menu(&menu,&selItem,acceptKbdEvents);
			switch(opt)
			{				
			    case ENTER: printf("sel item = %d\n",selItem);
					Customer_Details(Dis_Sel_Contract_No,"D");				
			    default: Customer_Details(Dis_Sel_Contract_No,"D");	
			}
		}		
	}
	else
	{
Mail_Addrs_return:
		lk_dispclr();
	    	lk_disptext(2,0," Permanent Address does ",0);
	    	lk_disptext(3,0,"    not exists",0);
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Mail_key = lk_getkey();
		if(Mail_key==ENTER)
		{		
		Customer_Details(Dis_Sel_Contract_No,"D");
		}
		else goto Mail_Addrs_return;
	}	

return SUCCESS;
}


/******************************************************************/
int Display_Office_Add(char* Dis_Sel_Contract_No)
{
	MENU_T menu;
        int opt=0;
        int selItem  = 0;
        int acceptKbdEvents;
        int maxEntries = 0,i;
        short scroll = 0;
	char Off_Addrs_Cust_Name[200],Off_Address_display[200];
	int lenAddress=0,lendisplay=0,Office_key;
	resultset Off_Addrs_No;

	memset(&Off_Addrs_No,0,sizeof(Off_Addrs_No));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Off_Addrs_No= get_result("SELECT EMP_ADDRESS1,EMP_ADDRESS2,EMP_ADDRESS3,EMP_ADDRESS4,EMP_ADDRESS_PIN,EMP_ADDRESS_LM,EMP_ADDRESS_CITY FROM ALLOCATION_DETAILS WHERE ALT_ACCOUNT_NO='%s';",Dis_Sel_Contract_No);

	memset(Off_Addrs_Cust_Name,0,sizeof(Off_Addrs_Cust_Name));
	sprintf(Off_Addrs_Cust_Name,"%s,%s,%s,%s,%s,%s,%s",Off_Addrs_No.recordset[0][0],Off_Addrs_No.recordset[0][1],Off_Addrs_No.recordset[0][2],Off_Addrs_No.recordset[0][3],Off_Addrs_No.recordset[0][4],Off_Addrs_No.recordset[0][5],Off_Addrs_No.recordset[0][6]); 

	lenAddress = strlen(Off_Addrs_Cust_Name);
	printf("\nlenAddress:%d",lenAddress);
	lendisplay = lenAddress/20;
	printf("\nlendisplay:%d",lendisplay);
	
	if(Off_Addrs_No.rows>0)
	{
		lk_dispclr(); 
       		menu.start                      =       0;
       		menu.maxEntries                 =       lendisplay+1;
        	maxEntries                      =       menu.maxEntries;
		    
	 	strcpy(menu.title,"Office Addr. Detail"); 		

		for(i=0; i<lendisplay;i++)
		{
		memset(Off_Address_display,0,sizeof(Off_Address_display));		
		sprintf(Off_Address_display,"%s",Off_Addrs_Cust_Name);					
		strcpy(menu.menu[0],Off_Address_display);
		}

		while(1)
		{	
		      opt = scroll_menu(&menu,&selItem,acceptKbdEvents);
			switch(opt)
			{				
			    case ENTER: Customer_Details(Dis_Sel_Contract_No,"D");
				
			    default: Customer_Details(Dis_Sel_Contract_No,"D");	
			}
		}		
	}
	else
	{
Office_Address_does:
		lk_dispclr();
	    	lk_disptext(2,0,"  Office Address does ",0);
	    	lk_disptext(3,0,"    not exists",0);
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Office_key = lk_getkey();
		if(Office_key==ENTER)
		{
		Customer_Details(Dis_Sel_Contract_No,"D");
		}
		else goto Office_Address_does;
	}	

return SUCCESS;
}
