/*******************************************************************/
//			   HDFC Bank CC
//		          soft_update.c
/*******************************************************************/
#include "header.h"
#include "sqlite3.h"


/******************************************************************/
Soft_Update()
{	
	char HHT_Version_Url[250],HHT_Version_Verify_Url[250],HHT_Version_info_send_File[200],HHT_Version_Verify_File[100],HHT_Version_Verify_File_login[100],HHT_Version_Verify_File_login_remove[100],HHT_Version_Verify_File_chmod[100],HHT_Version_Verify_File_unzip[100],HHT_Version_Customer_Info_File[100],HHT_Version_Info_File_chmod[100],Executive_Customer_Info_File_chmod[100],HHT_Version_Info_unzip[100],New_Version_file[100],MV_Upgrade_File[50];
	unsigned char response_buffer[1000];
	char Downloaded_Software[100];
	char Version_Name[50],New_Version_file_zip[100],Chmod_New_Version_file[100],Unzip_New_Version_file[100],Softname[25],HHT_Version_Info_File_rm[100],HHT_Version_Info_File_copy[100],HHT_Version_Info_File_txt[100];
	char domainname[25],apn[25];
	int i,ret_unzip;	
	char HHT_Version_file_Name[100],HHT_Version_Info_File_unzip[100];
	char HHT_Version_info[100],HHT_Version_info_RSA_b64[500];
	FILE * response_myfile;
	resultset Soft_Version_Diff;
	unsigned int HHT_Version_Info_Valid=0;	
	FILE * pFile;
	FILE *ptr_file;
    	char buf[50],size_download[50],size_download_cmp[50];
	int size;

	
	memset(&Soft_Version_Diff,0,sizeof(Soft_Version_Diff));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Soft_Version_Diff=get_result("Select httpip,loginstatus,hwid,New_Software_Ver,Current_Software_Ver,Localhttpip,SIM_ICCID,domain From operationalstatus;");

	memset(Softname,0,sizeof(Softname));
	memset(domainname,0,sizeof(domainname));		
	sprintf(Softname,"%s",Soft_Version_Diff.recordset[0][7]);	
	for(i=0;i<strlen(Softname);i++)
	{
		if(Softname[i] == '.')	break;
		memset(apn,0,sizeof(apn));
		sprintf(apn,"%c",Softname[i]);		
		strcat(domainname,apn);
		
	}
	system("cd /mnt/jffs2/");
	system("rm /mnt/jffs2/*.zip");
	system("rm /mnt/jffs2/*.IMG");
	system("rm /mnt/jffs2/*.img");
	system("rm -rf /mnt/jffs2/*.hdfc_v");
	system("rm -rf /mnt/jffs2/*.HDFC_vT");
	system("rm -rf /mnt/jffs2/*.HDFC_vR");
	////////////////////////////////////
	system("rm -rf /mnt/jffs2/hdfc_v*");
	system("rm -rf /mnt/jffs2/HDFC_vT*");	
	system("rm -rf /mnt/jffs2/HDFC_vR*");
	system("rm /mnt/jffs2/f_size.txt");		
	memset(HHT_Version_Url,0,sizeof(HHT_Version_Url));
#if URL_TEST	
	sprintf(HHT_Version_Url,"%s/HHTRequest/v%s/CurrentSoftwareVer.aspx",Soft_Version_Diff.recordset[0][0],Soft_Version_Diff.recordset[0][4]);
#endif
#if URL_NETMAGIC	
	sprintf(HHT_Version_Url,"%s/HHTRequest/v%s/CurrentSoftwareVer.aspx",Soft_Version_Diff.recordset[0][0],Soft_Version_Diff.recordset[0][4]);
#endif
	printf("\nHHT_Version_Url:%s",HHT_Version_Url);
	memset(HHT_Version_info,0,sizeof(HHT_Version_info));	
	sprintf(HHT_Version_info,"Validate,%s,%s,%s",Soft_Version_Diff.recordset[0][2],Soft_Version_Diff.recordset[0][3],domainname);
	printf("\nHHT_Version_info:%s",HHT_Version_info);
	printf("\nFinal_HHT_Version_info:%s,%s",HHT_Version_Url,HHT_Version_info);
	memset(HHT_Version_info_RSA_b64,0,sizeof(HHT_Version_info_RSA_b64));
	sprintf(HHT_Version_info_RSA_b64,"%s",RSAEncryption_Base64(HHT_Version_info,strlen(HHT_Version_info)));

	memset(HHT_Version_info_send_File,0,sizeof(HHT_Version_info_send_File));
	sprintf(HHT_Version_info_send_File,"/mnt/jffs2/%s_version.txt",Soft_Version_Diff.recordset[0][3]);
  	pFile = fopen (HHT_Version_info_send_File, "wb");
	fputs (HHT_Version_info_RSA_b64,pFile);
  	fclose (pFile);

	memset(HHT_Version_Customer_Info_File,0,sizeof(HHT_Version_Customer_Info_File));
	sprintf(HHT_Version_Customer_Info_File,"/mnt/jffs2/HDFC_vT_%s_%s.zip",domainname,Soft_Version_Diff.recordset[0][3]);
	
	printf("\nHHT_Version_Customer_Info_File%s:",HHT_Version_Customer_Info_File);
	HHT_Version_Info_Valid=curl_HHT_Version_Date_Request(HHT_Version_Url,HHT_Version_info_send_File,HHT_Version_Customer_Info_File);
		
	remove(HHT_Version_info_send_File);
	if(HHT_Version_Info_Valid==1)
	{
		response_myfile = fopen(HHT_Version_Customer_Info_File,"r");
		memset(response_buffer,0,1000);
		while (!feof(response_myfile))
		{
			fgets(response_buffer,50,response_myfile);
  		}
		fclose(response_myfile);
		if(strcmp(response_buffer,"Success")==0)
		{
			remove(HHT_Version_Customer_Info_File);
			return 0;
		}
		if(strcmp(response_buffer,"ErrorCode:0001")==0)
		{
			lk_dispclr();
			lk_disptext(3,0,"    Bad Request ",0);
			sleep(2);
			remove(HHT_Version_Customer_Info_File);
			After_main();
		}
		else if(strcmp(response_buffer,"ErrorCode:0002")==0)
		{
			lk_dispclr();
			lk_disptext(2,0,"     Please Enter",0);
			lk_disptext(4,0,"  Valid ExecutiveID",0);
			sleep(2);
			remove(HHT_Version_Customer_Info_File);
			After_main();
				
		}
		else if(strcmp(response_buffer,"ErrorCode:0004")==0)
		{
			lk_dispclr();
			lk_disptext(2,0,"  Error in Processing",0);
			lk_disptext(4,0,"      Request",0);
			sleep(2);
			remove(HHT_Version_Customer_Info_File);
			After_main();
		}
		else if(strcmp(response_buffer,"ErrorCode:0008")==0)
		{
			lk_dispclr();
			lk_disptext(2,0,"   Contracts Not",0);
			lk_disptext(3,0,"Allocated,Pls Contact",0);
			lk_disptext(4,0,"   Your Agency",0);
			sleep(2);
			remove(HHT_Version_Customer_Info_File);
			After_main();
		}
		else if(strcmp(response_buffer,"<h1>Bad Request (Invalid Hostname)</h1>")==0)
		{
			remove(HHT_Version_Customer_Info_File);
			After_main();
		}
		else if(strcmp(response_buffer,"<h1>Service Unavailable</h1>")==0)
		{
			remove(HHT_Version_Customer_Info_File);
			After_main();
		}
		else
		{
		memset(HHT_Version_Info_File_chmod,0,sizeof(HHT_Version_Info_File_chmod));
		sprintf(HHT_Version_Info_File_chmod,"chmod 777 %s",HHT_Version_Customer_Info_File);
		system(Executive_Customer_Info_File_chmod);
		system("cd /mnt/jffs2/");
		memset(HHT_Version_Info_unzip,0,sizeof(HHT_Version_Info_File_unzip));
		sprintf(HHT_Version_Info_unzip,"unzip %s",HHT_Version_Customer_Info_File);
		ret_unzip=system(HHT_Version_Info_unzip);
		if(ret_unzip==0) printf("\n success unzip"); 
		else
		{
		lk_dispclr();
	   	lk_disptext (1,1,"   Version Not ",0) ;
		lk_disptext (2,1,"    Downloaded",0) ;
		lk_disptext (3,1,"   Successfully",0) ;
	   	lk_disptext (4,1,"    Try Again",0) ;
		sleep(5);		
		system("cd /mnt/jffs2/");
		system("rm /mnt/jffs2/*.zip");
		system("rm /mnt/jffs2/*.IMG");
		system("rm /mnt/jffs2/*.img");
		system("rm -rf /mnt/jffs2/*.hdfc_v");
		system("rm -rf /mnt/jffs2/*.HDFC_vT");
		system("rm -rf /mnt/jffs2/*.HDFC_vR");
		////////////////////////////////////
		system("rm -rf /mnt/jffs2/hdfc_v*");
		system("rm -rf /mnt/jffs2/HDFC_vT*");	
		system("rm -rf /mnt/jffs2/HDFC_vR*");
		sleep(5);
		After_main();
		}	
		system("cd /mnt/jffs2/");
		memset(HHT_Version_Info_File_copy,0,sizeof(HHT_Version_Info_File_copy));
		sprintf(HHT_Version_Info_File_copy,"cp /mnt/jffs2/HDFC_vT_%s_%s/HDFC_vT_%s_%s.IMG /mnt/jffs2/",domainname,Soft_Version_Diff.recordset[0][3],domainname,Soft_Version_Diff.recordset[0][3]);
		system(HHT_Version_Info_File_copy);

		system("cd /mnt/jffs2/");
		memset(HHT_Version_Info_File_txt,0,sizeof(HHT_Version_Info_File_txt));
		sprintf(HHT_Version_Info_File_txt,"cp /mnt/jffs2/HDFC_vT_%s_%s/f_size.txt /mnt/jffs2/",domainname,Soft_Version_Diff.recordset[0][3]);
		system(HHT_Version_Info_File_txt);
		
		system("cd /mnt/jffs2/");
		memset(HHT_Version_Info_File_rm,0,sizeof(HHT_Version_Info_File_rm));
	sprintf(HHT_Version_Info_File_rm,"rm -rf /mnt/jffs2/HDFC_vT_%s_%s",domainname,Soft_Version_Diff.recordset[0][3]);
		system(HHT_Version_Info_File_rm);
		system("cd /mnt/jffs2/");
		
		memset(New_Version_file,0,sizeof(New_Version_file));
	        sprintf(New_Version_file,"HDFC_vT_%s_%s.IMG",domainname,Soft_Version_Diff.recordset[0][3]);

		memset(Downloaded_Software,0,100);
		sprintf(Downloaded_Software,"/mnt/jffs2/%s",New_Version_file);

		printf("\nNew_Version_file:%s",New_Version_file);
		printf("\nDownloaded_Software:%s",Downloaded_Software);
	
		if(stat(Downloaded_Software,&st)==0)
		{
		size = st.st_size;
		printf("\nsize_t %d",size);
		memset(size_download,0,sizeof(size_download));
		sprintf(size_download,"%d",size);
		printf("\nsize_download %s",size_download);
		memset(size_download_cmp,0,sizeof(size_download_cmp));
		sprintf(size_download_cmp,"%c%c%c%c%c%c",size_download[0],size_download[1],size_download[2],size_download[3],size_download[4],size_download[5]);	
		printf("\nsize_download_cmp : %s",size_download_cmp);	
		ptr_file =fopen("/mnt/jffs2/f_size.txt","r");
		if (!ptr_file)
		return 1;
		   
		while (fgets(buf,50, ptr_file)!=NULL)     		
		printf("%s",buf);
		printf("\nBuf-Check:%s",buf);
		printf("\nsize_download-Check %s",size_download);
		
		if(strncmp(buf,size_download,6)==0)		 
		{
		printf("\nNew_Version_file:%s",New_Version_file);
		memset(Version_Name,0,sizeof(Version_Name));
		strcpy(Version_Name,New_Version_file);
		system("mv /mnt/jffs2/app_new.img /mnt/jffs2/app_old.img");//This will work 2nd time
		memset(MV_Upgrade_File,0,50);
		sprintf(MV_Upgrade_File,"mv /mnt/jffs2/%s /mnt/jffs2/app_new.img",New_Version_file);
		system(MV_Upgrade_File);
		system("cp /mnt/jffs2/app_new.img /mnt/tmp/app.img");
	 	sleep(2);
		lk_dispclr() ;
		lk_disptext (2,0,"Fusing Application to Flash...",0);
		system ("/home/downloadapplication");
		lk_dispclr() ;
		printf("\nDownload Completed\n");
		lk_disptext(2,1,"Download Completed",0) ;
		lk_disptext(4,1," Upgrade Completed",0) ;
		sleep(2);
		Version_SIMNo_Request();
		write_file();
		write_file_date();
		system("rm /mnt/jffs2/*.zip");
		system("rm /mnt/jffs2/*.db");
		system("rm /mnt/jffs2/f_size.txt");
		system("rm /mnt/jffs2/*.cer");
		//system("rm /mnt/jffs2/core*");
		//////////////////////////////////////////
		lk_dispclr();
		lk_disptext (2,2," Shutting Down",1) ;
		lk_disptext (4,2,"  Start Again",1) ;
		ppp_close();
		close_sqlite();	
		lk_close();
		system("cd");
		system("poweroff");		
		sleep(20);
		}		
		else
		{
		printf("\nworking3-size_download\n");
		lk_dispclr() ;
	   	lk_disptext (1,1,"   Software Not ",0) ;
		lk_disptext (2,1,"    Downloaded",0) ;
		lk_disptext (3,1,"   Successfully",0) ;
	   	lk_disptext (4,1,"    Try Again",0) ;		
		system("rm /mnt/jffs2/*.zip");
		system("rm /mnt/jffs2/*.img");
		system("rm /mnt/jffs2/*.IMG");
		system("rm /mnt/jffs2/f_size.txt");
		sleep(5);
		After_main();		
		}
		}
		else
		{
		lk_dispclr() ;
	   	lk_disptext (1,1,"   Software Not ",0) ;
		lk_disptext (2,1,"    Downloaded",0) ;
		lk_disptext (3,1,"   Successfully",0) ;
	   	lk_disptext (4,1,"    Try Again",0) ;
		system("rm /mnt/jffs2/*.zip");
		system("rm /mnt/jffs2/*.img");
		system("rm /mnt/jffs2/*.IMG");
		system("rm /mnt/jffs2/f_size.txt");
		sleep(4);
		After_main();		
		}
	        }
	}
	else if(HHT_Version_Info_Valid==0)
	{
		remove(HHT_Version_info_send_File);
		Soft_Update();
	}
	else
	{
		lk_dispclr();
		lk_disptext(2,0,"    Please Try",0);
		lk_disptext(4,0,"      Again",0);
		sleep(2);
		remove(HHT_Version_info_send_File);
		After_main();			
	}
		
}



/******************************************************************/
int Download_Certificate()
{
	char certificate_expiry_date[20];
	char CDay[5],CMonth[5],CYear[5];	
	char *expiry=NULL;
	char bufExpiryStr[12];

	chk_rtc();
	if(stat(PUBLIC_CERTIFICATE,&st)==0)
	{	
	printf("Download_Certificate_check\n");
	memset(certificate_expiry_date,0,sizeof(certificate_expiry_date));	
	memset(bufExpiryStr,0,sizeof(bufExpiryStr));
	sprintf(certificate_expiry_date,"%s",parse_expiry_data(get_expiry_date(expiry), bufExpiryStr));
	printf("certificate_expiry_date:%s\n",certificate_expiry_date);
	sprintf(CDay,"%c%c",certificate_expiry_date[0],certificate_expiry_date[1]);
	sprintf(CMonth,"%c%c",certificate_expiry_date[2],certificate_expiry_date[3]);		
	sprintf(CYear,"%c%c",certificate_expiry_date[6],certificate_expiry_date[7]);
	printf("\nCDay,CMonth,CYear:%s,%s,%s",CDay,CMonth,CYear);	
	if((atoi(CYear)==(curt.tm_year-100))&&(atoi(CMonth)==(curt.tm_mon+1))&&((curt.tm_mday)>atoi(CDay)))	
		remove(PUBLIC_CERTIFICATE);
	}
	if(stat(PUBLIC_CERTIFICATE,&st)!=0)
	{
		lk_dispclr();        
		lk_disptext(3,3,"Downloading...",1);		
		Download_Certificate_check();		
		if(stat(PUBLIC_CERTIFICATE,&st)!=0) Download_Certificate_check();
		Download_Certificate(); 		
	}
	return 0;
}

/******************************************************************/

/******************************************************************/
Read_file_tran()
{
	char Tran_Domain_File_buffer[30],Tran_Domain_File_buffer_final[3][20];
	char unread_respstring[2],unread_respstring1[15];
	int unread_charfind,unread_charfind1,unread_charfind2=-1,unread_respcount=0;
	FILE * Tran_Domain_response_myfile;
	resultset apn_transno;
	char New_Software_Ver[80];//,New_Software_Ver_txt[80];

	Tran_Domain_response_myfile = fopen("/var/log/apn_trans.txt","r");
	memset(Tran_Domain_File_buffer,0,sizeof(Tran_Domain_File_buffer));
	while (!feof(Tran_Domain_response_myfile))
	{
		fgets(Tran_Domain_File_buffer,30,Tran_Domain_response_myfile);

        }
	fclose(Tran_Domain_response_myfile);
///////////////////
	memset(Tran_Domain_File_buffer_final,0,sizeof(Tran_Domain_File_buffer_final));
	if(strlen(Tran_Domain_File_buffer)>0)
	{

		for(unread_charfind=0;unread_charfind<=strlen(Tran_Domain_File_buffer);unread_charfind++)
		{
			if(Tran_Domain_File_buffer[unread_charfind]==35) 
			{
			memset(unread_respstring1,0,sizeof(unread_respstring1));
			for(unread_charfind1=unread_charfind2+1;unread_charfind1<=unread_charfind-1;unread_charfind1++)
			{
				memset(unread_respstring,0,sizeof(unread_respstring));
				sprintf(unread_respstring,"%c",Tran_Domain_File_buffer[unread_charfind1]);
				strcat(unread_respstring1,unread_respstring);
			}
			//printf("\nRespose string:%s",respstring1);
			sprintf(Tran_Domain_File_buffer_final[unread_respcount],"%s",unread_respstring1);
			printf("\nRespose string:%s",unread_respstring1);
			unread_charfind2=unread_charfind;
			unread_respcount++;
			}
		}
	}
////////////////////////
	if(atoi(Tran_Domain_File_buffer_final[1])>0&&atoi(Tran_Domain_File_buffer_final[1])<10000&&strlen(Tran_Domain_File_buffer_final[2])>2)	
	{
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	execute("UPDATE operationalstatus SET last_transaction_no='%s',domain='%s';",Tran_Domain_File_buffer_final[1],Tran_Domain_File_buffer_final[2]);
	}

	//remove("/var/log/apn_trans.txt");
}

/******************************************************************/

/******************************************************************/
Read_file_date_tran()
{
	char Tran_Domain_File_buffer[30],Tran_Domain_File_buffer_final[10][20];
	char unread_respstring[2],unread_respstring1[15],datestring[20];
	int unread_charfind,unread_charfind1,unread_charfind2=-1,unread_respcount=0;
	FILE * Tran_Domain_response_myfile;
	resultset apn_transno;
	char New_Software_Ver[80];//,New_Software_Ver_txt[80];


	Tran_Domain_response_myfile = fopen("/var/log/trans_date.txt","r");
	memset(Tran_Domain_File_buffer,0,sizeof(Tran_Domain_File_buffer));
	while (!feof(Tran_Domain_response_myfile))
	{
		fgets(Tran_Domain_File_buffer,30,Tran_Domain_response_myfile);

        }
	fclose(Tran_Domain_response_myfile);
///////////////////
	memset(Tran_Domain_File_buffer_final,0,sizeof(Tran_Domain_File_buffer_final));
	if(strlen(Tran_Domain_File_buffer)>0)
	{

		for(unread_charfind=0;unread_charfind<=strlen(Tran_Domain_File_buffer);unread_charfind++)
		{
			if(Tran_Domain_File_buffer[unread_charfind]==35) 
			{
			memset(unread_respstring1,0,sizeof(unread_respstring1));
			for(unread_charfind1=unread_charfind2+1;unread_charfind1<=unread_charfind-1;unread_charfind1++)
			{
				memset(unread_respstring,0,sizeof(unread_respstring));
				sprintf(unread_respstring,"%c",Tran_Domain_File_buffer[unread_charfind1]);
				strcat(unread_respstring1,unread_respstring);
			}
			//printf("\nRespose string:%s",respstring1);
			sprintf(Tran_Domain_File_buffer_final[unread_respcount],"%s",unread_respstring1);
			printf("\nRespose string:%s",unread_respstring1);
			unread_charfind2=unread_charfind;
			unread_respcount++;
			}
		}
		memset(datestring,0,sizeof(datestring));
		sprintf(datestring,"%s",unread_respstring1);
		printf("\ndatestring:%s\n",datestring);
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		execute("UPDATE operationalstatus SET Previous_date='%s';",datestring);
	}

	//remove("/var/log/trans_date.txt");
}


/******************************************************************/

/******************************************************************/
write_file_date()
{
	FILE *p = NULL;
	  char datastring[80];//,New_Software_Ver[50];
	  size_t len = 0;
	  resultset date_transno;

	  remove("/var/log/trans_date.txt");
	  close_sqlite();			
	  open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	  date_transno=get_result("SELECT Previous_date FROM operationalstatus");	
	  memset(datastring,0,sizeof(datastring));
	  sprintf(datastring,"#%s#\n",date_transno.recordset[0][0]);
	  p = fopen("/var/log/trans_date.txt", "wb+");
	  if (p== NULL) {
	  printf("Error in opening a file..");
	  }
	  len = strlen(datastring);
	  fwrite(datastring, len, 1, p);
	  fclose(p);
}

/******************************************************************/

/******************************************************************/
write_file()
{
	 FILE *p = NULL;
	  char datastring[80];//,New_Software_Ver[50];
	  size_t len = 0;
	  resultset apn_transno;

	  remove("/var/log/apn_trans.txt");			
	  close_sqlite();			
	  open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	  apn_transno=get_result("SELECT last_transaction_no,domain,New_Software_Ver FROM operationalstatus");	
	  memset(datastring,0,sizeof(datastring));
	  sprintf(datastring,"#%ld#%s#\n",atol(apn_transno.recordset[0][0]),apn_transno.recordset[0][1]);
//	  memset(New_Software_Ver,0,sizeof(New_Software_Ver));
//	  sprintf(New_Software_Ver,"/mnt/jffs2/apn_trans.txt",apn_transno.recordset[0][2]);
	  p = fopen("/var/log/apn_trans.txt", "wb+");
	  if (p== NULL) {
	  printf("Error in opening a file..");
	  }
	  len = strlen(datastring);
	  fwrite(datastring, len, 1, p);
	  fclose(p);
}

