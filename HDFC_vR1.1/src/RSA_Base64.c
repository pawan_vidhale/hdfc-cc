/*******************************************************************/
//			    HDFC
//		        RSA_Base64.c
/*******************************************************************/
/**
 *
 * @author FTL003
**/
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "header.h"
/*******************************************************************/

/*******************************************************************/
char *base64(const unsigned char *input, int length)
{
  BIO *bmem, *b64;
  BUF_MEM *bptr;

  b64 = BIO_new(BIO_f_base64());
  bmem = BIO_new(BIO_s_mem());
  b64 = BIO_push(b64, bmem);
  BIO_write(b64, input, length);
  BIO_flush(b64);
  BIO_get_mem_ptr(b64, &bptr);

  char *buff = (char *)malloc(bptr->length);
  memcpy(buff, bptr->data, bptr->length-1);
  buff[bptr->length-1] = 0;

  BIO_free_all(b64);
  return buff;
}
/******************************************************************
    			Assign RSA Key
******************************************************************/
int assign_key_rsa(RSA *rsa, unsigned char *key, int n_len, unsigned char *e_key, int e_len)
{
        rsa->n = BN_bin2bn(key, n_len, rsa->n);
        rsa->e = BN_bin2bn(e_key, e_len, rsa->e);
        return 0;
}
/*****************************************************************
    			RSA Encryption
*****************************************************************/
int rsa_encryption(unsigned char *in, int inlen, unsigned char *outbuf,int *outlen)
{
    unsigned char key[400]={0};
    unsigned char *pubKey;
    X509 *x;
    EVP_PKEY *epkey;
    int len;
    FILE *fp;
	BIO *bio, *b64;
    ERR_load_crypto_strings();

    RSA *rsa = RSA_new();
//    fp = fopen("/mnt/jffs2/public.cer","r");
    fp = fopen(PUBLIC_CERTIFICATE,"r");
    if(fp == NULL)
        printf(" NO public Certificate found\n\n");
    x = PEM_read_X509(fp,NULL,0,NULL); // Read PEM Certificate from FILE
    fclose(fp);

    epkey = X509_get_pubkey(x);
    int bitSize = EVP_PKEY_size(epkey); // Bit Size
    BIGNUM *publickey = epkey->pkey.rsa->n; // modulus
    BIGNUM *exp = epkey->pkey.rsa->e; // exponential key
    pubKey = (unsigned char *)malloc(sizeof(unsigned char) * bitSize);
    unsigned char *eKey = (unsigned char *)malloc(sizeof(unsigned char)*100);

    int n_len = BN_bn2bin(publickey,pubKey); // convert it
    int e_len = BN_bn2bin(exp,eKey); // convert it

    memcpy(key,pubKey,n_len);
    assign_key_rsa(rsa, key,n_len,eKey,e_len);
    if (!EVP_PKEY_assign_RSA(epkey,rsa)) {
        printf("key assign error\n");
        return -1;
        }
//    printf("\nIn:%s",in); printf("\nInlen:%d\n",inlen);
    memset(outbuf,0,sizeof(outbuf));
    len = RSA_public_encrypt(inlen, in, outbuf, epkey->pkey.rsa,RSA_PKCS1_PADDING); //RSA_NO_PADDING
    *outlen=0;
    *outlen = len;
    free(pubKey);
    EVP_PKEY_free(epkey);
    X509_free(x);
    return 0;
}
/********************************************************************/

/********************************************************************/
char *RSAEncryption_Base64(char *pass_msg_for_rsa,long int msg_length)
{
	unsigned char OutRSA[512];    int OutRSALen=0;
	if (rsa_encryption(pass_msg_for_rsa,msg_length,OutRSA, &OutRSALen) != 0) {
            printf ("RSA encrypt failed\n");
      	}
	printf("Rlen:%d\n",OutRSALen);
	return base64(OutRSA, OutRSALen);
}

/*******************************************************************
	 Retrieve Expiry Date from Certificate
********************************************************************/
char * get_expiry_date(char *expiryStr )
{
	X509 *x;
	unsigned char *not;
	int n=0;
	BIO *out;
	FILE *fp=fopen(PUBLIC_CERTIFICATE, "r");

	x = X509_new();
	x = PEM_read_X509(fp,NULL,NULL,NULL);
	fclose(fp);

	out = BIO_new(BIO_s_mem());
	ASN1_TIME_print(out, X509_get_notAfter(x));
	n = BIO_get_mem_data(out, &not);
	expiryStr = (char *) malloc (n+1);
	expiryStr[n] = '\0';
	memcpy(expiryStr, not, n);
	BIO_free(out);
	
	X509_free(x);
	printf("expiryStr:%s\n",expiryStr);
	return(expiryStr);
}
/*******************************************************************/

/*******************************************************************/
char * parse_expiry_data(char *timestamp, char *expiry)
{
	
	char *strTimestamp=NULL;
	int n, k, mon, year, date;
	char m[][4] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

	strTimestamp = strtok(timestamp," ");
	if (strTimestamp != NULL) {
		int j;

		for (j=0; j < 12; j++) {
			if (strcmp(m[j],strTimestamp) == 0) {
				mon = j+1;
				break;
			}
		}
	}
	strTimestamp = strtok(NULL," ");
	if (strTimestamp != NULL)
		date = atoi(strTimestamp);
	strTimestamp = strtok(NULL," ");
	strTimestamp = strtok(NULL," ");
	if (strTimestamp != NULL)
		year = atoi(strTimestamp);
        //sprintf(expiry, "%d%02d%02d", year, mon, date);
	sprintf(expiry, "%02d%02d%d", date, mon, year);
	printf("parse_expiry_data: value is :%s:\n", expiry);
	return expiry;
}
/********************************************************************/

/********************************************************************/
char *Certificate_expiry_data()
{
	char *expiry=NULL;
	char bufExpiryStr[12];
	//expiry = get_expiry_date(expiry,RSA_CER_file_Path);

	expiry = get_expiry_date(expiry);
	printf("expiry:%s\n",expiry);
	//printf("date:%s\n",parse_expiry_data(expiry, bufExpiryStr));
	return parse_expiry_data(expiry, bufExpiryStr);
}
/*******************************************************************/

/*******************************************************************/

void encrypt(char Encrypt_password[],int key)
{
    unsigned int i;
    for(i=0;i<strlen(Encrypt_password);++i)
    {
        Encrypt_password[i] = Encrypt_password[i] - key;
    }
}
 
void decrypt(char Decrypt_password[],int key)
{
    unsigned int i;
    for(i=0;i<strlen(Decrypt_password);++i)
    {
        Decrypt_password[i] = Decrypt_password[i] + key;
    }
}


char *Encrypted_sqlite_DB(char* Encrypt_password)
{
#if PRINTF
    printf("Passwrod     = %s\n",Encrypt_password);
#endif
    encrypt(Encrypt_password,0xFACA);   
    //printf("Encrypted value = %s\n",Encrypt_password);
    return Encrypt_password;
}


char *Decrypted_sqlite_DB(char* Decrypt_password)
{
#if PRINTF
    printf("Passwrod     = %s\n",Decrypt_password);
#endif
    decrypt(Decrypt_password,0xFACA);   
   //printf("Decrypted value = %s\n",Decrypt_password);
    return Decrypt_password;	
}



