/*******************************************************************/
//			  HDFC Bank CC
//		      customer_info_sync.c
/*******************************************************************/
/**
 *
 * 
**/
#include <header.h>
#include <stdio.h>
#include <stdlib.h>
#include "Pickup.pb-c.h"
#define MAX_MSG_SIZE  4194304	


/******************************************************************/
int Pickup_Info_Protobuff_Parser(char* Executive_ID)
{
	int len,download_key;
	FILE *ifp;
	char ch;
	int count=0;
	//uint8_t data_buff[MAX_MSG_SIZE];
	uint8_t *data_buff;
	int i,j,k;
	char Exec_ID_Info_File_Name[1000],Exec_ID_Info_File_Name_txt[500],Pickup_Info_file_Name_zip[500];
	resultset Disp_appl;	

	data_buff = (uint8_t *) malloc(MAX_MSG_SIZE);
	memset(data_buff,0,sizeof(data_buff));

	printf("\nworking-1");

	lk_dispclr();
	lk_disptext(2,3,"Pickup Info",0);
	lk_disptext(3,3,"Synchronizing",0);
        lk_disptext(4,3,"Please wait....",0);

	printf("\nworking-1");

	close_sqlite();
	//create_customerdatabase();
	memset(Pickup_Info_file_Name_zip,0,sizeof(Pickup_Info_file_Name_zip));
	sprintf(Pickup_Info_file_Name_zip,"/mnt/jffs2/%s_pickup.zip",Executive_ID);
	memset(Exec_ID_Info_File_Name_txt,0,sizeof(Exec_ID_Info_File_Name_txt));
	sprintf(Exec_ID_Info_File_Name_txt,"/mnt/jffs2/%s_pickup.txt",Executive_ID);
	memset(Exec_ID_Info_File_Name,0,sizeof(Exec_ID_Info_File_Name));
	sprintf(Exec_ID_Info_File_Name,"/mnt/jffs2/%s_pickup.pb",Executive_ID);
	printf("\nworking-1");
	if(stat(Exec_ID_Info_File_Name,&st)==0)
	{
	ifp = fopen(Exec_ID_Info_File_Name, "rb");
	if (ifp == NULL) {
	  fprintf(stderr, "Can't open input file in.list!\n");
	  return 0;
	}
	if(ifp != NULL)
	{
	printf("Successfully open file\n");
	size_t cur_len = 0, nread =0 ;
//	if(nread = fread(data_buff,1,NUMELEM,fd))
	    while ((nread=fread(data_buff,1, MAX_MSG_SIZE - cur_len, ifp)) != 0)
	    {
	      cur_len += nread;
	      if (cur_len == MAX_MSG_SIZE)
		{
		  fprintf(stderr, "max message length exceeded\n");
		  return 0;
		}
	    }
	printf("legnth:%d\n",cur_len);
	data_buff[cur_len]='\0';

	PICKUP__PICKUPDETAIL *unpacked;
	PICKUP__Entry *entry_unpacked;

	unpacked = pickup__pickupdetail__unpack(NULL, cur_len, data_buff);// Unpack allocation
	if (unpacked == NULL)      
	{
Unable_download:
		lk_dispclr();
		lk_disptext(2,2,"Unable to download",0);
		lk_disptext(3,2,"Pickup Info",0);
		lk_disptext(5,0,"Press Enter to Cont.",0);		
		fprintf(stderr, "error unpacking alloc incoming message\n");
		download_key = lk_getkey();
		if(download_key==ENTER)
		{
		return 0;
		}
		else goto Unable_download;		
	}
	else
	{
		
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		Disp_appl=get_result("SELECT localhttpip From operationalstatus;");
		if(strcmp(Disp_appl.recordset[0][0],"NDRA")==0)
		{	
		system("rm /mnt/jffs2/hdfccc.db");
		}
		close_sqlite();
		create_customerdatabase();
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		printf("No. of entry:%d\n",unpacked->n_entry);
		if(unpacked->n_entry==0) goto Unable_download; 
		for(i=0;i<(unpacked->n_entry);i++)
		{
#if PRINTF
			printf("\nCustumer Details:%d\n",i);
#endif
			entry_unpacked = pickup__entry__unpack(NULL, cur_len, data_buff);// unpack entry
			if (entry_unpacked == NULL)      fprintf(stderr, "error unpacking entry incoming message\n");
			execute("INSERT INTO PICKUP_DETAILS(CARD_NUMBER,PICKUP_ID,PICKUP_AMOUNT,PICKUP_DATE,PICKUP_TIME,PICKUP_ADDRESS,PICKUP_MODE ,PICKUP_AGENCY_CODE ,CUSTOMER_CONTACT_NO ,PLACE_CONTACTED,PERSON_CONTACTED,CONTACT_MODE ,CONTACTED_BY,CONTACT_DATE ,ACTION_DATE ,ACTION_TIME ,ACTION_CODE,REMARKS ,USER_ID ,MAKE_DATE,AGENCE_CODE,AGENCE_NAME,HUB,BRANCHID,CLMCODE,CLMID,Slab,	Filler1,Filler2 ,Filler3 ,Filler4) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",unpacked -> entry[i] -> card_no,unpacked -> entry[i] ->pickup_id,unpacked -> entry[i] ->pickup_amount,unpacked -> entry[i] ->pickup_date,unpacked -> entry[i] ->pickup_time,unpacked -> entry[i] ->pickup_address,unpacked -> entry[i] ->pickup_mode,unpacked -> entry[i] ->pickup_agency_code,unpacked -> entry[i] ->customer_contact_no,unpacked -> entry[i] ->place_contacted,unpacked -> entry[i] ->person_contacted,unpacked -> entry[i] ->contact_mode,unpacked -> entry[i] -> contacted_by,unpacked -> entry[i] ->contact_date,unpacked -> entry[i] ->action_date,unpacked -> entry[i] ->action_time,unpacked -> entry[i] ->action_code,unpacked -> entry[i] ->remarks,unpacked -> entry[i] ->user_id,unpacked -> entry[i] ->make_date,unpacked -> entry[i] ->agencycode,unpacked -> entry[i] ->agencyname,unpacked -> entry[i] ->hub,unpacked -> entry[i] ->branchid,unpacked -> entry[i] ->clmcode,unpacked -> entry[i] ->clmid,unpacked -> entry[i] ->slab,unpacked -> entry[i] ->filler1,unpacked -> entry[i] ->filler2,unpacked -> entry[i] ->filler3,unpacked -> entry[i] ->filler4);
		}

		lk_dispclr();
		lk_disptext(2,3,"Pickup Info",0);
		lk_disptext(3,3,"Synchronized",0);  
        	lk_disptext(4,3,"Successfully!",0);
		sleep(2);
		free(data_buff);
		remove(Exec_ID_Info_File_Name);
		remove(Pickup_Info_file_Name_zip);
		remove(Exec_ID_Info_File_Name_txt);	
		memset(Pickup_Info_file_Name_zip,0,sizeof(Pickup_Info_file_Name_zip));
		memset(Exec_ID_Info_File_Name_txt,0,sizeof(Exec_ID_Info_File_Name_txt));
		memset(Exec_ID_Info_File_Name,0,sizeof(Exec_ID_Info_File_Name));
		//pickup__pickupdetail__free_unpacked(entry_unpacked, NULL);// free entry	
		pickup__entry__free_unpacked(entry_unpacked, NULL);
		return 1;
	}
	pickup__pickupdetail__free_unpacked(unpacked,NULL);// free allocation
	}
	}
	free(data_buff);
	remove(Exec_ID_Info_File_Name);
	remove(Pickup_Info_file_Name_zip);
	remove(Exec_ID_Info_File_Name_txt);	
	memset(Pickup_Info_file_Name_zip,0,sizeof(Pickup_Info_file_Name_zip));
	memset(Exec_ID_Info_File_Name_txt,0,sizeof(Exec_ID_Info_File_Name_txt));
	memset(Exec_ID_Info_File_Name,0,sizeof(Exec_ID_Info_File_Name));
}




