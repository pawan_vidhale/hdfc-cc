/*******************************************************************/
//			    HDFC Bank CC
//			  curl requests.c
//* @author ftlit003
/*******************************************************************/
#include <header.h>
#include <ppp.h>
#include <curl.h>
#include <stdio.h>
#include <signal.h>
#include <easy.h>
/*******************************************************************/
//      Multipart POST & get response 
/********************************************************************/
int Http_MultiPartPOST_GET_Response(char *http_url, char *filename_send, char *filename)
{
	CURL *curl;
	CURLcode res;
	int result = 0;
	errno=0;
	FILE *out_fd = (FILE *) 0;

        struct curl_httppost *formpost=NULL;
        struct curl_httppost *lastptr=NULL;
        struct curl_slist *headerlist=NULL;

	curl_formadd(&formpost,
		     &lastptr,
		     CURLFORM_COPYNAME, "divContent",
		     CURLFORM_FILE, filename_send,
		     CURLFORM_END);

	curl_formadd(&formpost,
		       &lastptr,
		       CURLFORM_COPYNAME, "submit",
		       CURLFORM_COPYCONTENTS, "send",
		       CURLFORM_END);

	curl = curl_easy_init();

	if(curl) {
	out_fd = fopen (filename, "w");//open for read and write	
	curl_easy_setopt(curl, CURLOPT_FILE, out_fd);
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 60);        
	curl_easy_setopt(curl, CURLOPT_URL, http_url);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
	curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); 		
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

	res = curl_easy_perform(curl);
	/* always cleanup */
	result =1;
	if(res != CURLE_OK)
	{
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
		result = 0;
	}

	if(res == 0)
		result = 1;
	else
		//message was not successfully posted to server
		result = 0;
	fclose(out_fd);	
	curl_easy_cleanup(curl);
	curl_formfree(formpost);
	curl_slist_free_all (headerlist);
	}
	return result;
}


/********************************************************************/
//      Multipart POST & get response 
/********************************************************************/
int Http_MultiPartPOST_GET_Response_Customer_Data(char *http_url, char *filename_send, char *filename)
{
	CURL *curl;
	CURLcode res;
	int result = 0;
	double speed_upload, total_time;
	errno=0;
	FILE *out_fd = (FILE *) 0;

        struct curl_httppost *formpost=NULL;
        struct curl_httppost *lastptr=NULL;
        struct curl_slist *headerlist=NULL;

	curl_formadd(&formpost,
		     &lastptr,
		     CURLFORM_COPYNAME, "divContent",
		     CURLFORM_FILE, filename_send,
		     CURLFORM_END);

	curl_formadd(&formpost,
		       &lastptr,
		       CURLFORM_COPYNAME, "submit",
		       CURLFORM_COPYCONTENTS, "send",
		       CURLFORM_END);

	curl = curl_easy_init();

	if(curl) {
	out_fd = fopen (filename, "w");//open for read and write	
#if PRINTF 
	perror("1:\n");	
#endif	
	curl_easy_setopt(curl, CURLOPT_FILE, out_fd);
#if PRINTF 
	perror("2:\n");
#endif
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 120);
#if PRINTF 
	perror("3:\n");
#endif
	curl_easy_setopt(curl, CURLOPT_URL, http_url);
#if PRINTF 
	perror("6:\n");
#endif
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
#if PRINTF 
	perror("7:\n");
#endif
	curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
#if PRINTF 
	perror("8:\n");
#endif
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
#if PRINTF 
	perror("9:\n");
#endif
  	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
#if PRINTF 
	perror("10:\n");
#endif
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
#if PRINTF 
	perror("11:\n");
#endif
	res = curl_easy_perform(curl);
#if PRINTF 
	perror("12:\n");
#endif

	curl_easy_getinfo(curl, CURLINFO_SPEED_UPLOAD, &speed_upload);
    	curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &total_time);
 
        fprintf(stderr, "\nSpeed: %.3f bytes/sec during %.3f seconds\n",speed_upload,total_time);

	if(res == 0)		
		result = 1;		
	else		
		//message was not successfully posted to server
		result = 0;
		perror("13:\n");
	fclose(out_fd);
	//Cleaning up free pointers
	curl_easy_cleanup(curl);
	curl_formfree(formpost);
	curl_slist_free_all (headerlist);
	}
	return result;
}

/********************************************************************/
//      Multipart POST & get response 
/********************************************************************/
int Http_MultiPartPOST_GET_Response_Image_Data(char *http_url, char *filename_send, char *filename)
{
	CURL *curl;
	CURLcode res;
	int result = 0;
	double speed_upload, total_time;
	errno=0;
	FILE *out_fd = (FILE *) 0;

        struct curl_httppost *formpost=NULL;
        struct curl_httppost *lastptr=NULL;
        struct curl_slist *headerlist=NULL;

	curl_formadd(&formpost,
		     &lastptr,
		     CURLFORM_COPYNAME, "divContent",
		     CURLFORM_FILE, filename_send,
		     CURLFORM_END);

	curl_formadd(&formpost,
		       &lastptr,
		       CURLFORM_COPYNAME, "submit",
		       CURLFORM_COPYCONTENTS, "send",
		       CURLFORM_END);

	curl = curl_easy_init();

	if(curl) {
	out_fd = fopen (filename, "w");//open for read and write	
#if PRINTF 
	perror("1:\n");	
#endif	
	curl_easy_setopt(curl, CURLOPT_FILE, out_fd);
#if PRINTF 
	perror("2:\n");
#endif
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 800);
#if PRINTF 
	perror("3:\n");
#endif
	curl_easy_setopt(curl, CURLOPT_URL, http_url);
#if PRINTF 
	perror("6:\n");
#endif
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
#if PRINTF 
	perror("7:\n");
#endif
	curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
#if PRINTF 
	perror("8:\n");
#endif
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
#if PRINTF 
	perror("9:\n");
#endif
  	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
#if PRINTF 
	perror("10:\n");
#endif
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
#if PRINTF 
	perror("11:\n");
#endif
	res = curl_easy_perform(curl);
#if PRINTF 
	perror("12:\n");
#endif
	curl_easy_getinfo(curl, CURLINFO_SPEED_UPLOAD, &speed_upload);
    	curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &total_time); 
        fprintf(stderr, "\nSpeed: %.3f bytes/sec during %.3f seconds\n",speed_upload,total_time);

	if(res == 0)		
		result = 1;		
	else		
		//message was not successfully posted to server
		result = 0;
		perror("13:\n");
	fclose(out_fd);
	//Cleaning up free pointers
	curl_easy_cleanup(curl);
	curl_formfree(formpost);
	curl_slist_free_all (headerlist);
	curl_global_cleanup();
	}
	return result;
}


/*******************************************************************/
//		HTTP DRA ID verification (Post Request) Using
/*******************************************************************/
int curl_Dra_Verification_Data(char http_url[],char Send_file_name[],char file_name[])
{
        struct stat file_buf;
        unsigned char size[20];
        int ret=0;
                                                                                      
        printf("Downloading file from server\n");
        lk_dispclr();
        lk_disptext(2,2,"   Verifying",0);
	lk_disptext(3,1,"  Executive ID",0);
        lk_disptext(4,3,"Please wait....",0);
        Http_MultiPartPOST_GET_Response(http_url,Send_file_name,file_name);
	stat(file_name,&file_buf);
	printf("file_size:%x\n",(unsigned int)file_buf.st_size);
	if(((unsigned int)file_buf.st_size)<=0x00)
	{
		lk_dispclr();
		lk_disptext(2,0," Executive Information",0);
		lk_disptext(3,0,"    Not Found Or",0);
		lk_disptext(4,0," Network Is unreachable",0);		
		return 0;
	}
        return 1;
}

/*******************************************************************/
//		HTTP DRA change Password (Post Request) Using
/*******************************************************************/
int curl_Dra_change_Pwd_Request_Data(char http_url[],char Send_file_name[],char file_name[])
{
        struct stat file_buf;
        unsigned char size[20];
        int ret=0;
                                                                            
        printf("Downloading file from server\n");
        lk_dispclr();
        lk_disptext(2,1,"Change Pwd Is In",0);
	lk_disptext(3,1,"     Process",0);
        lk_disptext(4,3,"Please wait....",0);
        Http_MultiPartPOST_GET_Response(http_url,Send_file_name,file_name);
	stat(file_name,&file_buf);
	printf("file_size:%x\n",(unsigned int)file_buf.st_size);
	if(((unsigned int)file_buf.st_size)<=0x00)	return 0;
        return 1;
}

/*******************************************************************/
//		HTTP Dra Customer Info (Post Request) Using
/*******************************************************************/
int curl_Dra_Customer_Info_Data(char http_url[],char Send_file_name[],char file_name[])
{
	int Customer_key;
        struct stat file_buf;
        unsigned char size[20];
        int ret=0;
        
        printf("Downloading file from server\n");
        lk_dispclr();
        lk_disptext(2,0,"   Downloading",0);
	lk_disptext(3,0,"   Allocation Info",0);
        lk_disptext(4,3,"Please wait....",0);
        Http_MultiPartPOST_GET_Response_Customer_Data(http_url,Send_file_name,file_name);
	stat(file_name,&file_buf);
	printf("file_size:%x\n",(unsigned int)file_buf.st_size);
	if(((unsigned int)file_buf.st_size)<=0x00)
	{
Customer_Info:
		lk_dispclr();
		lk_disptext(1,1,"We can't communicate ",0);
		lk_disptext(2,1,"with the server at",0);
		lk_disptext(3,1,"moment. Please try",0);
		lk_disptext(4,1,"again later",0);
		lk_disptext(5,0,"Press Enter to Cont.",0);		
		Customer_key = lk_getkey();
		if(Customer_key==ENTER)
		{		
		return 0;
		}
		else goto Customer_Info;
	}
        return 1;
}

/*******************************************************************/
//		HTTP Dra Pickup Info (Post Request) Using
/*******************************************************************/
int curl_Dra_Customer_Pickup_Info_Data(char http_url[],char Send_file_name[],char file_name[])
{
	int Customer_key;
        struct stat file_buf;
        unsigned char size[20];
        int ret=0;
        
        printf("Downloading file from server\n");
        lk_dispclr();
        lk_disptext(2,0,"    Downloading",0);
	lk_disptext(3,1,"    Pickup Info",0);
        lk_disptext(4,3,"Please wait....",0);
        Http_MultiPartPOST_GET_Response_Customer_Data(http_url,Send_file_name,file_name);
	stat(file_name,&file_buf);
	printf("file_size:%x\n",(unsigned int)file_buf.st_size);
	if(((unsigned int)file_buf.st_size)<=0x00)
	{
Customer_Info:
		lk_dispclr();
		lk_disptext(1,1,"We can't communicate ",0);
		lk_disptext(2,1,"with the server at",0);
		lk_disptext(3,1,"moment. Please try",0);
		lk_disptext(4,1,"again later",0);
		lk_disptext(5,0,"Press Enter to Cont.",0);		
		Customer_key = lk_getkey();
		if(Customer_key==ENTER)
		{		
		return 0;
		}
		else goto Customer_Info;
	}
        return 1;
}

/*******************************************************************/
int curl_Dra_Verification_Wallet_Data(char http_url[],char Send_file_name[],char file_name[])
{
        struct stat file_buf;
        unsigned char size[20];
        int ret=0;
                                                                             
        printf("Downloading file from server\n");
        lk_dispclr();
        lk_disptext(2,3,"   Verifying",0);
	lk_disptext(3,3," Wallet Limit",0);
        lk_disptext(4,3,"Please wait....",0);
        Http_MultiPartPOST_GET_Response(http_url,Send_file_name,file_name);
	stat(file_name,&file_buf);
	printf("\nfile_size:%x\n",(unsigned int)file_buf.st_size);
	if(((unsigned int)file_buf.st_size)<=0x00) return 0;
	else return 1;
}

/*******************************************************************/
//		HTTP Customer Transaction (Post Request) Using
/*******************************************************************/
int curl_Customer_Transaction_Data(char http_url[],char Send_file_name[],char file_name[])
{
        struct stat file_buf;
        unsigned char size[20];
        int ret=0;
                                                                               
        printf("Downloading file from server\n");
        lk_dispclr();
        lk_disptext(2,1," Transaction Is In",0);
	lk_disptext(3,1,"     Process",0);
        lk_disptext(4,3,"Please wait....",0);
        Http_MultiPartPOST_GET_Response(http_url,Send_file_name,file_name);
	stat(file_name,&file_buf);
	printf("file_size:%x\n",(unsigned int)file_buf.st_size);
	if(((unsigned int)file_buf.st_size)<=0x00)	return 0;
        return 1;
}

/*******************************************************************/
//		HTTP Customer FollowUp (Post Request) Using
/*******************************************************************/
int curl_Customer_Feedback_Data(char http_url[],char Send_file_name[],char file_name[])
{
        struct stat file_buf;
        unsigned char size[20];
        int ret=0;
                                                                                           
        printf("Downloading file from server\n");
        lk_dispclr();
        lk_disptext(2,1,"FollowUp Upload In",0);
	lk_disptext(3,1,"     Process",0);
        lk_disptext(4,3,"Please wait....",0);
        Http_MultiPartPOST_GET_Response(http_url,Send_file_name,file_name);
	stat(file_name,&file_buf);
	printf("file_size:%x\n",(unsigned int)file_buf.st_size);
	if(((unsigned int)file_buf.st_size)<=0x00)	return 0;
        return 1;
}

/*******************************************************************/
//		HTTP Customer FollowUp (Post Request) Using
/*******************************************************************/
int curl_Surrender_Data(char http_url[],char Send_file_name[],char file_name[])
{
        struct stat file_buf;
        unsigned char size[20];
        int ret=0;
        	                                                                            
        printf("Downloading file from server\n");
        lk_dispclr();
        lk_disptext(2,1,"Machine Surrendered",0);
	lk_disptext(3,1,"     Process",0);
        lk_disptext(4,3,"Please wait....",0);
        Http_MultiPartPOST_GET_Response(http_url,Send_file_name,file_name);
	stat(file_name,&file_buf);
	printf("file_size:%x\n",(unsigned int)file_buf.st_size);	
	if(((unsigned int)file_buf.st_size)<=0x00)	return 0;	
        return 1;
}

/*******************************************************************/
//		HTTP Public certificate url (Post Request) Using
/*******************************************************************/
int curl_publiccertificate_Verification_request(char http_url[],char file_name[])
{
        struct stat file_buf;
        unsigned char size[20];
        int ret=0;
                                                                                           
        printf("\n Downloading file from server\n");
        lk_dispclr();
	lk_disptext(2,3,"Updating Info",0);
        lk_disptext(4,3,"Please wait....",0);
        GET_Public_File(http_url,file_name);	
	stat(file_name,&file_buf);
	printf("\nfile_size:%x\n",(unsigned int)file_buf.st_size);
	if(((unsigned int)file_buf.st_size)==0x5a8) return 1;
        return 0;
}

/*******************************************************************/
//		HTTP HHT Version Date Request (Post Request) Using
/*******************************************************************/
int curl_HHT_Version_Date_Request(char http_url[],char Send_file_name[],char file_name[])
{
        struct stat file_buf;
        unsigned char size[20];
        int ret=0;
                                                                                           
        printf("\n Downloading file from server\n");
        lk_dispclr();
	lk_disptext(2,3,"Updating Info",0);
        lk_disptext(4,3,"Please wait....",0);
        Http_MultiPartPOST_GET_Response_Image_Data(http_url,Send_file_name,file_name);	
	stat(file_name,&file_buf);
	printf("\nfile_size:%x\n",(unsigned int)file_buf.st_size);
	if(((unsigned int)file_buf.st_size)<=0x00) return 0;
        return 1;
} 
 
/*******************************************************************/
//		HTTP Version url (Post Request) Using
/*******************************************************************/
int curl_HHT_Version_url_request(char http_url[],char Send_file_name[],char file_name[])
{
        struct stat file_buf;
        unsigned char size[20];
        int ret=0;
                                                                                           
        printf("\n Downloading file from server\n");
        lk_dispclr();
	lk_disptext(2,3,"Updating Info",0);
        lk_disptext(4,3,"Please wait....",0);
        Http_MultiPartPOST_GET_Response(http_url,Send_file_name,file_name);
	stat(file_name,&file_buf);
	printf("\nfile_size:%x\n",(unsigned int)file_buf.st_size);
	if(((unsigned int)file_buf.st_size)<=0x00) return 0;
        return 1;
}

/*******************************************************************/
//		HTTP BOD & EOD (Post Request) Using
/*******************************************************************/
int curl_BOD_EOD_Data(char http_url[],char Send_file_name[],char file_name[],char* BOD_EOD_Flag)
{
        struct stat file_buf;
        unsigned char size[20];
        int ret=0;
                                      
	printf("\nBOD_EOD_Flag:%s",BOD_EOD_Flag);  
	if(strcmp(BOD_EOD_Flag,"0")==0)
	{
        lk_dispclr();
        lk_disptext(2,1,"    BOD Is In",0);
	lk_disptext(3,1,"     Process",0);
	lk_disptext(4,3,"Please wait....",0);
	}
	else
	{
	lk_dispclr();
        lk_disptext(2,1,"    EOD Is In",0);
	lk_disptext(3,1,"     Process",0);
	lk_disptext(4,3,"Please wait....",0);
	}       
        Http_MultiPartPOST_GET_Response(http_url,Send_file_name,file_name);
	stat(file_name,&file_buf);
	printf("file_size:%x\n",(unsigned int)file_buf.st_size);
	if(((unsigned int)file_buf.st_size)<=0x00)	return 0;
        return 1;
}
/*******************************************************************/
//		HTTP Lock password url (Post Request) Using
/*******************************************************************/
int curl_Lockpassword_url_request(char http_url[],char Send_file_name[],char file_name[])
{
        struct stat file_buf;
        unsigned char size[20];
        int ret=0;
                                                                                           
        printf("\n Downloading file from server\n");	
        lk_dispclr();
	lk_disptext(2,3,"Lock Request",0);
        lk_disptext(4,3,"Please wait....",0);
        Http_MultiPartPOST_GET_Response(http_url,Send_file_name,file_name);
	stat(file_name,&file_buf);
	printf("\nfile_size:%x\n",(unsigned int)file_buf.st_size);
	if(((unsigned int)file_buf.st_size)<=0x00) return 0;
        return 1;
} 


/******************************************************************************/
int GET_Public_File(char *http_url, char *filename)
{
	CURL *curl;
	CURLcode res;
	FILE *out_fd = (FILE *) 0;
	int result = 0;
	errno=0;

	struct curl_httppost *formpost=NULL;
        struct curl_httppost *lastptr=NULL;
        struct curl_slist *headerlist=NULL;

	curl_formadd(&formpost,
		     &lastptr,
		     CURLFORM_COPYNAME, "divContent",		    
		     CURLFORM_END);

	curl_formadd(&formpost,
		       &lastptr,
		       CURLFORM_COPYNAME, "submit",
		       CURLFORM_COPYCONTENTS, "send",
		       CURLFORM_END);


	curl = curl_easy_init();	
	if(curl)
	{
		printf("\nhttp_url:%s\n",http_url);
		printf("\nfilename:%s\n",filename);
		
		perror("1:\n");
   	 	err_printf("Downloading %s file...\n", filename);
		out_fd = fopen (filename, "w");		
		res=curl_easy_setopt(curl, CURLOPT_FILE, out_fd);		
	       	res=curl_easy_setopt(curl, CURLOPT_TIMEOUT, 160);		
    		res=curl_easy_setopt(curl, CURLOPT_URL, http_url);		
		res=curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);		
		res=curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);		
		res=curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);		
  		res=curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);		
		res=curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

  		res = curl_easy_perform(curl); //post away!

		if(res == 0)
			result = 1;
		else
			//message was not successfully posted to server
			result = 0;
		fclose(out_fd);
		
	}
	//there must be a corresponding curl_easy_cleanup() to curl_easy_init()
	curl_easy_cleanup(curl);	
	
	return result;
}

/******************************************************************/
int curl_http_request(void)
{
  char IP_curl[150];
  CURL *curl;
  CURLcode res;
  resultset Exid_IP;

  close_sqlite();
  open_sqlite("/mnt/jffs2/hdfcccoperational.db");
  Exid_IP=get_result("select httpip from operationalstatus");
  memset(IP_curl,0,sizeof(IP_curl));
  sprintf(IP_curl,"%s/HHTRequest/v1_0/CollectionExecutiveLoginValidation.aspx",Exid_IP.recordset[0][0]);

  curl_global_init(CURL_GLOBAL_DEFAULT);

  curl = curl_easy_init();
  if(curl) {
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 60);   
	curl_easy_setopt(curl, CURLOPT_URL,IP_curl);
	//curl_easy_setopt(curl, CURLOPT_URL,"http://124.153.86.129");
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);	
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
        res = curl_easy_perform(curl);
        if(res != CURLE_OK)
        fprintf(stderr, "curl_easy_perform() failed: %s\n",curl_easy_strerror(res));
        /* always cleanup */
        curl_easy_cleanup(curl);
  }
  curl_global_cleanup();
  if(res==0) return 0;
  else return -1;
}

