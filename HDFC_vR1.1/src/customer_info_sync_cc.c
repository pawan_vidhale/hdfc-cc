/*******************************************************************/
//			  HDFC Bank CC
//		      customer_info_sync.c
/*******************************************************************/
/**
 *
 * 
**/
#include <header.h>
#include <stdio.h>
#include <stdlib.h>
#include "allocation_cc.pb-c.h"
#define MAX_MSG_SIZE  4194304			      		      	 		     
//#define MAX_MSG_SIZE    8388608	 		     		               
/*******************************************************************/
//Declaration
/*******************************************************************/
/******************************************************************/

/******************************************************************/
int Customer_Info_Sync(char* Exec_ID)
{	
							
	int Customer_Details_Return,Pickup_Details_Return;
	resultset Disp_appl;	
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Disp_appl=get_result("SELECT localhttpip From operationalstatus;");
	
	if(strcmp(Disp_appl.recordset[0][0],"DRA")==0)
	{
	printf("\nApplication%s",Disp_appl.recordset[0][0]);
	system("cd /mnt/jffs2/");
	system("rm *.pb *.zip *.txt");	
	Customer_Details_Return=Customer_Info_Request(Exec_ID);	//Request to server...
	printf("\nCustomer_Details_Return:%d",Customer_Details_Return);
	if(Customer_Details_Return==1)	Customer_Info_Protobuff_Parser(Exec_ID);	//parsing Agent Info...	
	}
	printf("\nApplication%s",Disp_appl.recordset[0][0]);
	system("cd /mnt/jffs2/");
	system("rm *.pb *.zip *.txt");
	Pickup_Details_Return=Pickup_Info_Request(Exec_ID);	
	printf("\nPickup_Details_Return:%d",Pickup_Details_Return);
	if(Pickup_Details_Return==1)  Pickup_Info_Protobuff_Parser(Exec_ID);
	system("cd /mnt/jffs2/");
	system("rm *.pb *.zip *.txt");
	return 0;
}
/******************************************************************/

/******************************************************************/
int Customer_Info_Protobuff_Parser(char* Executive_ID)
{
	int len,download_key;
	FILE *ifp;
	char ch;
	int count=0;
	//uint8_t data_buff[MAX_MSG_SIZE];
	uint8_t *data_buff;
	int i,j,k;
	char Exec_ID_Info_File_Name[1000],Exec_ID_Info_File_Name_txt[500],Exec_ID_Info_File_Name_zip[500];


	data_buff = (uint8_t *) malloc(MAX_MSG_SIZE);
	memset(data_buff,0,sizeof(data_buff));

	lk_dispclr();
	lk_disptext(2,3,"Allocation Info",0);
	lk_disptext(3,3,"Synchronizing",0);
        lk_disptext(4,3,"Please wait....",0);

	close_sqlite();
	create_customerdatabase();
	memset(Exec_ID_Info_File_Name_zip,0,sizeof(Exec_ID_Info_File_Name_zip));
	sprintf(Exec_ID_Info_File_Name_zip,"/mnt/jffs2/%s_info.zip",Executive_ID);
	memset(Exec_ID_Info_File_Name_txt,0,sizeof(Exec_ID_Info_File_Name_txt));
	sprintf(Exec_ID_Info_File_Name_txt,"/mnt/jffs2/%s_info.txt",Executive_ID);
	memset(Exec_ID_Info_File_Name,0,sizeof(Exec_ID_Info_File_Name));
	sprintf(Exec_ID_Info_File_Name,"/mnt/jffs2/%s_info.pb",Executive_ID);
	if(stat(Exec_ID_Info_File_Name,&st)==0)
	{
	ifp = fopen(Exec_ID_Info_File_Name, "rb");
	if (ifp == NULL) {
	  fprintf(stderr, "Can't open input file in.list!\n");
	  return 0;
	}
	if(ifp != NULL)
	{
	printf("Successfully open file\n");
	size_t cur_len = 0, nread =0 ;
//	if(nread = fread(data_buff,1,NUMELEM,fd))
	    while ((nread=fread(data_buff,1, MAX_MSG_SIZE - cur_len, ifp)) != 0)
	    {
	      cur_len += nread;
	      if (cur_len == MAX_MSG_SIZE)
		{
		  fprintf(stderr, "max message length exceeded\n");
		  return 0;
		}
	    }
	printf("legnth:%d\n",cur_len);
	data_buff[cur_len]='\0';

	ALLOC__Allocation *unpacked;
	ALLOC__Entry *entry_unpacked;
	unpacked = alloc__allocation__unpack(NULL, cur_len, data_buff);// Unpack allocation
	if (unpacked == NULL)      
	{
Unable_download:
		lk_dispclr();
		lk_disptext(2,2,"Unable to download",0);
		lk_disptext(3,2,"Allocation Info",0);
		lk_disptext(5,0,"Press Enter to Cont.",0);		
		fprintf(stderr, "error unpacking alloc incoming message\n");
		download_key = lk_getkey();
		if(download_key==ENTER)
		{
		return 0;
		}
		else goto Unable_download;		
	}
	else
	{
		system("rm /mnt/jffs2/hdfccc.db");
		close_sqlite();
		create_customerdatabase();
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		printf("No. of entry:%d\n",unpacked->n_entry);
		if(unpacked->n_entry==0) goto Unable_download; 
		for(i=0;i<(unpacked->n_entry);i++)
		{
#if PRINTF
			printf("\nCustumer Details:%d\n",i);
#endif
			entry_unpacked = alloc__entry__unpack(NULL, cur_len, data_buff);// unpack entry
			if (entry_unpacked == NULL)      fprintf(stderr, "error unpacking entry incoming message\n");
			execute("INSERT INTO ALLOCATION_DETAILS(BRANCH_CODE ,CARD_NUMBER , ALT_ACCOUNT_NO, BILL_CYCLE, COUNT_PAY_CURRCYC, DELINQENCY_STRING, CUST_ID,	ADDRESS_M1,	ADDRESS_M2,	ADDRESS_M3,	ADDRESS_M4,	M_ADDRESS_CITY, M_ADDRESS_STATE,	M_ADDRESS_ZIP ,	M_ADDRESS_LM ,	HOME_PHONE,	MOBILE_NO,	GENDER,	SS_NAME,	EMP_CODE,	PRS_EMAIL_ID, OFF_EMAIL_ID,	EMP_NAME,	DESIGNATION,	BAND, WORK_PHONE,	WORK_PHONE_E,	CURRENT_BL,	MEMO_BL, TOTAL_PAST_DUE, TOTAL_CURR_DUE,	TOTAL_AMT_DUE, OVERLIMIT_AMT,	DATE_L_DELINQUENCY,	DATE_L_R_CHEQUE,	NO_OF_AUTOP_RET,	L_SMT_BNG_BAL ,	L_SMT_ENG_BAL,	L_SMT_PAST_DUE,	L_SMT_TMT_PAY_DUE,NO_OF_AUTO_RET_LSTAT,	AT_RET_AMT_LSTAT,	LSTAT_CURR_DUE,	DATE_OF_LPAY,	AMT_OF_LPAY,	DATE_OF_LPUR,	AMT_LPUR,	DATE_L_CYCLE,	LTD_RETAIL_PURAMT,	LTD_RETAIL_RETAMT,	LTD_CASH_TRS_AMT ,	ADDRESS_L1,	ADDRESS_L2,	ADDRESS_L3,	ADDRESS_L4,	ADDRESS_ALT_CITY,	ADDRESS_ALT_STATE,	ADDRESS_ALT_ZIP,	PAN_NO,	M_ADDRESS_TIME_CC,	ADDRESS_P1,	ADDRESS_P2,	ADDRESS_P3, ADDRESS_P_CITY, ADDRESS_P_ZIP, ADDRESS_P_LMARK, ADDRESS_P_TELPNO, CPY_CAT_CODE, OCC_TYPE, REGION_CODE, LOAN_BAL, DATE_OF_ADDRESSCHANGE, DATE_OF_LSTAT, DATE_OF_NSTAT, LOADED_BAL, BUCKET, BUCKET_DEST,	EMP_ADDRESS1,	EMP_ADDRESS2,	EMP_ADDRESS3,	EMP_ADDRESS4,	EMP_ADDRESS_PIN,	EMP_ADDRESS_LM,	EMP_ADDRESS_CITY, EMP_TELE_NO,ADDON_CARD1, ADDON_CARD2, ADDON_CARD3, ADDON_CARD4, DATE_ACC_CHARGED_OFF, EXTRA1,AGENCE_NAME,AGENCE_CODE,filler1,filler2,filler3,filler4) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",unpacked -> entry[i] -> branch_code,unpacked -> entry[i] ->card_number,unpacked -> entry[i] ->alt_account_no,unpacked -> entry[i] ->bill_cycle,unpacked -> entry[i] ->count_pay_currcyc,unpacked -> entry[i] ->delinquency_string,unpacked -> entry[i] ->cust_id,unpacked -> entry[i] ->address_m1,unpacked -> entry[i] ->address_m2,unpacked -> entry[i] ->address_m3,unpacked -> entry[i] ->address_m4,unpacked -> entry[i] ->m_address_city,unpacked -> entry[i] -> m_address_state,unpacked -> entry[i] ->m_address_zip,unpacked -> entry[i] ->m_address_lm,unpacked -> entry[i] ->home_phone,unpacked -> entry[i] ->mobile_no,unpacked -> entry[i] ->gender,unpacked -> entry[i] ->ss_name,unpacked -> entry[i] ->emp_code,unpacked -> entry[i] ->prs_email_id,unpacked -> entry[i] ->off_email_id,unpacked -> entry[i] ->emp_name,unpacked -> entry[i] ->designation,unpacked -> entry[i] ->band,unpacked -> entry[i] ->work_phone,unpacked -> entry[i] ->work_phone_e,unpacked -> entry[i] ->current_bl,unpacked -> entry[i] ->memo_bl,unpacked -> entry[i] ->total_past_due,unpacked -> entry[i] ->total_curr_due,unpacked -> entry[i] ->total_amt_due,unpacked -> entry[i] ->overlimit_amt,unpacked -> entry[i] ->date_l_delinquency,unpacked -> entry[i] ->date_l_r_cheque,unpacked -> entry[i] ->no_of_autop_ret,unpacked -> entry[i] ->l_smt_bng_bal,unpacked -> entry[i] ->l_smt_eng_bal,unpacked -> entry[i] ->l_smt_past_due,unpacked -> entry[i] ->l_smt_tmt_pay_due,unpacked -> entry[i] ->no_of_autop_ret_lstat,unpacked -> entry[i] ->at_ret_amt_lstat,unpacked -> entry[i] ->lstat_curr_due,unpacked -> entry[i] ->date_of_lpay,unpacked -> entry[i] ->amt_of_lpay,unpacked -> entry[i] ->date_of_lpur,unpacked -> entry[i] ->amt_lpur,unpacked -> entry[i] ->date_l_cycle,unpacked -> entry[i] ->ltd_retail_puramt,unpacked -> entry[i] ->ltd_retail_retamt,unpacked -> entry[i] ->ltd_cash_trs_amt,unpacked -> entry[i] ->address_l1,unpacked -> entry[i] ->address_l2,unpacked -> entry[i] ->address_l3,unpacked -> entry[i] ->address_l4,unpacked -> entry[i] ->address_alt_city,unpacked -> entry[i] ->address_alt_state,unpacked -> entry[i] ->address_alt_zip,unpacked -> entry[i] ->pan_no,unpacked -> entry[i] ->m_aadress_time_cc,unpacked -> entry[i] ->address_p1,unpacked -> entry[i] ->address_p2,unpacked -> entry[i] ->address_p3,unpacked -> entry[i] ->address_p_city,unpacked -> entry[i] ->address_p_zip,unpacked -> entry[i] ->address_p_lmark,unpacked -> entry[i] ->address_p_telpno,unpacked -> entry[i] ->cpy_cat_code,unpacked -> entry[i] ->occ_type,unpacked -> entry[i] ->region_code,unpacked -> entry[i] ->loan_bal,unpacked -> entry[i] ->date_of_addresschange,unpacked -> entry[i] ->date_of_lstat,unpacked -> entry[i] ->date_of_nstat,unpacked -> entry[i] ->loaded_bal,unpacked -> entry[i] ->bucket,unpacked -> entry[i] ->bucket_dest,unpacked -> entry[i] ->emp_address1,unpacked -> entry[i] ->emp_address2,unpacked -> entry[i] ->emp_address3,unpacked -> entry[i] ->emp_address4,unpacked -> entry[i] ->emp_address_pin,unpacked -> entry[i] ->emp_address_lm,unpacked -> entry[i] ->emp_address_city,unpacked -> entry[i] ->emp_tele_no,unpacked -> entry[i] ->addon_card1,unpacked -> entry[i] ->addon_card2,unpacked -> entry[i] ->addon_card3,unpacked -> entry[i] ->addon_card4,unpacked -> entry[i] ->date_acc_charged_off,unpacked -> entry[i] ->extra1,unpacked -> entry[i] ->agence_name,unpacked -> entry[i] ->agence_code,unpacked -> entry[i] ->filler1,unpacked -> entry[i] ->filler2,unpacked -> entry[i] ->filler3,unpacked -> entry[i] ->filler4);
			

#if PRINTF
			printf("No. of cbh:%d\n",unpacked -> entry[i] -> n_cbh);
			printf("No. of th:%d\n",unpacked -> entry[i] -> n_th);
#endif
			for(j=0;j<(unpacked -> entry[i] -> n_cbh);j++)
			{
				ALLOC__CBH *cbh_unpacked;	
				cbh_unpacked = alloc__cbh__unpack(NULL, cur_len, data_buff);// unpack cbh
				if (cbh_unpacked == NULL)      fprintf(stderr, "error unpacking entry incoming message\n");
				/*printf("instrument_type[%d][%d]:%s\n",i,j,unpacked -> entry[i] -> cbh[j] -> instrument_type);
				printf("cheque_no[%d][%d]:%s\n",i,j,unpacked -> entry[i] -> cbh[j] -> cheque_no);
				printf("cheque_date[%d][%d]:%s\n",i,j,unpacked -> entry[i] -> cbh[j] -> cheque_date);
				printf("cheque_amt[%d][%d]:%s\n",i,j,unpacked -> entry[i] -> cbh[j] -> cheque_amt);*/

				//execute("UPDATE CHEQUE_BOUNCE SET APPL_ID='%s',INSTRUMENT_TYPE='%s',CHEQUE_NO='%s',	CHEQUE_DATE='%s',CHEQUE_AMT='%s',PRESENTATION_DATE='%s',BANK_NAME='%s',BOUNCE_REASON='%s',RETURNED_DATE='%s',	DOWNLOAD_DATE='%s',PROCESS_STATUS='%s',PROCESS_DATE='%s';",unpacked -> entry[i] ->appl_id,unpacked -> entry[i] -> cbh[j] -> instrument_type,unpacked -> entry[i] -> cbh[j] -> cheque_no,unpacked -> entry[i] -> cbh[j] -> cheque_date,unpacked -> entry[i] -> cbh[j] -> cheque_amt,unpacked -> entry[i] -> cbh[j] -> presentation_date,unpacked -> entry[i] -> cbh[j] -> bank_name,unpacked -> entry[i] -> cbh[j] -> bounce_reason,unpacked -> entry[i] -> cbh[j] -> returned_date,unpacked -> entry[i] -> cbh[j] -> c_process_status,unpacked -> entry[i] -> cbh[j] -> c_download_date,unpacked -> entry[i] -> cbh[j] -> c_process_date);

				execute("INSERT INTO CHEQUE_BOUNCE(INSTRUMENT_TYPE,	CHEQUE_NO,	CHEQUE_DATE,	CHEQUE_AMT,	PRESENTATION_DATE,	BANK_NAME,	BOUNCE_REASON,	RETURNED_DATE,	C_PROCESS_STATUS , C_DOWNLOAD_DATE,	C_PROCESS_DATE) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",unpacked -> entry[i] -> cbh[j] -> instrument_type,unpacked -> entry[i] -> cbh[j] -> cheque_no,unpacked -> entry[i] -> cbh[j] -> cheque_date,unpacked -> entry[i] -> cbh[j] -> cheque_amt,unpacked -> entry[i] -> cbh[j] -> presentation_date,unpacked -> entry[i] -> cbh[j] -> bank_name,unpacked -> entry[i] -> cbh[j] -> bounce_reason,unpacked -> entry[i] -> cbh[j] -> returned_date,unpacked -> entry[i] -> cbh[j] -> c_process_status,unpacked -> entry[i] -> cbh[j] -> c_download_date,unpacked -> entry[i] -> cbh[j] -> c_process_date);

				alloc__cbh__free_unpacked(cbh_unpacked,NULL);// free cbh
			}

			for(k=0;k<(unpacked -> entry[i] -> n_th);k++)
			{
				ALLOC__TH *th_unpacked;
				th_unpacked = alloc__th__unpack(NULL, cur_len, data_buff);// unpack th
				if (th_unpacked == NULL)      fprintf(stderr, "error unpacking entry incoming message\n");
				/*printf("contact_date[%d][%d]:%s\n",i,k,unpacked -> entry[i] -> th[k] -> contact_date);
				printf("contact_mode[%d][%d]:%s\n",i,k,unpacked -> entry[i] -> th[k] -> contact_mode);
				printf("contact_place[%d][%d]:%s\n",i,k,unpacked -> entry[i] -> th[k] -> contact_place);	
				printf("person_contacted[%d][%d]:%s\n",i,k,unpacked -> entry[i] -> th[k] -> person_contacted);*/
			
				execute("INSERT INTO TRAIL_HISTORY(CONTACT_DATE,	CONTACT_MODE,	CONTACT_PLACE,	PERSON_CONTACTED,	ACTION_CODE,	ACTION_AMOUNT,	REMARKS,	CONTACTED_BY,	USERID,	CASE_CLEAR_FLAG,	CONTACT_TIME,	CONTACT_OBJECTIVE,	PTP_DATE,	H_UNIT_ID,	H_DOWNLOAD_DATE,	H_PROCESS_STATUS,	H_PROCESS_DATE) VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",unpacked -> entry[i] -> th[k] -> contact_date,unpacked -> entry[i] -> th[k] -> contact_mode,unpacked -> entry[i] -> th[k] -> contact_place,unpacked -> entry[i] -> th[k] -> person_contacted,unpacked -> entry[i] -> th[k] -> action_code,unpacked -> entry[i] -> th[k] -> action_amount,unpacked -> entry[i] -> th[k] -> remarks,unpacked -> entry[i] -> th[k] -> contacted_by,unpacked -> entry[i] -> th[k] -> userid,unpacked -> entry[i] -> th[k] -> case_clear_flag,unpacked -> entry[i] -> th[k] -> contact_time,unpacked -> entry[i] -> th[k] -> contact_objective,unpacked -> entry[i] -> th[k] -> ptp_date,unpacked -> entry[i] -> th[k] -> h_unit_id,unpacked -> entry[i] -> th[k] -> h_download_date,unpacked -> entry[i] -> th[k] -> h_process_status,unpacked -> entry[i] -> th[k] -> h_process_date);
				alloc__th__free_unpacked(th_unpacked,NULL);// free th
			}	

		}//for
		lk_dispclr();
		lk_disptext(2,3,"Allocation Info",0);
		lk_disptext(3,3,"Synchronized",0);  
        	lk_disptext(4,3,"Successfully!",0);
		sleep(5);
		free(data_buff);
		remove(Exec_ID_Info_File_Name);
		remove(Exec_ID_Info_File_Name_zip);
		remove(Exec_ID_Info_File_Name_txt);	
		memset(Exec_ID_Info_File_Name_zip,0,sizeof(Exec_ID_Info_File_Name_zip));
		memset(Exec_ID_Info_File_Name_txt,0,sizeof(Exec_ID_Info_File_Name_txt));
		memset(Exec_ID_Info_File_Name,0,sizeof(Exec_ID_Info_File_Name));
		alloc__entry__free_unpacked(entry_unpacked, NULL);// free entry	
		return 1;
	
	}//unpack else
	alloc__allocation__free_unpacked(unpacked,NULL);// free allocation
	}
	}
	free(data_buff);
	remove(Exec_ID_Info_File_Name);
	remove(Exec_ID_Info_File_Name_zip);
	remove(Exec_ID_Info_File_Name_txt);	
	memset(Exec_ID_Info_File_Name_zip,0,sizeof(Exec_ID_Info_File_Name_zip));
	memset(Exec_ID_Info_File_Name_txt,0,sizeof(Exec_ID_Info_File_Name_txt));
	memset(Exec_ID_Info_File_Name,0,sizeof(Exec_ID_Info_File_Name));

}
/******************************************************************/

/******************************************************************/


