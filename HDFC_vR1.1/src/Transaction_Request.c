/*******************************************************************************

*******************************************************************************/
#include <header.h>
#include <ppp.h>
#include <curl.h>
#include <stdio.h>
#include <signal.h>
#include <easy.h>
/*******************************************************************/
//Declaration
/*******************************************************************/


/*******************************************************************/

/*******************************************************************/
int Upload_Transaction_Request()
{
	unsigned int Transaction_Status_rows;
	resultset Transaction_Status,Transaction_Ip;	
	unsigned int Transaction_Post_Request_Response;
	unsigned char Customer_Transaction_Url[250],Customer_Transaction_File[50];
	unsigned char Transaction_No_buffer[50];
	char Transaction_File_send[100];
	char Customer_Transaction[800];
	FILE * pFile;
	FILE * Transaction_response_myfile;
        int Trans_i;

	memset(&Transaction_Status,0,sizeof(Transaction_Status));
	memset(&Transaction_Ip,0,sizeof(Transaction_Ip));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Transaction_Ip=get_result("Select httpip,localhttpip,Current_Software_Ver from operationalstatus;");
	close_sqlite();	
        open_sqlite("/mnt/jffs2/hdfccctrans.db"); // open operational    
	Transaction_Status=get_result("SELECT Trans_Counter,Transaction_ID,Transaction_Details from unuploadedtransactiondetails WHERE Status='0';");
	if(Transaction_Status.rows>0)
	{
	for(Trans_i=0;Trans_i<Transaction_Status.rows;Trans_i++)
	{
		memset(Customer_Transaction_Url,0,sizeof(Customer_Transaction_Url));
#if URL_TEST
		sprintf(Customer_Transaction_Url,"%s/HHTRequest/v%s/RequestForTransaction.aspx",Transaction_Ip.recordset[0][0],Transaction_Ip.recordset[0][2]);
#endif
#if URL_NETMAGIC		
		sprintf(Customer_Transaction_Url,"%s/HHTRequest/v%s/RequestForTransaction.aspx",Transaction_Ip.recordset[0][0],Transaction_Ip.recordset[0][2]);
#endif
		memset(Customer_Transaction,0,sizeof(Customer_Transaction));
		sprintf(Customer_Transaction,"%s",Transaction_Status.recordset[Trans_i][2]);
		memset(Transaction_File_send,0,sizeof(Transaction_File_send));
		sprintf(Transaction_File_send,"/mnt/jffs2/%s_trans.txt",Transaction_Status.recordset[Trans_i][1]);	
	  	pFile = fopen (Transaction_File_send, "wb");
		fputs (Customer_Transaction,pFile);
	  	fclose (pFile);
               	memset(Customer_Transaction_File,0,sizeof(Customer_Transaction_File));
	        sprintf(Customer_Transaction_File,"/mnt/jffs2/%s.txt",Transaction_Status.recordset[Trans_i][1]);
		printf("\nCustomer_Transaction_File:%s",Customer_Transaction_File);
		Transaction_Post_Request_Response=curl_Customer_Transaction_Data(Customer_Transaction_Url,Transaction_File_send,Customer_Transaction_File);
		remove(Transaction_File_send);
		if(Transaction_Post_Request_Response==1)
		{
			Transaction_response_myfile = fopen(Customer_Transaction_File,"r");
			memset(Transaction_No_buffer,0,sizeof(Transaction_No_buffer));
			while (!feof(Transaction_response_myfile))
			{
				fgets(Transaction_No_buffer,50,Transaction_response_myfile);
				printf("\nResponse buffer:%s",Transaction_No_buffer);
				printf("\nResponse buffer length :%d",strlen(Transaction_No_buffer));
                        }
	                fclose(Transaction_response_myfile);
			if(strcmp(Transaction_No_buffer,"Success")==0)
			{
				lk_dispclr();
				lk_disptext(2,0,"    Transaction ",0);
				lk_disptext(3,0,"    Successful",0);
				sleep(1);
				close_sqlite();	
        			open_sqlite("/mnt/jffs2/hdfccctrans.db");
				execute("Update unuploadedtransactiondetails SET status='1' WHERE Transaction_ID='%s';",Transaction_Status.recordset[Trans_i][1]);
				execute("Delete FROM unuploadedtransactiondetails WHERE Status='1';");
				sleep(2);				
			}
			else if(strcmp(Transaction_No_buffer,"ErrorCode:0002")==0)
			{
				lk_dispclr();
				lk_disptext(2,0,"    Transaction ",0);
				lk_disptext(4,0,"      Failure",0);
			}
			else if(strcmp(Transaction_No_buffer,"ErrorCode:0006")==0)
			{
				lk_dispclr();
				lk_disptext(2,0,"    Low Price",0);
				lk_disptext(4,0,"  Upload Failure",0);
				sleep(2);
			}
			else if(strcmp(Transaction_No_buffer,"ErrorCode:0001")==0)
			{
				lk_dispclr();
				lk_disptext(2,0,"    Upload ",0);
				lk_disptext(4,0,"   Failure ",0);
				sleep(2);
			}
			else if(strcmp(Transaction_No_buffer,"ErrorCode:0003")==0)
			{
				lk_dispclr();
				lk_disptext(2,0,"     Transcation  ",0);
				lk_disptext(4,0,"    Already Exits ",0);
				sleep(2);
			}
			else 
			{
				lk_dispclr();
				lk_disptext(2,0,"    Transcation",0);
				lk_disptext(4,0,"      Failure  ",0);
				sleep(2);
			}		
		remove(Customer_Transaction_File);
		}
		else
		{			
			lk_dispclr();
			lk_disptext(2,0,"  Unable to Upload ",0);
			lk_disptext(3,0,"  Transaction save",0);
			lk_disptext(4,0,"  in offline mode",0);
			sleep(2);
			remove(Customer_Transaction_File);
 		}
		remove(Customer_Transaction_File);
	}
	}
	else
	{
		lk_dispclr();
		lk_disptext(2,0,"  All Transaction",0);
		lk_disptext(3,0,"     Uploaded",0);
		lk_disptext(4,0,"   Successfully",0);
		sleep(2);
	}
//return SUCCESS;
Dra_Menu();
}
/*******************************************************************/

/*******************************************************************/
int Upload_Transaction_Request_For_Thrd()
{
	resultset Transaction_Status_For_Thrd,Transaction_Ip_For_Thrd;	
	unsigned int Transaction_Post_Request_Response_For_Thrd;
	unsigned char Customer_Transaction_Url_For_Thrd[800],Customer_Transaction_Url_For_Thrd_Data[800],Customer_Transaction_File_For_Thrd[800];
	unsigned char Transaction_No_buffer_For_Thrd[100];
	FILE * Transaction_response_myfile_For_Thrd;
        int Trans_Thrd_i;
	char Transaction_File_send_Thrd[800];
	char Customer_Transaction_Thrd[800];
	FILE * pFile;

	memset(&Transaction_Status_For_Thrd,0,sizeof(Transaction_Status_For_Thrd));
	memset(&Transaction_Ip_For_Thrd,0,sizeof(Transaction_Ip_For_Thrd));

	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Transaction_Ip_For_Thrd=get_result("Select httpip,localhttpip,Current_Software_Ver from operationalstatus;");
	close_sqlite();	
        open_sqlite("/mnt/jffs2/hdfccctrans.db"); // open operational    
	Transaction_Status_For_Thrd=get_result("SELECT Trans_Counter,Transaction_ID,Transaction_Details from unuploadedtransactiondetails WHERE Status='0';");
	
	if(Transaction_Status_For_Thrd.rows>0)
	{
	for(Trans_Thrd_i=0;Trans_Thrd_i<Transaction_Status_For_Thrd.rows;Trans_Thrd_i++)
	{
 		memset(Customer_Transaction_Url_For_Thrd,0,sizeof(Customer_Transaction_Url_For_Thrd));
#if URL_TEST	
		sprintf(Customer_Transaction_Url_For_Thrd,"%s/HHTRequest/v%s/RequestForTransaction.aspx",Transaction_Ip_For_Thrd.recordset[0][0],Transaction_Ip_For_Thrd.recordset[0][2]);
#endif
#if URL_NETMAGIC
		sprintf(Customer_Transaction_Url_For_Thrd,"%s/HHTRequest/v%s/RequestForTransaction.aspx",Transaction_Ip_For_Thrd.recordset[0][0],Transaction_Ip_For_Thrd.recordset[0][2]);
#endif
		memset(Customer_Transaction_Thrd,0,sizeof(Customer_Transaction_Thrd));
		//sprintf(Customer_Transaction_Thrd,"Validate,%s",Transaction_Status_For_Thrd.recordset[Trans_Thrd_i][2]);
		sprintf(Customer_Transaction_Thrd,"%s",Transaction_Status_For_Thrd.recordset[Trans_Thrd_i][2]);
		//memset(Customer_Transaction_RSA_b64_Thrd,0,sizeof(Customer_Transaction_RSA_b64_Thrd));
		//sprintf(Customer_Transaction_RSA_b64_Thrd,"%s",RSAEncryption_Base64(Customer_Transaction_Thrd,strlen(Customer_Transaction_Thrd)));

		memset(Transaction_File_send_Thrd,0,sizeof(Transaction_File_send_Thrd));
		sprintf(Transaction_File_send_Thrd,"/mnt/jffs2/%s_trans.txt",Transaction_Status_For_Thrd.recordset[Trans_Thrd_i][1]);
		
	  	pFile = fopen (Transaction_File_send_Thrd, "wb");
		fputs (Customer_Transaction_Thrd,pFile);
	  	fclose (pFile);
               	memset(Customer_Transaction_File_For_Thrd,0,sizeof(Customer_Transaction_File_For_Thrd));
	        sprintf(Customer_Transaction_File_For_Thrd,"/mnt/jffs2/%s.txt",Transaction_Status_For_Thrd.recordset[Trans_Thrd_i][1]);
		printf("\nCustomer_Transaction_File_For_Thrd:%s",Customer_Transaction_File_For_Thrd);

		Transaction_Post_Request_Response_For_Thrd=curl_Customer_Transaction_Data(Customer_Transaction_Url_For_Thrd,Transaction_File_send_Thrd,Customer_Transaction_File_For_Thrd);
		remove(Transaction_File_send_Thrd);
		if(Transaction_Post_Request_Response_For_Thrd==1)
		{
				Transaction_response_myfile_For_Thrd = fopen(Customer_Transaction_File_For_Thrd,"r");
				memset(Transaction_No_buffer_For_Thrd,0,sizeof(Transaction_No_buffer_For_Thrd));
				while (!feof(Transaction_response_myfile_For_Thrd))
				{
					fgets(Transaction_No_buffer_For_Thrd,100,Transaction_response_myfile_For_Thrd);
					printf("\nResponse buffer_For_Thrd:%s",Transaction_No_buffer_For_Thrd);
					printf("\nResponse buffer length :%d",strlen(Transaction_No_buffer_For_Thrd));
                               	}
	                        fclose(Transaction_response_myfile_For_Thrd);
				if(strcmp(Transaction_No_buffer_For_Thrd,"Success")==0)
				{
					close_sqlite();	
        				open_sqlite("/mnt/jffs2/hdfccctrans.db");
					execute("Update unuploadedtransactiondetails SET status='1' WHERE Transaction_ID='%s';",Transaction_Status_For_Thrd.recordset[Trans_Thrd_i][1]);
					execute("Delete FROM unuploadedtransactiondetails WHERE Status='1';");
				}
		}
		remove(Customer_Transaction_File_For_Thrd);
	}
	}

return 0;
}


/******************************************************************/
int Upload_FollowUp_Request()
{
	unsigned int FollowUp_Status_rows;
	resultset FollowUp_Status,FollowUp_Ip;	
	unsigned int FollowUp_Post_Request_Response;
	unsigned char Customer_FollowUp_Url[800],Customer_FollowUp_send_file[800],Customer_FollowUp_File[800];
	char Customer_FollowUp[800];
	FILE * pFile;
	unsigned char FollowUp_No_buffer[50];
	FILE * FollowUp_response_myfile;
        int FollowUp_i,Machin_lock_key,Lost_Request_key,Lost_Request_rt,Machine_Request_key;


	memset(&FollowUp_Status,0,sizeof(FollowUp_Status));
	memset(&FollowUp_Ip,0,sizeof(FollowUp_Ip));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	FollowUp_Ip=get_result("Select httpip,localhttpip,Executive_id,Current_Software_Ver from operationalstatus;");
	close_sqlite();	
        open_sqlite("/mnt/jffs2/hdfccctrans.db"); // open operational    
//        FollowUp_Status=get_result("SELECT Executive_ID,HWID,Loan_No,followupdetails from unuploadedfollowupdetails WHERE FollowUp_Status='0';");
	FollowUp_Status=get_result("SELECT followupdetails,Counter from unuploadedfollowupdetails WHERE FollowUp_Status='0';");
	if(FollowUp_Status.rows>0)
	{
		for(FollowUp_i=0;FollowUp_i<FollowUp_Status.rows;FollowUp_i++)
		{
		memset(Customer_FollowUp_Url,0,sizeof(Customer_FollowUp_Url));
#if URL_TEST		
		sprintf(Customer_FollowUp_Url,"%s/HHTRequest/v%s/FollowUp.aspx",FollowUp_Ip.recordset[0][0],FollowUp_Ip.recordset[0][3]);
#endif
#if URL_NETMAGIC
		sprintf(Customer_FollowUp_Url,"%s/HHTRequest/v%s/FollowUp.aspx",FollowUp_Ip.recordset[0][0],FollowUp_Ip.recordset[0][3]);
#endif
		memset(Customer_FollowUp,0,sizeof(Customer_FollowUp));
		sprintf(Customer_FollowUp,"%s",FollowUp_Status.recordset[FollowUp_i][0]);
		//memset(Customer_FollowUp_RSA_b64,0,sizeof(Customer_FollowUp_RSA_b64));
		//sprintf(Customer_FollowUp_RSA_b64,"%s",RSAEncryption_Base64(Customer_FollowUp,strlen(Customer_FollowUp)));

		memset(Customer_FollowUp_send_file,0,sizeof(Customer_FollowUp_send_file));
		sprintf(Customer_FollowUp_send_file,"/mnt/jffs2/%s_ptp.txt",FollowUp_Status.recordset[FollowUp_i][1]);

	  	pFile = fopen (Customer_FollowUp_send_file, "wb");
		fputs (Customer_FollowUp,pFile);
	  	fclose (pFile);

               	memset(Customer_FollowUp_File,0,sizeof(Customer_FollowUp_File));
	        sprintf(Customer_FollowUp_File,"/mnt/jffs2/%s.txt",FollowUp_Status.recordset[FollowUp_i][1]);

#if PRINTF		
		printf("\nCustomer_FollowUp_Url:%s",Customer_FollowUp_Url);
		printf("\nCustomer_FollowUp_send_file:%s",Customer_FollowUp_send_file);	
		printf("\nCustomer_FollowUp_File:%s",Customer_FollowUp_File);	
#endif


		FollowUp_Post_Request_Response=curl_Customer_Feedback_Data(Customer_FollowUp_Url,Customer_FollowUp_send_file,Customer_FollowUp_File);
		remove(Customer_FollowUp_send_file);
		if(FollowUp_Post_Request_Response==1)
		{
			FollowUp_response_myfile = fopen(Customer_FollowUp_File,"r");
			memset(FollowUp_No_buffer,0,sizeof(FollowUp_No_buffer));
			while (!feof(FollowUp_response_myfile))
			{
				fgets(FollowUp_No_buffer,50,FollowUp_response_myfile);
				printf("\nResponse buffer:%s",FollowUp_No_buffer);
				printf("\nResponse buffer length :%d",strlen(FollowUp_No_buffer));
                       	}
	                fclose(FollowUp_response_myfile);
			if(strcmp(FollowUp_No_buffer,"Success")==0)
			{
				lk_dispclr();
				lk_disptext(2,0,"      FollowUp",0);
				lk_disptext(4,0," Successfully Updated",0);
				close_sqlite();	
        			open_sqlite("/mnt/jffs2/hdfccctrans.db");
				execute("Update unuploadedfollowupdetails SET FollowUp_status='1' WHERE Counter='%ld';",atol(FollowUp_Status.recordset[FollowUp_i][1]));
				execute("Delete FROM unuploadedfollowupdetails WHERE FollowUp_Status='1';");
				sleep(2);
			}
			else if(strcmp(FollowUp_No_buffer,"ErrorCode:0002")==0)
			{
				lk_dispclr();
				lk_disptext(2,0,"  Feedback ",0);
				lk_disptext(4,0,"      Failure",0);
			}
			else if(strcmp(FollowUp_No_buffer,"ErrorCode:0006")==0)
			{
				lk_dispclr();
				lk_disptext(2,0,"    Low Price",0);
				lk_disptext(4,0,"  Upload Failure",0);
				sleep(2);
			}
			else if(strcmp(FollowUp_No_buffer,"ErrorCode:0001")==0)
			{
				lk_dispclr();
				lk_disptext(2,0,"    Upload ",0);
				lk_disptext(4,0,"   Failure ",0);
				sleep(2);
			}
			else if(strcmp(FollowUp_No_buffer,"ErrorCode:0003")==0)
			{
				lk_dispclr();
				lk_disptext(2,0,"       FollowUp   ",0);
				lk_disptext(4,0,"    Already Exits",0);
				sleep(2);
			}
			else if(strcmp(FollowUp_No_buffer,"ErrorCode:0007")==0)
			{
Machine_locked:
				lk_dispclr();
				lk_disptext(2,0,"  Machine is Locked",0);
				lk_disptext(4,0,"  Please contact HO",0);
				lk_disptext(5,0,"Press Enter to Cont.",0);				
				Machin_lock_key = lk_getkey();
				if(Machin_lock_key==ENTER)
				{
				close_sqlite();	
        			open_sqlite("/mnt/jffs2/hdfccctrans.db");
				execute("Update unuploadedfollowupdetails SET FollowUp_status='1' WHERE Counter='%ld';",atol(FollowUp_Status.recordset[FollowUp_i][1]));
				close_sqlite();						
				open_sqlite("/mnt/jffs2/hdfcccoperational.db");
				execute("UPDATE operationalstatus SET ExecutiveStatus='0';");
				remove(Customer_FollowUp_File);				
				Dra_Menu();
				}
				else goto Machine_locked;		
			}
			else if(strcmp(FollowUp_No_buffer,"ErrorCode:0009")==0)
			{
Machine_Error_Request:
				lk_dispclr();
				lk_disptext(2,0,"  Machine Surrendered",0);
				lk_disptext(3,0,"  Please contact HO",0);			
				lk_disptext(5,0,"Press Enter to Cont.",0);				
				Machine_Request_key = lk_getkey();
				if(Machine_Request_key==ENTER)
				{
				close_sqlite();	
        			open_sqlite("/mnt/jffs2/hdfccctrans.db");
				execute("Update unuploadedfollowupdetails SET FollowUp_status='1' WHERE Counter='%ld';",atol(FollowUp_Status.recordset[FollowUp_i][1]));
				close_sqlite();						
				open_sqlite("/mnt/jffs2/hdfcccoperational.db");
				execute("UPDATE operationalstatus SET ExecutiveStatus='0';");
				remove(Customer_FollowUp_File);
				Dra_Menu();
				}
				else goto Machine_Error_Request; 
			}
			else if(strcmp(FollowUp_No_buffer,"ErrorCode:00010")==0)
			{
Lost_Request_rt:
				lk_dispclr();
				lk_disptext(2,0,"  Machine Lost,",0);
				lk_disptext(3,0,"  Please contact HO",0);
				lk_disptext(5,0,"Press Enter to Cont.",0);				
				Lost_Request_key = lk_getkey();
				if(Lost_Request_key==ENTER)
				{
				close_sqlite();	
        			open_sqlite("/mnt/jffs2/hdfccctrans.db");
				execute("Update unuploadedfollowupdetails SET FollowUp_status='1' WHERE Counter='%ld';",atol(FollowUp_Status.recordset[FollowUp_i][1]));
				close_sqlite();						
				open_sqlite("/mnt/jffs2/hdfcccoperational.db");
				execute("UPDATE operationalstatus SET ExecutiveStatus='0';");
				remove(Customer_FollowUp_File);
				Dra_Menu();
				}
				else goto Lost_Request_rt; 
			}
                        else 
			{
				lk_dispclr();
				lk_disptext(2,0,"  FollowUp ",0);
				lk_disptext(4,0,"    Failure",0);
				sleep(2);
			}		
			remove(Customer_FollowUp_File);
		}
		else
		{			

			lk_dispclr();
			lk_disptext(2,0,"  Unable to Upload ",0);
			lk_disptext(3,0,"  FollowUp save ",0);
			lk_disptext(4,0,"  in offline mode",0);			
			sleep(2);
			remove(Customer_FollowUp_File);
 		}
		remove(Customer_FollowUp_File);
		}
	}
	else
	{
		lk_dispclr();
		lk_disptext(2,0,"All Feedback Uploaded",0);
		lk_disptext(4,0,"   Successfully",0);
		sleep(2);
	}
return SUCCESS;
}
/*******************************************************************/

/*******************************************************************/
int Upload_FollowUp_Request_For_Thrd()
{
	resultset FollowUp_Status_For_Thrd,FollowUp_Ip_For_Thrd;	
	unsigned int FollowUp_Post_Request_Response_For_Thrd;
	unsigned char Customer_FollowUp_Url_For_Thrd[800],Customer_FollowUp_send_file_For_Thrd[800],Customer_FollowUp_File_For_Thrd[800];
	char Customer_FollowUp_For_Thrd[800],Customer_FollowUp_RSA_b64_For_Thrd[800];
	FILE * pFile;
	unsigned char FollowUp_No_buffer_For_Thrd[50];
	FILE * FollowUp_response_myfile_For_Thrd;
        int FollowUp_Thrd_i;

	memset(&FollowUp_Status_For_Thrd,0,sizeof(FollowUp_Status_For_Thrd));
	memset(&FollowUp_Ip_For_Thrd,0,sizeof(FollowUp_Ip_For_Thrd));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	FollowUp_Ip_For_Thrd=get_result("Select httpip,Localhttpip,Executive_id,Current_Software_Ver from operationalstatus;");
	close_sqlite();	
        open_sqlite("/mnt/jffs2/hdfccctrans.db"); // open operational    
        FollowUp_Status_For_Thrd=get_result("SELECT  followupdetails,Counter from unuploadedfollowupdetails WHERE FollowUp_Status='0';");
	if(FollowUp_Status_For_Thrd.rows>0)
	{
		for(FollowUp_Thrd_i=0;FollowUp_Thrd_i<FollowUp_Status_For_Thrd.rows;FollowUp_Thrd_i++)
		{
		memset(Customer_FollowUp_Url_For_Thrd,0,sizeof(Customer_FollowUp_Url_For_Thrd));
#if URL_TEST
		sprintf(Customer_FollowUp_Url_For_Thrd,"%s/HHTRequest/v%s/FollowUp.aspx",FollowUp_Ip_For_Thrd.recordset[0][0],FollowUp_Ip_For_Thrd.recordset[0][3]);
#endif
#if URL_NETMAGIC
		sprintf(Customer_FollowUp_Url_For_Thrd,"%s/HHTRequest/v%s/FollowUp.aspx",FollowUp_Ip_For_Thrd.recordset[0][0],FollowUp_Ip_For_Thrd.recordset[0][3]);
#endif			

		memset(Customer_FollowUp_For_Thrd,0,sizeof(Customer_FollowUp_For_Thrd));
		sprintf(Customer_FollowUp_For_Thrd,"%s",FollowUp_Status_For_Thrd.recordset[FollowUp_Thrd_i][0]);
		//memset(Customer_FollowUp_RSA_b64_For_Thrd,0,sizeof(Customer_FollowUp_RSA_b64_For_Thrd));
		//sprintf(Customer_FollowUp_RSA_b64_For_Thrd,"%s",RSAEncryption_Base64(Customer_FollowUp_For_Thrd,strlen(Customer_FollowUp_For_Thrd)));

		memset(Customer_FollowUp_send_file_For_Thrd,0,sizeof(Customer_FollowUp_send_file_For_Thrd));
		sprintf(Customer_FollowUp_send_file_For_Thrd,"/mnt/jffs2/%s_ptp.txt",FollowUp_Status_For_Thrd.recordset[FollowUp_Thrd_i][1]);
	  	pFile = fopen (Customer_FollowUp_send_file_For_Thrd, "wb");
		fputs (Customer_FollowUp_For_Thrd,pFile);
	  	fclose (pFile);

               	memset(Customer_FollowUp_File_For_Thrd,0,sizeof(Customer_FollowUp_File_For_Thrd));
	        sprintf(Customer_FollowUp_File_For_Thrd,"/mnt/jffs2/%s.txt",FollowUp_Status_For_Thrd.recordset[FollowUp_Thrd_i][1]);
#if PRINTF
		printf("\nCustomer_FollowUp_Url_For_Thrd:%s",Customer_FollowUp_Url_For_Thrd);
		printf("\nCustomer_FollowUp_send_file_For_Thrd:%s",Customer_FollowUp_send_file_For_Thrd);	
		printf("\nCustomer_FollowUp_File_For_Thrd:%s",Customer_FollowUp_File_For_Thrd);	
#endif

		FollowUp_Post_Request_Response_For_Thrd=curl_Customer_Feedback_Data(Customer_FollowUp_Url_For_Thrd,Customer_FollowUp_send_file_For_Thrd,Customer_FollowUp_File_For_Thrd);
		remove(Customer_FollowUp_send_file_For_Thrd);
		if(FollowUp_Post_Request_Response_For_Thrd==1)
		{
			FollowUp_response_myfile_For_Thrd = fopen(Customer_FollowUp_File_For_Thrd,"r");
			memset(FollowUp_No_buffer_For_Thrd,0,sizeof(FollowUp_No_buffer_For_Thrd));
			while (!feof(FollowUp_response_myfile_For_Thrd))
			{
				fgets(FollowUp_No_buffer_For_Thrd,50,FollowUp_response_myfile_For_Thrd);
				printf("\nResponse buffer_For_Thrd:%s",FollowUp_No_buffer_For_Thrd);
				printf("\nResponse buffer length _For_Thrd:%d",strlen(FollowUp_No_buffer_For_Thrd));
                       	}
                        fclose(FollowUp_response_myfile_For_Thrd);
			if(strcmp(FollowUp_No_buffer_For_Thrd,"Success")==0)
			{
				close_sqlite();	
       				open_sqlite("/mnt/jffs2/hdfccctrans.db");
				execute("Update unuploadedfollowupdetails SET FollowUp_status='1' WHERE Counter='%ld';",atol(FollowUp_Status_For_Thrd.recordset[FollowUp_Thrd_i][1]));
				execute("Delete FROM unuploadedfollowupdetails WHERE FollowUp_Status='1';");
				sleep(2);
			}
		}
		else
		{
			lk_dispclr();
			lk_disptext(2,0,"   Upload FollowUp ",0);
			lk_disptext(3,0,"       Failure",0);
			//lk_disptext(4,0,"   GPRS Connected",0);
			sleep(2);
			remove(Customer_FollowUp_File_For_Thrd);
 		}		
		}
	}
return SUCCESS;
}

/*****************************************************************************/
int Lock_password_Request()
{
	unsigned int FollowUp_Status_rows;
	resultset Password_Status,Password_Ip;	
	unsigned int lockpassword_Post_Request_Response;
	unsigned char Customer_password_Url[500],Customer_password_send_file[500],Customer_password_File[500];
	char Customer_password[500],Customer_FollowUp_RSA_b64[500];
	FILE * pFile;
	unsigned char lockpassword_buffer[50];
	FILE * lockpassword_response_myfile;
        int Password_i;
	int Lock_key,Bad_key,Error_key,Executive_key,Try_key;

	memset(&Password_Status,0,sizeof(Password_Status));
	memset(&Password_Ip,0,sizeof(Password_Ip));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Password_Ip=get_result("Select httpip,localhttpip,Executive_id,Current_Software_Ver from operationalstatus;");
	close_sqlite();	
        open_sqlite("/mnt/jffs2/hdfccctrans.db"); // open operational    

	Password_Status=get_result("SELECT lockpassword_details,Counter from lockpassword_details WHERE lockpassword_Status='0';");
	if(Password_Status.rows>0)
	{
		for(Password_i=0;Password_i<Password_Status.rows;Password_i++)
		{
		memset(Customer_password_Url,0,sizeof(Customer_password_Url));
#if URL_TEST		
		sprintf(Customer_password_Url,"%s/HHTRequest/v%s/lockpassword.aspx",Password_Ip.recordset[0][0],Password_Ip.recordset[0][3]);
#endif
#if URL_NETMAGIC
		sprintf(Customer_password_Url,"%s/HHTRequest/v%s/lockpassword.aspx",Password_Ip.recordset[0][0],Password_Ip.recordset[0][3]);
#endif
		memset(Customer_password,0,sizeof(Customer_password));
		sprintf(Customer_password,"%s",Password_Status.recordset[Password_i][0]);
		

		memset(Customer_password_send_file,0,sizeof(Customer_password_send_file));
		sprintf(Customer_password_send_file,"/mnt/jffs2/%s_pass.txt",Password_Status.recordset[Password_i][1]);

	  	pFile = fopen (Customer_password_send_file, "wb");
		fputs (Customer_password,pFile);
	  	fclose (pFile);

               	memset(Customer_password_File,0,sizeof(Customer_password_File));
	        sprintf(Customer_password_File,"/mnt/jffs2/%s.txt",Password_Status.recordset[Password_i][1]);

#if PRINTF	
		printf("\nCustomer_Password_Url:%s",Customer_password_Url);
		printf("\nCustomer_Password_Url_send:%s",Customer_password_send_file);	
		printf("\nCustomer_Password_File:%s",Customer_password_File);	
#endif

		lockpassword_Post_Request_Response=curl_Lockpassword_url_request(Customer_password_Url,Customer_password_send_file,Customer_password_File);
		//remove(Customer_FollowUp_send_file);
		if(lockpassword_Post_Request_Response==1)
		{
			lockpassword_response_myfile = fopen(Customer_password_File,"r");
			memset(lockpassword_buffer,0,sizeof(lockpassword_buffer));
			while (!feof(lockpassword_response_myfile))
			{
				fgets(lockpassword_buffer,50,lockpassword_response_myfile);
				
                       	}
	                fclose(lockpassword_response_myfile);
			if(strcmp(lockpassword_buffer,"Success")==0)
			{
				close_sqlite();	
        			open_sqlite("/mnt/jffs2/hdfccctrans.db");
				execute("Update lockpassword_details SET lockpassword_Status='1' WHERE Counter='%ld';",atol(Password_Status.recordset[Password_i][1]));
				execute("Delete FROM lockpassword_details WHERE lockpassword_Status='1';");
				remove(Customer_password_File);
				remove(Customer_password_send_file);
				sleep(2);
			}		
			else if(strcmp(lockpassword_buffer,"ErrorCode:0003")==0)
			{
Lock_password:
				lk_dispclr();
				lk_disptext(2,0,"  Lock Password",0);
				lk_disptext(4,0,"     Failure",0);
				lk_disptext(5,0,"Press Enter to Cont.",0);
				Lock_key = lk_getkey();
				if(Lock_key==ENTER)
				{			
				remove(Customer_password_File);
				remove(Customer_password_send_file);
				return 0;
				}
				else goto Lock_password;
			}				
			else if(strcmp(lockpassword_buffer,"ErrorCode:0001")==0)
			{
Bad_request:
				lk_dispclr();
				lk_disptext(2,0,"    Bad Request ",0);
				lk_disptext(5,0,"Press Enter to Cont.",0);
				Bad_key = lk_getkey();
				if(Bad_key==ENTER)
				{
				remove(Customer_password_File);
				remove(Customer_password_send_file);
				return 0;
				}
				else goto Bad_request;
			}
			else if(strcmp(lockpassword_buffer,"ErrorCode:0004")==0)
			{
Error_Request_pass:
				lk_dispclr();
				lk_disptext(2,0,"  Error in Processing",0);
				lk_disptext(4,0,"      Request",0);
				lk_disptext(5,0,"Press Enter to Cont.",0);
				Error_key = lk_getkey();
				if(Error_key==ENTER)
				{			
				remove(Customer_password_File);
				remove(Customer_password_send_file);
				return 0;
				}
				else goto Error_Request_pass;
			}
			else if(strcmp(lockpassword_buffer,"ErrorCode:0007")==0)
			{
ExecutiveID_lock:
				lk_dispclr();
				lk_disptext(2,0," Executive ID",0);
				lk_disptext(4,0,"     Lock",0);	
				lk_disptext(5,0,"Press Enter to Cont.",0);		
				execute("Update operationalstatus SET password_status='0';");
				Executive_key = lk_getkey();
				if(Executive_key==ENTER)
				{
				remove(Customer_password_File);
				remove(Customer_password_send_file);
				return 0;
				}
				else goto ExecutiveID_lock;
			}
			else
			{
Pass_Try_Again:
				lk_dispclr();
				lk_disptext(2,0,"    Please Try",0);
				lk_disptext(4,0,"    Again Later",0);
				lk_disptext(5,0,"Press Enter to Cont.",0);
				Try_key = lk_getkey();
				if(Try_key==ENTER)
				{
				remove(Customer_password_File);
				remove(Customer_password_send_file);
				return 0;
				}
				else goto Pass_Try_Again;
			}
		}
		else
		{
Again_laster:
			lk_dispclr();
			lk_disptext(2,0,"    Please Try",0);
			lk_disptext(4,0,"    Again Later",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			Try_key = lk_getkey();
			if(Try_key==ENTER)
			{
			remove(Customer_password_File);
			remove(Customer_password_send_file);			
			return 0;
			}
			else goto Again_laster;
		}
		}
	}
return SUCCESS;

}


