/*******************************************************************/
//			     HDFC Bank CC
//		        Customer profile.c
/*******************************************************************/
/**
 *
 * @author ftl003
**/
#include <header.h>
/*******************************************************************/
/******************************************************************/
int After_selecting_customer(char* contract_no,char* Flag_case)
{
	int opt=0;
        MENU_T menu;
        int selItem  = 0;
        int acceptKbdEvents = TRUE;
        int maxEntries = 0;
        short scroll = 0;	
	
	printf("\ncontract_no:%s",contract_no);
	printf("\nFlag_case:%s",Flag_case);

	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       3;
        maxEntries                      =       menu.maxEntries;

	strcpy(menu.title," Customer Profile");                    
        strcpy(menu.menu[0],"1.Payment");
	strcpy(menu.menu[1],"2.Follow Up");
	strcpy(menu.menu[2],"3.Customer Details");
	

	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case CANCEL:	Dra_Menu();
				break;

		case ENTER:
			switch (selItem+1 )
                    	{
			case 1:	
#if PRINTF
				printf("\ncontract_no:%s",contract_no); 
#endif
				Payment(contract_no,Flag_case);
				break;
			case 2: Follow_Up(contract_no,Flag_case);
				break;
			case 3:
#if PRINTF
				printf("\ncontract_no:%s",contract_no);
				printf("\nFlag_case:%s",Flag_case);
#endif
				if(strcmp(Flag_case,"D")==0) Customer_Details(contract_no,Flag_case);
				else Pickup_Customer_Details(contract_no,Flag_case);
				break;			
			default : break;
			}		
		default : break;
		}//switch
	}//while
	return 0;
}
