#ifndef protobuf-c-config_H
#define protobuf-c-config_H

#cmakedefine CMAKE_HAVE_PTHREAD_H 1
#cmakedefine HAVE_ALLOCA_H 1
#cmakedefine HAVE_MALLOC_H 1
#cmakedefine HAVE_SYS_POLL_H 1
#cmakedefine HAVE_SYS_SELECT_H 1
#cmakedefine HAVE_INTTYPES_H 1
#cmakedefine HAVE_SYS_UIO_H 1
#cmakedefine HAVE_UNISTD_H 1
#cmakedefine HAVE_IO_H 1
#cmakedefine WIN32 1

#endif
