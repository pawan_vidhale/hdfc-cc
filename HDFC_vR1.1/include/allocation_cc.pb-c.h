/* Generated by the protocol buffer compiler.  DO NOT EDIT! */

#ifndef PROTOBUF_C_allocation_5fcc_2eproto__INCLUDED
#define PROTOBUF_C_allocation_5fcc_2eproto__INCLUDED

#include "protobuf-c/protobuf-c.h"

PROTOBUF_C_BEGIN_DECLS


typedef struct _ALLOC__Allocation ALLOC__Allocation;
typedef struct _ALLOC__Entry ALLOC__Entry;
typedef struct _ALLOC__CBH ALLOC__CBH;
typedef struct _ALLOC__TH ALLOC__TH;


/* --- enums --- */


/* --- messages --- */

struct  _ALLOC__Allocation
{
  ProtobufCMessage base;
  size_t n_entry;
  ALLOC__Entry **entry;
};
#define ALLOC__ALLOCATION__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&alloc__allocation__descriptor) \
    , 0,NULL }


struct  _ALLOC__Entry
{
  ProtobufCMessage base;
  char *branch_code;
  char *card_number;
  char *alt_account_no;
  char *bill_cycle;
  char *count_pay_currcyc;
  char *delinquency_string;
  char *cust_id;
  char *address_m1;
  char *address_m2;
  char *address_m3;
  char *address_m4;
  char *m_address_city;
  char *m_address_state;
  char *m_address_zip;
  char *m_address_lm;
  char *home_phone;
  char *mobile_no;
  char *gender;
  char *ss_name;
  char *emp_code;
  char *prs_email_id;
  char *off_email_id;
  char *emp_name;
  char *designation;
  char *band;
  char *work_phone;
  char *work_phone_e;
  char *current_bl;
  char *memo_bl;
  char *total_past_due;
  char *total_curr_due;
  char *total_amt_due;
  char *overlimit_amt;
  char *date_l_delinquency;
  char *date_l_r_cheque;
  char *no_of_autop_ret;
  char *l_smt_bng_bal;
  char *l_smt_eng_bal;
  char *l_smt_past_due;
  char *l_smt_tmt_pay_due;
  char *no_of_autop_ret_lstat;
  char *at_ret_amt_lstat;
  char *lstat_curr_due;
  char *date_of_lpay;
  char *amt_of_lpay;
  char *date_of_lpur;
  char *amt_lpur;
  char *date_l_cycle;
  char *ltd_retail_puramt;
  char *ltd_retail_retamt;
  char *ltd_cash_trs_amt;
  char *address_l1;
  char *address_l2;
  char *address_l3;
  char *address_l4;
  char *address_alt_city;
  char *address_alt_state;
  char *address_alt_zip;
  char *pan_no;
  char *m_aadress_time_cc;
  char *address_p1;
  char *address_p2;
  char *address_p3;
  char *address_p_city;
  char *address_p_zip;
  char *address_p_lmark;
  char *address_p_telpno;
  char *cpy_cat_code;
  char *occ_type;
  char *region_code;
  char *loan_bal;
  char *date_of_addresschange;
  char *date_of_lstat;
  char *date_of_nstat;
  char *loaded_bal;
  char *bucket;
  char *bucket_dest;
  char *emp_address1;
  char *emp_address2;
  char *emp_address3;
  char *emp_address4;
  char *emp_address_pin;
  char *emp_address_lm;
  char *emp_address_city;
  char *emp_tele_no;
  char *addon_card1;
  char *addon_card2;
  char *addon_card3;
  char *addon_card4;
  char *date_acc_charged_off;
  char *extra1;
  char *agence_name;
  char *agence_code;
  char *filler1;
  char *filler2;
  char *filler3;
  char *filler4;
  size_t n_cbh;
  ALLOC__CBH **cbh;
  size_t n_th;
  ALLOC__TH **th;
};
#define ALLOC__ENTRY__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&alloc__entry__descriptor) \
    , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0,NULL, 0,NULL }


struct  _ALLOC__CBH
{
  ProtobufCMessage base;
  char *instrument_type;
  char *cheque_no;
  char *cheque_date;
  char *cheque_amt;
  char *presentation_date;
  char *bank_name;
  char *bounce_reason;
  char *returned_date;
  char *c_process_status;
  char *c_download_date;
  char *c_process_date;
};
#define ALLOC__CBH__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&alloc__cbh__descriptor) \
    , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL }


struct  _ALLOC__TH
{
  ProtobufCMessage base;
  char *contact_date;
  char *contact_mode;
  char *contact_place;
  char *person_contacted;
  char *action_code;
  char *action_amount;
  char *remarks;
  char *contacted_by;
  char *userid;
  char *case_clear_flag;
  char *contact_time;
  char *contact_objective;
  char *ptp_date;
  char *h_unit_id;
  char *h_download_date;
  char *h_process_status;
  char *h_process_date;
};
#define ALLOC__TH__INIT \
 { PROTOBUF_C_MESSAGE_INIT (&alloc__th__descriptor) \
    , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL }


/* ALLOC__Allocation methods */
void   alloc__allocation__init
                     (ALLOC__Allocation         *message);
size_t alloc__allocation__get_packed_size
                     (const ALLOC__Allocation   *message);
size_t alloc__allocation__pack
                     (const ALLOC__Allocation   *message,
                      uint8_t             *out);
size_t alloc__allocation__pack_to_buffer
                     (const ALLOC__Allocation   *message,
                      ProtobufCBuffer     *buffer);
ALLOC__Allocation *
       alloc__allocation__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   alloc__allocation__free_unpacked
                     (ALLOC__Allocation *message,
                      ProtobufCAllocator *allocator);
/* ALLOC__Entry methods */
void   alloc__entry__init
                     (ALLOC__Entry         *message);
size_t alloc__entry__get_packed_size
                     (const ALLOC__Entry   *message);
size_t alloc__entry__pack
                     (const ALLOC__Entry   *message,
                      uint8_t             *out);
size_t alloc__entry__pack_to_buffer
                     (const ALLOC__Entry   *message,
                      ProtobufCBuffer     *buffer);
ALLOC__Entry *
       alloc__entry__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   alloc__entry__free_unpacked
                     (ALLOC__Entry *message,
                      ProtobufCAllocator *allocator);
/* ALLOC__CBH methods */
void   alloc__cbh__init
                     (ALLOC__CBH         *message);
size_t alloc__cbh__get_packed_size
                     (const ALLOC__CBH   *message);
size_t alloc__cbh__pack
                     (const ALLOC__CBH   *message,
                      uint8_t             *out);
size_t alloc__cbh__pack_to_buffer
                     (const ALLOC__CBH   *message,
                      ProtobufCBuffer     *buffer);
ALLOC__CBH *
       alloc__cbh__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   alloc__cbh__free_unpacked
                     (ALLOC__CBH *message,
                      ProtobufCAllocator *allocator);
/* ALLOC__TH methods */
void   alloc__th__init
                     (ALLOC__TH         *message);
size_t alloc__th__get_packed_size
                     (const ALLOC__TH   *message);
size_t alloc__th__pack
                     (const ALLOC__TH   *message,
                      uint8_t             *out);
size_t alloc__th__pack_to_buffer
                     (const ALLOC__TH   *message,
                      ProtobufCBuffer     *buffer);
ALLOC__TH *
       alloc__th__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data);
void   alloc__th__free_unpacked
                     (ALLOC__TH *message,
                      ProtobufCAllocator *allocator);
/* --- per-message closures --- */

typedef void (*ALLOC__Allocation_Closure)
                 (const ALLOC__Allocation *message,
                  void *closure_data);
typedef void (*ALLOC__Entry_Closure)
                 (const ALLOC__Entry *message,
                  void *closure_data);
typedef void (*ALLOC__CBH_Closure)
                 (const ALLOC__CBH *message,
                  void *closure_data);
typedef void (*ALLOC__TH_Closure)
                 (const ALLOC__TH *message,
                  void *closure_data);

/* --- services --- */


/* --- descriptors --- */

extern const ProtobufCMessageDescriptor alloc__allocation__descriptor;
extern const ProtobufCMessageDescriptor alloc__entry__descriptor;
extern const ProtobufCMessageDescriptor alloc__cbh__descriptor;
extern const ProtobufCMessageDescriptor alloc__th__descriptor;

PROTOBUF_C_END_DECLS


#endif  /* PROTOBUF_allocation_5fcc_2eproto__INCLUDED */
