
#include "../include/sqlite3.h"
#include "../include/header.h"

#define ELEMENT_NUM(_array) ( sizeof(_array)/sizeof(_array[0]) )

sqlite3 *db_conn;
int ret = 0;
char OPEN_FLAG = 0;

int open_sqlite(char *db_name)
{
	if(OPEN_FLAG == 0)
	{
		if(sqlite3_open(db_name, &db_conn) == SQLITE_OK)
		{
			OPEN_FLAG = 1;
			printf("\nOPEN_FALG = %d\n",OPEN_FLAG);
			printf("database opened\n");
			return 1;
		}
   		else
		{
			printf("database opening failed\n");
      	  		return 0;
		}
	}
	else
	{
		printf("\ndatabase already opened\n");
		return 1;
	}
	return 1;
}
/******************************************************************************/

int close_sqlite()
{
	if(OPEN_FLAG == 1)
	{
		if(sqlite3_close(db_conn) == SQLITE_OK)
		{
			printf("database closed\n");
			OPEN_FLAG = 0;
			printf("OPEN_FLAG = %d\n",OPEN_FLAG);
        		return 1;
		}
		else
		{
			printf("database closing failed\n");
        		return 0;
		}
	}
	else
	{
		printf("database not yet opened\n");
		return 1;
	}
}

//only type s and d are allowed as arguments
int execute(const char* fmt, ...)
{
	char *err_messg;
	int ret=0, result = 0;
	
	char sql_string[2000];//this honestly needs to be more elegant; will do for now
	va_list args;	
	va_start(args, fmt);


	memset(sql_string,0,sizeof(sql_string));

	sql_string[0] = '\0';
	ret = vsprintf(sql_string, fmt, args);
	va_end(args);	
	err_printf(sql_string);//
	
	if(!ret)
		result = 0;
	else
		result = 1;
	if(result != -1)
	{
		if(sqlite3_exec(db_conn, sql_string, NULL, 0, &err_messg) == SQLITE_OK)
		{
			result = 1;
		}
		else
		{
			fprintf(stdout,"SQL error: %s\n", err_messg);
			result = 0;
		}
	}
		
	return result;
}

//you must open_sqlite first before using execute_file
int execute_file(char *filename)
{
	char *err_messg;
	FILE *read_fd = (FILE *) 0;
	char sql_string[1024];
	
	ret = 0;
	
	read_fd = fopen (filename, "r");//open file for read	
	if (read_fd != NULL)
	{
		rewind(read_fd);
		while(!feof (read_fd))
   		{
			m_fgets(sql_string, 1024, read_fd);
			
			//ie if string is not empty, then execute - ha no more newline errors!
			if(strcmp(sql_string, "") != 0)
			{
				//err_printf("SQL_STRING: %s\n", sql_string);	
				
				if(sqlite3_exec(db_conn, sql_string, NULL, 0, &err_messg) == SQLITE_OK)
				{
					ret = 1;
					continue;
				}
				else
				{
					fprintf(stdout,"SQL error: %s\n", err_messg);
					ret = 0;
					break;
				}
			}
		}
	}
	
	fclose(read_fd);	
    	return ret;
}

char *m_fgets(char *line, int n, FILE *fd)
{
	int c = 0;
  	char *cstring;

  	cstring = line;
  	while(--n>0 && ( c = getc(fd) ) != EOF)
  	{
		if (c == '\n')
			break;

		*cstring++ = c;
  	}
	*cstring++ = '\0';
	
  	if (c == EOF && cstring == line)//ie nothing in file!
		line = NULL;
	
	if (c == EOF)
		line = NULL;
  	
  	return line;
}

resultset get_result(const char* fmt, ...)
{
	int success = 0;
	int nrow=0, ncol=0, i=0, j=0, count=0;
	char *err_messg;
	char **result;
	char ***recordset;
	resultset resultset_table;
	
	char sql_string[1500];//this honestly needs to be more elegant; will do for now
	va_list args;	
	va_start(args, fmt);
	sql_string[0] = '\0';
	ret = vsprintf(sql_string, fmt, args);
	va_end(args);	
	fprintf(stdout,"\n%s\n", sql_string);

	
	//initialize resultset_table;
	resultset_table.rows = 0;
	resultset_table.cols = 0;
	resultset_table.recordset = NULL;
	
	ret = sqlite3_get_table(
					db_conn,              
					sql_string,
					&result,
					&nrow,
					&ncol,
					&err_messg
				);
	
	fprintf(stdout,"nrow=%d ncol=%d\n",nrow,ncol);
	
	recordset = (char ***)malloc(nrow * sizeof(char **));
	for(count=ncol; count<((nrow + 1)*ncol); count++)
	{
		recordset[i] = (char **)malloc(ncol * sizeof(char *));
		for(j=0; j<ncol; j++)
		{
			err_printf("%s ",result[count]);//
			recordset[i][j] = (char *) malloc( (strlen(result[count]) + 1) );
			strcpy(recordset[i][j], result[count]);
			
			if(j != (ncol - 1))
				count++;
		}
		i++;
		err_printf("\n");//
	}
	sqlite3_free_table(result);
	
	if( ret != SQLITE_OK )
	{
		fprintf(stdout,"SQL error: %s\n", err_messg);
		success = 0; 
	}
	else
	{
		resultset_table.rows = nrow;
		resultset_table.cols = ncol;
		resultset_table.recordset = recordset;
		success = 1;
	}
	
    return resultset_table;
}

//will free all allocd memory ie only recordset memory (since only that allocd)
void free_result(resultset resultset_table)
{
	int i=0,j=0;

	if(resultset_table.recordset != NULL)
        {
                for(i=0;i<resultset_table.rows;i++)
                {
                        for(j=0;j<resultset_table.cols;j++)
                        {
                                free(resultset_table.recordset[i][j]);
                        }
                        free(resultset_table.recordset[i]);
                }
                free(resultset_table.recordset);
        }
}

//if DEBUG is on then print message to stderr using fprintf function
int err_printf(const char *fmt, ...)
{
        int i;
                                                                                                 
        va_list ap;
        va_start(ap, fmt);
        i = vfprintf(stderr, fmt, ap);
        va_end(ap);
                                                                                                 
        return i;
}
//returns the row index in the resultset that was selected
//column is the column of the resultset that you want to display

