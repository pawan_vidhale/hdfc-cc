/*******************************************************************/
//			    HDFC Bank CC
//			    Customer Pickup.c
/*******************************************************************/
//@author FTL003
#include <header.h>

/*******************************************************************/
int Pickup_Customer_Details(char* Pickup_Contract_no,char* Pickup_Flag_case)
{
	int opt=0;
        MENU_T menu;
        int selItem  = 0;
        int acceptKbdEvents = TRUE;
        int maxEntries = 0;
        short scroll = 0;		
	

	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       6;
        maxEntries                      =       menu.maxEntries;

	strcpy(menu.title," Pickup Profile");                    
        strcpy(menu.menu[0],"1.Card no.");
	strcpy(menu.menu[1],"2.Pickup id");
	strcpy(menu.menu[2],"3.Pickup Amount");	
	strcpy(menu.menu[3],"4.Customer Name");
	strcpy(menu.menu[4],"5.Pickup Address");
	strcpy(menu.menu[5],"6.Customer contact no.");	
		

	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case CANCEL:	After_selecting_customer(Pickup_Contract_no,Pickup_Flag_case);				
				break;

		case ENTER:
			switch (selItem+1 )
                    	{
			case 1: Pickup_Card_No(Pickup_Contract_no,Pickup_Flag_case);
				break;
			case 2: Pickup_Id(Pickup_Contract_no,Pickup_Flag_case);
				break;
			case 3:	Pickup_Amount_check(Pickup_Contract_no,Pickup_Flag_case);			
				break;
			case 4: Customer_Check_Name(Pickup_Contract_no,Pickup_Flag_case);
				break;
			case 5: Pickup_Address_check(Pickup_Contract_no,Pickup_Flag_case);
				break;
			case 6:	Customer_Contact_number(Pickup_Contract_no,Pickup_Flag_case);			
				break;	
		
			default : break;
			}		
		default : break;
		}//switch
	}//while
return 0;
}

/*************************************************************/
int Pickup_Card_No(char* Pickup_Contract,char* Pickup_Flag)
{
	resultset Customer_Product;

	memset(&Customer_Product,0,sizeof(Customer_Product));

	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Customer_Product= get_result("SELECT CARD_NUMBER FROM PICKUP_DETAILS WHERE CARD_NUMBER='%s';",Pickup_Contract);
	if(Customer_Product.rows>0)
	{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,1,"  Customer Pickup",0);
		lk_disphlight(1);
		lk_disptext(2,0,"Card No.:",0);
		lk_disptext(3,0,Customer_Product.recordset[0][0],0);		
		lk_getkey();
	}
	else Pickup_Customer_Details(Pickup_Contract,Pickup_Flag);
return SUCCESS;
}


/*************************************************************/
int Pickup_Id(char* ID_Contract,char* ID_Flag)
{
	resultset Customer_Product;

	memset(&Customer_Product,0,sizeof(Customer_Product));

	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Customer_Product= get_result("SELECT PICKUP_ID FROM PICKUP_DETAILS WHERE CARD_NUMBER='%s';",ID_Contract);
	if(Customer_Product.rows>0)
	{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,1,"  Customer Pickup",0);
		lk_disphlight(1);
		lk_disptext(2,0,"Pickup id.:",0);
		lk_disptext(3,0,Customer_Product.recordset[0][0],0);		
		lk_getkey();
	}
	else Pickup_Customer_Details(ID_Contract,ID_Flag);
return SUCCESS;
}


/*************************************************************/
int Pickup_Amount_check(char* Amount_check_contract,char* Pickup_Amount_Flag)
{
	resultset Customer_Product;

	memset(&Customer_Product,0,sizeof(Customer_Product));

	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Customer_Product= get_result("SELECT PICKUP_AMOUNT FROM PICKUP_DETAILS WHERE CARD_NUMBER='%s';",Amount_check_contract);
	if(Customer_Product.rows>0)
	{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,1,"  Customer Pickup",0);
		lk_disphlight(1);
		lk_disptext(2,0,"Pickup Amount:",0);
		lk_disptext(3,0,Customer_Product.recordset[0][0],0);		
		lk_getkey();
	}
	else Pickup_Customer_Details(Amount_check_contract,Pickup_Amount_Flag);
return SUCCESS;
}


/*************************************************************/
int Customer_Check_Name(char* Customer_contract,char* Customer_Flag)
{
	resultset Customer_Product;

	memset(&Customer_Product,0,sizeof(Customer_Product));

	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Customer_Product= get_result("SELECT Filler1 FROM PICKUP_DETAILS WHERE CARD_NUMBER='%s';",Customer_contract);
	if(Customer_Product.rows>0)
	{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,1,"  Customer Pickup",0);
		lk_disphlight(1);
		lk_disptext(2,0,"Customer Name:",0);
		lk_disptext(3,0,Customer_Product.recordset[0][0],0);		
		lk_getkey();
	}
	else Pickup_Customer_Details(Customer_contract,Customer_Flag);
return SUCCESS;
}


/*************************************************************/
int Pickup_Address_check(char* Pickup_Address_contract,char* Pickup_Address_Flag)
{
	resultset Customer_Product;

	memset(&Customer_Product,0,sizeof(Customer_Product));

	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Customer_Product= get_result("SELECT pickup_address FROM PICKUP_DETAILS WHERE CARD_NUMBER='%s';",Pickup_Address_contract);
	if(Customer_Product.rows>0)
	{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,1,"  Customer Pickup",0);
		lk_disphlight(1);
		lk_disptext(2,0,"Pickup Address:",0);
		lk_disptext(3,0,Customer_Product.recordset[0][0],0);		
		lk_getkey();
	}
	else Pickup_Customer_Details(Pickup_Address_contract,Pickup_Address_Flag);
return SUCCESS;
}




/*************************************************************/
int Customer_Contact_number(char* Pickup_Contact_contract,char* Pickup_Contcat_Flag)
{
	resultset Customer_Product;

	memset(&Customer_Product,0,sizeof(Customer_Product));

	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	Customer_Product= get_result("SELECT CUSTOMER_CONTACT_NO FROM PICKUP_DETAILS WHERE CARD_NUMBER='%s';",Pickup_Contact_contract);
	if(Customer_Product.rows>0)
	{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,1,"  Customer Pickup",0);
		lk_disphlight(1);
		lk_disptext(2,0,"Pickup Contact no.:",0);
		lk_disptext(3,0,Customer_Product.recordset[0][0],0);		
		lk_getkey();
	}
	else Pickup_Customer_Details(Pickup_Contact_contract,Pickup_Contcat_Flag);
return SUCCESS;
}
