/*******************************************************************
			   HDFC Bank CC
		            Dra_menu.c
/*******************************************************************/
/* 
 * @author ftlit003 
**/
/*******************************************************************/
#include <header.h>

/*******************************************************************/
int Dra_Menu()
{
	int opt=0;
	int LogOut_Exe_key;
        MENU_T menu;
        int selItem  = 0;
        int acceptKbdEvents = TRUE;
        int maxEntries = 0;
        short scroll = 0;
	char dec_eod_exec[25];
	resultset Disp_hwid;
	resultset Dra_lock,Prev_date,get_sqlite_date,Disp_appl;

	memset(&Dra_lock,0,sizeof(Dra_lock));
	memset(&Disp_hwid,0,sizeof(Disp_hwid));
	memset(&Prev_date,0,sizeof(Prev_date));
	memset(&get_sqlite_date,0,sizeof(get_sqlite_date));

	lk_buff_clear();
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db"); // open Data
	execute("Update operationalstatus SET last_transaction_no='1' WHERE last_transaction_no='9999';");
	Prev_date=get_result("SELECT Previous_date FROM operationalstatus;");
	get_sqlite_date=get_result("SELECT date('now')");	
	Dra_lock=get_result("SELECT ExecutiveStatus FROM operationalstatus;");	
	if((atoi(Dra_lock.recordset[0][0])==0)) After_main();
	
/********************************************************Changes*********************************************************/

	Disp_appl=get_result("SELECT localhttpip From operationalstatus;");
	
	if(strcmp(Disp_appl.recordset[0][0],"DRA")==0)
	{
	printf("\nApplication%s",Disp_appl.recordset[0][0]);
	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       6;
        maxEntries                      =       menu.maxEntries;

	strcpy(menu.title,"     DRA Menu");
	strcpy(menu.menu[0],"1.Pickup  Cases");                            
	strcpy(menu.menu[1],"2.Allocated Cases");// search by contract number or customer Name
	strcpy(menu.menu[2],"3.Synchronize");
	strcpy(menu.menu[3],"4.End Of Day");	
	strcpy(menu.menu[4],"5.HWID");
	strcpy(menu.menu[5],"6.Shutdown");

	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case ENTER:
			switch (selItem+1 )
                    	{
			case 1: Upcoming_Follow_up_menu();
				break;
			case 2: Search_By_CC_No();
				break;
		
			case 3: lk_dispclr(); 
				lk_disptext(2,1,"  Synchronizing...",1);   
				Synchronize();
				break;
	
			case 4: End_Of_Day("0");
				Dra_Menu();
				break;

			case 5:	close_sqlite();			
				open_sqlite("/mnt/jffs2/hdfcccoperational.db");
				Disp_hwid=get_result("SELECT hwid From operationalstatus;");
				lk_dispclr();
				lk_disptext(1,6,"  HWID",1);
				lk_disptext(3,6,Disp_hwid.recordset[0][0],1);
				sleep(5);
				break;
	
			case 6: 
				close_sqlite();
				lk_dispclr();
				lk_disptext(3,2,"Shutdown...",1);
				ppp_close();
				lk_close();   
				system("poweroff");
				sleep(13);
	   			break;
			
			default : break;
			}		
		default : break;
		}//switch
	}//while
    }
    else
	{
	printf("\nApplication%s",Disp_appl.recordset[0][0]);	
	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       5;
        maxEntries                      =       menu.maxEntries;

	strcpy(menu.title,"  NON-DRA Menu");
	strcpy(menu.menu[0],"1.Pickup");                            
	strcpy(menu.menu[1],"2.Synchronize");
	strcpy(menu.menu[2],"3.End Of Day");
	strcpy(menu.menu[3],"4.HWID");
	strcpy(menu.menu[4],"5.Shutdown");

	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case ENTER:
			switch (selItem+1 )
                    	{
			case 1: Upcoming_Follow_up_menu();
				break;
		
			case 2: lk_dispclr(); 
				lk_disptext(2,1,"  Synchronizing...",1);   
				Synchronize();
				break;
	
			case 3: End_Of_Day("0");
				break;

			case 4:	close_sqlite();			
				open_sqlite("/mnt/jffs2/hdfcccoperational.db");
				Disp_hwid=get_result("SELECT hwid From operationalstatus;");
				lk_dispclr();
				lk_disptext(1,6,"  HWID",1);
				lk_disptext(3,6,Disp_hwid.recordset[0][0],1);
				sleep(5);
				break;
	
			case 5: 
				close_sqlite();
				lk_dispclr();
				lk_disptext(3,2,"Shutdown...",1);
				ppp_close();
				lk_close();   
				system("poweroff");
				sleep(13);
	   			break;
			
			default : break;
			}		
		default : break;
		}//switch
	   }//while
	}
}

/******************************************************************/
int Upcoming_Follow_up_menu()
{
	MENU_L menu;
	int Action_key;
        int opt=0;
        int selItem  = 0;
        int acceptKbdEvents;
        int maxEntries = 0;
	int retval;
	register int i,n,j,k=0;
        short scroll = 0;
	char Pickup_Contract_No_search[22],Action_Cust_Name[22],Action_Name[22];
	char Credit_Number[5000];
	resultset Action_Contract_No,Action_Customer_name,Disp_appl;

	memset(&Action_Contract_No,0,sizeof(Action_Contract_No));	
	/***************Upcoming_Follow************************/	
	memset(&Action_Contract_No,0,sizeof(Action_Contract_No));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	memset(Pickup_Contract_No_search,0,sizeof(Pickup_Contract_No_search));
	lk_dispclr();
	lk_disptext(0,1,"   HDFC Bank CC",0);
	lk_disptext(1,1,"   Search Pickup",0);
	lk_disphlight(1);
	lk_disptext(2,0,"Enter Card Number or",0);
	lk_disptext(3,0,"Customer name",0);
	
	if((retval=lk_getalpha(5,0,Pickup_Contract_No_search,20,strlen(Pickup_Contract_No_search),0))<0) Dra_Menu();
	Pickup_Contract_No_search[retval]='\0';
 
	lk_dispclr(); 
	lk_disptext(2,3,"  Please wait.. ",0);	
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	if(strlen(Pickup_Contract_No_search)>0&&(Check_Alpha_Numeric(Pickup_Contract_No_search)==1))	
	Action_Contract_No = get_result("SELECT CARD_NUMBER,Filler1 FROM PICKUP_DETAILS WHERE CARD_NUMBER like '%c%s%c' OR CARD_NUMBER='%s' OR Filler1 like '%c%s%c' OR Filler1 ='%s' ORDER BY CONTACT_DATE ASC;",'%',Pickup_Contract_No_search,'%',Pickup_Contract_No_search,'%',Pickup_Contract_No_search,'%',Pickup_Contract_No_search);
	else Action_Contract_No = get_result("SELECT CARD_NUMBER,Filler1 FROM PICKUP_DETAILS ORDER BY CONTACT_DATE ASC;");	
	
			
	if(Action_Contract_No.rows>0)
	{
		lk_dispclr(); 
		menu.start                      =       0;
		menu.maxEntries                 =      Action_Contract_No.rows*2;
		maxEntries                      =      menu.maxEntries;
		    
	 	strcpy(menu.title,"Select Customer");  
			 
		for(i=0,j=1,k=0; i< Action_Contract_No.rows;i++)
	    	{
			memset(Action_Cust_Name,0,sizeof(Action_Cust_Name));					
			memset(Action_Name,0,sizeof(Action_Name));
			sprintf(Action_Cust_Name,"%s",Action_Contract_No.recordset[i][0]);
			snprintf(Action_Name,20,"%s",Action_Contract_No.recordset[i][1]);
			strcpy(menu.menu[k],Action_Cust_Name);
			k=k+2;
			strcpy(menu.menu[j],Action_Name);
			j=j+2;
		}
		while(1)
		{	
		      opt = scroll_menu(&menu,&selItem,acceptKbdEvents);
			switch(opt)
			{
			    case CANCEL: Upcoming_Follow_up_menu();	

			    case ENTER: printf("sel item = %d\n",selItem);
					After_selecting_customer(Action_Contract_No.recordset[(selItem/2)][0],"P");
						
			    default: Search_By_CC_No();	
			}
		}
	}
	else
	{
Action_Trail:
		lk_dispclr();
	    	lk_disptext(2,0,"     Pickup       ",0);
		lk_disptext(3,0,"  Detail Does Not     ",0);
	    	lk_disptext(4,0,"      Exists          ",0);
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Action_key = lk_getkey();
		if(Action_key==ENTER)
		{
		Dra_Menu();
		}
		else goto Action_Trail;		
	}
	return 0;

}

/******************************************************************************************************/
int Search_By_CC_No()
{
	MENU_L menu;
	int Loan_key;
        int opt=0;
        int selItem  = 0;
        int acceptKbdEvents;
        int maxEntries = 0,i,j,k;
        short scroll = 0;
	char Customer_Contract_No_search[22],Customer_Name[22];
	resultset Customer_Contract_No;
	int retval=0;
	char Loan_Number_Cust_Name[50];


	memset(&Customer_Contract_No,0,sizeof(Customer_Contract_No));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	memset(Customer_Contract_No_search,0,sizeof(Customer_Contract_No_search));
	lk_dispclr();
	lk_disptext(0,1,"   HDFC Bank CC",0);
	lk_disptext(1,1,"Search Delinquent CC",0);
	lk_disphlight(1);
	lk_disptext(2,0,"Enter Card Number or",0);
	lk_disptext(3,0,"Customer name",0);
	
	if((retval=lk_getalpha(5,0,Customer_Contract_No_search,20,strlen(Customer_Contract_No_search),0))<0) Dra_Menu();
	Customer_Contract_No_search[retval]='\0';
 
	lk_dispclr(); 
	lk_disptext(2,3,"  Please wait.. ",0);	
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	if(strlen(Customer_Contract_No_search)>0&&(Check_Alpha_Numeric(Customer_Contract_No_search)==1))
	Customer_Contract_No = get_result("SELECT CARD_NUMBER,CUST_ID FROM ALLOCATION_DETAILS WHERE CARD_NUMBER like '%c%s%c' OR CARD_NUMBER='%s' OR CUST_ID like '%c%s%c' OR CUST_ID ='%s';",'%',Customer_Contract_No_search,'%',Customer_Contract_No_search,'%',Customer_Contract_No_search,'%',Customer_Contract_No_search);	
	else Customer_Contract_No = get_result("SELECT CARD_NUMBER,CUST_ID FROM ALLOCATION_DETAILS;");
	if(Customer_Contract_No.rows>0)
	{
		lk_dispclr(); 
		menu.start                      =       0;
		menu.maxEntries                 =      Customer_Contract_No.rows*2;
		maxEntries                      =      menu.maxEntries;
		    
	 	strcpy(menu.title,"Select Customer");  
			 
		for(i=0,j=1,k=0; i< Customer_Contract_No.rows;i++)
	    	{
			memset(Loan_Number_Cust_Name,0,sizeof(Loan_Number_Cust_Name));					
			memset(Customer_Name,0,sizeof(Customer_Name));
			sprintf(Loan_Number_Cust_Name,"%s",Customer_Contract_No.recordset[i][0]);
			snprintf(Customer_Name,20,"%s",Customer_Contract_No.recordset[i][1]);
			strcpy(menu.menu[k],Loan_Number_Cust_Name);
			k=k+2;
			strcpy(menu.menu[j],Customer_Name);
			j=j+2;
		}
		while(1)
		{	
		      opt = scroll_menu(&menu,&selItem,acceptKbdEvents);
			switch(opt)
			{
			    case CANCEL: Search_By_CC_No();	

			    case ENTER: printf("sel item = %d\n",selItem);
					
					After_selecting_customer(Customer_Contract_No.recordset[(selItem/2)][0],"D");
							
			    default: Search_By_CC_No();	
			}
		}
	}
	else
	{
Loan_detail:
		lk_dispclr();
	    	lk_disptext(2,0," Card Number Detail",0);
	    	lk_disptext(3,0," Does Not Exists",0);
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Loan_key = lk_getkey();
		if(Loan_key==ENTER)
		{
		Search_By_CC_No();
		}
		else goto Loan_detail;
	}
	return 0;
}

Report()
{
	char Printing_Time_Date[42],Printing_Agent_Id[200];
	resultset table1,table2,table3,table4,table5,table6,table7,table8,table9;
	unsigned int i;
	char buff[300],str[200],str1[1000],str2[200],Battery_return[25];
	char report_trans_id[100],print_report_trans_id[100],dec_eod_exec[25],str_formate[100],str_formate_cancel[100];


	memset(&table1,0,sizeof(table1));
	memset(&table2,0,sizeof(table2));
	memset(&table3,0,sizeof(table3));
	memset(&table4,0,sizeof(table4));
	memset(&table5,0,sizeof(table5));
	memset(&table6,0,sizeof(table6));
	memset(&table7,0,sizeof(table7));
	memset(&table8,0,sizeof(table8));
	memset(&table9,0,sizeof(table9));

	memset(Printing_Time_Date,0,sizeof(Printing_Time_Date));
	sprintf(Printing_Time_Date,"Date: %02d-%02d-%02d          Time: %02d:%02d:%02d",curt.tm_mday,curt.tm_mon+1,(curt.tm_year-100),curt.tm_hour,curt.tm_min,curt.tm_sec);
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	table1=get_result("Select Executive_id,Executive_Name From operationalstatus;");	
	memset(dec_eod_exec,0,sizeof(dec_eod_exec));
	sprintf(dec_eod_exec,"%s",Decrypted_sqlite_DB(table1.recordset[0][0]));	
	memset(Printing_Agent_Id,0,sizeof(Printing_Agent_Id));
	sprintf(Printing_Agent_Id,"Execu ID: %10.10s Execu Name: %7.7s",dec_eod_exec,table1.recordset[0][1]);
	prn_open();
	goto Print_Report;
Print_Report:
	memset(Battery_return,0,sizeof(Battery_return));
	sprintf(Battery_return,"%s",Battery_Print_test());
	if(atof(Battery_return)<(6.92))
	{
	lk_dispclr();
	lk_disptext(1,5,"Battery Low!",1);
	lk_disptext(3,0,"  Plug Power Cord",0);//plug Power code cable
	lk_disptext(4,0," Cable & Press ENTER",0);
	lk_disptext(5,0,"  Key To continue",0);
	lk_getkey();	
	}
	if(prn_paperstatus()!=0)
        {
           	lk_dispclr();
           	lk_disptext(1,5,"No Paper !",1);
	  	lk_disptext(3,0,"Insert Paper & Press",0);
	  	lk_disptext(4,0," Any Key To continue",0);
           	lk_getkey();
		if(prn_paperstatus()!=0)
		{		
		return 1;
		} 
        }
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccctrans.db"); // open Data
	lk_dispclr(); 
	lk_disptext(2,2,"Printing..",1);
	lk_disptext(4,2,"Collection Report",1);
	prn_write_text("----------------------------------------",40,1);
	prn_write_text("Collection Report",20,2);
	prn_write_text("----------------------------------------",40,1);
	prn_write_text(Printing_Agent_Id,strlen(Printing_Agent_Id),1);
	prn_write_text(Printing_Time_Date,strlen(Printing_Time_Date),1);
	prn_write_text("----------------------------------------",40,1);
	prn_write_text("Cash Collection                         ",40,1);
	prn_write_text("----------------------------------------",40,1);
	prn_write_text("RN        CC No.              Amt(Rs)   ",40,1);
	prn_write_text("----------------------------------------",40,1);
	table2=get_result("SELECT Loan_No,Total_Amt,Transaction_ID From transactiondetails WHERE Instument_Type='Cash' AND Trn_cancel='0' ORDER BY Transaction_ID ASC;");
	if(table2.rows>0)		
	{
		for(i=0;i<table2.rows;i++)
		{
			memset(str1,0,sizeof(str1));
			memset(str,0,sizeof(str));
			memset(report_trans_id,0,sizeof(report_trans_id));
			memset(print_report_trans_id,0,sizeof(print_report_trans_id));
			sprintf(report_trans_id,"%s",table2.recordset[i][2]);			
			sprintf(print_report_trans_id,"%c%c%c",report_trans_id[strlen(report_trans_id)-3],report_trans_id[strlen(report_trans_id)-2],report_trans_id[strlen(report_trans_id)-1]);//no
			sprintf(str,"%-3s",print_report_trans_id);//no
			strcat(str1,str);
			memset(str,0,sizeof(str));
                        sprintf(str," %-24s",table2.recordset[i][0]);//Contract No.
			strcat(str1,str);		
			memset(str,0,sizeof(str));
			sprintf(str," %10.0f\n",atof(table2.recordset[i][1]));//amount
                        strcat(str1,str);
			prn_write_text(str1,strlen(str1),1);
		}
		
		prn_write_text("----------------------------------------",40,1);
		memset(str2,0,sizeof(str2));
		table4=get_result("SELECT sum(Total_Amt) from transactiondetails WHERE Instument_Type='Cash' AND Trn_cancel='0';");
		
		sprintf(str2,"TOTAL: Rs.%-.0f/-",atof(table4.recordset[0][0]));
		
		prn_write_text(str2,strlen(str2),2);
		prn_paper_feed(1);
	}
	else
	{
		prn_write_text("No Cash Collection                      ",40,1);
		prn_write_text("----------------------------------------",40,1);
		prn_paper_feed(1);
	}

	prn_write_text("----------------------------------------",40,1);
	prn_write_text("Cheque/DD Collection                    ",40,1);
	prn_write_text("----------------------------------------",40,1);
	prn_write_text("RN     CC No.         PayType Amt(Rs)   ",40,1);
	prn_write_text("    Chq/DD No      Chq/DD Date(DD-MM-YY)",40,1);
	prn_write_text("----------------------------------------",40,1);
	table3=get_result("SELECT Loan_No,Instument_Type,Total_Amt,chq_DD_No,Chq_DD_Date,Transaction_ID From transactiondetails WHERE (Instument_Type='Cheque' or Instument_Type='DD') AND Trn_cancel='0' ORDER BY Transaction_ID ASC;");		
	if(table3.rows>0)
	{
		for(i=0;i<table3.rows;i++)
		{
			memset(str1,0,sizeof(str1));
			memset(str,0,sizeof(str));
			memset(report_trans_id,0,sizeof(report_trans_id));
			memset(print_report_trans_id,0,sizeof(print_report_trans_id));
			sprintf(report_trans_id,"%s",table3.recordset[i][5]);			
			sprintf(print_report_trans_id,"%c%c%c",report_trans_id[strlen(report_trans_id)-3],report_trans_id[strlen(report_trans_id)-2],report_trans_id[strlen(report_trans_id)-1]);//no
			sprintf(str,"%-3s",print_report_trans_id);//no
			strcat(str1,str);
			memset(str,0,sizeof(str));
                        sprintf(str," %-16.16s",table3.recordset[i][0]);//ContractNo.
			strcat(str1,str);
			memset(str,0,sizeof(str));
                        sprintf(str," %-7.7s",table3.recordset[i][1]);//PayType
			strcat(str1,str);		
			memset(str,0,sizeof(str));
			sprintf(str," %10.0f\n",atof(table3.recordset[i][2]));//amount
                        strcat(str1,str);
			memset(str,0,sizeof(str));
                        sprintf(str,"     %-6.6s",table3.recordset[i][3]);//chq no
			strcat(str1,str);		
			memset(str,0,sizeof(str));
			memset(str_formate_cancel,0,sizeof(str_formate_cancel));
			sprintf(str_formate_cancel,"%s",table3.recordset[i][4]);//chq Date
			sprintf(str,"          %10c%c-%c%c-%c%c\n",str_formate_cancel[0],str_formate_cancel[1],str_formate_cancel[2],str_formate_cancel[3],str_formate_cancel[4],str_formate_cancel[5]);//chq Date
                        strcat(str1,str);
			prn_write_text(str1,strlen(str1),1);
		}
		prn_write_text("----------------------------------------",40,1);
		memset(str2,0,sizeof(str2));
		table5=get_result("SELECT sum(Total_Amt) from transactiondetails WHERE (Instument_Type='Cheque' or Instument_Type='DD') AND Trn_cancel='0';");
		sprintf(str2,"TOTAL: Rs.%-.0f/-",atof(table5.recordset[0][0]));
		prn_write_text(str2,strlen(str2),2);
	}
	else
	{
		prn_write_text("No Cheque/DD Collection                 ",40,1);
		prn_write_text("----------------------------------------",40,1);
	}
	
	prn_write_text("----------------------------------------",40,1);
	prn_write_text("Cash Receipt Cancellation               ",40,1);
	prn_write_text("----------------------------------------",40,1);
	prn_write_text("RN     CC No.                 Amt(Rs)   ",40,1);
	prn_write_text("----------------------------------------",40,1);
	
	table6=get_result("SELECT Loan_No,Total_Amt,Transaction_ID From transactiondetails WHERE Instument_Type='Cash' AND (Trn_cancel='1' OR Trn_cancel='2' OR Trn_cancel='3') ORDER BY Transaction_ID ASC;");
	
	if(table6.rows>0)		
	{		
		for(i=0;i<table6.rows;i++)
		{
			printf("Trn_cancel-0");
			memset(str1,0,sizeof(str1));
			memset(str,0,sizeof(str));
			memset(report_trans_id,0,sizeof(report_trans_id));
			memset(print_report_trans_id,0,sizeof(print_report_trans_id));
			sprintf(report_trans_id,"%s",table6.recordset[i][2]);			
			sprintf(print_report_trans_id,"%c%c%c",report_trans_id[strlen(report_trans_id)-3],report_trans_id[strlen(report_trans_id)-2],report_trans_id[strlen(report_trans_id)-1]);//no
			sprintf(str,"%-3s",print_report_trans_id);//no
			strcat(str1,str);
			memset(str,0,sizeof(str));
                        sprintf(str," %-24s",table6.recordset[i][0]);//Contract No.
			strcat(str1,str);		
			memset(str,0,sizeof(str));
			sprintf(str," %10.0f\n",atof(table6.recordset[i][1]));//amount
                        strcat(str1,str);
			prn_write_text(str1,strlen(str1),1);
		}
		printf("Trn_cancel-1");
		prn_write_text("----------------------------------------",40,1);
		memset(str2,0,sizeof(str2));
		table7=get_result("SELECT sum(Total_Amt) from transactiondetails WHERE Instument_Type='Cash' AND (Trn_cancel='1' OR Trn_cancel='2' OR Trn_cancel='3');");
		printf("Trn_cancel-2");
		sprintf(str2,"TOTAL: Rs.%-.0f/-",atof(table7.recordset[0][0]));
		prn_write_text(str2,strlen(str2),2);
		prn_paper_feed(1);
	}
	else
	{
		prn_write_text("No Cash Receipt Cancellation            ",40,1);
		prn_write_text("----------------------------------------",40,1);
		prn_paper_feed(1);
	}

	prn_write_text("----------------------------------------",40,1);
	prn_write_text("Cheque/DD Receipt Cancellation          ",40,1);
	prn_write_text("----------------------------------------",40,1);
	prn_write_text("RN     CC No.         PayType Amt(Rs)   ",40,1);
	prn_write_text("    Chq/DD No      Chq/DD Date(DD-MM-YY)",40,1);
	prn_write_text("----------------------------------------",40,1);
	table8=get_result("SELECT Loan_No,Instument_Type,Total_Amt,chq_DD_No,Chq_DD_Date,Transaction_ID From transactiondetails WHERE (Instument_Type='Cheque' or Instument_Type='DD') AND (Trn_cancel='1' OR Trn_cancel='2' OR Trn_cancel='3') ORDER BY Transaction_ID ASC;");		
	if(table8.rows>0)
	{
		for(i=0;i<table8.rows;i++)
		{
			memset(str1,0,sizeof(str1));
			memset(str,0,sizeof(str));
			memset(report_trans_id,0,sizeof(report_trans_id));
			memset(print_report_trans_id,0,sizeof(print_report_trans_id));
			sprintf(report_trans_id,"%s",table8.recordset[i][5]);			
			sprintf(print_report_trans_id,"%c%c%c",report_trans_id[strlen(report_trans_id)-3],report_trans_id[strlen(report_trans_id)-2],report_trans_id[strlen(report_trans_id)-1]);//no
			sprintf(str,"%-3s",print_report_trans_id);//no
			strcat(str1,str);
			memset(str,0,sizeof(str));
                        sprintf(str," %-16.16s",table8.recordset[i][0]);//ContractNo.
			strcat(str1,str);
			memset(str,0,sizeof(str));
                        sprintf(str," %-7.7s",table8.recordset[i][1]);//PayType
			strcat(str1,str);		
			memset(str,0,sizeof(str));
			sprintf(str," %10.0f\n",atof(table8.recordset[i][2]));//amount
                        strcat(str1,str);
			memset(str,0,sizeof(str));
                        sprintf(str,"     %-6.6s",table8.recordset[i][3]);//chq no
			strcat(str1,str);		
			memset(str,0,sizeof(str));
			memset(str_formate,0,sizeof(str_formate));
			sprintf(str_formate,"%s",table8.recordset[i][4]);//chq Date
			sprintf(str,"         %10c%c-%c%c-%c%c\n",str_formate[0],str_formate[1],str_formate[2],str_formate[3],str_formate[4],str_formate[5]);//chq Date
                        strcat(str1,str);
			prn_write_text(str1,strlen(str1),1);
		}
		prn_write_text("----------------------------------------",40,1);
		memset(str2,0,sizeof(str2));
		table9=get_result("SELECT sum(Total_Amt) from transactiondetails WHERE (Instument_Type='Cheque' or Instument_Type='DD') AND (Trn_cancel='1' OR Trn_cancel='2' OR Trn_cancel='3');");
		sprintf(str2,"TOTAL: Rs.%-.0f/-",atof(table9.recordset[0][0]));
		prn_write_text(str2,strlen(str2),2);
	}
	else
	{
		prn_write_text("No Cheque/DD Receipt Cancellation       ",40,1);
		prn_write_text("----------------------------------------",40,1);
	}

	prn_paper_feed(4);
	prn_close();
return SUCCESS;
}
/******************************************************************/

/******************************************************************/
int End_Of_Day(char* Flag_Logout)
{
	unsigned int logout_key,ret_png,ret_report;
	char End_Of_Day_String[200];	
	char EOD_Data[75],dec_eod_exec[25],EOD_BOD_RSA_b64[500];
	resultset get_EOD;
	resultset unuploaded_trans,unuploaded_Cancellation_trans,unuploaded_followUp,unuploaded_BOD_EOD,BOD_EOD_Status;

	memset(&get_EOD,0,sizeof(get_EOD));
	memset(&unuploaded_trans,0,sizeof(unuploaded_trans));
	memset(&unuploaded_Cancellation_trans,0,sizeof(unuploaded_Cancellation_trans));
	memset(&unuploaded_followUp,0,sizeof(unuploaded_followUp));
	memset(&unuploaded_BOD_EOD,0,sizeof(unuploaded_BOD_EOD));			
	
	printf("Flag_Logout:%s",Flag_Logout);	
	if(atoi(Flag_Logout)==0)
	{
	lk_dispclr();
    	lk_disptext(2,0,"  Do u wish to End",0);
	lk_disptext(3,0,"      of Day?",0);
    	lk_disptext(4,0,"   1.Yes  2.No",0);
    	while(1)
	{
	logout_key = lk_getkey();
	printf("logout_key%d",logout_key);
	if((logout_key == 1)||(logout_key == 2)||(logout_key == -3))	break;
	}		
	if(logout_key == 1)
	{
Connection:
		ret_png=Png_Status_Reconnect();		
		if(ret_png==-1)
		{
			printf("Flag_Logout:%d",atoi(Flag_Logout));
			if(atoi(Flag_Logout)==1) After_main(); 
			else Dra_Menu();
		}		
		Upload_Transaction_Request_For_Thrd();
		Upload_FollowUp_Request_For_Thrd();		
		Upload_BOD_EOD_Request_Trid();
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfccctrans.db"); // open Data
		execute("Delete FROM unuploadedtransactiondetails WHERE Status='1';");
		execute("Delete FROM unuploadedfollowupdetails WHERE FollowUp_Status='1';");
		execute("Delete FROM unuploaded_BOD_EOD_details WHERE BOD_EOD_Status='1';");
		unuploaded_trans=get_result("SELECT * FROM unuploadedtransactiondetails;");
		unuploaded_followUp=get_result("SELECT * FROM unuploadedfollowupdetails;");
		unuploaded_BOD_EOD=get_result("SELECT * FROM unuploaded_BOD_EOD_details;");		
		if((unuploaded_trans.rows==0)&&(unuploaded_followUp.rows==0)&&(unuploaded_BOD_EOD.rows==0))		
		{			
			Png_Status_Without_Reconnect();					
			close_sqlite();			
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");			
			get_EOD=get_result("Select Executive_id,hwid,loginstatus From operationalstatus;");		
			memset(dec_eod_exec,0,sizeof(dec_eod_exec));
			sprintf(dec_eod_exec,"%s",Decrypted_sqlite_DB(get_EOD.recordset[0][0]));
			/***************************************************/
			memset(EOD_Data,0,sizeof(EOD_Data));
			sprintf(EOD_Data,"Validate,%s,%s,%02d=%02d=%04d,%02d=%02d=%02d,E",dec_eod_exec,get_EOD.recordset[0][1],(curt.tm_mon+1),(curt.tm_mday),(curt.tm_year+1900),curt.tm_hour,curt.tm_min,curt.tm_sec);

			memset(EOD_BOD_RSA_b64,0,sizeof(EOD_BOD_RSA_b64));
			sprintf(EOD_BOD_RSA_b64,"%s",RSAEncryption_Base64(EOD_Data,strlen(EOD_Data)));

			close_sqlite();	
			open_sqlite("/mnt/jffs2/hdfccctrans.db"); // open trans   
			execute("INSERT INTO unuploaded_BOD_EOD_details(BOD_EOD_details,BOD_EOD_Status) VALUES('%s','0');",EOD_BOD_RSA_b64);			
			/***************************************************/
			Upload_BOD_EOD_Request_Trid();
			BOD_EOD_Status=get_result("SELECT BOD_EOD_details,Counter from unuploaded_BOD_EOD_details WHERE BOD_EOD_Status='0';");
			if(BOD_EOD_Status.rows==0)
			{			
			prn_open();
			prn_write_text("----------------------------------------",40,1);
			prn_write_text("End Of Day   ",20,2);
			memset(End_Of_Day_String,0,sizeof(End_Of_Day_String));
			lk_getrtc(&curt);
			sprintf(End_Of_Day_String,"This slip denotes that all transactions as of (%02d-%02d-%02d)/(%02d-%02d-%02d),have been successfully uploaded",curt.tm_hour,curt.tm_min,curt.tm_sec,curt.tm_mday,curt.tm_mon+1,curt.tm_year-100);
			prn_write_text(End_Of_Day_String,strlen(End_Of_Day_String),1);
			prn_close();			
			ret_report=Report();			
			if(ret_report==1) return 0; 			
			close_sqlite();			
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			execute("UPDATE operationalstatus SET loginstatus='0';");		
			close_sqlite();			
			open_sqlite("/mnt/jffs2/hdfccctrans.db");
			execute("DELETE FROM transactiondetails;");
			lk_dispclr();
			lk_disptext(2,3," EOD Completed ",1);
			//lk_disptext(4,3,"   Successfully",1);
			sleep(2);			
			system("rm /mnt/jffs2/hdfccctrans.db");
			After_main();
			}
			else
			{
			goto Connection;
			}			
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;	
	}
	}
	else goto Connection;	
	
return SUCCESS;
}

