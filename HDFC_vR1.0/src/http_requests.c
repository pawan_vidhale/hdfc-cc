/*******************************************************************/
//			    HDFC CC Bank
//			  http requests.c
/*******************************************************************/
/*
 * @author FTL003
*/
#include <header.h>
#include <curl.h>


/******************************************************************/
int Executive_Info_Request(char* Executive_ID_Verify)
{
	int ExecutiveID_key,Bad_key,Error_Request_key,Error_Service_key,Machin_lock_key,Enrolled_key,
Error_Process_key,Loginfailure_key,Lost_Request_key,Machine_Request_key,log_Request_key;
	char Executive_Verify_Url[250],Executive_Verify_File[500],Executive_Verify_File_login[100],Executive_Verify_File_login_remove[100],Executive_Verify_File_chmod[100],Executive_Verify_File_unzip[100];
	unsigned char response_buffer[1000];
	char Dra_Exec_info[100],Dra_login_RSA_b64[500];
	FILE * response_myfile;
	resultset Executive_Ip;
	unsigned int Executive_Code_Valid=0,ret_unzip=0;	
	FILE * pFile;

	memset(&Executive_Ip,0,sizeof(Executive_Ip));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Executive_Ip=get_result("Select hwid,httpip,localhttpip,Current_Software_Ver from operationalstatus;");

	memset(Executive_Verify_Url,0,sizeof(Executive_Verify_Url));

#if URL_NETMAGIC	
	sprintf(Executive_Verify_Url,"%s/HHTRequest/v%s/CollectionExecutiveLoginValidation.aspx",Executive_Ip.recordset[0][1],Executive_Ip.recordset[0][3]);
#endif	
	memset(Dra_Exec_info,0,sizeof(Dra_Exec_info));
	sprintf(Dra_Exec_info,"Validate,%s,%s",Executive_ID_Verify,Executive_Ip.recordset[0][0]);
	printf("%s\n",Dra_Exec_info);
	memset(Dra_login_RSA_b64,0,sizeof(Dra_login_RSA_b64));
	sprintf(Dra_login_RSA_b64,"%s",RSAEncryption_Base64(Dra_Exec_info,strlen(Dra_Exec_info)));

	memset(Executive_Verify_File_login,0,sizeof(Executive_Verify_File_login));
	sprintf(Executive_Verify_File_login,"/mnt/jffs2/%s_login.txt",Executive_ID_Verify);
		
  	pFile = fopen (Executive_Verify_File_login, "wb");
	fputs (Dra_login_RSA_b64,pFile);
  	fclose (pFile);

	memset(Executive_Verify_File,0,sizeof(Executive_Verify_File));
	sprintf(Executive_Verify_File,"/mnt/jffs2/%s.zip",Executive_ID_Verify);	
	Executive_Code_Valid=curl_Dra_Verification_Data(Executive_Verify_Url,Executive_Verify_File_login,Executive_Verify_File);
		
	remove(Executive_Verify_File_login);
	if(Executive_Code_Valid==1)
	{
		response_myfile = fopen(Executive_Verify_File,"r");
		memset(response_buffer,0,sizeof(response_buffer));
		while (!feof(response_myfile))
		{			
			fgets(response_buffer,50,response_myfile);			

  		}
		fclose(response_myfile);
		if(strcmp(response_buffer,"ErrorCode:0001")==0)
		{
Bad_Request_Key:
			lk_dispclr();
			lk_disptext(3,0,"    Bad Request ",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);				
			remove(Executive_Verify_File);
			Bad_key = lk_getkey();
			if(Bad_key==ENTER)
			{	
			Dra_login();
			}
			else goto Bad_Request_Key; 
			
		}
		else if(strcmp(response_buffer,"ErrorCode:0002")==0)
		{
Enter_Valid_ID:
			lk_dispclr();
			lk_disptext(2,3," Please Enter",0);
			lk_disptext(3,3," Valid DRA ID",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);					
			remove(Executive_Verify_File);
			ExecutiveID_key = lk_getkey();
			if(ExecutiveID_key==ENTER)
			{			
			Dra_login();
			}
			else goto Enter_Valid_ID;	
		}
		else if(strcmp(response_buffer,"ErrorCode:0003")==0)
		{
login_failure:
			lk_dispclr();
			lk_disptext(2,0,"   Login ",0);
			lk_disptext(4,0,"     Failure",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);			
			remove(Executive_Verify_File);
			Loginfailure_key = lk_getkey();
			if(Loginfailure_key==ENTER)
			{
			Dra_login();
			}
			else goto login_failure;
		}
		else if(strcmp(response_buffer,"ErrorCode:0004")==0)
		{
Error_Process:
			lk_dispclr();
			lk_disptext(2,0,"  Error in Processing  ",0);
			lk_disptext(4,0,"      Request",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);			
			remove(Executive_Verify_File);
			Error_Process_key = lk_getkey();
			if(Error_Process_key==ENTER)
			{
			Dra_login();
			}
			else goto Error_Process;
		}
		else if(strcmp(response_buffer,"ErrorCode:0005")==0)
		{
Enrolled_Not:
			lk_dispclr();
			lk_disptext(2,0,"  DRA Is Not ",0);
			lk_disptext(4,0,"      Enrolled",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			remove(Executive_Verify_File);						
			Enrolled_key = lk_getkey();
			if(Enrolled_key==ENTER)
			{
			Dra_login();			
			}
			else goto Enrolled_Not;
		}
		else if(strcmp(response_buffer,"ErrorCode:0007")==0)
		{
Machine_locked:
			lk_dispclr();
			lk_disptext(2,0,"  Machine is Locked",0);
			lk_disptext(4,0,"  Please contact HO",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);						
			Machin_lock_key = lk_getkey();
			if(Machin_lock_key==ENTER)
			{			
			close_sqlite();						
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			execute("UPDATE operationalstatus SET ExecutiveStatus='0';");
			remove(Executive_Verify_File);
			Dra_login();
			}
			else goto Machine_locked;	
		}
		else if(strcmp(response_buffer,"ErrorCode:0008")==0)
		{
Error_Request:
			lk_dispclr();
			lk_disptext(2,0,"  Error in Processing  ",0);
			lk_disptext(4,0,"      Request",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);							
			Error_Request_key = lk_getkey();
			if(Error_Request_key==ENTER)
			{
			remove(Executive_Verify_File);
			Dra_login();
			}
			else goto Error_Request; 
		}
		else if(strcmp(response_buffer,"ErrorCode:0009")==0)
		{
Machine_Error_Request:
			lk_dispclr();
			lk_disptext(2,0," Machine Surrendered",0);
			lk_disptext(3,0,"  Please contact HO",0);			
			lk_disptext(5,0,"Press Enter to Cont.",0);						
			Machine_Request_key = lk_getkey();
			if(Machine_Request_key==ENTER)
			{
			close_sqlite();						
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			execute("UPDATE operationalstatus SET ExecutiveStatus='0';");
			remove(Executive_Verify_File);
			Dra_login();			
			}
			else goto Machine_Error_Request; 
		}
		else if(strcmp(response_buffer,"ErrorCode:00010")==0)
		{
Lost_Request_rt:
			lk_dispclr();
			lk_disptext(2,0,"  Machine Lost,",0);
			lk_disptext(3,0,"  Please contact HO",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);				
			remove(Executive_Verify_File);
			Lost_Request_key = lk_getkey();
			if(Lost_Request_key==ENTER)
			{
			close_sqlite();						
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			execute("UPDATE operationalstatus SET ExecutiveStatus='0';");
			Dra_login();
			}
			else goto Lost_Request_rt; 
		}
		else if(strcmp(response_buffer,"ErrorCode:00011")==0)
		{
further_Request_rt:
			lk_dispclr();
			lk_disptext(2,0," Machine Surrendered",0);
			lk_disptext(3,0,"  In Processing",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);				
			remove(Executive_Verify_File);
			log_Request_key = lk_getkey();
			if(log_Request_key==ENTER)
			{			
			remove(Executive_Verify_File);
			Machine_Surrender_Request(Executive_ID_Verify);
			}
			else goto further_Request_rt; 
		}
		else if(strcmp(response_buffer,"<h1>Bad Request (Invalid Hostname)</h1>")==0)
		{
			remove(Executive_Verify_File);
			Dra_login();
		}
		else if(strcmp(response_buffer,"<h1>Service Unavailable</h1>")==0)
		{
			remove(Executive_Verify_File);
			Dra_login();
		}
		else if(strcmp(response_buffer,"0")==0)
		{
Error_Service:
			lk_dispclr();
			lk_disptext(2,0,"  Service Unavailable",0);
			lk_disptext(4,0,"      Request",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			remove(Executive_Verify_File);
			Error_Service_key = lk_getkey();
			if(Error_Service_key==ENTER)
			{			
			Dra_login();
			}
			else goto Error_Service;
		}		
		else
		{
		memset(Executive_Verify_File_chmod,0,sizeof(Executive_Verify_File_chmod));
		sprintf(Executive_Verify_File_chmod,"chmod 777 %s",Executive_Verify_File);
		system(Executive_Verify_File_chmod);
		system("cd /mnt/jffs2/");
		memset(Executive_Verify_File_unzip,0,sizeof(Executive_Verify_File_unzip));
		sprintf(Executive_Verify_File_unzip,"unzip %s",Executive_Verify_File);
		ret_unzip=system(Executive_Verify_File_unzip);
		if(ret_unzip==0)
		{		
		return 1;
		}
		else
		{
Service_File:
		lk_dispclr();
		lk_disptext(2,0,"  Service Unavailable",0);
		lk_disptext(3,0,"      Request",0);
		lk_disptext(5,0,"Press Enter to Cont.",0);
		remove(Executive_Verify_File);
		Error_Service_key = lk_getkey();
		if(Error_Service_key==ENTER)
		{
		Dra_login();
		}
		else goto Service_File;		
		}
		}
	}
	else if(Executive_Code_Valid==0)
	{
		remove(Executive_Verify_File);
		return 0;
	}
	else
	{
		lk_dispclr();
		lk_disptext(2,0,"    Please Try",0);
		lk_disptext(4,0,"      Again",0);
		sleep(2);
		remove(Executive_Verify_File);
		return 0;
	}
}

/******************************************************************/
int Executive_Status_Request(char* Executive_ID_Verify)
{
	int Machin_lock_key,Machine_Request_key,Lost_Request_key;
	char Executive_Verify_Url[250],Executive_Verify_File[500],Executive_Verify_File_login[100],Executive_Verify_File_login_remove[100],Executive_Verify_File_chmod[100],Executive_Verify_File_unzip[100];
	unsigned char response_buffer[1000];
	char Exec_info[100],Executive_login_RSA_b64[500];
	FILE * response_myfile;
	resultset Executive_Ip;
	unsigned int Executive_Code_Valid=0;	
	FILE * pFile;

	printf("\nExecutive_ID:%s",Executive_ID_Verify);

	memset(&Executive_Ip,0,sizeof(Executive_Ip));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Executive_Ip=get_result("Select hwid,httpip,localhttpip,Current_Software_Ver from operationalstatus;");

	memset(Executive_Verify_Url,0,sizeof(Executive_Verify_Url));
#if URL_TEST
	sprintf(Executive_Verify_Url,"%s/HHTRequest/v%s/CollectionExecutiveLoginValidation.aspx",Executive_Ip.recordset[0][1],Executive_Ip.recordset[0][3]);
#endif
#if URL_NETMAGIC
	sprintf(Executive_Verify_Url,"%s/HHTRequest/v%s/CollectionExecutiveLoginValidation.aspx",Executive_Ip.recordset[0][1],Executive_Ip.recordset[0][3]);
#endif
	memset(Exec_info,0,sizeof(Exec_info));
	sprintf(Exec_info,"Validate,%s,%s",Executive_ID_Verify,Executive_Ip.recordset[0][0]);
	memset(Executive_login_RSA_b64,0,sizeof(Executive_login_RSA_b64));
	sprintf(Executive_login_RSA_b64,"%s",RSAEncryption_Base64(Exec_info,strlen(Exec_info)));

	memset(Executive_Verify_File_login,0,sizeof(Executive_Verify_File_login));
	sprintf(Executive_Verify_File_login,"/mnt/jffs2/%s_login.txt",Executive_ID_Verify);
  	pFile = fopen (Executive_Verify_File_login, "wb");
	fputs (Executive_login_RSA_b64,pFile);
  	fclose (pFile);

	memset(Executive_Verify_File,0,sizeof(Executive_Verify_File));
	sprintf(Executive_Verify_File,"/mnt/jffs2/%s.zip",Executive_ID_Verify);	
	Executive_Code_Valid=curl_Dra_Verification_Wallet_Data(Executive_Verify_Url,Executive_Verify_File_login,Executive_Verify_File);
		
	remove(Executive_Verify_File_login);
	printf("\nExecutive_ID:%s",Executive_ID_Verify);
	
	if(Executive_Code_Valid==1)
	{
		
		response_myfile = fopen(Executive_Verify_File,"r");
		memset(response_buffer,0,sizeof(response_buffer));
		while (!feof(response_myfile))
		{
			fgets(response_buffer,50,response_myfile);
#if PRINTF			
//			printf("\nResponse buffer length :%d",strlen(response_buffer));
#endif
  		}
		fclose(response_myfile);
		if(strcmp(response_buffer,"ErrorCode:0007")==0)
		{
Machine_locked:
			lk_dispclr();
			lk_disptext(2,0,"  Machine is Locked",0);
			lk_disptext(4,0,"  Please contact HO",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			printf("\nResponse buffer:%s",response_buffer);	
			remove(Executive_Verify_File);
			Machin_lock_key = lk_getkey();
			if(Machin_lock_key==ENTER)
			{
			close_sqlite();						
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			execute("UPDATE operationalstatus SET ExecutiveStatus='0';");
			Dra_login();
			}
			else goto Machine_locked;		
		}
		else if(strcmp(response_buffer,"ErrorCode:0009")==0)
		{
Machine_Error_Request:
			lk_dispclr();
			lk_disptext(2,0," Machine Surrendered",0);
			lk_disptext(3,0,"  Please contact HO",0);			
			lk_disptext(5,0,"Press Enter to Cont.",0);
			printf("\nResponse buffer:%s",response_buffer);	
			remove(Executive_Verify_File);
			Machine_Request_key = lk_getkey();
			if(Machine_Request_key==ENTER)
			{
			close_sqlite();						
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			execute("UPDATE operationalstatus SET ExecutiveStatus='0';");
			Dra_login();
			}
			else goto Machine_Error_Request; 
		}
		else if(strcmp(response_buffer,"ErrorCode:00010")==0)
		{
Lost_Request_rt:
			lk_dispclr();
			lk_disptext(2,0,"  Machine Lost,",0);
			lk_disptext(3,0,"  Please contact HO",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			printf("\nResponse buffer:%s",response_buffer);	
			remove(Executive_Verify_File);
			Lost_Request_key = lk_getkey();
			if(Lost_Request_key==ENTER)
			{
			close_sqlite();						
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			execute("UPDATE operationalstatus SET ExecutiveStatus='0';");
			Dra_login();
			}
			else goto Lost_Request_rt; 
		}
		else if((strcmp(response_buffer,"ErrorCode:0001")==0)||(strcmp(response_buffer,"ErrorCode:0002")==0)||(strcmp(response_buffer,"ErrorCode:0003")==0)||(strcmp(response_buffer,"ErrorCode:0004")==0)||(strcmp(response_buffer,"ErrorCode:0005")==0)||(strcmp(response_buffer,"<h1>Bad Request (Invalid Hostname)</h1>")==0)||(strcmp(response_buffer,"<h1>Service Unavailable</h1>")==0))
		{
			remove(Executive_Verify_File);
			return 0;
		}				
		else
		{			
			memset(Executive_Verify_File_chmod,0,sizeof(Executive_Verify_File_chmod));
			sprintf(Executive_Verify_File_chmod,"chmod 777 %s",Executive_Verify_File);
			system(Executive_Verify_File_chmod);			
			system("cd /mnt/jffs2/");
			memset(Executive_Verify_File_unzip,0,sizeof(Executive_Verify_File_unzip));
			sprintf(Executive_Verify_File_unzip,"unzip %s",Executive_Verify_File);
			system(Executive_Verify_File_unzip);			
			return 1;
		}
	}
	else 
	{
		remove(Executive_Verify_File);
		return 0;
	}
}

/******************************************************************/
int Customer_Info_Request(char* Executive_ID_Customer_Info)
{
	int Contracts_key,Service_key,Error_Service_key,log_Request_key;
	char Executive_Customer_Info_Url[250],Customer_info_send_File[200],Customer_info_send_File_remove[200],Executive_Customer_Info_File[100],Executive_Customer_Info_File_chmod[100],Executive_Customer_Info_File_unzip[100];
	unsigned char response_buffer[1000];
	char Customer_info[100],Customer_info_RSA_b64[500];
	FILE * pFile;
	FILE * response_myfile;
	resultset Executive_Customer_Info_Ip;
	unsigned int Executive_Customer_Info_Valid=0;	

	printf("\nIn Customer Details");
	memset(&Executive_Customer_Info_Ip,0,sizeof(Executive_Customer_Info_Ip));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Executive_Customer_Info_Ip=get_result("Select hwid,httpip,localhttpip,Current_Software_Ver from operationalstatus;");

	memset(Executive_Customer_Info_Url,0,sizeof(Executive_Customer_Info_Url));
#if URL_NETMAGIC
	sprintf(Executive_Customer_Info_Url,"%s/HHTRequest/v%s/RequestForAllocationData.aspx",Executive_Customer_Info_Ip.recordset[0][1],Executive_Customer_Info_Ip.recordset[0][3]);
#endif
#if PRINTF 
	printf("\nExecutive_Customer_Info_Url:%s",Executive_Customer_Info_Url);	
#endif
	memset(Customer_info,0,sizeof(Customer_info));
	sprintf(Customer_info,"Validate,%s,%s",Executive_ID_Customer_Info,Executive_Customer_Info_Ip.recordset[0][0]);
	memset(Customer_info_RSA_b64,0,sizeof(Customer_info_RSA_b64));
	sprintf(Customer_info_RSA_b64,"%s",RSAEncryption_Base64(Customer_info,strlen(Customer_info)));

	memset(Customer_info_send_File,0,sizeof(Customer_info_send_File));
	sprintf(Customer_info_send_File,"/mnt/jffs2/%s_cust.txt",Executive_ID_Customer_Info);
  	pFile = fopen (Customer_info_send_File, "wb");
	fputs (Customer_info_RSA_b64,pFile);
  	fclose (pFile);

	memset(Executive_Customer_Info_File,0,sizeof(Executive_Customer_Info_File));
	sprintf(Executive_Customer_Info_File,"/mnt/jffs2/%s_info.zip",Executive_ID_Customer_Info);	
	Executive_Customer_Info_Valid=curl_Dra_Customer_Info_Data(Executive_Customer_Info_Url,Customer_info_send_File,Executive_Customer_Info_File);
		
	remove(Customer_info_send_File);
	if(Executive_Customer_Info_Valid==1)
	{
		response_myfile = fopen(Executive_Customer_Info_File,"r");
		memset(response_buffer,0,1000);
		while (!feof(response_myfile))
		{
			fgets(response_buffer,50,response_myfile);			
  		}
		fclose(response_myfile);
		if(strcmp(response_buffer,"ErrorCode:0001")==0)
		{
			lk_dispclr();
			lk_disptext(3,0,"    Bad Request ",0);
			sleep(2);
			remove(Executive_Customer_Info_File);
			return 0;
		}
		else if(strcmp(response_buffer,"ErrorCode:0002")==0)
		{
			lk_dispclr();
			lk_disptext(2,0,"     Please Enter",0);
			lk_disptext(4,0,"  Valid ExecutiveID",0);
			sleep(2);
			remove(Executive_Customer_Info_File);
			return 0;	
		}
		else if(strcmp(response_buffer,"ErrorCode:0004")==0)
		{
			lk_dispclr();
			lk_disptext(2,0,"  Error in Processing",0);
			lk_disptext(4,0,"      Request",0);
			sleep(2);
			remove(Executive_Customer_Info_File);
			return 0;
		}
		else if(strcmp(response_buffer,"ErrorCode:0008")==0)
		{
Allocated_Contact:
			lk_dispclr();
			lk_disptext(2,0,"   Allocation Not",0);
			lk_disptext(3,0,"Allocated,Pls Contact",0);
			lk_disptext(4,0,"   Your Agency",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			Contracts_key = lk_getkey();
			if(Contracts_key == ENTER)
			{
			close_sqlite();
			open_sqlite("/mnt/jffs2/hdfccc.db");
			execute("delete from allocation_details");
			execute("delete from cheque_bounce");			
			execute("delete from pay_type");
			execute("delete from trail_history");
			close_sqlite();
			open_sqlite("/mnt/jffs2/hdfccc.db");			
			execute("delete from PICKUP_DETAILS");
			close_sqlite();
			remove(Executive_Customer_Info_File);
			return 0;
			}
			else goto Allocated_Contact;	
		}
		else if(strcmp(response_buffer,"ErrorCode:00011")==0)
		{
further_Request_rt:
			lk_dispclr();
			lk_disptext(1,1,"Apologize for",0);
			lk_disptext(2,1,"inconvenience to",0);
			lk_disptext(3,1,"proceed further on",0);
			lk_disptext(4,1,"kindly do log-in.",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);				
			remove(Executive_Customer_Info_File);
			log_Request_key = lk_getkey();
			if(log_Request_key==ENTER)
			{				
			return 0;
			}
			else goto further_Request_rt; 
		}
		else if(strcmp(response_buffer,"<h1>Bad Request (Invalid Hostname)</h1>")==0)
		{
			remove(Executive_Customer_Info_File);
			return 0;
		}
		else if(strcmp(response_buffer,"<h1>Service Unavailable</h1>")==0)
		{
			remove(Executive_Customer_Info_File);
			return 0;
		}
		else if(strcmp(response_buffer,"0")==0)
		{
Unavailable_service:
			lk_dispclr();
			lk_disptext(2,0," Service Unavailable",0);
			lk_disptext(3,0," Pls Contact HO",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			Service_key = lk_getkey();
			if(Service_key==ENTER)
			{
			remove(Executive_Customer_Info_File);
			return 0;
			}
			else goto Unavailable_service;
		}
		else
		{
		memset(Executive_Customer_Info_File_chmod,0,sizeof(Executive_Customer_Info_File_chmod));
		sprintf(Executive_Customer_Info_File_chmod,"chmod 777 %s",Executive_Customer_Info_File);
		system(Executive_Customer_Info_File_chmod);
		system("cd /mnt/jffs2/");
		if(stat(Executive_Customer_Info_File,&st)==0)
		{
		memset(Executive_Customer_Info_File_unzip,0,sizeof(Executive_Customer_Info_File_unzip));
		sprintf(Executive_Customer_Info_File_unzip,"unzip %s",Executive_Customer_Info_File);
		system(Executive_Customer_Info_File_unzip);
		return 1;
		}
		else
		{
Service_File:
		lk_dispclr();
		lk_disptext(2,0,"  Service Unavailable",0);
		lk_disptext(4,0,"      Request",0);
		lk_disptext(5,0,"Press Enter to Cont.",0);
		remove(Executive_Customer_Info_File);
		Error_Service_key = lk_getkey();
		if(Error_Service_key==ENTER)
		{
		return 0;
		}
		else goto Service_File;		
		}		
		}		
	}
	else if(Executive_Customer_Info_Valid==0)
	{
		remove(Executive_Customer_Info_File);
		return 0;
	}
	else
	{
		lk_dispclr();
		lk_disptext(2,0,"    Please Try",0);
		lk_disptext(4,0,"      Again",0);
		sleep(2);
		remove(Executive_Customer_Info_File);
		return 0;	
	}
}

/******************************************************************/
int Pickup_Info_Request(char* Executive_ID_Pickup_info)
{
	int Contracts_key,Service_key,Error_Service_key,log_Request_key;
	char Executive_Customer_Info_Url[250],Customer_info_send_File[200],Customer_info_send_File_remove[200],Executive_Customer_Info_File[100],Executive_Customer_Info_File_chmod[100],Executive_Customer_Info_File_unzip[100];
	unsigned char response_buffer[1000];
	char Customer_info[100],Customer_info_RSA_b64[500];
	FILE * pFile;
	FILE * response_myfile;
	resultset Executive_Customer_Info_Ip;
	unsigned int Executive_Customer_Info_Valid=0,ret_customer=0;	

	printf("\nIn Pick Customer Details");
	memset(&Executive_Customer_Info_Ip,0,sizeof(Executive_Customer_Info_Ip));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Executive_Customer_Info_Ip=get_result("Select hwid,httpip,localhttpip,Current_Software_Ver from operationalstatus;");

	memset(Executive_Customer_Info_Url,0,sizeof(Executive_Customer_Info_Url));

#if URL_NETMAGIC
	sprintf(Executive_Customer_Info_Url,"%s/HHTRequest/v%s/RequestForPickUp.aspx",Executive_Customer_Info_Ip.recordset[0][1],Executive_Customer_Info_Ip.recordset[0][3]);
#endif
#if PRINTF
	printf("\nExecutive_Customer_Info_Url:%s",Executive_Customer_Info_Url);	
#endif
	memset(Customer_info,0,sizeof(Customer_info));
	sprintf(Customer_info,"Validate,%s,%s",Executive_ID_Pickup_info,Executive_Customer_Info_Ip.recordset[0][0]);
	memset(Customer_info_RSA_b64,0,sizeof(Customer_info_RSA_b64));
	sprintf(Customer_info_RSA_b64,"%s",RSAEncryption_Base64(Customer_info,strlen(Customer_info)));

	memset(Customer_info_send_File,0,sizeof(Customer_info_send_File));
	sprintf(Customer_info_send_File,"/mnt/jffs2/%s_pickup.txt",Executive_ID_Pickup_info);
  	pFile = fopen (Customer_info_send_File, "wb");
	fputs (Customer_info_RSA_b64,pFile);
  	fclose (pFile);

	memset(Executive_Customer_Info_File,0,sizeof(Executive_Customer_Info_File));
	sprintf(Executive_Customer_Info_File,"/mnt/jffs2/%s_pickup.zip",Executive_ID_Pickup_info);	
	Executive_Customer_Info_Valid=curl_Dra_Customer_Pickup_Info_Data(Executive_Customer_Info_Url,Customer_info_send_File,Executive_Customer_Info_File);
		
	remove(Customer_info_send_File);
	if(Executive_Customer_Info_Valid==1)
	{
		response_myfile = fopen(Executive_Customer_Info_File,"r");
		memset(response_buffer,0,1000);
		while (!feof(response_myfile))
		{
			fgets(response_buffer,50,response_myfile);			
  		}
		fclose(response_myfile);
		if(strcmp(response_buffer,"ErrorCode:0001")==0)
		{
			lk_dispclr();
			lk_disptext(3,0,"    Bad Request ",0);
			sleep(2);
			remove(Executive_Customer_Info_File);
			return 0;
		}
		else if(strcmp(response_buffer,"ErrorCode:0002")==0)
		{
			lk_dispclr();
			lk_disptext(2,0,"     Please Enter",0);
			lk_disptext(4,0,"  Valid DRA ID",0);
			sleep(2);
			remove(Executive_Customer_Info_File);
			Dra_login();	
		}
		else if(strcmp(response_buffer,"ErrorCode:0004")==0)
		{
			lk_dispclr();
			lk_disptext(2,0,"  Error in Processing",0);
			lk_disptext(4,0,"      Request",0);
			sleep(2);
			remove(Executive_Customer_Info_File);
			return 0;
		}
		else if(strcmp(response_buffer,"ErrorCode:0008")==0)
		{
Allocated_Contact:
			lk_dispclr();
			lk_disptext(2,0,"  Pickup Not",0);
			lk_disptext(3,0,"Allocated,Pls Contact",0);
			lk_disptext(4,0,"   Your Agency",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			Contracts_key = lk_getkey();
			if(Contracts_key == ENTER)
			{
			close_sqlite();
			open_sqlite("/mnt/jffs2/hdfccc.db");			
			execute("delete from PICKUP_DETAILS");
			close_sqlite();
			remove(Executive_Customer_Info_File);
			return 0;
			}
			else goto Allocated_Contact;	
		}
		else if(strcmp(response_buffer,"ErrorCode:00011")==0)
		{
further_Request_rt:
			lk_dispclr();
			lk_disptext(1,1,"Apologize for",0);
			lk_disptext(2,1,"inconvenience to",0);
			lk_disptext(3,1,"proceed further on",0);
			lk_disptext(4,1,"kindly do log-in.",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);				
			remove(Executive_Customer_Info_File);
			log_Request_key = lk_getkey();
			if(log_Request_key==ENTER)
			{				
			return 0;
			}
			else goto further_Request_rt; 
		}
		else if(strcmp(response_buffer,"<h1>Bad Request (Invalid Hostname)</h1>")==0)
		{
			remove(Executive_Customer_Info_File);
			return 0;
		}
		else if(strcmp(response_buffer,"<h1>Service Unavailable</h1>")==0)
		{
			//remove(Executive_Customer_Info_File);
			return 0;
		}
		else if(strcmp(response_buffer,"0")==0)
		{
Unavailable_service:
			lk_dispclr();
			lk_disptext(2,0," Service Unavailable",0);
			lk_disptext(3,0," Pls Contact HO",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			Service_key = lk_getkey();
			if(Service_key==ENTER)
			{
			remove(Executive_Customer_Info_File);
			return 0;
			}
			else goto Unavailable_service;
		}
		else
		{
		memset(Executive_Customer_Info_File_chmod,0,sizeof(Executive_Customer_Info_File_chmod));
		sprintf(Executive_Customer_Info_File_chmod,"chmod 777 %s",Executive_Customer_Info_File);
		system(Executive_Customer_Info_File_chmod);
		system("cd /mnt/jffs2/");
		
		memset(Executive_Customer_Info_File_unzip,0,sizeof(Executive_Customer_Info_File_unzip));
		sprintf(Executive_Customer_Info_File_unzip,"unzip %s",Executive_Customer_Info_File);
		ret_customer=system(Executive_Customer_Info_File_unzip);
		if(ret_customer==0)
		{
			printf("\nsuccess pikup");
			return 1;
		}
		else
		{
			printf("\nfail pikup");
			return 0;
		}				
		}		
	}
	else if(Executive_Customer_Info_Valid==0)
	{		
		remove(Executive_Customer_Info_File);
		return 0;
		//Dra_login();
	}
	else
	{
		lk_dispclr();
		lk_disptext(2,0,"    Please Try",0);
		lk_disptext(4,0,"      Again",0);
		sleep(2);
		remove(Executive_Customer_Info_File);
		return 0;
	}
}


/******************************************************************/
int Download_Certificate_check()
{
	char Public_certificate_Verify_Url[250],publiccertificate_file[100],certif_info[100],Executive_Verify_File_login_remove[100],Certificate_File[100],Executive_Verify_File_unzip[100];
	unsigned char response_buffer[1000],Customer_publiccertificate_RSA_b64[100];
	char Exec_info[100],Executive_login_RSA_b64[500],Final_public_URL[200];
	FILE * response_myfile;
	resultset Executive_Ip;
	unsigned int Certificate_Code_Valid=0,shutdown_key=0;	
	FILE * pFile;

	memset(&Executive_Ip,0,sizeof(Executive_Ip));

	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Executive_Ip=get_result("Select hwid,httpip,localhttpip,hwid,Current_Software_Ver from operationalstatus;");

	memset(Public_certificate_Verify_Url,0,sizeof(Public_certificate_Verify_Url));
#if URL_NETMAGIC	
	sprintf(Public_certificate_Verify_Url,"%s/HHTRequest/v%s/publiccertificate.aspx?Requestdata=",Executive_Ip.recordset[0][1],Executive_Ip.recordset[0][4]);	
#endif
	
	memset(certif_info,0,sizeof(certif_info));
	sprintf(certif_info,"Validate,%s",Executive_Ip.recordset[0][3]);
	printf("\ncertif_info:%s\n",certif_info);
	memset(Customer_publiccertificate_RSA_b64,0,sizeof(Customer_publiccertificate_RSA_b64));
	sprintf(Customer_publiccertificate_RSA_b64,"%s",certif_info);	
	memset(publiccertificate_file,0,sizeof(publiccertificate_file));

	strcpy(publiccertificate_file,"public.cer");
	
	memset(Final_public_URL,0,sizeof(Final_public_URL));
	sprintf(Final_public_URL,"%s%s",Public_certificate_Verify_Url,certif_info);		
	printf("\nFinal_public_URL:%s",Final_public_URL);  	
	memset(Certificate_File,0,sizeof(Certificate_File));
	sprintf(Certificate_File,"/mnt/jffs2/%s.txt",Executive_Ip.recordset[0][3]);		

	printf("\nPublic_certificate_Verify_Url:%s",Public_certificate_Verify_Url);
	printf("\nCertificate_File:%s",Certificate_File);

	Certificate_Code_Valid=curl_publiccertificate_Verification_request(Final_public_URL,publiccertificate_file);		
	
	printf("\nPublic certificate Verify_Url");
	if(Certificate_Code_Valid==1)
	{		
		response_myfile = fopen(publiccertificate_file,"r");
		memset(response_buffer,0,sizeof(response_buffer));
		while (!feof(response_myfile))
		{
			fgets(response_buffer,50,response_myfile);

  		}		
		fclose(response_myfile);
		if(strcmp(response_buffer,"ErrorCode:0001")==0)
		{
Error_screen_Bad:
			lk_dispclr();
			lk_disptext(2,0,"    Bad Request ",0);
			lk_disptext(4,0,"Press Ent. to Shutd.",0);
			shutdown_key=lk_getkey();
			if(shutdown_key==ENTER)
			{
			remove(publiccertificate_file);
			lk_dispclr();
			lk_disptext(2,1,"  Please Wait...",1);
	   		lk_disptext(4,1,"  Shutdown",1);
			close_sqlite();
			lk_close();
			system("poweroff"); 	
			sleep(20);
			system("shutdown");
			sleep(10);
			}
			else goto Error_screen_Bad;
		}
		else if(strcmp(response_buffer,"ErrorCode:0002")==0)
		{
			lk_dispclr();
			lk_disptext(2,3," Please Enter",0);
			lk_disptext(3,1," Valid DRA ID",0);
			sleep(2);
			remove(publiccertificate_file);
			return 0;	
		}
		else if(strcmp(response_buffer,"ErrorCode:0003")==0)
		{
			lk_dispclr();
			lk_disptext(2,0,"   Login ",0);
			lk_disptext(4,0,"     Failure",0);
			sleep(2);
			remove(publiccertificate_file);
			return 0;
		}
		else if(strcmp(response_buffer,"ErrorCode:0004")==0)
		{
			lk_dispclr();
			lk_disptext(2,0,"  Error in Processing  ",0);
			lk_disptext(4,0,"      Request",0);
			sleep(2);
			remove(publiccertificate_file);	
			return 0;		
		}
		else if(strcmp(response_buffer,"ErrorCode:0005")==0)
		{
			lk_dispclr();
			lk_disptext(2,0,"     DRA Is Not ",0);
			lk_disptext(4,0,"      Enrolled",0);
			sleep(2);
			remove(publiccertificate_file);	
			return 0;		
		}
		else if(strcmp(response_buffer,"ErrorCode:0007")==0)
		{
			lk_dispclr();
			lk_disptext(2,0,"  Machine is Locked",0);
			lk_disptext(4,0,"  Please contact HO",0);
			sleep(2);
			remove(Certificate_File);
			close_sqlite();			
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			execute("UPDATE operationalstatus SET ExecutiveStatus='0';");	
			return 0;			
		}
		else if(strcmp(response_buffer,"ErrorCode:0008")==0)
		{
			lk_dispclr();
			lk_disptext(2,0,"  Error in Processing  ",0);
			lk_disptext(4,0,"      Request",0);
			sleep(2);
			remove(publiccertificate_file);
			return 0;
		}
		else if(strcmp(response_buffer,"<h1>Bad Request (Invalid Hostname)</h1>")==0)
		{
			remove(Certificate_File);
			return 0;
		}
		else if(strcmp(response_buffer,"<h1>Service Unavailable</h1>")==0)
		{
			remove(publiccertificate_file);
			return 0;
		}		
		else
		{
		printf("\nPublic certificate Found");		
		system("chmod 777 /mnt/jffs2/public.cer");		
		return 0;
		}
	}
	else if(Certificate_Code_Valid==0)
	{
		remove(publiccertificate_file);
		Certificate_Code_Notfound();
	}
	else
	{
		lk_dispclr();
		lk_disptext(2,0,"    Please Try",0);
		lk_disptext(4,0,"      Again",0);
		sleep(2);
		remove(publiccertificate_file);
		return 0;		
	}	
}


/******************************************************************/
int Certificate_Code_Notfound()
{
	int shutdown_key;	
	
	lk_dispclr();
	lk_disptext(2,0,"     Updating Info",0);
	lk_disptext(3,0,"   Verify Not Found",0);
	lk_disptext(4,0,"   Please Try Again.",0);
	lk_disptext(5,0,"Press Ent. to Shutdown",0);
	shutdown_key=lk_getkey();
	if(shutdown_key==ENTER)
	{	
	lk_dispclr();
	lk_disptext(2,1,"  Please Wait...",1);
	lk_disptext(4,1,"  Shutdown",1);
	close_sqlite();
	lk_close();
	system("poweroff"); 	
	sleep(20);
	system("shutdown");
	sleep(10);			
	}
	else Certificate_Code_Notfound();
}

/*******************************************************************/
int Dra_PWD_Request(char* Cha_Pwd_Request)
{
	int Pass_key,Bad_key,Executive_key,Please_Try_key,Please_Again_key;
	char Executive_change_Pwd_Url[250],Executive_change_Pwd_send_File[100],Executive_change_Pwd_File[100];
	unsigned char change_Pwd_response_buffer[100],dec_executive_pwd[25],enc_executive_pwd[25];
	unsigned int Executive_change_Pwd_Valid;
	char Executive_change_Pwd[100],Executive_change_Pwd_RSA_b64[500];
	FILE * pFile;
	FILE * response_myfile;
	resultset change_Pwd;

	memset(&change_Pwd,0,sizeof(change_Pwd));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	
	change_Pwd = get_result("SELECT httpip,Executive_id,hwid,Current_Software_Ver FROM operationalstatus;");
	memset(Executive_change_Pwd_Url,0,sizeof(Executive_change_Pwd_Url));

#if URL_NETMAGIC
	sprintf(Executive_change_Pwd_Url,"%s/HHTRequest/v%s/ChangePasswordExecutive.aspx",change_Pwd.recordset[0][0],change_Pwd.recordset[0][3]);
#endif
	memset(dec_executive_pwd,0,sizeof(dec_executive_pwd));
	sprintf(dec_executive_pwd,"%s",Decrypted_sqlite_DB(change_Pwd.recordset[0][1]));

	memset(Executive_change_Pwd,0,sizeof(Executive_change_Pwd));
	sprintf(Executive_change_Pwd,"Validate,%s,%s,%s",dec_executive_pwd,change_Pwd.recordset[0][2],Cha_Pwd_Request);
	memset(Executive_change_Pwd_RSA_b64,0,sizeof(Executive_change_Pwd_RSA_b64));
	sprintf(Executive_change_Pwd_RSA_b64,"%s",RSAEncryption_Base64(Executive_change_Pwd,strlen(Executive_change_Pwd)));

	memset(Executive_change_Pwd_send_File,0,sizeof(Executive_change_Pwd_send_File));
	sprintf(Executive_change_Pwd_send_File,"/mnt/jffs2/%s_pwd.txt",change_Pwd.recordset[0][2]);
  	pFile = fopen (Executive_change_Pwd_send_File, "wb");
	fputs (Executive_change_Pwd_RSA_b64,pFile);
  	fclose (pFile);
	
	memset(Executive_change_Pwd_File,0,sizeof(Executive_change_Pwd_File));
	sprintf(Executive_change_Pwd_File,"/mnt/jffs2/%s.txt",change_Pwd.recordset[0][2]);
	Executive_change_Pwd_Valid=curl_Dra_change_Pwd_Request_Data(Executive_change_Pwd_Url,Executive_change_Pwd_send_File,Executive_change_Pwd_File);
	remove(Executive_change_Pwd_send_File);
	if(Executive_change_Pwd_Valid==1)
	{
		response_myfile = fopen(Executive_change_Pwd_File,"r");
		memset(change_Pwd_response_buffer,0,100);
		while (!feof(response_myfile))
		{
			fgets(change_Pwd_response_buffer,50,response_myfile);			
  		}
		fclose(response_myfile);
		if(strcmp(change_Pwd_response_buffer,"Success")==0)
		{
			lk_dispclr();
			lk_disptext(2,0,"  Password Changed",0);
			lk_disptext(4,0,"    Successfully",0);
			sleep(2);			
			memset(enc_executive_pwd,0,sizeof(enc_executive_pwd));
			sprintf(enc_executive_pwd,"%s",Encrypted_sqlite_DB(Cha_Pwd_Request));
			execute("UPDATE operationalstatus SET Executive_Password='%s';",enc_executive_pwd);
			remove(Executive_change_Pwd_File);
			return 1;
		}
		if(strcmp(change_Pwd_response_buffer,"ErrorCode:0003")==0)
		{
Pass_Enter:
			lk_dispclr();
			lk_disptext(2,0,"  Change Password",0);
			lk_disptext(4,0,"     Failure",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			Pass_key = lk_getkey();
			if(Pass_key==ENTER)
			{
			remove(Executive_change_Pwd_File);				
			Dra_login();
			}
			else goto Pass_Enter;
		}				
		else if(strcmp(change_Pwd_response_buffer,"ErrorCode:0001")==0)
		{
Bad_Request_Key:
			lk_dispclr();
			lk_disptext(2,0,"    Bad Request ",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);			
			Bad_key = lk_getkey();
			if(Bad_key==ENTER)
			{
			printf("\nResponse buffer:%s",change_Pwd_response_buffer);
			remove(Executive_change_Pwd_File);
			Dra_login();
			}
			else goto Bad_Request_Key;
		}
		else if(strcmp(change_Pwd_response_buffer,"ErrorCode:0004")==0)
		{
Error_Process:
			lk_dispclr();
			lk_disptext(1,0," Password is already",0);
			lk_disptext(2,0,"  used, try another",0);
			lk_disptext(3,0,"      one",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			Bad_key = lk_getkey();
			if(Bad_key==ENTER)
			{			
			remove(Executive_change_Pwd_File);
			Dra_login();
			}
			else goto Error_Process;
		}
		else if(strcmp(change_Pwd_response_buffer,"ErrorCode:0002")==0)
		{
Valid_Executive:
			lk_dispclr();
			lk_disptext(2,3,"  Please Enter",0);
			lk_disptext(4,1,"  Valid DRA ID",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			Executive_key = lk_getkey();
			if(Executive_key==ENTER)
			{			
			remove(Executive_change_Pwd_File);
			Dra_login();
			}
			else goto Valid_Executive;
		}
		else
		{
Try_Again:
			lk_dispclr();
			lk_disptext(2,0,"    Please Try",0);
			lk_disptext(4,0,"    Again Later",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			Please_Try_key = lk_getkey();
			if(Please_Try_key==ENTER)
			{			
			remove(Executive_change_Pwd_File);
			Dra_login();
			}
			else goto Try_Again;
		}
	}
	else
	{
Try_Cont:
		lk_dispclr();
		lk_disptext(2,0," Can't Proceed for",0);
		lk_disptext(4,0,"  password reset",0);
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Please_Again_key = lk_getkey();
		if(Please_Again_key==ENTER)		
		{
		remove(Executive_change_Pwd_File);
		Dra_login();
		}
		else goto Try_Cont;
	}
}

/******************************************************************/
/******************************************************************/
int Version_SIMNo_Request()
{
	char HHT_Version_SIMNo_Url[200],HHT_Version_SIMNo_Send_file[100],HHT_Version_SIMNo_file[100];
	resultset Soft_Version_SIMNo;
	unsigned char Version_SIMNo_No_buffer[50];
	char HHT_Version_SIMNo[100],HHT_Version_SIMNo_RSA_b64[500];
	FILE * pFile;
	FILE * Version_SIMNo_response_myfile;
	unsigned int Version_SIMNo_Response;	
	

	memset(&Soft_Version_SIMNo,0,sizeof(Soft_Version_SIMNo));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Soft_Version_SIMNo=get_result("Select httpip,localhttpip,hwid,Current_Software_Ver,SIM_ICCID From operationalstatus;");

	memset(HHT_Version_SIMNo_Url,0,sizeof(HHT_Version_SIMNo_Url));
#if URL_TEST
	sprintf(HHT_Version_SIMNo_Url,"%s/HHTRequest/v%s/HHTToVersionUpdate.aspx",Soft_Version_SIMNo.recordset[0][0],Soft_Version_SIMNo.recordset[0][3]);
#endif
#if URL_NETMAGIC
	sprintf(HHT_Version_SIMNo_Url,"%s/HHTRequest/v%s/HHTToVersionUpdate.aspx",Soft_Version_SIMNo.recordset[0][0],Soft_Version_SIMNo.recordset[0][3]);
#endif

	memset(HHT_Version_SIMNo,0,sizeof(HHT_Version_SIMNo));
	sprintf(HHT_Version_SIMNo,"Validate,%s,%s,%s",Soft_Version_SIMNo.recordset[0][2],Soft_Version_SIMNo.recordset[0][3],Soft_Version_SIMNo.recordset[0][4]);


	printf("HHT_Version_SIMNo_Url:%s,%s",HHT_Version_SIMNo_Url,HHT_Version_SIMNo);

	memset(HHT_Version_SIMNo_RSA_b64,0,sizeof(HHT_Version_SIMNo_RSA_b64));
	sprintf(HHT_Version_SIMNo_RSA_b64,"%s",RSAEncryption_Base64(HHT_Version_SIMNo,strlen(HHT_Version_SIMNo)));

	memset(HHT_Version_SIMNo_Send_file,0,sizeof(HHT_Version_SIMNo_Send_file));
	sprintf(HHT_Version_SIMNo_Send_file,"/mnt/jffs2/%s_ver.txt",Soft_Version_SIMNo.recordset[0][2]);
  	pFile = fopen (HHT_Version_SIMNo_Send_file, "wb");
	fputs (HHT_Version_SIMNo_RSA_b64,pFile);
  	fclose (pFile);
	memset(HHT_Version_SIMNo_file,0,sizeof(HHT_Version_SIMNo_file));
	sprintf(HHT_Version_SIMNo_file,"/mnt/jffs2/%s.txt",Soft_Version_SIMNo.recordset[0][2]);
	Version_SIMNo_Response=curl_HHT_Version_url_request(HHT_Version_SIMNo_Url,HHT_Version_SIMNo_Send_file,HHT_Version_SIMNo_file);
	remove(HHT_Version_SIMNo_Send_file);
	if(Version_SIMNo_Response==1)
	{
///////////////////////
		Version_SIMNo_response_myfile = fopen(HHT_Version_SIMNo_file,"r");
		memset(Version_SIMNo_No_buffer,0,sizeof(Version_SIMNo_No_buffer));
		while (!feof(Version_SIMNo_response_myfile))
		{
			fgets(Version_SIMNo_No_buffer,50,Version_SIMNo_response_myfile);

                }
	        fclose(Version_SIMNo_response_myfile);
		if(strcmp(Version_SIMNo_No_buffer,"Success")==0)
		{			
			close_sqlite();			
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			execute("Update operationalstatus SET GPRS_Setting_Flag=1;");
		}		
	}
	remove(HHT_Version_SIMNo_file);
	return SUCCESS;
}

/*******************************************************************/
int Machine_Surrender_Request(char* Surrender_Request_DRA)
{
	int Surrender_Post_Request_Response,Surrender_HO,Executive_key_Surrender;
	char Surrender_Verify_Url[200],Surrender_info[200],Surrender_timenow[20],Surrender_datenow[20],Surrender_RSA_b64[500],Surrender_Verify_File[50],Surrender_Transaction_File[50],Surrender_No_buffer[1000];
	FILE * pFile;
	FILE * Surrender_response_myfile;
	resultset Executive_Ip,unuploaded_tran_surrender,unuploaded_folloup_surrender;

	memset(&Executive_Ip,0,sizeof(Executive_Ip));
	memset(&unuploaded_tran_surrender,0,sizeof(unuploaded_tran_surrender));
	memset(&unuploaded_folloup_surrender,0,sizeof(unuploaded_folloup_surrender));

	
	Upload_Transaction_Request_For_Thrd();
	Upload_FollowUp_Request_For_Thrd();
	unuploaded_tran_surrender=get_result("SELECT * FROM unuploadedtransactiondetails;");
	unuploaded_folloup_surrender=get_result("SELECT * FROM unuploadedfollowupdetails;");
	if((unuploaded_tran_surrender.rows==0)&&(unuploaded_folloup_surrender.rows==0))
	{
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Executive_Ip=get_result("Select Executive_id,hwid,httpip,localhttpip,Current_Software_Ver from operationalstatus;");
	memset(Surrender_Verify_Url,0,sizeof(Surrender_Verify_Url));
#if URL_TEST	
	sprintf(Surrender_Verify_Url,"%s/HHTRequest/v%s/Machinesurrender.aspx",Executive_Ip.recordset[0][2],Executive_Ip.recordset[0][4]);
#endif
#if URL_NETMAGIC	
	sprintf(Surrender_Verify_Url,"%s/HHTRequest/v%s/Machinesurrender.aspx",Executive_Ip.recordset[0][2],Executive_Ip.recordset[0][4]);
#endif	

	memset(Surrender_timenow,0,sizeof(Surrender_timenow));
	sprintf(Surrender_timenow,"%02d=%02d=%02d",curt.tm_hour,curt.tm_min,curt.tm_sec);	
	memset(Surrender_datenow,0,sizeof(Surrender_datenow));
	sprintf(Surrender_datenow,"%02d=%02d=%04d",(curt.tm_mon+1),(curt.tm_mday),(curt.tm_year+1900));
	memset(Surrender_info,0,sizeof(Surrender_info));
	sprintf(Surrender_info,"Validate,%s,%s,%s,%s",Surrender_Request_DRA,Executive_Ip.recordset[0][1],Surrender_datenow,Surrender_timenow);
	printf("%s\n",Surrender_info);
	memset(Surrender_RSA_b64,0,sizeof(Surrender_RSA_b64));
	sprintf(Surrender_RSA_b64,"%s",RSAEncryption_Base64(Surrender_info,strlen(Surrender_info)));	
	memset(Surrender_Verify_File,0,sizeof(Surrender_Verify_File));
	sprintf(Surrender_Verify_File,"/mnt/jffs2/%s_surrender.txt",Surrender_Request_DRA);		
  	pFile = fopen (Surrender_Verify_File, "wb");
	fputs (Surrender_RSA_b64,pFile);
  	fclose (pFile);
        sprintf(Surrender_Transaction_File,"/mnt/jffs2/%s.txt",Surrender_Request_DRA);
	printf("\nSurrender_Transaction_File:%s",Surrender_Transaction_File);
	Surrender_Post_Request_Response=curl_Surrender_Data(Surrender_Verify_Url,Surrender_Verify_File,Surrender_Transaction_File);
	
	remove(Surrender_Verify_File);
	if(Surrender_Post_Request_Response==1)
	{
		
		Surrender_response_myfile = fopen(Surrender_Transaction_File,"r");
		memset(Surrender_No_buffer,0,sizeof(Surrender_No_buffer));		
		while (!feof(Surrender_response_myfile))
		{			
			fgets(Surrender_No_buffer,500,Surrender_response_myfile);
			printf("\nResponse buffer:%s",Surrender_No_buffer);
			printf("\nResponse buffer length :%d",strlen(Surrender_No_buffer));
                }
                fclose(Surrender_response_myfile);
		if(strcmp(Surrender_No_buffer,"Success")==0)
		{
Machin_Surrender:
			lk_dispclr();
			lk_disptext(2,0," Machine Surrendered ",0);
			lk_disptext(3,0,"    Successfully",0);	
			lk_disptext(5,0,"Enter to Shutdown.",0);
			Executive_key_Surrender = lk_getkey();
			if(Executive_key_Surrender==ENTER)
			{			
			close_sqlite();			
			system("cd /mnt/jffs2/");
			system("rm *.pb *.zip *.txt");
			system("rm /mnt/jffs2/hdfccc.db");
			system("rm /mnt/jffs2/hdfccctrans.db");
			close_sqlite();
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			execute("Update operationalstatus SET Executive_id='HDFC@1234';");
			remove(Surrender_Transaction_File);
			close_sqlite();	
			lk_dispclr();
			lk_disptext(3,2,"Shutdown...",1);
			ppp_close();
			lk_close();   
			system("poweroff");
			sleep(13);	
			}
			else goto Machin_Surrender;							
		}
		else
		{
Surrender_Again:
			lk_dispclr();
			lk_disptext(2,0," Machine Surrendered",0);
			lk_disptext(4,0," Failure Please Contact",0);
			lk_disptext(5,0,"         HO",0);
			Surrender_HO=lk_getkey();
			if(Surrender_HO==ENTER)
			{			
			remove(Surrender_Transaction_File);
			}
			else goto Surrender_Again;			
		}	
	}
	else
	{

		lk_dispclr();
		lk_disptext(2,0," Machine Surrendered",0);
		lk_disptext(4,0,"  Failure Try Again",0);
		sleep(5);
		remove(Surrender_Transaction_File);
	}
	}
	else
	{
		lk_dispclr();
		lk_disptext(2,0," Machine Surrendered",0);
		lk_disptext(4,0,"  Failure Try Again",0);
		sleep(5);		
	}
After_main();
}

