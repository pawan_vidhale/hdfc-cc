#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<termios.h>
#include<string.h>
#include<stdio.h>
#include<unistd.h>
#include<termios.h>
#include<sys/types.h>
#include<signal.h>
#include<fcntl.h>
#include<string.h>
#include<sys/time.h>
#include<sys/ioctl.h>
#include<curl.h>
#include<header.h>

#define BAUD 115200

char gprsip[50]="";
int gprsconnection=0;
void ppp_close(void);


/******************************************************************/
int gprs_ppp_test ( void )
{
        unsigned int test;
        int sigstrength=0,ret=0;
        char buff[30]="";

	while(1)
	{
			
                ppp_close();
		sleep(5);
		ret=network_registration();

		lk_dispclr();
		if(ret==0 || ret==1)
                lk_disptext(0,1,"Network Registered",0);
		else
                lk_disptext(0,1,"Network not Regist.",0);

                sigstrength=  signal_strength();

/*
                if (sigstrength<0)
                        return -1;

                else if (sigstrength < 8)
                        return -2;
*/

                sprintf(buff,"Signal Strength %d",sigstrength);

                lk_disptext(1,1,buff,0);
                lk_disptext(2,1,"Connecting to server ",1);
                lk_disptext(4,3,"  Please Wait",1);

		//if (ppp_open() <0)

		if(ppp_open() < 0)
		{
		sleep(2);
		ppp_close();
                if(ppp_open() < 0)
                {
                        lk_dispclr();
                        lk_disptext(2,0,"Connection Failed",0);
                        lk_disptext(3,0,"1.Try Again(GPRS)",0);
                        lk_disptext(4,0,"2.Continue(offline)",0);
                        gprsconnection=0;                      
                        test=lk_getkey();
                        if(test==1)
                        {
				lk_dispclr();
				lk_disptext(2,0,"Please Wait...",0);
                                continue;
                        }
                        else if(test==2)
			{
			return -1;
			}
                }
                else gprsconnection=1;
		}
		else gprsconnection=1;
                gett_ip();
                break;
        }
        return 0;
}


/******************************************************************/
int pppdial_gprs(char *dialno)
{
char str[1000];
FILE *rfp;


printf("\nTest dialno\n");
sprintf(str,"#!/bin/sh\nTELEPHONE=\"%s\" \nexport TELEPHONE \nrm -f /var/run/ppp.link \nexec /usr/sbin/pppd    \\\n        debug /dev/ttymxc0  \\\n        115200 0.0.0.0:0.0.0.0  \\\n        connect /etc/ppp/ppp-on-dialer",dialno);

rfp=fopen("/etc/ppp/dialout","w");
if(rfp<0)return -1;
fprintf(rfp,"%s",str);
fclose(rfp);
#ifdef TEST
printf("%s",str);
printf("\n");
#endif
return 0;
}

/******************************************************************/
int  ppp_options(char *userid,char *password)
{
char str[1000];
FILE *rfp;

sprintf(str,"-detach\nlock\nasyncmap 0\ncrtscts\ndefaultroute\nusepeerdns\nmodem\nmru 552\nmtu 552\nname %s\npassword %s",userid,password);

rfp=fopen("/etc/ppp/options","w");
if(rfp<0) return -1;
fprintf(rfp,"%s",str);
fclose(rfp);
#ifdef TEST
printf("%s",str);
printf("\n");
#endif

return 0;
}

/******************************************************************/
int ppp_dial_update(char *dialno,char *gprsdomain)
{
char str[1000];
FILE *rfp;
sprintf(str,"#!/bin/sh\n/usr/sbin/chat -v      \\\n        TIMEOUT         5    \\\n        ABORT           \'\\nBUSY\\r\'      \\\n        ABORT           \'\\nNO ANSWER\\r\' \\\n        ABORT          \'\\nRINGING\\r\\n\\r\\nRINGING\\r\'    \\\n        ECHO            ON      \\\n        \'\'               AT     \\\n        \'OK-+++\\C-OK'   ATH0    \\\n        \'\'              AT+CSQ      \\\n        OK              \'AT+CGATT=1\' \\\n        OK              \'AT+CGDCONT=1,\"IP\",\"%s\"\' \\\n        TIMEOUT         50      \\\n        OK              ATDT\'%s\'  \\\n        CONNECT         \'\'",gprsdomain,dialno);

rfp=fopen("/etc/ppp/ppp-on-dialer","w");
if(rfp<0) return -1;
fprintf(rfp,"%s",str);
fclose(rfp);
#ifdef TEST
printf("%s",str);
printf("\n");
#endif
return 0;
}

/******************************************************************/
int gett_ip()
{
      int fd;
      struct ifreq ifr;
      fd=socket(AF_INET,SOCK_DGRAM,0);
      ifr.ifr_addr.sa_family = AF_INET;
      strncpy(ifr.ifr_name,"ppp0",IFNAMSIZ-1);
      ioctl(fd,SIOCGIFADDR,&ifr);
      close(fd);
      sprintf(gprsip,"%d.%d.%d.%d",(unsigned char)ifr.ifr_addr.sa_data[2],
                                   (unsigned char)ifr.ifr_addr.sa_data[3],
                                   (unsigned char)ifr.ifr_addr.sa_data[4],
                                   (unsigned char)ifr.ifr_addr.sa_data[5],
                                   (unsigned char)ifr.ifr_addr.sa_data[6]);
     return 0;
}

/******************************************************************/
int copy_file (char *dest,char *src)
{
        unsigned char c[512];
        int in, out,n;

        in = open(src , O_RDONLY);
        out = open(dest, O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR);
        if (in == -1 || out == -1)
        {
         fprintf(stderr,"unable to oepn file %d %d\n" ,in,out);
         return -1;
        }
        while(1)
         {
                n=read(in,c,512);
                if (n==0)
                break;
                write(out,c,n);
         }
        close(in);
        close(out);
}

/******************************************************************/
int gsm_init()
{
	int con_try_gprs;
	int simstatus=0,nwreg=0,signal=0,result=0,ret_update=0,loop_count=0;
	char buff[30]="";
	int try=0;	
	
	system("kill -9 `pidof gl14_cfun`");
label:
	if(try++>4)return -1;
						
	sleep(5);
	system("sim2_enable");
	
	update_9600_115200_check();	
	fprintf(stdout,"Checking for SIM CARD ...\n");
	printf("\nSim card status:%d",simstatus);
	for(loop_count=0;loop_count<8;loop_count++)
	{
		printf("\ntry:%d",try);
		simstatus=sim_status_test();	
		if(try==4)
		{
			result=0;	
			break;
		}
		if(simstatus==0)
		{			
			fprintf(stdout,"SIM Card Found\n");
			result=1;	
			break;
		}		
		else if(loop_count==3)
		{			
			goto label;
		}
	}
	
	if(result!=1&&simstatus==-1) sim_not_inserted();
	else iccid_test();	
sim_network:	
	result=0;
	try=0;	
	nwreg=network_registration();
	if(nwreg==0)
	{
		nwreg=0;
		lk_dispclr();
		lk_disptext(2,0,"  SIM NETWORK IS",0);
		lk_disptext(3,0,"     REGISTERED ",0);	
		fprintf(stdout,"SIM NETWORK IS REGISTERED\n");		
	}
	else if(nwreg==1)
	{
		lk_dispclr();
		lk_disptext(2,0,"    SIM NETWORK IS",0);
		lk_disptext(3,0," REGISTERED WITH ROAMING",0); 
		fprintf(stdout,"SIM NETWORK IS REGISTERED WITH ROAMING\n");		
	}
	else 	
	{
		lk_dispclr();
		lk_disptext(2,0,"     SIM Network",0);
		lk_disptext(3,0,"  Registration Failed",0);
		fprintf(stdout,"SIM Network Registration Failed\n");
	}

	update_scripts();
ppp_start:
	fprintf(stdout,"checking Signal Strength\n");
	signal=signal_strength();

	ret_update=update_9600_115200();

	memset(buff,0,sizeof(buff));
	sprintf(buff,"Signal Strength %d",signal);	
		
	lk_dispclr(); 	
        lk_disptext(1,1,buff,0);
        lk_disptext(2,1,"Connecting to server ",1);
        lk_disptext(4,3,"  Please Wait",1);

	if(ppp_open() < 0)
	{
	sleep(5);
	ppp_close();
	
	if(ppp_open() < 0)
        {
		lk_dispclr();
                lk_disptext(2,0,"Connection Failed",0);
                lk_disptext(3,0,"1.Try Again(GPRS)",0);
                lk_disptext(4,0,"2.Continue(offline)",0);
                gprsconnection=0;
                ppp_close();
                sleep(2);
		con_try_gprs=lk_getkey();
                if(con_try_gprs==1)
                {
			lk_dispclr();
			lk_disptext(2,0,"Please Wait...",0);
			lk_disptext(3,0,"Searching Network",0);
			if(nwreg==0) goto ppp_start; 
			else goto sim_network;
		}
		else if(con_try_gprs==2) return -1;
	}
	else gprsconnection=1;
	}
	else gprsconnection=1;
        gett_ip();

	return 0;
}		
          
/******************************************************************/
void ppp_close(void )
{
char p_id[8];
char str[50];
int fd9;
int ret;

fd9 = open("/var/run/ppp0.pid",O_RDONLY);
if (fd9)
  {
    ret=read(fd9, p_id,6);
    p_id[ret] = '\0';
    sprintf(str, "kill -1 %s", p_id);
    system(str);
    close(fd9);
  }
system("insmod /home/imx_dtrrts.ko > /dev/null 2>&1 ");
sleep(2);
system("rmmod imx_dtrrts.ko > /dev/null 2>&1");
}


/******************************************************************/
int ppp_open(void)
{

int fd9,i;
printf("PPP starting\n");
system("rm -f /etc/ppp/resolv.conf");
system("/etc/ppp/dialout&");
sleep(6);
i=0;

while (i<25)
     {
        fd9 = open("/etc/ppp/resolv.conf", O_RDONLY);
        printf("%d\n", fd9);
        if (fd9 > 0)
          {
           close(fd9);
           break;
          }
        sleep(1);
        i++;
     }
  if(i>=25)
   {
     ppp_close();
     return -1;
  }
 printf("PPP Connected \n");
 copy_file ("/etc/resolv.conf","/etc/ppp/resolv.conf");
 sleep(2);
 return 0;

}

             
/******************************************************************/
int update_9600_115200_check(void)
{
        int res;
        char response[256];
        char *expect[32]={"OK","ERROR",NULL};

        res=ModemOpen("/dev/ttymxc0");
        if(res<0)
        {
                fprintf(stderr,"Modem Open failed\n");
                return -1;
        }
        setmodemparams(9600);
        memset(response,0,256);
	res=Test_modem_response_without_signal("AT+IPR=115200\r\n",response,expect,2);
	fprintf(stderr,"The value of res=%d\n",res); 
        Modemclose();
        if (res) return -1;
        else return 0;
}

/******************************************************************/
int update_9600_115200(void)
{
        int res;
        char response[256];
        char *expect[32]={"OK","ERROR",NULL};

        res=ModemOpen("/dev/ttymxc0");
        if(res<0)
        {
                fprintf(stderr,"Modem Open failed\n");
                return -1;
        }
        setmodemparams(BAUD);
        memset(response,0,256);
	res=Test_modem_response_without_signal("AT+IPR=115200\r\n",response,expect,10);
        fprintf(stderr,"The value of res=%d\n",res);
        
        Modemclose();

        if (res) return -1;

        else return 0;

}

/*******************************************************************/
int iccid_test (void)
{
        int res,i;
        char response[256];
        char *expect[32]={"READY","ERROR",NULL};
	resultset ICCID_Ret,ICCID_Ret_flag;
	unsigned int check_space_iccid=10;	
			
	
        res=ModemOpen("/dev/ttymxc0");
        if(res<0)
         {
                fprintf(stderr,"Modem Open failed\n");
                return ERROR;                
         }	
        memset(response,0,256);
        res=Test_modem_response_without_signal("AT+CCID\r\n",response,expect,10);
        fprintf(stderr,"The value of res=%d\n",res);
        sleep(1); 
#if PRINTF_IN_AT_COMMAND 
        printf("\nResponse of GSM Sim iccid = %s\n",response);
#endif
	for(i=0;i<strlen(response);i++)
        {
                if(response[i]<0x20)
                	response[i]=0x20;
        	else if(response[i]=='O'&&response[i+1]=='K')
        		{
                 		response[i]='\0';break;
        		}
        }
        Modemclose(); 
	for(i=0;i<strlen(response);i++)
         	if((response[i]==0x20)&&(response[i+1]>=48))
			check_space_iccid=i+1;
		
/////////////////////
#if PRINTF_IN_AT_COMMAND 
       	printf("Response of ICCID2 =%s\n",response+check_space_iccid);
#endif
	sleep(1); 
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	execute("Update operationalstatus SET SIM_ICCID='%20s';",response+check_space_iccid);
	return 0;
}


/******************************************************************/
int signal_strength (void)
{
	int res,i=0,result=0;
	char response[256];
	char *expect[32]={"OK","ERROR",NULL};
	char *c,num[7];

	res=ModemOpen("/dev/ttymxc0");
	if(res<0)
	{
		fprintf(stderr,"Modem Open failed\n");
		return -1;
	}
	setmodemparams(BAUD);
	memset(response,0,256);
	res=Test_modem_response_without_signal("AT+CSQ\r\n",response,expect,2);

	c=strstr(response,":");

	memset(num,0,sizeof(num));

	if(c!=NULL)
	{
		for(i=0;c[i]!=',';i++)
		{

		}
		num[0]=c[i-2];
		num[1]=c[i-1];
		num[2]='\0';
		result=atoi(num);

		printf("\nSIGNAL STRENGTH=%d\n",result);

		if(result>=0 && result<=31)
		{
			fprintf(stdout,"Network Signal =%s \n",num);
			Modemclose();
			lk_gsmsignal(result);
			return result;
		}
		else
		{
			lk_gsmsignal(0);
			fprintf(stderr,"NO SIGNAL\n");
			Modemclose();
			return -1;
		}
	}
	Modemclose();
	return 0;
}

/******************************************************************/
int gprs_registration()
{
	int res=0;
	char response[256],flag=0;
	char *expect[32]={"OK","ERROR","READY",NULL};

	res=ModemOpen("/dev/ttymxc0");
	if(res<0)
	{
		fprintf(stderr,"Modem Open failed\n");
		return -1;
	}

	setmodemparams(BAUD);
	memset(response,0,256);


	res=Test_modem_response_without_signal("AT+CGATT=1\r\n",response,expect,10);
	sleep(1) ;
	res=Test_modem_response_without_signal("AT+CGDCONT=1\r\n",response,expect,10);
	sleep(1) ;
	res=Test_modem_response_without_signal("AT+CGREG?\r\n",response,expect,10);
	sleep(1) ;
	Modemclose();

	if(strstr(response,",1"))
	{
		return 0;
	}
	else if(strstr(response,",5"))
	{
		return 1;
	}
	else
	{
		return -1;
	}
	return 0;
}

/******************************************************************/
int network_registration()
{
        int res=0;
        char response[256],flag=0;
        char *expect[32]={"OK","ERROR","READY",NULL};
	int count=0;

        res=ModemOpen("/dev/ttymxc0");
        if(res<0)
        {
                fprintf(stderr,"Modem Open failed\n");
                return -1;
        }

        setmodemparams(BAUD);
	lk_dispclr();
   	lk_disptext(3,1,"  Please Wait...",1);
	fprintf(stdout,"Waiting for Network Registration ...\n");
retry:
	count++;
        memset(response,0,256);
        res=Test_modem_response_without_signal("AT+CREG?\r\n",response,expect,4);
	fprintf(stdout,"%s\n",response);

	if (strstr(response,"0,1"));
	{Modemclose();
	 return 0;
	
	}
		

        if(strstr(response,",1"))
        {
		Modemclose();
                return 0;
        }
        else if(strstr(response,",5"))
        {
		Modemclose();
                return 1;
        }
        else if (count>14)
        {
		Modemclose();	
                return -1;
        }
	else {  sleep(1) ;  goto retry; }
	
        return 0;
}

/*******************************************************************/
int sim_status_test (void)
{
        int res;
        char response[256];
        char *expect[32]={"READY","ERROR",NULL};

        res=ModemOpen("/dev/ttymxc0");
        if(res<0)
        {
                fprintf(stderr,"Modem Open failed\n");
                return ERROR;                
        }
        setmodemparams(115200);

        memset(response,0,256);
        res=Test_modem_response_without_signal("AT+CPIN?\r\n",response,expect,10);
        fprintf(stderr,"The value of res=%d\n",res);        
        printf("Response of GSM Sim = %s\n",response);
        Modemclose();
       	sleep(2);	
        if(res==1)
	{		
	return -1; // sim is not there
	}
        else
	{
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		execute("UPDATE operationalstatus SET simstatus=1"); 
		return 0;
	}      // sim is present
}

/******************************************************************/
Png_Status_Without_Reconnect()
{

	int request_reponse;

	lk_dispclr();
        lk_disptext(2,2," GPRS Connection",0);
      	lk_disptext(3,2,"  Please Wait...",0);

      	printf("\nGPRS without ReConnect\n");
	request_reponse=curl_http_request();
	
	if(request_reponse==0)
        {
		  lk_dispclr();
                  lk_disptext(2,3,"     GPRS ",0);
		  lk_disptext(4,3,"   Connected",0);
                  printf("\nGPRS Connected\n");
		  gprsconnection=1;
	}
        else
        {
		  lk_dispclr();
                  lk_disptext(2,0,"Unable To Connect GPRS",0);
		  lk_disptext(3,0,"    Transaction",0);
		  lk_disptext(4,0,"   Not Uploaded",0);
		  printf("\nUnable To Connect GPRS\n");
	  	  gprsconnection=0;
	}

}


/******************************************************************/
Png_Status_Reconnect()
{
        int request_reponse;
        int test;
	int ret;

             
        lk_dispclr();
        lk_disptext(2,2," GPRS Connection",0);
      	lk_disptext(3,2,"  Please Wait...",0);

        printf("\nGPRS with ReConnect\n");
        request_reponse=curl_http_request();
       
        if(request_reponse==0)
        {
                lk_dispclr();
                lk_disptext(2,3,"     GPRS ",0);
                lk_disptext(4,3,"   Connected",0);
                printf("\nGPRS Connected\n");
                gprsconnection=1;
		return 0;
        }
        else
        {
		lk_dispclr();
		lk_disptext(2,3,"Unable To Connect",0);
		lk_disptext(3,3,"    GPRS",0);
		printf("\nUnable To Connect GPRS\n");
		gprsconnection=0;			
		lk_dispclr();
		lk_disptext(2,1,"Re-Connecting Server",0);
		lk_disptext(3,1,"   Please Wait",1);
		ret = gprs_ppp_test();
		if (ret==0)
		{
                	gprsconnection=1;
			return 1;
		}
		else  
		{gprsconnection=0;
		 return -1;
		}
			
        }
}
/******************************************************************/
int update_scripts()
{
 pppdial_gprs("*99***1#");
 ppp_options("void","void");
 ppp_dial_update("*99***1#", "gprsnac.com");
// ppp_options("1234","1234");
// ppp_dial_update("*99***1#", "internet"); 
 return 0;
}

/******************************************************************/
int sim_not_inserted()
{
	fprintf(stdout,"SIM IS NOT PERSENT\n");		
	execute("UPDATE operationalstatus SET simstatus=0");		
    	lk_dispclr();
	lk_disptext(1,7,"  HDFC CC",0);
	lk_disptext(3,0,"   Sim not Inserted",0);
	lk_disptext(4,0,"  Please Insert Sim",0);
	lk_disptext(5,0," And Restart Terminal",0);
	lk_getkey();
	lk_dispclr();
	lk_disptext(3,1,"  Please Wait...",1);
	close_sqlite();
	lk_close();
	system("poweroff"); 	
	sleep(20);
	system("shutdown");
	sleep(10);
}
