/*******************************************************************/
//			    HDFC Bank CC
//			  BOD_EOD_requests.c
/*******************************************************************/
/**
 *
 * @author FTLIT003
**/
#include <header.h>
#include <stdio.h>


/*******************************************************************/
int Upload_BOD_EOD_Request()
{
	resultset BOD_EOD_Status,BOD_EOD_Ip;	
	unsigned int BOD_EOD_Post_Request_Response;
	unsigned char BOD_EOD_Url[250],BOD_EOD_File[100],BOD_EOD_send_File[100];
	char BOD_EOD[100],BOD_EOD_RSA_b64[500];
	FILE * pFile;
	unsigned char BOD_EOD_No_buffer[50];
	FILE * BOD_EOD_response_myfile;
        int BOD_EOD_i;

	memset(&BOD_EOD_Status,0,sizeof(BOD_EOD_Status));
	memset(&BOD_EOD_Ip,0,sizeof(BOD_EOD_Ip));

	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	BOD_EOD_Ip=get_result("Select httpip,localhttpip,Current_Software_Ver from operationalstatus;");
	close_sqlite();	
        open_sqlite("/mnt/jffs2/hdfccctrans.db"); // open backup    
        BOD_EOD_Status=get_result("SELECT BOD_EOD_details,Counter from unuploaded_BOD_EOD_details WHERE BOD_EOD_Status='0';");
	if(BOD_EOD_Status.rows>0)
	{
		for(BOD_EOD_i=0;BOD_EOD_i<BOD_EOD_Status.rows;BOD_EOD_i++)
		{
		memset(BOD_EOD_Url,0,sizeof(BOD_EOD_Url));
#if URL_TEST
		sprintf(BOD_EOD_Url,"%s/HHTRequest/v%s/RequestForBEOD.aspx",BOD_EOD_Ip.recordset[0][0],BOD_EOD_Ip.recordset[0][2]);
#endif
#if URL_NETMAGIC
		sprintf(BOD_EOD_Url,"%s/HHTRequest/v%s/RequestForBEOD.aspx",BOD_EOD_Ip.recordset[0][0],BOD_EOD_Ip.recordset[0][2]);
#endif	
		memset(BOD_EOD_RSA_b64,0,sizeof(BOD_EOD_RSA_b64));
		sprintf(BOD_EOD_RSA_b64,"%s",BOD_EOD_Status.recordset[BOD_EOD_i][0]);

		memset(BOD_EOD_send_File,0,sizeof(BOD_EOD_send_File));
		sprintf(BOD_EOD_send_File,"/mnt/jffs2/%s_beod.txt",BOD_EOD_Status.recordset[BOD_EOD_i][1]);
	  	pFile = fopen (BOD_EOD_send_File, "wb");
		fputs (BOD_EOD_RSA_b64,pFile);
	  	fclose (pFile);
	
		memset(BOD_EOD_File,0,sizeof(BOD_EOD_File));
		sprintf(BOD_EOD_File,"/mnt/jffs2/%s.txt",BOD_EOD_Status.recordset[BOD_EOD_i][1]);
		BOD_EOD_Post_Request_Response=curl_BOD_EOD_Data(BOD_EOD_Url,BOD_EOD_send_File,BOD_EOD_File,"0");
		remove(BOD_EOD_send_File);
		if(BOD_EOD_Post_Request_Response==1)
		{
			BOD_EOD_response_myfile = fopen(BOD_EOD_File,"r");
			memset(BOD_EOD_No_buffer,0,sizeof(BOD_EOD_No_buffer));
			while (!feof(BOD_EOD_response_myfile))
			{
				fgets(BOD_EOD_No_buffer,50,BOD_EOD_response_myfile);
				printf("\nResponse buffer:%s",BOD_EOD_No_buffer);
				printf("\nResponse buffer length :%d",strlen(BOD_EOD_No_buffer));
                       	}
                        fclose(BOD_EOD_response_myfile);
			if(strcmp(BOD_EOD_No_buffer,"Success")==0)
			{
				close_sqlite();	
       				 open_sqlite("/mnt/jffs2/hdfccctrans.db"); 
				execute("Update unuploaded_BOD_EOD_details SET BOD_EOD_status='1' WHERE Counter='%ld';",atol(BOD_EOD_Status.recordset[BOD_EOD_i][1]));
				//execute("Delete FROM unuploaded_BOD_EOD_details WHERE BOD_EOD_Status='1';");
				sleep(2);
								
			}
		}
		remove(BOD_EOD_File);
		}
	}
return 0;
}
/*******************************************************************/

/*******************************************************************/
int Upload_BOD_EOD_Request_Trid()
{
	resultset BOD_EOD_Status,BOD_EOD_Ip;	
	unsigned int BOD_EOD_Post_Request_Response;
	unsigned char BOD_EOD_Url[250],BOD_EOD_File[100],BOD_EOD_send_File[100];
	char BOD_EOD[100],BOD_EOD_RSA_b64[500];
	FILE * pFile;
	unsigned char BOD_EOD_No_buffer[50];
	FILE * BOD_EOD_response_myfile;
        int BOD_EOD_i;

	memset(&BOD_EOD_Status,0,sizeof(BOD_EOD_Status));
	memset(&BOD_EOD_Ip,0,sizeof(BOD_EOD_Ip));

	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	BOD_EOD_Ip=get_result("Select httpip,localhttpip,Current_Software_Ver from operationalstatus;");
	close_sqlite();	
        open_sqlite("/mnt/jffs2/hdfccctrans.db"); // open backup    
        BOD_EOD_Status=get_result("SELECT BOD_EOD_details,Counter from unuploaded_BOD_EOD_details WHERE BOD_EOD_Status='0';");
	if(BOD_EOD_Status.rows>0)
	{
		for(BOD_EOD_i=0;BOD_EOD_i<BOD_EOD_Status.rows;BOD_EOD_i++)
		{
		memset(BOD_EOD_Url,0,sizeof(BOD_EOD_Url));
#if URL_TEST
		sprintf(BOD_EOD_Url,"%s/HHTRequest/v%s/RequestForBEOD.aspx",BOD_EOD_Ip.recordset[0][0],BOD_EOD_Ip.recordset[0][2]);
#endif
#if URL_NETMAGIC
		sprintf(BOD_EOD_Url,"%s/HHTRequest/v%s/RequestForBEOD.aspx",BOD_EOD_Ip.recordset[0][0],BOD_EOD_Ip.recordset[0][2]);
#endif		
		memset(BOD_EOD_RSA_b64,0,sizeof(BOD_EOD_RSA_b64));
		sprintf(BOD_EOD_RSA_b64,"%s",BOD_EOD_Status.recordset[BOD_EOD_i][0]);

		memset(BOD_EOD_send_File,0,sizeof(BOD_EOD_send_File));
		sprintf(BOD_EOD_send_File,"/mnt/jffs2/%s_beod.txt",BOD_EOD_Status.recordset[BOD_EOD_i][1]);
	  	pFile = fopen (BOD_EOD_send_File, "wb");
		fputs (BOD_EOD_RSA_b64,pFile);
	  	fclose (pFile);
	
		memset(BOD_EOD_File,0,sizeof(BOD_EOD_File));
		sprintf(BOD_EOD_File,"/mnt/jffs2/%s.txt",BOD_EOD_Status.recordset[BOD_EOD_i][1]);
		BOD_EOD_Post_Request_Response=curl_BOD_EOD_Data(BOD_EOD_Url,BOD_EOD_send_File,BOD_EOD_File,"1");
		remove(BOD_EOD_send_File);
		if(BOD_EOD_Post_Request_Response==1)
		{
			BOD_EOD_response_myfile = fopen(BOD_EOD_File,"r");
			memset(BOD_EOD_No_buffer,0,sizeof(BOD_EOD_No_buffer));
			while (!feof(BOD_EOD_response_myfile))
			{
				fgets(BOD_EOD_No_buffer,50,BOD_EOD_response_myfile);
				printf("\nResponse buffer:%s",BOD_EOD_No_buffer);
				printf("\nResponse buffer length :%d",strlen(BOD_EOD_No_buffer));
                       	}
                        fclose(BOD_EOD_response_myfile);
			if(strcmp(BOD_EOD_No_buffer,"Success")==0)
			{
				close_sqlite();	
       				 open_sqlite("/mnt/jffs2/hdfccctrans.db"); 
				execute("Update unuploaded_BOD_EOD_details SET BOD_EOD_status='1' WHERE Counter='%ld';",atol(BOD_EOD_Status.recordset[BOD_EOD_i][1]));
				//execute("Delete FROM unuploaded_BOD_EOD_details WHERE BOD_EOD_Status='1';");
				sleep(2);
				
			}
		}		
		remove(BOD_EOD_File);
		}
	}
return 0;
}

