#include <header.h>
#include <stdio.h>
#include <stdlib.h>
#include <curl.h>
#include "CreInfo.pb-c.h"
#define MAX_MSG_SIZE  4194304
/*******************************************************************/
//Declaration
/*******************************************************************/

/******************************************************************/

/******************************************************************/
int Executive_Info_Sync(char* Dra_Exec_ID)
{
	int Executive_Login_Return;
	system("cd /mnt/jffs2/");
	system("rm *.pb *.zip *.txt");
	printf("\nDRA_Exec_ID:%s",Dra_Exec_ID);
	Executive_Login_Return=Executive_Info_Request(Dra_Exec_ID);		//Request to server...
	printf("\nExecutive_Login_Return:%d",Executive_Login_Return);
	if(Executive_Login_Return==1) Executive_Info_Protobuff_Parser(Dra_Exec_ID);	//parsing Agent Info...
	system("cd /mnt/jffs2/");
	system("rm *.pb *.zip *.txt");
	return 0;
}
/******************************************************************/

/******************************************************************/
int Executive_Check()
{
	resultset Executive_ID;
	int Executive_Login_Return;
	char dec_executive[25];
	//Png_Status_Without_Reconnect();

	memset(&Executive_ID,0,sizeof(Executive_ID));
	
#if PRINTF 
	printf("\ngprsconnection:%d",gprsconnection);
#endif
	if(gprsconnection==1)
	{
		system("cd /mnt/jffs2/");
		system("rm *.pb *.zip *.txt");
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		Executive_ID=get_result("SELECT Executive_id FROM operationalstatus;");
		memset(dec_executive,0,sizeof(dec_executive));
		sprintf(dec_executive,"%s",Decrypted_sqlite_DB(Executive_ID.recordset[0][0]));
		Executive_Login_Return=Executive_Status_Request(dec_executive);	//Request to server...
 		printf("\nExecutive_Login_Return:%d",Executive_Login_Return);
		if(Executive_Login_Return==1)
		{
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		Executive_ID=get_result("SELECT Executive_id FROM operationalstatus;");
		memset(dec_executive,0,sizeof(dec_executive));
		sprintf(dec_executive,"%s",Decrypted_sqlite_DB(Executive_ID.recordset[0][0]));
		Executive_Info_Protobuff_Parser(dec_executive);
		system("cd /mnt/jffs2/");
		system("rm *.pb *.zip *.txt");
		}
	}
	return 1;
}
/******************************************************************/

/******************************************************************/
int Executive_Info_Protobuff_Parser(char* Dra_Executive_ID)
{
	int len;
	FILE *ifp;
	char ch;
	int i=0;
	int count=0;	
	uint8_t *data_buff;
	char Exec_ID_File_Name[1000],Exec_ID_File_Name_txt[500],Exec_ID_File_Name_zip[500];
	char Enc_executive[25],Enc_pass[25];
	
	resultset get_otp_count;
	int ret,chk_rtc_index,rtc_flag=0;


	data_buff = (uint8_t *) malloc(MAX_MSG_SIZE);
	memset(data_buff,0,sizeof(data_buff));
	memset(&get_otp_count,0,sizeof(get_otp_count));


	printf("\nDra_Executive_ID:%s",Dra_Executive_ID);	
	memset(Exec_ID_File_Name_zip,0,sizeof(Exec_ID_File_Name_zip));
	sprintf(Exec_ID_File_Name_zip,"/mnt/jffs2/%s.zip",Dra_Executive_ID);
	memset(Exec_ID_File_Name_txt,0,sizeof(Exec_ID_File_Name_txt));
	sprintf(Exec_ID_File_Name_txt,"/mnt/jffs2/%s.txt",Dra_Executive_ID);
	memset(Exec_ID_File_Name,0,sizeof(Exec_ID_File_Name));
	sprintf(Exec_ID_File_Name,"/mnt/jffs2/%s.pb",Dra_Executive_ID);
	if(stat(Exec_ID_File_Name,&st)==0)
	{
	ifp = fopen(Exec_ID_File_Name, "rb");
	if (ifp == NULL) {
	  fprintf(stderr, "Can't open input file in.list!\n");
	  return 0;
	}
	if (ifp != NULL) 
	{
	printf("Successfully open file\n");
	size_t cur_len = 0, nread =0 ;
//	if(nread = fread(data_buff,1,NUMELEM,fd))
	    while ((nread=fread(data_buff,1, MAX_MSG_SIZE - cur_len, ifp)) != 0)
	    {
	      cur_len += nread;
	      if (cur_len == MAX_MSG_SIZE)
		{
		  fprintf(stderr, "max message length exceeded\n");
		  return 0;
		}
	    }
	printf("legnth:%d\n",cur_len);
	data_buff[cur_len]='\0';

	CreInfo__ExecutiveInFo *login;
	
	login = cre_info__executive_in_fo__unpack(NULL, cur_len, data_buff);// Unpack allocation
	if (login == NULL || cur_len == 0)  
	{
		fprintf(stderr, "error unpacking alloc incoming message\n");
		return 0;
	}
	else 
	{
#if PRINTF 
	printf("executive_name:%s\n",login->executive_name);
	printf("executive_id:%s\n",login->executive_id);
	printf("executive_pwd:%s\n",login->executive_pwd);
	printf("executive status:%s\n",login->executive_status);
	printf("executive duedays:%s\n",login->executive_duedays);
	printf("DueHours:%ld\n",((curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365)));
	printf("executive walletbalance:%s\n",login->executive_walletbalance);	
	printf("server_date:%s\n",login->server_date);
	printf("server_time:%s\n",login->server_time);
	printf("machine_sim_number:%s\n",login->imei_number);
	printf("sim_apn_name:%s\n",login->sim_apn_name);
	printf("password_status:%s\n",login->password_status);
	printf("software_version_name:%s\n",login->software_version);
	printf("http_ip:%s\n",login->http_ip);
	printf("local_http_ip:%s\n",login->local_http_ip);
	printf("change_pwd_days:%s\n",login->change_pwd_days);
	printf("no_of_reprint_allow_per_day:%s\n",login->reprint_allow_per_day);
	printf("No_of_PIN_allow:%s\n",login->pin_allow);
	printf("otpn:%d\n",login->n_otpn);
	printf("\nOTP-0");
#endif		
	
	if(strlen(login->executive_name)>0&&strlen(login->executive_id)>0&&strlen(login->executive_pwd)>0&&strlen(login->executive_status)>0&&strlen(login->executive_duedays)>0&&strlen(login->executive_walletbalance)>0&&strlen(login->server_date)>0&&strlen(login->server_time)>0&&strlen(login-> imei_number)>0&&strlen(login->sim_apn_name)>0&&strlen(login->password_status)>0&&strlen(login->software_version)>0&&strlen(login->http_ip)>0&&strlen(login->local_http_ip)>0&&strlen(login->change_pwd_days)>0&&strlen(login->reprint_allow_per_day)>0&&strlen(login->pin_allow)&&(login->n_otpn>0))
	{
		printf("\nBefore parsing executive id\n");
		close_sqlite();	
		open_sqlite("/mnt/jffs2/hdfcccoperational.db"); 
		execute("Update operationalstatus SET Executive_Name='%s',Executive_id='%s',Executive_Password='%s',ExecutiveStatus='%s',DueDays='%s',DueHours='%ld',WalletBalance='%s',ServerDate='%s',ServerTime='%s',SIM_ICCID_Server='%s',Domain='%s',password_status='%s',New_Software_Ver='%s',httpip='%s',localhttpip='%s',ChangePwdDay='%s',ReprintPerDay='%s',NoOfPin_Allow='%s',NoOfCancellationOTP='%d';",login->executive_name,login->executive_id,login->executive_pwd,login->executive_status,login->executive_duedays,((curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365)),login->executive_walletbalance,login->server_date,login->server_time,login-> imei_number,login->sim_apn_name,login-> password_status,login->software_version,login->http_ip,login->local_http_ip,login->change_pwd_days,login->reprint_allow_per_day,login->pin_allow,login->n_otpn);	
		
	


		printf("\nAfter parsing executive id\n");		
		memset(Enc_executive,0,sizeof(Enc_executive));
		sprintf(Enc_executive,"%s",Encrypted_sqlite_DB(login->executive_id));
		memset(Enc_pass,0,sizeof(Enc_pass));
		sprintf(Enc_pass,"%s",Encrypted_sqlite_DB(login->executive_pwd));
		
		execute("Update operationalstatus SET Executive_id='%s',Executive_Password='%s';",Enc_executive,Enc_pass);
		printf("\nOTP\n");
			
		for(i=0;i<login->n_otpn;i++)
		{
			
			CreInfo__OTPn *Cre_otp;
			Cre_otp = cre_info__otpn__unpack(NULL, cur_len, data_buff);// unpack entry
			if (Cre_otp == NULL)      fprintf(stderr, "error unpacking otp incoming message\n");
#if PRINTF
			printf("Otp%d:%s\n",i+1,login -> otpn[i] -> otp);
#endif
			get_otp_count=get_result("Select OTP_count FROM CancellationOTPs where OTP_count='%d'",i+1);
			if(get_otp_count.rows>0)
				execute("Update CancellationOTPs SET OTP='%s' where OTP_count='%d';",login -> otpn[i] -> otp,i+1);	
			else
				execute("INSERT INTO CancellationOTPs(OTP_count,OTP) VALUES('%d','%s');",i+1,login -> otpn[i] -> otp);
			cre_info__otpn__free_unpacked(Cre_otp,NULL);
		
		}
		//////////////////////////////////////////
		for (chk_rtc_index=0; chk_rtc_index<3;chk_rtc_index++) {
		memset(&curt,0,sizeof(struct tm));
		ret=lk_getrtc_new(&curt);
		if (ret == 0){rtc_flag=1;break;}
		}
		if(rtc_flag!=1)
		{
			printf("\nRTC Read Error\n");
			lk_dispclr();
			lk_disptext(1,7,"  HDFC CC",0);
			lk_disptext(3,0,"   RTC Not Working",0);
			lk_disptext(4,0,"Pls Restart Terminal",0);
			lk_disptext(5,0,"    And Check ",0);
			lk_getkey();
			lk_dispclr();
	   		lk_disptext(3,1,"  Please Wait...",1);
			close_sqlite();
			lk_close();
			system("poweroff"); 	
			sleep(20);
			system("shutdown");
			sleep(10);
		}
		printf("\nCurrent time:%ld",((curt.tm_mday*24+(curt.tm_mon+1)*31+(curt.tm_year+1900)*365)));
		if((strlen(login->server_date)==6)&&(strlen(login->server_time)==6)&&(chk_date(login->server_date)>=0)&&(chk_time(login->server_time)>=0))
		{			
			lk_getrtc(&curt);
			HHT_time_setting(&curt,login->server_time);
		        lk_setrtc(&curt);
			lk_getrtc(&curt);
			HHT_Date_setting(&curt,login->server_date);
		        lk_setrtc(&curt);
		}		
		free(data_buff);
		remove(Exec_ID_File_Name);
		remove(Exec_ID_File_Name_txt);
		remove(Exec_ID_File_Name_zip);
		memset(Exec_ID_File_Name,0,sizeof(Exec_ID_File_Name));
		memset(Exec_ID_File_Name_txt,0,sizeof(Exec_ID_File_Name_txt));
		memset(Exec_ID_File_Name,0,sizeof(Exec_ID_File_Name));
		cre_info__executive_in_fo__free_unpacked(login,NULL);
		//SIM_Auth();
		return 1;
	}
	else
	{
		lk_dispclr();
		lk_disptext(2,0," Executive Information",0);
		lk_disptext(3,0,"      Is Wrong     ",0);
		lk_disptext(4,0," Please contact HO",0);
		sleep(3);
		free(data_buff);
		remove(Exec_ID_File_Name);
		remove(Exec_ID_File_Name_txt);
		remove(Exec_ID_File_Name_zip);
		memset(Exec_ID_File_Name,0,sizeof(Exec_ID_File_Name));
		memset(Exec_ID_File_Name_txt,0,sizeof(Exec_ID_File_Name_txt));
		memset(Exec_ID_File_Name,0,sizeof(Exec_ID_File_Name));
		cre_info__executive_in_fo__free_unpacked(login,NULL);
		return 0;
	}
	}
	//printf("\nOTP-4");
	cre_info__executive_in_fo__free_unpacked(login,NULL);
	//system("date");
	}//ifp NULL check
	else return 0;
	}//file status check
	free(data_buff);
	remove(Exec_ID_File_Name);
	remove(Exec_ID_File_Name_txt);
	remove(Exec_ID_File_Name_zip);
	memset(Exec_ID_File_Name,0,sizeof(Exec_ID_File_Name));
	memset(Exec_ID_File_Name_txt,0,sizeof(Exec_ID_File_Name_txt));
	memset(Exec_ID_File_Name,0,sizeof(Exec_ID_File_Name));
}

/******************************************************************/

/******************************************************************/

