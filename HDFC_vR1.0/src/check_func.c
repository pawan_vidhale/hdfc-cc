/*******************************************************************/
//			   HDFC CC
//		        check_func.c
/*******************************************************************/
#include <header.h>
#include <curl.h>
#include <math.h>
/*******************************************************************/
int Check_Numeric(char* Numbers)//Need to correct
{
	regex_t regex;
        int reti;
        char msgbuf[100];
	
	reti = regcomp(&regex,"[[:alpha:][:punct:][:space:]]", 0);
	if(reti) return 0;

	reti = regexec(&regex,Numbers, 0, NULL, 0);
        if(!reti){ //wrong match
                puts("Match");//get
		return 0;
        }
        else if( reti == REG_NOMATCH ){
	//Right Not match
                puts("No match");//Not get
		return 1;
        }
        else {
                regerror(reti, &regex, msgbuf, sizeof(msgbuf));
                fprintf(stderr, "Regex match failed: %s\n", msgbuf);
		return 0;
        }
}
/******************************************************************/

/******************************************************************/
int Check_Alpha(char* Alphabet)//Need to correct
{
	regex_t regex;
        int reti;
        char msgbuf[100];
	
	reti = regcomp(&regex,"[[:digit:][:punct:][:space:]]", 0);
	if(reti) return 0;

	reti = regexec(&regex,Alphabet, 0, NULL, 0);
        if(!reti){ //wrong match
                puts("Match");//get
		return 0;
        }
        else if( reti == REG_NOMATCH ){
	//Right Not match
                puts("No match");//Not get
		return 1;
        }
        else {
                regerror(reti, &regex, msgbuf, sizeof(msgbuf));
                fprintf(stderr, "Regex match failed: %s\n", msgbuf);
		return 0;
        }
}
/******************************************************************/

/******************************************************************/
int Check_Alpha_With_Space(char* Alphabet)//Need to correct
{
	regex_t regex;
        int reti;
        char msgbuf[100];
	
	reti = regcomp(&regex,"[[:digit:][:punct:]]", 0);
	if(reti) return 0;

	reti = regexec(&regex,Alphabet, 0, NULL, 0);
        if(!reti){ //wrong match
                puts("Match");//get
		return 0;
        }
        else if( reti == REG_NOMATCH ){
	//Right Not match
                puts("No match");//Not get
		return 1;
        }
        else {
                regerror(reti, &regex, msgbuf, sizeof(msgbuf));
                fprintf(stderr, "Regex match failed: %s\n", msgbuf);
		return 0;
        }
}
/******************************************************************/

/******************************************************************/
int Check_Alpha_Numeric(char* Alpha_Numeric)//Need to correct
{
	regex_t regex;
        int reti;
        char msgbuf[100];
	
	reti = regcomp(&regex,"[[:punct:][:space:]]", 0);
	if(reti) return 0;

	reti = regexec(&regex,Alpha_Numeric, 0, NULL, 0);
        if(!reti){ //wrong match
                puts("Match");//get
		return 0;
        }
        else if( reti == REG_NOMATCH ){
	//Right Not match
                puts("No match");//Not get
		return 1;
        }
        else {
                regerror(reti, &regex, msgbuf, sizeof(msgbuf));
                fprintf(stderr, "Regex match failed: %s\n", msgbuf);
		return 0;
        }
}
/******************************************************************/

/******************************************************************/
int Check_Alpha_Numeric_With_Space(char* Alpha_Numeric)//Need to correct
{
	regex_t regex;
        int reti;
        char msgbuf[100];
	
	reti = regcomp(&regex,"[[:punct:]]", 0);
	if(reti) return 0;

	reti = regexec(&regex,Alpha_Numeric, 0, NULL, 0);
        if(!reti){ //wrong match
                puts("Match");//get
		return 0;
        }
        else if( reti == REG_NOMATCH ){
	//Right Not match
                puts("No match");//Not get
		return 1;
        }
        else {
                regerror(reti, &regex, msgbuf, sizeof(msgbuf));
                fprintf(stderr, "Regex match failed: %s\n", msgbuf);
		return 0;
        }
}

/******************************************************************/

/******************************************************************/
char *replace(char *st) {
  static char buffer[125];
  char st1[2];
  int st_length;
  if(strlen(st)>0)
  {
	  memset(buffer,0,125);
	  for(st_length=0;st_length<=strlen(st);st_length++)
	  {
		if((st[st_length]==39)||(st[st_length]==44))
		{
			strcat(buffer,"=");
		}
		else
		{
			 memset(st1,0,2);
			 sprintf(st1,"%c",st[st_length]);
			 strcat(buffer,st1);
		}
	  }
	  printf("\nbuffer:%s",buffer);
  return buffer;
  }
  else return st;
}


/******************************************************************/

/******************************************************************/
int Check_Action_Date(char* Entered_Action_Date)
{
	long int Current_Date=0,Entered_Date=0,Prev_Date=0;
	char date[10],day[3],month[3],year[3];

	
	chk_rtc();
	memset(date,0,sizeof(date));	
	memset(day,0,sizeof(day));
	memset(month,0,sizeof(month));
	memset(year,0,sizeof(year));

	sprintf(date,"%s",Entered_Action_Date);
	sprintf(day,"%c%c",date[0],date[1]);
	sprintf(month,"%c%c",date[2],date[3]);
	sprintf(year,"%c%c",date[4],date[5]);

	Current_Date=(curt.tm_mday+((curt.tm_mon+1)*31)+((curt.tm_year+1900)*365));
#if PRINTF 
	printf("\nCurrent_Date:%ld",Current_Date);
#endif
	Entered_Date=(atoi(day)+(atoi(month)*31)+((atoi(year)+2000)*365));
#if PRINTF 	
	printf("\nEntered_Date:%ld",Entered_Date);
#endif
	if((Current_Date <= Entered_Date)&&(atoi(month)==(curt.tm_mon+1))&&(atoi(year)<=(curt.tm_year-100))) return 1;
	//else if((Current_Date <= Entered_Date)&&(atoi(month)==(curt.tm_mon+2))&&(atoi(year)<=(curt.tm_year-100))) return 1;
	else return -1;
	//sprintf(Dis_Tran_ChqDD_Date,"%02d%02d%02d",atoi(day),atoi(month),atoi(year));
}


/******************************************************************/
int Check_point(char* Numbers)
{	
	int i,k,z,count=0,count_zero=0;
		
	for(i=0;i<strlen(Numbers);i++)
	{	       
		if(Numbers[0]=='.') return 0;
		else if(Numbers[i]=='.')
			{
			k=i+2;
				printf("\nk:%d",k);	
				printf("\nstrlen:%d",strlen(Numbers));				
				for(z=k;z<=strlen(Numbers);z++)
				{					
					count_zero++;	
				}				
			count++;	
			}
	  if(count_zero>2||count_zero==1) return 0;									
	}	
	if(count==0) return 1;
	else if(count==1) return 1; 
	else if(count>=2) return 0;
	return 0;
}

int check_PAN(char* PAN_No)
{
	if(((PAN_No[0]>64)&&(PAN_No[0]<91))&&((PAN_No[1]>64)&&(PAN_No[1]<91))&&((PAN_No[2]>64)&&(PAN_No[2]<91))&&((PAN_No[3]>64)&&(PAN_No[3]<91))&&((PAN_No[4]>64)&&(PAN_No[4]<91))&&((PAN_No[5]>47)&&(PAN_No[5]<58))&&((PAN_No[6]>47)&&(PAN_No[6]<58))&&((PAN_No[7]>47)&&(PAN_No[7]<58))&&((PAN_No[8]>47)&&(PAN_No[8]<58))&&((PAN_No[9]>64)&&(PAN_No[9]<91)))
	return 1;
	else return 0;
}



/******************************************************************/
int *space(char* space)
{
	int st_length;
	static char buffer[125];
	char st1[2];
	
	if(strlen(space)==0)
  	{
	memset(buffer,0,125);
        for(st_length=0;st_length<=strlen(space);st_length++)
        {
		if((space[st_length]==32))
		{
		strcat(buffer,"="); 	
		}
		else
		{
		memset(st1,0,2);
                sprintf(st1,"%c",space[st_length]);
                strcat(buffer,st1);   	
		}
	}
	printf("\nbuffer:%s",buffer);
  	return 0;
	}
	
	else return 0;
}


/******************************************************************/

/******************************************************************/
int Check_Date_Chq(char* Entered_Cheque_DD_Date)
{
	long int Current_Date=0,Entered_Date=0,Prev_Date=0;
	char date[10],day[3],month[3],year[3];
	char Negative[20],Negativecheck[20],datenow[20],Enter_date[20],check[20];
	unsigned int  count_days;
	resultset count_months;		

	chk_rtc();
	memset(date,0,sizeof(date));	
	memset(day,0,sizeof(day));
	memset(month,0,sizeof(month));
	memset(year,0,sizeof(year));
	sprintf(date,"%s",Entered_Cheque_DD_Date);
	sprintf(day,"%c%c",date[0],date[1]);
	sprintf(month,"%c%c",date[2],date[3]);
	sprintf(year,"%c%c",date[4],date[5]);
	Prev_Date=((curt.tm_mday+((curt.tm_mon+1)*31)+((curt.tm_year-100)*365))-92);
#if PRINTF 
	printf("\nPrev_Date:%ld",Prev_Date);
#endif
	Entered_Date=(atoi(day)+(atoi(month)*31)+((atoi(year))*365));
#if PRINTF 
	printf("\nEntered_Date:%ld",Entered_Date);
#endif
	memset(datenow,0,sizeof(datenow));
	sprintf(datenow,"%04d-%02d-%02d",(curt.tm_year+1900),(curt.tm_mon+1),(curt.tm_mday));
	printf("\ndatanow:%s",datenow);
	memset(Enter_date,0,sizeof(Enter_date));
	sprintf(Enter_date,"20%s-%s-%s",year,month,day);
	printf("\nEnter_date:%s",Enter_date);

	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccc.db");
	count_months=get_result("select ( julianday('%s')) - ( julianday( '%s' ))",Enter_date,datenow);
	memset(Negative,0,sizeof(Negative));	 	
	sprintf(Negative,"%d",atoi(count_months.recordset[0][0]));	
	memset(Negativecheck,0,sizeof(Negativecheck));
	sprintf(Negativecheck,"%c",Negative[0]);
	memset(check,0,sizeof(check));
	sprintf(check,"%c%c%c%c",Negative[0],Negative[1],Negative[2],Negative[3]);
	printf("\ncheck:%s",check);
	printf("\nNegativecheck:%s",Negativecheck);	
	count_days = atoi(count_months.recordset[0][0]);	
	printf("\ncount_days:%d",count_days);

	if((90 > count_days)&&strcmp(Negativecheck,"-")!=0)
	{		
	printf("\ncount_days-1");	
	return 1;
	}
	else if(Entered_Date > Prev_Date)
	{
		if(atoi(check) > 90)
		{
		printf("\ncount_days-2");
		return 1;
		}
	printf("\ncount_days-3");
	return 1;
	}
	else
	{
	printf("\ncount_days-4"); 
	return -1;
	}
}
/******************************************************************/

/******************************************************************/
int Check_Date_DD(char* Entered_Cheque_DD_Date)
{
	long int Current_Date=0,Entered_Date=0,Prev_Date=0;
	char date[10],day[3],month[3],year[3];
	char Negative[20],Negativecheck[20],datenow[20],Enter_date[20],check[20];
	unsigned int  count_days;
	resultset count_months;	

	chk_rtc();
	memset(date,0,sizeof(date));	
	memset(day,0,sizeof(day));
	memset(month,0,sizeof(month));
	memset(year,0,sizeof(year));
	sprintf(date,"%s",Entered_Cheque_DD_Date);
	sprintf(day,"%c%c",date[0],date[1]);
	sprintf(month,"%c%c",date[2],date[3]);
	sprintf(year,"%c%c",date[4],date[5]);
	Prev_Date=((curt.tm_mday+((curt.tm_mon+1)*31)+((curt.tm_year-100)*365))-92);
#if PRINTF 
	printf("\nPrev_Date:%ld",Prev_Date);
#endif
	Entered_Date=(atoi(day)+(atoi(month)*31)+((atoi(year))*365));
#if PRINTF 
	printf("\nEntered_Date:%ld",Entered_Date);
#endif

	memset(datenow,0,sizeof(datenow));
	sprintf(datenow,"%04d-%02d-%02d",(curt.tm_year+1900),(curt.tm_mon+1),(curt.tm_mday));
	printf("\ndatanow:%s",datenow);
	memset(Enter_date,0,sizeof(Enter_date));
	sprintf(Enter_date,"20%s-%s-%s",year,month,day);
	printf("\nEnter_date:%s",Enter_date);

	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccc.db");
	count_months=get_result("select ( julianday('%s')) - ( julianday( '%s' ))",Enter_date,datenow);
	memset(Negative,0,sizeof(Negative));	 	
	sprintf(Negative,"%d",atoi(count_months.recordset[0][0]));	
	memset(Negativecheck,0,sizeof(Negativecheck));
	sprintf(Negativecheck,"%c",Negative[0]);
	memset(check,0,sizeof(check));
	sprintf(check,"%c%c%c%c",Negative[0],Negative[1],Negative[2],Negative[3]);
	printf("\ncheck:%s",check);
	printf("\nNegativecheck:%s",Negativecheck);	
	count_days = atoi(count_months.recordset[0][0]);	
	printf("\ncount_days:%d",count_days);

	if((90 > count_days)&&strcmp(Negativecheck,"-")!=0)
	{		
	return 1;
	}
	else if(Entered_Date > Prev_Date)
	{
		if(atoi(check) > 90)
		{		
		return -1;			
		}	
	return 1;
	}
	else
	{	 
	return -1;	
	}
}


char *jday(char *juliandays)
{
	int ret=0,chk_rtc_index,rtc_flag=0;
	int dd,mm,yy,julian;

	for (chk_rtc_index=0; chk_rtc_index<3;chk_rtc_index++) {
		memset(&curt,0,sizeof(struct tm));
		ret=lk_getrtc_new(&curt);
		if (ret == 0){rtc_flag=1;break;}
	}
	if((rtc_flag!=1)||((curt.tm_mday)<0||(curt.tm_mon+1)<0||(curt.tm_year-100)<0||(curt.tm_mday)>31||(curt.tm_mon+1)>12||(curt.tm_year+1900)>3000))
	{
	        printf("\nRTC Read Error\n");
		lk_dispclr();
		lk_disptext(1,7,"  HDFC",0);
		lk_disptext(3,0,"   RTC Not Working",0);
		lk_disptext(4,0,"Pls Restart Terminal",0);
		lk_disptext(5,0,"    And Check ",0);
		lk_getkey();
		lk_dispclr();
   		lk_disptext(3,1,"  Please Wait...",1);
		close_sqlite();
		lk_close();
		system("poweroff"); 	
		sleep(20);
		system("shutdown");
		sleep(10);
	}	
	
	dd = curt.tm_mday;
	mm = curt.tm_mon+1;
	yy = curt.tm_year-100;

	printf("date,month,year:%d,%d,%d",dd,mm,yy);
	//printf("\nenter a valid date (dd mm yy)");
	//scanf("%d%d%d",&dd,&mm,&yy);
	julian=dd;
	mm=mm-1;
	switch(mm)
	{
	case 11 : julian=julian+30;
	case 10 : julian=julian+31;
	case 9 : julian=julian+30;
	case 8 : julian=julian+31;
	case 7 : julian=julian+31;
	case 6: julian=julian+30;
	case 5: julian=julian+31;
	case 4 : julian=julian+30;
	case 3 : julian=julian+31;
	case 2 : if(yy%4==0)julian=julian+29;
		     else julian=julian+28;
	case 1 : julian=julian+31;
	}
	printf("\nJulian date is %d",julian);
	
	if((julian/10)==0) sprintf(juliandays,"%03d",julian);
	if((julian/10)<=9 && (julian/10)>0) sprintf(juliandays,"%03d",julian);
	if((julian/10)>9) sprintf(juliandays,"%d",julian);	
	printf("\njuliandays  %s",juliandays);
	return juliandays;
	
}


