/*******************************************************************/
//      	     HDFC bank CC
//		      Follow_Up_cc.c
/*******************************************************************/
#include <header.h>
#include "sqlite3.h"
/******************************************************************/
int Follow_Up(char* follow_up_contract_no,char* follow_up_pickupflag)
{
	int opt=0,retval=0,Action_valid=0;
        MENU_T menu;
        int selItem  = 0;
        int acceptKbdEvents = TRUE;
        int maxEntries = 0;
        short scroll = 0;
	char Action_Contract_No_search[20];
	resultset Action_CC_No;	

	memset(&Action_CC_No,0,sizeof(Action_CC_No));
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccc.db"); // open Data
	memset(Action_Contract_No_search,0,sizeof(Action_Contract_No_search));
	lk_dispclr();
	lk_disptext(0,1,"   HDFC Bank CC",0);
	lk_disptext(1,1,"      Search",0);
	lk_disphlight(1);
	lk_disptext(2,0,"Enter Action Code",0);		
	if((retval=lk_getalpha(5,0,Action_Contract_No_search,20,strlen(Action_Contract_No_search),0))<0) After_selecting_customer(follow_up_contract_no,follow_up_pickupflag);
	Action_Contract_No_search[retval]='\0';	
	if(strlen(Action_Contract_No_search)==0)
	{
	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       18;
        maxEntries                      =       menu.maxEntries;

	strcpy(menu.title," Action Menu"); 
	strcpy(menu.menu[0],"1.Appointment");                   
        strcpy(menu.menu[1],"2.Broken Settlement");
	strcpy(menu.menu[2],"3.Cancellation");
	strcpy(menu.menu[3],"4.Cash In Hand");
	strcpy(menu.menu[4],"5.Call Back");
	strcpy(menu.menu[5],"6.Deceased Customer");
	strcpy(menu.menu[6],"7.Dispute");		
	strcpy(menu.menu[7],"8.Field Visit");
	strcpy(menu.menu[8],"9.Incoming Call");	
	strcpy(menu.menu[9],"10.Left Message");
	strcpy(menu.menu[10],"11.New Contact No. Iden");	
	strcpy(menu.menu[11],"12.Promise To Pay");	
	strcpy(menu.menu[12],"13.Ringing No Resp.");
	strcpy(menu.menu[13],"14.Refuse To Pay");
	strcpy(menu.menu[14],"15.Non Contact. Cust.");
	strcpy(menu.menu[15],"16.Skip Traced");
	strcpy(menu.menu[16],"17.Fully Settled");
	strcpy(menu.menu[17],"18.Telecall");
	
	while(1)
        {    
	      lk_buff_clear();	
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case CANCEL:	After_selecting_customer(follow_up_contract_no,follow_up_pickupflag);
				break;

		case ENTER:
			switch (selItem+1 )
                    	{
			case 1: Payee_Type(follow_up_contract_no,"1",follow_up_pickupflag);
				break;
			case 2: Payee_Type(follow_up_contract_no,"2",follow_up_pickupflag);
				break;
			case 3: Payee_Type(follow_up_contract_no,"3",follow_up_pickupflag);
				break;
			case 4: Payee_Type(follow_up_contract_no,"4",follow_up_pickupflag);
				break;
			case 5:	Payee_Type(follow_up_contract_no,"5",follow_up_pickupflag);
				break;
			case 6: Payee_Type(follow_up_contract_no,"6",follow_up_pickupflag);
				break;
			case 7: Payee_Type(follow_up_contract_no,"7",follow_up_pickupflag); 
				break;
			case 8: Payee_Type(follow_up_contract_no,"8",follow_up_pickupflag); 
				break;
			case 9: Payee_Type(follow_up_contract_no,"9",follow_up_pickupflag); 
				break;
			case 10:Payee_Type(follow_up_contract_no,"10",follow_up_pickupflag); 
				break;	
			case 11:Payee_Type(follow_up_contract_no,"11",follow_up_pickupflag);
				break;
			case 12:Payee_Type(follow_up_contract_no,"12",follow_up_pickupflag);  
				break;	
			case 13:Payee_Type(follow_up_contract_no,"13",follow_up_pickupflag);  
				break;	
			case 14:Payee_Type(follow_up_contract_no,"14",follow_up_pickupflag);  
				break;	
			case 15:Payee_Type(follow_up_contract_no,"15",follow_up_pickupflag);  
				break;	
			case 16:Payee_Type(follow_up_contract_no,"16",follow_up_pickupflag);  
				break;	
			case 17:Payee_Type(follow_up_contract_no,"17",follow_up_pickupflag);  
				break;	
			case 18:Payee_Type(follow_up_contract_no,"18",follow_up_pickupflag);  
				break;	
			default : break;							
			}		
		default : break;
		}//switch
	}//while
	}
	else
	{
	lk_dispclr(); 
	lk_disptext(2,3,"  Please wait.. ",0);	
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db"); // open Data
	if(strlen(Action_Contract_No_search)>0&&(Check_Alpha_With_Space(Action_Contract_No_search)==1))
	printf("\nAction_Contract_No_search=%s\n",Action_Contract_No_search);
	Action_CC_No = get_result("SELECT Action_Code,Action_Disc,Action_Number FROM Action_Status WHERE Action_Code='%s';",Action_Contract_No_search);
	if(Action_CC_No.rows>0)
	{
		
		if(strcmp(Action_CC_No.recordset[0][0],Action_Contract_No_search)==0) Payee_Type(follow_up_contract_no,Action_CC_No.recordset[0][2],follow_up_pickupflag);
		else
		{
Action_Code_Enter:
			lk_dispclr(); 
			lk_disptext(2,2,"  Please Enter ",0);	
			lk_disptext(3,2,"Valid Acton Code",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			Action_valid=lk_getkey();
			if(Action_valid==ENTER)
			{
			Follow_Up(follow_up_contract_no,follow_up_pickupflag);
			}
			else goto Action_Code_Enter;
		}
	}
	else
	{
Action_Code_Enter_zero:
			lk_dispclr(); 
			lk_disptext(2,2,"  Please Enter ",0);	
			lk_disptext(3,2,"Valid Acton Code",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			Action_valid=lk_getkey();
			if(Action_valid==ENTER)
			{
			Follow_Up(follow_up_contract_no,follow_up_pickupflag);
			}
			else goto Action_Code_Enter_zero;
		
	}
	}
return 0;
}

/**********************************************************************/

/**********************************************************************/
int Payee_Type(char* Payee_contract_no,char* Payee_Action_code,char* Payee_Pickup_Flag)
{
	int opt=0;
        MENU_T menu;
        int selItem  = 0;
        int acceptKbdEvents = TRUE;
        int maxEntries = 0;
        short scroll = 0;
	

	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       3;
        maxEntries                      =       menu.maxEntries;

	strcpy(menu.title,"Contact Person"); 
	strcpy(menu.menu[0],"1.Customer");//                   
        strcpy(menu.menu[1],"2.Relative");//
	strcpy(menu.menu[2],"3.Others");//
	
	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case CANCEL:	Follow_Up(Payee_contract_no,Payee_Pickup_Flag);
				break;
		case ENTER:
			switch (selItem+1 )
                    	{
			case 1: Place_Contact(Payee_contract_no,Payee_Action_code,"1",Payee_Pickup_Flag);
				break;
			case 2: Place_Contact(Payee_contract_no,Payee_Action_code,"2",Payee_Pickup_Flag);
				break;
			case 3: Place_Contact(Payee_contract_no,Payee_Action_code,"3",Payee_Pickup_Flag);
				break;				
			default : break;
			}		
		default : break;
		}//switch
	}//while
	
}

/**********************************************************************/

/**********************************************************************/
int Place_Contact(char* Place_contract_no,char* Place_Action_code,char* Place_Payee_Type,char* Place_Pickup_Flag)
{
	int opt=0;
        MENU_T menu;
        int selItem  = 0;
        int acceptKbdEvents = TRUE;
        int maxEntries = 0;
        short scroll = 0;	

	printf("\nPlace_Action_code:%s",Place_Action_code);
	
	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       3;
        maxEntries                      =       menu.maxEntries;

	strcpy(menu.title,"Contact Place");
	strcpy(menu.menu[0],"1.Residence");//                   
        strcpy(menu.menu[1],"2.Office");//
	strcpy(menu.menu[2],"3.Others");//
		

	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case CANCEL:	Payee_Type(Place_contract_no,Place_Action_code,Place_Pickup_Flag);
				break;

		case ENTER:
			switch (selItem+1 )
                    	{
			case 1: Final(Place_contract_no,Place_Action_code,Place_Payee_Type,"1",Place_Pickup_Flag);
				break;
			case 2: Final(Place_contract_no,Place_Action_code,Place_Payee_Type,"2",Place_Pickup_Flag);
				break;
			case 3: Final(Place_contract_no,Place_Action_code,Place_Payee_Type,"3",Place_Pickup_Flag);
				break;
			default : break;
			}		
		default : break;
		}//switch
	}//while


}

/**********************************************************************/

/**********************************************************************/
int Final(char* Final_contract_no,char* Final_Action_code,char* Final_Payee_Type,char* Final_Place_Contacted,char* Final_Pickup_Flag)
{

	if((strcmp(Final_Action_code,"1")==0)||(strcmp(Final_Action_code,"5")==0))
	{
		Action_Date_details(Final_contract_no,Final_Action_code,Final_Payee_Type,Final_Place_Contacted,Final_Pickup_Flag);
	}
	else if((strcmp(Final_Action_code,"12")==0))
	{
		Action_Date_Amount_details(Final_contract_no,Final_Action_code,Final_Payee_Type,Final_Place_Contacted,Final_Pickup_Flag);	
	}
	else if((strcmp(Final_Action_code,"16")==0))
	{
		Action_Address_details(Final_contract_no,Final_Action_code,Final_Payee_Type,Final_Place_Contacted,Final_Pickup_Flag);
	}
	else if((strcmp(Final_Action_code,"2")==0)||(strcmp(Final_Action_code,"3")==0)||(strcmp(Final_Action_code,"4")==0)||(strcmp(Final_Action_code,"6")==0)||(strcmp(Final_Action_code,"7")==0)||(strcmp(Final_Action_code,"8")==0)||(strcmp(Final_Action_code,"9")==0)||(strcmp(Final_Action_code,"10")==0)||(strcmp(Final_Action_code,"11")==0)||(strcmp(Final_Action_code,"13")==0)||(strcmp(Final_Action_code,"14")==0)||(strcmp(Final_Action_code,"15")==0)||(strcmp(Final_Action_code,"17")==0)||(strcmp(Final_Action_code,"18")==0))
	{
		Action_Remarks(Final_contract_no,Final_Action_code,Final_Payee_Type,Final_Place_Contacted,Final_Pickup_Flag); 
	}
}
/******************************************************************/

/******************************************************************/

int Action_Date_details(char* Action_contract_no,char* Action_Action_code,char* Action_Payee_Type,char* Action_Place_Contacted,char* Action_Date_Pickup_Flag)
{
	char Action_Date[12],Action_Remark[55],Action_Remark1[55],Capture_date[12],Capture_Time[12],Action_data_show[12],Action_data_time[12],dec_followup_exec[25],followupdetails[800],Customer_FollowUp_RSA_b64[800],upcommingfollowup[800],Action_date_number[20],hwid_followup[15],Customer_ID[20];
	int retval=0,remark_retval=0,Enter_Valid_key;
	resultset Exec_ID_HWD,Disp_hwid,Get_allocation_customer_no,Get_pickup_customer_no;

	memset(&Exec_ID_HWD,0,sizeof(Exec_ID_HWD));
	memset(&Disp_hwid,0,sizeof(Disp_hwid));

	memset(Action_date_number,0,sizeof(Action_date_number));
/***********************************for Callback Menu*******************************************************/
	 
       	if(strcmp(Action_Action_code,"5")==0)
	{
	int opt=0;
        MENU_T menu;
        int selItem  = 0;
        int acceptKbdEvents = TRUE;
        int maxEntries = 0;
        short scroll = 0;	

	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       3;
        maxEntries                      =       menu.maxEntries;
Callback_Menu:
	strcpy(menu.title,"     Callback"); 
	strcpy(menu.menu[0],"1.Residence");//                   
        strcpy(menu.menu[1],"2.Office");
	strcpy(menu.menu[2],"3.Mobile");//
	
	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case CANCEL:	Place_Contact(Action_contract_no,Action_Action_code,Action_Payee_Type,Action_Date_Pickup_Flag);
				break;

		case ENTER:
			switch (selItem+1 )
                    	{
			case 1:	strcpy(Action_date_number,"5");
				//strcpy(Action_code,"5");
				break;
			case 2: strcpy(Action_date_number,"6");
				//strcpy(Action_code,"6");
				break;
			case 3: strcpy(Action_date_number,"7");
				//strcpy(Action_code,"7");
				break;
			default : break;
			}		
		default : break;
		}//switch
		break;
	}//while		
        }
	/*******************************************************************************************************/
	lk_dispclr();
	lk_disptext(0,1,"     HDFC Bank",0);
	lk_disptext(1,4,"Details",0);
	lk_disphlight(1);	
	lk_disptext(2,0,"Enter Date           (DD-MM-YY)",0);
	memset(Action_Date,0,sizeof(Action_Date));
	if((retval=lk_getnumeric(5,1,Action_Date,6,strlen(Action_Date),0))<0) 
	if(strcmp(Action_Action_code,"5")==0) goto Callback_Menu;
	else Place_Contact(Action_contract_no,Action_Action_code,Action_Payee_Type,Action_Date_Pickup_Flag);
	Action_Date[retval]='\0';
	printf("\nEnter Date length:%d",strlen(Action_Date));
	printf("\nchk date return:%d\n",chk_date(Action_Date));
	if((chk_date(Action_Date)>0)&&(strlen(Action_Date)==6)&&(Check_Action_Date(Action_Date)==1)&&(Check_Numeric(Action_Date)==1))
	{
Enter_Remark:
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank",0);
		lk_disptext(1,4,"Details",0);
		lk_disphlight(1);
		lk_disptext(2,0,"Enter Remark",0);
		memset(Action_Remark1,0,sizeof(Action_Remark1));
		memset(Action_Remark,0,sizeof(Action_Remark));
		if((remark_retval=lk_getalpha(5,0,Action_Remark1,50,strlen(Action_Remark1),0))<0) Action_Date_details(Action_contract_no,Action_Action_code,Action_Payee_Type,Action_Place_Contacted,Action_Date_Pickup_Flag);
		Action_Remark1[remark_retval]='\0';
		if((strlen(Action_Remark1)==0))
		{
Valid_Remark:
		lk_dispclr();
		lk_disptext(2,0,"    Please Enter",0);
		lk_disptext(3,0,"      Remark    ",0);		
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Enter_Valid_key = lk_getkey();
		if(Enter_Valid_key==ENTER)
		{
		goto Enter_Remark;
		}
		else goto Valid_Remark;
		}
		
		sprintf(Action_Remark,"%s",replace(Action_Remark1));

		memset(Capture_Time,0,sizeof(Capture_Time));
		sprintf(Capture_Time,"%02d=%02d=%02d",curt.tm_hour,curt.tm_min,curt.tm_sec);	
		memset(Capture_date,0,sizeof(Capture_date));
		sprintf(Capture_date,"%02d=%02d=%04d",(curt.tm_mon+1),(curt.tm_mday),(curt.tm_year+1900));

		memset(Action_data_show,0,sizeof(Action_data_show));
		sprintf(Action_data_show,"%c%c=%c%c=20%c%c",Action_Date[2],Action_Date[3],Action_Date[0],Action_Date[1],Action_Date[4],Action_Date[5],Action_Date[6]);	

		printf("\nAction_data_show:%s\n",Action_data_show);		
		
#if PRINTF
		printf("\nAction_contract_no:%s",Action_contract_no);
		printf("\nAction_Action_code:%s",Action_Action_code);
		printf("\nAction_Payee_Type:%s",Action_Payee_Type);
		printf("\nAction_Place_Contacted:%s",Action_Place_Contacted);
		printf("\nAction_data_show:%s",Action_data_show);
		printf("\nAction_date_number:%s",Action_date_number);
		printf("\nAction_Remark:%s",Action_Remark);
		printf("\nCapture_date:%s",Capture_date);
		printf("\nCapture_Time:%s",Capture_Time);
		printf("\nAction_Date_Pickup_Flag:%s",Action_Date_Pickup_Flag);
#endif

/***************************data to upload from Action_date**************************************************/
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		Exec_ID_HWD=get_result("Select Executive_id,hwid from operationalstatus");		
		printf("\nExec_ID:%s",Exec_ID_HWD.recordset[0][0]);	
		memset(dec_followup_exec,0,sizeof(dec_followup_exec));
		sprintf(dec_followup_exec,"%s",Decrypted_sqlite_DB(Exec_ID_HWD.recordset[0][0]));
		
		memset(hwid_followup,0,sizeof(hwid_followup));
		sprintf(hwid_followup,"%s",Exec_ID_HWD.recordset[0][1]);

/******************************************************************************************************/
	if(strcmp(Action_Date_Pickup_Flag,"P")==0) 
	{
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		Get_pickup_customer_no=get_result("SELECT Filler2 from PICKUP_DETAILS WHERE CARD_NUMBER='%s';",Action_contract_no);
		memset(Customer_ID,0,sizeof(Customer_ID));
		sprintf(Customer_ID,"%s",Get_pickup_customer_no.recordset[0][0]);
						
	}
	
	else
	{
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		Get_allocation_customer_no= get_result("SELECT filler1 from allocation_details WHERE CARD_NUMBER='%s';",Action_contract_no);
		memset(Customer_ID,0,sizeof(Customer_ID));
		sprintf(Customer_ID,"%s",Get_allocation_customer_no.recordset[0][0]);

	}		
/**************************************************************************************/		
		if(strcmp(Action_Action_code,"5")==0)	
		{
		memset(followupdetails,0,sizeof(followupdetails));
		sprintf(followupdetails,"Validate,%s,%s,%s,%s,%s,%s,%s,0,%s,%s,%s,%s,%s,0,%s,%s,0",dec_followup_exec,hwid_followup,Action_contract_no,Action_date_number,Action_date_number,Action_Payee_Type,Action_Place_Contacted,Action_data_show,Capture_Time,Capture_date,Capture_Time,Action_Remark,Action_Date_Pickup_Flag,Customer_ID);
#if PRINTF
		printf("\nfollowupdetails:%s",followupdetails);	
#endif
		memset(Customer_FollowUp_RSA_b64,0,sizeof(Customer_FollowUp_RSA_b64));
		sprintf(Customer_FollowUp_RSA_b64,"%s",RSAEncryption_Base64(followupdetails,strlen(followupdetails)));
#if PRINTF
		printf("\nCustomer_FollowUp_RSA_b64",Customer_FollowUp_RSA_b64);
#endif
		}
		else
		{
		memset(Action_date_number,0,sizeof(Action_date_number));
		if(strcmp(Action_Action_code,"1")==0)
		strcpy(Action_date_number,"1");
		memset(followupdetails,0,sizeof(followupdetails));
		sprintf(followupdetails,"Validate,%s,%s,%s,%s,%s,%s,%s,0,%s,%s,%s,%s,%s,0,%s,%s,0",dec_followup_exec,hwid_followup,Action_contract_no,Action_date_number,Action_date_number,Action_Payee_Type,Action_Place_Contacted,Action_data_show,Capture_Time,Capture_date,Capture_Time,Action_Remark,Action_Date_Pickup_Flag,Customer_ID);

		printf("\nfollowupdetails:%s",followupdetails);

		memset(Customer_FollowUp_RSA_b64,0,sizeof(Customer_FollowUp_RSA_b64));
		sprintf(Customer_FollowUp_RSA_b64,"%s",RSAEncryption_Base64(followupdetails,strlen(followupdetails)));
		printf("\nCustomer_FollowUp_RSA_b64",Customer_FollowUp_RSA_b64);
		}

		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccctrans.db");
		execute("INSERT INTO unuploadedfollowupdetails(followupdetails,FollowUp_Status) VALUES('%s','0');",Customer_FollowUp_RSA_b64);
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		execute("delete from PICKUP_DETAILS where CARD_NUMBER='%s';",Action_contract_no);		
		Upload_FollowUp_Request();
		Dra_Menu();
		//Follow_Up(Action_contract_no,Action_Date_Pickup_Flag);		
	}
	else 
	{
Valid_Action:
		lk_dispclr();
		lk_disptext(2,0,"    Please Enter",0);
		lk_disptext(3,0,"    Valid Date",0);		
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Enter_Valid_key = lk_getkey();
		if(Enter_Valid_key==ENTER)
		{
		Action_Date_details(Action_contract_no,Action_Action_code,Action_Payee_Type,Action_Place_Contacted,Action_Date_Pickup_Flag);
		}
		else goto Valid_Action;
	}
	return 0;
}

/*********************************************************************************/
int Action_Date_Amount_details(char* Action_contract_No,char* Action_Action_code,char* Action_Payee_Type,char* Action_Place_Contacted,char* Action_Data_Amount_Pickupflag)
{
	char Action_Date[8],Action_Time[8],Action_Remark[55],Action_Remark1[55],Current_timestr[10],Action_data_show[12],Action_time_show[12],dec_followup_exec[25],Action_ptp[20],Enter_Valid_key;
	char Capture_date[12],Capture_Time[12],Action_Amount[10],Capture_date_current[12],Action_Time_check[15],followupdetails[300],Customer_FollowUp_RSA_b64[300],Action_amount_number[20],hwid_followup[15],Customer_ID[20];
	int retval=0,rval=0,remark_retval=0,Amt_rval=0;
	int Action_Time_key,Action_Date_key,PTP_key,Low_load_balance,Low_curr_balance;
	resultset Exec_ID_HWD,Disp_hwid,Get_allocation_customer_no,Get_pickup_customer_no;;
	char Curr_Bal[30];
	resultset Check_Balance;
	memset(&Check_Balance,0,sizeof(Check_Balance));
	memset(&Exec_ID_HWD,0,sizeof(Exec_ID_HWD));
	memset(&Disp_hwid,0,sizeof(Disp_hwid));

	/*****************************Menu For Promise to pay*******************************************************/
	
	if(strcmp(Action_Action_code,"12")==0)
	{
	int opt=0;
        MENU_T menu;
        int selItem  = 0;
        int acceptKbdEvents = TRUE;
        int maxEntries = 0;
        short scroll = 0;	

	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       4;
        maxEntries                      =       menu.maxEntries;
Action_pay:
	strcpy(menu.title,"       Pay"); 
	strcpy(menu.menu[0],"1.Cash");                 
        strcpy(menu.menu[1],"2.Netbanking");
	strcpy(menu.menu[2],"3.Cheque");
	strcpy(menu.menu[3],"4.ATM");
	
	memset(Action_amount_number,0,sizeof(Action_amount_number));
	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case CANCEL:	Place_Contact(Action_contract_No,Action_Action_code,Action_Payee_Type,Action_Data_Amount_Pickupflag);
				break;

		case ENTER:
			switch (selItem+1 )
                    	{
			case 1:	strcpy(Action_amount_number,"17");
				break;
			case 2: strcpy(Action_amount_number,"18");
				break;
			case 3: strcpy(Action_amount_number,"19");
				break;
			case 4: strcpy(Action_amount_number,"20");
				break;
			default : break;
			}		
		default : break;
		}
		break;
	}//while
		
        }


 Enter_Amount:
	lk_dispclr();
	lk_disptext(0,1,"     HDFC Bank CC",0);
	lk_disptext(1,4,"   Details",0);
	lk_disphlight(1);
	lk_disptext(3,0," Enter Amount",0);
	memset(Action_Amount,0,sizeof(Action_Amount));
	if((Amt_rval=lk_getnumeric(5,1,Action_Amount,9,strlen(Action_Amount),0))<0) goto Action_pay;
	
	Action_Amount[Amt_rval]='\0';
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccc.db");	
	Check_Balance = get_result("SELECT CURRENT_BL,BUCKET_DEST,LOADED_BAL FROM ALLOCATION_DETAILS WHERE CARD_NUMBER='%s';",Action_contract_No);
	if(strcmp(Action_Data_Amount_Pickupflag,"D")==0)	
	{
		if(strcmp(Check_Balance.recordset[0][1],"RECOVERIES")==0)
		{
			if((atof(Action_Amount)>atof(Check_Balance.recordset[0][2])))
			{
Exceed_loaded:	
				lk_dispclr();
				lk_disptext(2,2,"Enter Amount exceed ",0);
				lk_disptext(3,2,"Loaded Balance",0);
				lk_disptext(4,2,"Enter Valid Amount",0);
				lk_disptext(5,0,"Press Enter to Cont.",0);
				Low_load_balance= lk_getkey();
				if(Low_load_balance==ENTER)	
				{			
					goto Enter_Amount;
				}
				else goto Exceed_loaded;
			}
		}
		else
			if((atof(Action_Amount)>atof(Check_Balance.recordset[0][0])))
			{
Exceed_current:			lk_dispclr();
				lk_disptext(2,2,"Enter Amount exceed ",0);
				lk_disptext(3,2,"Current Balance",0);
				lk_disptext(4,2,"Enter Valid Amount",0);
				lk_disptext(5,0,"Press Enter to Cont.",0);
				Low_curr_balance= lk_getkey();
				if(Low_curr_balance==ENTER)	
				{			
					goto Enter_Amount;
				}
				else goto Exceed_current;
			}
	}

	if((strlen(Action_Amount)>0)&&(atol(Action_Amount)>0))
	
	//if((strlen(Action_Amount)>0)&&(atol(Action_Amount)>0)&&(Check_Numeric(Action_Amount)==1))
	//if((strlen(Action_Amount)>0)&&(atol(Action_Amount)>0))
	{	
Action_Date_Label:	
	lk_dispclr();
	lk_disptext(0,1,"     HDFC Bank CC",0);
	lk_disptext(1,4,"  Details",0);
	lk_disphlight(1);	
	lk_disptext(2,0,"Enter Date           (DD-MM-YY)",0);
	memset(Action_Date,0,sizeof(Action_Date));
	if((retval=lk_getnumeric(5,1,Action_Date,6,strlen(Action_Date),0))<0) goto Enter_Amount;
	Action_Date[retval]='\0';
	printf("\nEnter Date length:%d",strlen(Action_Date));
	printf("\nchk date return:%d\n",chk_date(Action_Date));
	
	if((chk_date(Action_Date)>0)&&(strlen(Action_Date)==6)&&(Check_Action_Date(Action_Date)==1)&&(Check_Numeric(Action_Date)==1))
		{				

Enter_Remark:			
			lk_dispclr();
			lk_disptext(0,1,"     HDFC Bank CC",0);
			lk_disptext(1,4,"  Details",0);
			lk_disphlight(1);
			lk_disptext(2,0,"   Enter Remark",0);
			memset(Action_Remark1,0,sizeof(Action_Remark1));
			memset(Action_Remark,0,sizeof(Action_Remark));
			if((remark_retval=lk_getalpha(5,0,Action_Remark1,50,strlen(Action_Remark1),0))<0)  Place_Contact(Action_contract_No,Action_Action_code,Action_Payee_Type,Action_Data_Amount_Pickupflag);
			Action_Remark1[remark_retval]='\0';
			
		if((strlen(Action_Remark1)==0))
		{
Valid_Remark:
		lk_dispclr();
		lk_disptext(2,0,"    Please Enter",0);
		lk_disptext(3,0,"      Remark    ",0);		
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Enter_Valid_key = lk_getkey();
		if(Enter_Valid_key==ENTER)
		{
		goto Enter_Remark;
		}
		else goto Valid_Remark;
		}
		
			sprintf(Action_Remark,"%s",replace(Action_Remark1));
	
			memset(Capture_Time,0,sizeof(Capture_Time));
			sprintf(Capture_Time,"%02d=%02d=%02d",curt.tm_hour,curt.tm_min,curt.tm_sec);	
			memset(Capture_date,0,sizeof(Capture_date));
			sprintf(Capture_date,"%02d=%02d=%04d",(curt.tm_mon+1),(curt.tm_mday),(curt.tm_year+1900));	
		
			memset(Action_data_show,0,sizeof(Action_data_show));			
			sprintf(Action_data_show,"%c%c=%c%c=20%c%c",Action_Date[2],Action_Date[3],Action_Date[0],Action_Date[1],Action_Date[4],Action_Date[5],Action_Date[6]);	
		
			memset(Action_time_show,0,sizeof(Action_time_show));	
			sprintf(Action_time_show,"%c%c=%c%c=00",Action_Time[0],Action_Time[1],Action_Time[2],Action_Time[3]);	
			printf("\nAction_data_show:%s\n",Action_data_show);
			printf("\nAction_time_show:%s\n",Action_time_show);
			
#if PRINTF
			printf("\nAction_contract_no:%s",Action_contract_No);
			printf("\nAction_Action_code:%s",Action_Action_code);
			printf("\nAction_Payee_Type:%s",Action_Payee_Type);
			printf("\nAction_Place_Contacted:%s",Action_Place_Contacted);
			printf("\nAction_Amount:%s",Action_Amount);
			printf("\nAction_amount_number:%s",Action_amount_number);	
			printf("\nAction_data_show:%s",Action_data_show);
			printf("\nAction_time_show:%s\n",Action_time_show);
			printf("\nAction_Remark:%s",Action_Remark);			
			printf("\nCapture_date:%s",Capture_date);
			printf("\nCapture_Time:%s",Capture_Time);
#endif			
			
			close_sqlite();
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			Exec_ID_HWD=get_result("Select Executive_id,hwid from operationalstatus");		
			printf("\nExec_ID:%s",Exec_ID_HWD.recordset[0][0]);	
			memset(dec_followup_exec,0,sizeof(dec_followup_exec));
			sprintf(dec_followup_exec,"%s",Decrypted_sqlite_DB(Exec_ID_HWD.recordset[0][0]));	
			memset(hwid_followup,0,sizeof(hwid_followup));
			sprintf(hwid_followup,"%s",Exec_ID_HWD.recordset[0][1]);

/******************************************************************************************************/
	if(strcmp(Action_Data_Amount_Pickupflag,"P")==0)
	{
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		Get_pickup_customer_no=get_result("SELECT Filler2 from PICKUP_DETAILS WHERE CARD_NUMBER='%s';",Action_contract_No);
		memset(Customer_ID,0,sizeof(Customer_ID));
		sprintf(Customer_ID,"%s",Get_pickup_customer_no.recordset[0][0]);
						
	}
	
	else
	{
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		Get_allocation_customer_no= get_result("SELECT filler1 from allocation_details WHERE CARD_NUMBER='%s';",Action_contract_No);
		memset(Customer_ID,0,sizeof(Customer_ID));
		sprintf(Customer_ID,"%s",Get_allocation_customer_no.recordset[0][0]);

	}		
/**************************************************************************************/	



			memset(followupdetails,0,sizeof(followupdetails));
			sprintf(followupdetails,"Validate,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,0,%s,%s,0",dec_followup_exec,hwid_followup,Action_contract_No,Action_amount_number,Action_amount_number,Action_Payee_Type,Action_Place_Contacted,Action_Amount,Action_Date,Capture_Time,Capture_date,Capture_Time,Action_Remark,Action_Data_Amount_Pickupflag,Customer_ID);
#if PRINTF
			printf("\nfollowupdetails:%s",followupdetails);	
#endif				
			memset(Customer_FollowUp_RSA_b64,0,sizeof(Customer_FollowUp_RSA_b64));
			sprintf(Customer_FollowUp_RSA_b64,"%s",RSAEncryption_Base64(followupdetails,strlen(followupdetails)));
#if PRINTF
			printf("\nCustomer_FollowUp_RSA_b64",Customer_FollowUp_RSA_b64);
#endif
			close_sqlite();
			open_sqlite("/mnt/jffs2/hdfccctrans.db");		
			execute("INSERT INTO unuploadedfollowupdetails(followupdetails,FollowUp_Status) VALUES('%s','0');",Customer_FollowUp_RSA_b64);			
			close_sqlite();
			open_sqlite("/mnt/jffs2/hdfccc.db");
			execute("delete from PICKUP_DETAILS where CARD_NUMBER='%s';",Action_contract_No);
			Upload_FollowUp_Request();
			Dra_Menu();
			//Follow_Up(Action_contract_No,Action_Data_Amount_Pickupflag);			
		}
		else 
		{
	Enter_Action_date:
		lk_dispclr();
		lk_disptext(2,0,"    Please Enter",0);
		lk_disptext(3,0,"    Valid Date",0);		
		lk_disptext(5,0,"Press Enter to Cont.",0);		
		Action_Date_key = lk_getkey();
		if(Action_Date_key==ENTER)
		{
		goto Action_Date_Label;
		}
		else goto Enter_Action_date;		
	}	
	}
	else
	{
PTP_Amount_return:
	lk_dispclr();
	lk_disptext(2,1,"    Please Enter",0);
	lk_disptext(3,1,"    Valid Amount",0);
	lk_disptext(5,0,"   Press Enter to Cont.",0);
	PTP_key = lk_getkey();
		if(PTP_key==ENTER)
		{	
		goto Enter_Amount;
		}
		else goto PTP_Amount_return;
	}
 
return 0;
}

/******************************************************************/


int Action_Remarks(char* Action_contract_No,char* Action_Action_code,char* Action_Payee_Type,char* Action_Place_Contacted,char* Action_Remark_Pickup_Flag)
{
	char Action_Remark[55],Action_Remark1[55],Capture_date[12],Capture_Time[12],Action_Address1[55],Action_Address[55];
	char Action_data_show[12],Action_time_show[12],followupdetails[300],Customer_FollowUp_RSA_b64[300],Action_dispute[20],dec_followup_exec[25],Action_remark_number[20],hwid_followup[15],Customer_ID[20];
	int remark_retval=0,Enter_Valid_key,address_retval=0,ADD_key;
	resultset Exec_ID_HWD,Get_allocation_customer_no,Get_pickup_customer_no;

	memset(&Exec_ID_HWD,0,sizeof(Exec_ID_HWD));

/***********************************for MSG left*******************************************************/
	 
       	if(strcmp(Action_Action_code,"10")==0)
	{
	int opt=0;
        MENU_T menu;
        int selItem  = 0;
        int acceptKbdEvents = TRUE;
        int maxEntries = 0;
        short scroll = 0;	
	

	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       4;
        maxEntries                      =       menu.maxEntries;
MSG_Menu:
	strcpy(menu.title,"      Message"); 
	strcpy(menu.menu[0],"1.Residence.");//                   
        strcpy(menu.menu[1],"2.Office");
	strcpy(menu.menu[2],"3.Alternate");//
	strcpy(menu.menu[3],"4.Third Party");
	memset(Action_remark_number,0,sizeof(Action_remark_number));
	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case CANCEL:	Place_Contact(Action_contract_No,Action_Action_code,Action_Payee_Type,Action_Remark_Pickup_Flag);
				break;

		case ENTER:
			switch (selItem+1 )
                    	{
			case 1:	strcpy(Action_remark_number,"12");
				break;
			case 2: strcpy(Action_remark_number,"13");
				break;
			case 3: strcpy(Action_remark_number,"14");
				break;
			case 4: strcpy(Action_remark_number,"15");
				break;
			default : break;
			}		
		default : break;
		}//switch
		break;
	}//while
		
        }	

/*******************************************************************************************************/

/**************************  Refuse To Pay Menu  ************************************************/

	if(strcmp(Action_Action_code,"14")==0)
	{
	int opt=0;
        MENU_T menu;
        int selItem  = 0;
        int acceptKbdEvents = TRUE;
        int maxEntries = 0;
        short scroll = 0;	
	
Refuse_Menu:
	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       6;
        maxEntries                      =       menu.maxEntries;

	strcpy(menu.title,"Reason"); 
	strcpy(menu.menu[0],"1.Finanical Problem");//                   
        strcpy(menu.menu[1],"2.Jobless");//
	strcpy(menu.menu[2],"3.Acting Tough");//
	strcpy(menu.menu[3],"4.Dispute");//
	strcpy(menu.menu[4],"5.Will Deal Directly with Bank");//
	strcpy(menu.menu[5],"6.Want Statement");
	memset(Action_remark_number,0,sizeof(Action_remark_number));
	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case CANCEL:	Place_Contact(Action_contract_No,Action_Action_code,Action_Payee_Type,Action_Remark_Pickup_Flag);
				break;

		case ENTER:
			switch (selItem+1 )
                    	{
			case 1:	strcpy(Action_remark_number,"22");
				break;
			case 2: strcpy(Action_remark_number,"23");
				break;
			case 3: strcpy(Action_remark_number,"24");
				break;
			case 4: strcpy(Action_remark_number,"25");
				break;
			case 5: strcpy(Action_remark_number,"26");
				break;
			case 6: strcpy(Action_remark_number,"27");
				break;
			default : break;
			}		
		default : break;
		}//switch
		break;
	}//while
		
        }	

/******************************************************************************/
/************************** No Contact Reason Menu ************************************************/

	if(strcmp(Action_Action_code,"15")==0)
	{
	int opt=0;
        MENU_T menu;
        int selItem  = 0;
        int acceptKbdEvents = TRUE;
        int maxEntries = 0;
        short scroll = 0;	
	
No_Contact_Menu:
	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       6;
        maxEntries                      =       menu.maxEntries;

	strcpy(menu.title,"      Reason"); 
	strcpy(menu.menu[0],"1.No cont. on all add.");//                   
        strcpy(menu.menu[1],"2.Outstation");//
	strcpy(menu.menu[2],"3.Door Locked");//
	strcpy(menu.menu[3],"4.Shifted from Resi. ");//
	strcpy(menu.menu[4],"5.Shifted from Off.");//
	strcpy(menu.menu[5],"6.Shifted from Alter.");
	memset(Action_remark_number,0,sizeof(Action_remark_number));
	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case CANCEL:	Place_Contact(Action_contract_No,Action_Action_code,Action_Payee_Type,Action_Remark_Pickup_Flag);
				break;

		case ENTER:
			switch (selItem+1 )
                    	{
			case 1:	strcpy(Action_remark_number,"28");
				printf("\nAction_remark_number%s",Action_remark_number);
				break;
			case 2: strcpy(Action_remark_number,"29");
				break;
			case 3: strcpy(Action_remark_number,"30");
				break;
			case 4: strcpy(Action_remark_number,"31");
				break;
			case 5: strcpy(Action_remark_number,"32");
				break;
			case 6: strcpy(Action_remark_number,"33");
				break;
			default : break;
			}		
		default : break;
		}//switch
		break;
	}//while
		
        }	

/******************************************************************************/
Enter_Remark:
	lk_dispclr();
	lk_disptext(0,1,"     HDFC Bank CC",0);
	lk_disptext(1,4,"Details",0);
	lk_disphlight(1);
	lk_disptext(2,0,"Enter Remark",0);
	memset(Action_Remark1,0,sizeof(Action_Remark1));
	memset(Action_Remark,0,sizeof(Action_Remark));
	if((remark_retval=lk_getalpha(5,0,Action_Remark1,50,strlen(Action_Remark1),0))<0) 
	if(strcmp(Action_Action_code,"10")==0) goto MSG_Menu;
	else if(strcmp(Action_Action_code,"14")==0) goto Refuse_Menu;
	else if(strcmp(Action_Action_code,"15")==0) goto No_Contact_Menu;
	else Place_Contact(Action_contract_No,Action_Action_code,Action_Payee_Type,Action_Remark_Pickup_Flag);
	Action_Remark1[remark_retval]='\0';
		if((strlen(Action_Remark1)==0))
		{
Valid_Remark:
		lk_dispclr();
		lk_disptext(2,0,"    Please Enter",0);
		lk_disptext(3,0,"      Remark    ",0);		
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Enter_Valid_key = lk_getkey();
		if(Enter_Valid_key==ENTER)
		{
		goto Enter_Remark;
		}
		else goto Valid_Remark;
		}
		
	
		sprintf(Action_Remark,"%s",replace(Action_Remark1));
		
		lk_getrtc(&curt);
		memset(Capture_Time,0,sizeof(Capture_Time));
		sprintf(Capture_Time,"%02d=%02d=%02d",curt.tm_hour,curt.tm_min,curt.tm_sec);
#if PRINTF
		printf("Capture_Time%s",Capture_Time);	
#endif
		memset(Capture_date,0,sizeof(Capture_date));
		sprintf(Capture_date,"%02d=%02d=%04d",(curt.tm_mon+1),(curt.tm_mday),(curt.tm_year+1900));
#if PRINTF
		printf("Capture_date%s",Capture_date);
#endif

#if PRINTF
		printf("\nAction_contract_no:%s",Action_contract_No);
		printf("\nAction_Action_code:%s",Action_Action_code);
		printf("\nAction_Payee_Type:%s",Action_Payee_Type);
		printf("\nAction_Place_Contacted:%s",Action_Place_Contacted);
		printf("\nAction_data_show:%s",Action_data_show);
		printf("\nAction_date_menu:%s",Action_remark_number);
		printf("\nAction_Remark:%s",Action_Remark);
		printf("\nCapture_date:%s",Capture_date);
		printf("\nCapture_Time:%s",Capture_Time);
#endif

/******************************************data to upload from Action_remark**************************************************/
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		Exec_ID_HWD=get_result("Select Executive_id,hwid from operationalstatus");		
		printf("\nExec_ID:%s",Exec_ID_HWD.recordset[0][0]);	
		memset(dec_followup_exec,0,sizeof(dec_followup_exec));
		sprintf(dec_followup_exec,"%s",Decrypted_sqlite_DB(Exec_ID_HWD.recordset[0][0]));
		
		memset(hwid_followup,0,sizeof(hwid_followup));
		sprintf(hwid_followup,"%s",Exec_ID_HWD.recordset[0][1]);

/******************************************************************************************************/
	if(strcmp(Action_Remark_Pickup_Flag,"P")==0) 
	{
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		Get_pickup_customer_no=get_result("SELECT Filler2 from PICKUP_DETAILS WHERE CARD_NUMBER='%s';",Action_contract_No);
		memset(Customer_ID,0,sizeof(Customer_ID));
		sprintf(Customer_ID,"%s",Get_pickup_customer_no.recordset[0][0]);
						
	}
	
	else
	{
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		Get_allocation_customer_no= get_result("SELECT filler1 from allocation_details WHERE CARD_NUMBER='%s';",Action_contract_No);
		memset(Customer_ID,0,sizeof(Customer_ID));
		sprintf(Customer_ID,"%s",Get_allocation_customer_no.recordset[0][0]);

	}		
/**************************************************************************************/	




		
		if((strcmp(Action_Action_code,"10")==0)||(strcmp(Action_Action_code,"14")==0)||(strcmp(Action_Action_code,"15")==0))
		{
		printf("\nAction_remark_menu_test");
		printf("\nAction_remark_menu:%s",Action_remark_number);
		memset(followupdetails,0,sizeof(followupdetails));
		sprintf(followupdetails,"Validate,%s,%s,%s,%s,%s,%s,%s,0,%s,%s,%s,%s,%s,0,%s,%s,0",dec_followup_exec,hwid_followup,Action_contract_No,Action_remark_number,Action_remark_number,Action_Payee_Type,Action_Place_Contacted,Capture_date,Capture_Time,Capture_date,Capture_Time,Action_Remark,Action_Remark_Pickup_Flag,Customer_ID);
#if PRINTF
		printf("\nfollowupdetails:%s",followupdetails);	
#endif
		memset(Customer_FollowUp_RSA_b64,0,sizeof(Customer_FollowUp_RSA_b64));
		sprintf(Customer_FollowUp_RSA_b64,"%s",RSAEncryption_Base64(followupdetails,strlen(followupdetails)));
#if PRINTF
		printf("\nCustomer_FollowUp_RSA_b64:%s",Customer_FollowUp_RSA_b64);	
#endif
		}
		else
		{
		memset(Action_remark_number,0,sizeof(Action_remark_number));
		if(strcmp(Action_Action_code,"2")==0) strcpy(Action_remark_number,"2");

		if(strcmp(Action_Action_code,"3")==0) strcpy(Action_remark_number,"3");

		if(strcmp(Action_Action_code,"4")==0) strcpy(Action_remark_number,"4");

		if(strcmp(Action_Action_code,"6")==0) strcpy(Action_remark_number,"8");

		if(strcmp(Action_Action_code,"7")==0) strcpy(Action_remark_number,"9");

		if(strcmp(Action_Action_code,"8")==0) strcpy(Action_remark_number,"10");

		if(strcmp(Action_Action_code,"9")==0) strcpy(Action_remark_number,"11");

		if(strcmp(Action_Action_code,"11")==0) strcpy(Action_remark_number,"16");

		if(strcmp(Action_Action_code,"13")==0) strcpy(Action_remark_number,"21");

		if(strcmp(Action_Action_code,"17")==0) strcpy(Action_remark_number,"35");
		
		if(strcmp(Action_Action_code,"18")==0) strcpy(Action_remark_number,"36");

		memset(followupdetails,0,sizeof(followupdetails));
		sprintf(followupdetails,"Validate,%s,%s,%s,%s,%s,%s,%s,0,%s,%s,%s,%s,%s,0,%s,%s,0",dec_followup_exec,hwid_followup,Action_contract_No,Action_remark_number,Action_remark_number,Action_Payee_Type,Action_Place_Contacted,Capture_date,Capture_Time,Capture_date,Capture_Time,Action_Remark,Action_Remark_Pickup_Flag,Customer_ID);
#if PRINTF
		printf("\nfollowupdetails:%s",followupdetails);	
#endif
		memset(Customer_FollowUp_RSA_b64,0,sizeof(Customer_FollowUp_RSA_b64));
		sprintf(Customer_FollowUp_RSA_b64,"%s",RSAEncryption_Base64(followupdetails,strlen(followupdetails)));
#if PRINTF
		printf("\nCustomer_FollowUp_RSA_b64:%s",Customer_FollowUp_RSA_b64);
#endif
		}
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccctrans.db");
		execute("INSERT INTO unuploadedfollowupdetails(followupdetails,FollowUp_Status) VALUES('%s','0');",Customer_FollowUp_RSA_b64);	
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		execute("delete from PICKUP_DETAILS where CARD_NUMBER='%s';",Action_contract_No);		
		Upload_FollowUp_Request();
		Dra_Menu();		
		//Follow_Up(Action_contract_No,Action_Remark_Pickup_Flag);
			
}

/******************************************************************/
int Action_Address_details(char* Action_contract_no,char* Action_Action_code,char* Action_Payee_Type,char* Action_Place_Contacted,char* Action_Address_Pickup_Flag)
{
	char Action_Remark[55],Action_Remark1[55],Capture_date[12],Capture_Time[12],Action_Address1[55],Action_Address[55];
	char Action_data_show[12],Action_time_show[12],followupdetails[300],Customer_FollowUp_RSA_b64[300],Action_dispute[20],dec_followup_exec[25],Action_Address_number[20],hwid_followup[15],Customer_ID[20];
	int remark_retval=0,Enter_Valid_key,address_retval=0,ADD_key;
	resultset Exec_ID_HWD,Get_allocation_customer_no,Get_pickup_customer_no;

	memset(&Exec_ID_HWD,0,sizeof(Exec_ID_HWD));
Address:
	lk_dispclr();
	lk_disptext(0,1,"     HDFC Bank CC",0);
	lk_disptext(1,4,"Address",0);
	lk_disphlight(1);
	lk_disptext(2,0,"Enter Address",0);
	memset(Action_Address1,0,sizeof(Action_Address1));
	memset(Action_Address,0,sizeof(Action_Address));
	if((address_retval=lk_getalpha(5,0,Action_Address1,50,strlen(Action_Address1),0))<0) Place_Contact(Action_contract_no,Action_Action_code,Action_Payee_Type,Action_Address_Pickup_Flag);
	Action_Address1[address_retval]='\0';	
	printf("\nlength:%d",strlen(Action_Address1));
	if((strlen(Action_Address1)>0))//&&(Check_Alpha_Numeric_With_Space(Action_Remark1)==1))
		{
		sprintf(Action_Address,"%s",replace(Action_Address1));

		lk_getrtc(&curt);
		memset(Capture_Time,0,sizeof(Capture_Time));
		sprintf(Capture_Time,"%02d=%02d=%02d",curt.tm_hour,curt.tm_min,curt.tm_sec);	
		memset(Capture_date,0,sizeof(Capture_date));
		sprintf(Capture_date,"%02d=%02d=%04d",(curt.tm_mon+1),(curt.tm_mday),(curt.tm_year+1900));
		
#if PRINTF
		printf("\nAction_contract_no:%s",Action_contract_no);
		printf("\nAction_Action_code:%s",Action_Action_code);
		printf("\nAction_Payee_Type:%s",Action_Payee_Type);
		printf("\nAction_Place_Contacted:%s",Action_Place_Contacted);		
		printf("\nAction_Address%s",Action_Address);
		printf("\nAction_Remark:%s",Action_Remark);
		printf("\nCapture_date:%s",Capture_date);
		printf("\nCapture_Time:%s",Capture_Time);
#endif
		
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		Exec_ID_HWD=get_result("Select Executive_id,hwid from operationalstatus");		
		printf("\nExec_ID:%s",Exec_ID_HWD.recordset[0][0]);	
		memset(dec_followup_exec,0,sizeof(dec_followup_exec));
		sprintf(dec_followup_exec,"%s",Decrypted_sqlite_DB(Exec_ID_HWD.recordset[0][0]));
		
		memset(hwid_followup,0,sizeof(hwid_followup));
		sprintf(hwid_followup,"%s",Exec_ID_HWD.recordset[0][1]);

/******************************************************************************************************/
	if(strcmp(Action_Address_Pickup_Flag,"P")==0)
	{
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		Get_pickup_customer_no=get_result("SELECT Filler2 from PICKUP_DETAILS WHERE CARD_NUMBER='%s';",Action_contract_no);
		memset(Customer_ID,0,sizeof(Customer_ID));
		sprintf(Customer_ID,"%s",Get_pickup_customer_no.recordset[0][0]);
						
	}
	
	else
	{
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		Get_allocation_customer_no= get_result("SELECT filler1 from allocation_details WHERE CARD_NUMBER='%s';",Action_contract_no);
		memset(Customer_ID,0,sizeof(Customer_ID));
		sprintf(Customer_ID,"%s",Get_allocation_customer_no.recordset[0][0]);

	}		
/**************************************************************************************/	

		memset(Action_Address_number,0,sizeof(Action_Address_number));
		strcpy(Action_Address_number,"34");
		memset(followupdetails,0,sizeof(followupdetails));
		sprintf(followupdetails,"Validate,%s,%s,%s,%s,%s,%s,%s,0,%s,%s,%s,%s,0,%s,%s,%s,0",dec_followup_exec,hwid_followup,Action_contract_no,Action_Address_number,Action_Address_number,Action_Payee_Type,Action_Place_Contacted,Capture_date,Capture_Time,Capture_date,Capture_Time,Action_Address,Action_Address_Pickup_Flag,Customer_ID);
#if PRINTF
		printf("\nfollowupdetails:%s",followupdetails);	
#endif
		memset(Customer_FollowUp_RSA_b64,0,sizeof(Customer_FollowUp_RSA_b64));	
		memset(Customer_FollowUp_RSA_b64,0,sizeof(Customer_FollowUp_RSA_b64));
		sprintf(Customer_FollowUp_RSA_b64,"%s",RSAEncryption_Base64(followupdetails,strlen(followupdetails)));
#if PRINTF		
		printf("\nCustomer_FollowUp_RSA_b64:%s",Customer_FollowUp_RSA_b64);	
#endif
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccctrans.db");
		execute("INSERT INTO unuploadedfollowupdetails(followupdetails,FollowUp_Status) VALUES('%s','0');",Customer_FollowUp_RSA_b64);
		close_sqlite();
		open_sqlite("/mnt/jffs2/hdfccc.db");
		execute("delete from PICKUP_DETAILS where CARD_NUMBER='%s';",Action_contract_no);		
		Upload_FollowUp_Request();
		Dra_Menu();		
		//Follow_Up(Action_contract_no,Action_Address_Pickup_Flag);
	}
	else
	{
Address_Mandatory:

	lk_dispclr();
	lk_disptext(2,1,"   Please Enter",0);
	lk_disptext(3,1,"     Address",0);
	lk_disptext(4,0,"  It is Mandatory",0);
	lk_disptext(5,0,"Press Enter to Cont.",0);
	ADD_key = lk_getkey();
		if(ADD_key==ENTER)
		{	
		Action_Address_details(Action_contract_no,Action_Action_code,Action_Payee_Type,Action_Place_Contacted,Action_Address_Pickup_Flag);
		}
		else goto Address_Mandatory;
	}

}
