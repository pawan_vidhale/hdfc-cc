/*******************************************************************/
//			    HDFC CC Bank
//			    Payment.c
/*******************************************************************/
//@author FTL003
#include <header.h>



/*******************************************************************/
/*******************************************************************/
int Payment(char* Payment_contract_no,char* Pick_Payment_Flag)
{	
	MENU_T menu;
        int opt=0,ret;
        int selItem  = 0;
        int acceptKbdEvents;
        int maxEntries = 0;
        short scroll = 0;
	char Battery_return[20];
	int payee_type_retn=0,Battery_key;	
	char Curr_Bal[30],Roll_Amt[30],Stab_Amt[30],Norm_Amt[30],Load_Bal[30];
	resultset Display_Balance;

	
	memset(Battery_return,0,sizeof(Battery_return));
	sprintf(Battery_return,"%s",Battery_Print_test());

	if(atof(Battery_return)<(6.82))
	{
Battery_Low:
	lk_dispclr();
	lk_disptext(1,5,"Battery Low!",1);
	lk_disptext(3,0," Can't Do Transact.",1);
	lk_disptext(5,0,"Press Enter to Cont.",0);
	Battery_key = lk_getkey();
	if(Battery_key==ENTER)
	{
	printf("\nBattery is Now lower than 15%\n");
	Dra_Menu();
	}
	else goto Battery_Low; 
	}
	else if(atof(Battery_return)<(6.92))
	{
	lk_dispclr();
	lk_disptext(1,5,"Battery Low!",1);
	lk_disptext(3,0,"  Plug Power Cord",0);//plug Power code cable
	lk_disptext(4,0," Cable & Press ENTER",0);
	lk_disptext(5,0,"  Key To continue",0);
	lk_getkey();
	}
	memset(&Display_Balance,0,sizeof(Display_Balance));

	printf("\nPayment_contract_no:%s\n",Payment_contract_no);
	printf("\nPick_Payment_Flag:%s\n",Pick_Payment_Flag);


	if(strcmp(Pick_Payment_Flag,"D")==0)
	{
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccc.db");	
	Display_Balance = get_result("SELECT CURRENT_BL,TOTAL_AMT_DUE,LOADED_BAL,OVERLIMIT_AMT,TOTAL_CURR_DUE FROM ALLOCATION_DETAILS WHERE CARD_NUMBER='%s';",Payment_contract_no);
	memset(Curr_Bal,0,sizeof(Curr_Bal));
	sprintf(Curr_Bal,"Curr Bal:Rs%s/-",Display_Balance.recordset[0][0]);
	memset(Roll_Amt,0,sizeof(Roll_Amt));
	sprintf(Roll_Amt,"Roll Amt:Rs%s/-",Display_Balance.recordset[0][1]);
	memset(Stab_Amt,0,sizeof(Stab_Amt));
	sprintf(Stab_Amt,"Stab Amt:Rs%s/-",Display_Balance.recordset[0][3]);	
	memset(Norm_Amt,0,sizeof(Norm_Amt));
	sprintf(Norm_Amt,"Norm Amt:Rs%s/-",Display_Balance.recordset[0][4]);
	memset(Load_Bal,0,sizeof(Load_Bal));
	sprintf(Load_Bal,"Load Bal:Rs%s/-",Display_Balance.recordset[0][2]);
	}
	else
	{
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccc.db");	
	Display_Balance = get_result("SELECT PICKUP_AMOUNT FROM PICKUP_DETAILS WHERE CARD_NUMBER='%s';",Payment_contract_no);
	memset(Curr_Bal,0,sizeof(Curr_Bal));
	sprintf(Curr_Bal,"Curr Bal:Rs 0/-");
	memset(Roll_Amt,0,sizeof(Roll_Amt));
	sprintf(Roll_Amt,"Roll Amt:Rs 0/-");
	memset(Stab_Amt,0,sizeof(Stab_Amt));
	sprintf(Stab_Amt,"Stab Amt:Rs%s/-",Display_Balance.recordset[0][0]);	
	memset(Norm_Amt,0,sizeof(Norm_Amt));
	sprintf(Norm_Amt,"Norm Amt:Rs 0/-");
	memset(Load_Bal,0,sizeof(Load_Bal));
	sprintf(Load_Bal,"Load Bal:Rs 0/-");
	}
	

	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       6;
        maxEntries                      =      menu.maxEntries;
	

	strcpy(menu.title,"  Payment Menu");  
	strcpy(menu.menu[0],"Paid Amt");
      	strcpy(menu.menu[1],Curr_Bal);
	strcpy(menu.menu[2],Roll_Amt);
	strcpy(menu.menu[3],Stab_Amt);
	strcpy(menu.menu[4],Norm_Amt);
	strcpy(menu.menu[5],Load_Bal);	

	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case CANCEL:	After_selecting_customer(Payment_contract_no,Pick_Payment_Flag);
				break;

		case ENTER:
			switch (selItem+1 )
                    	{
			case 1: Payment_mode(Payment_contract_no,Pick_Payment_Flag);				
				break;
			}
			default : break;
		}
	}
}



/*******************************************************************/
int Payment_mode(char* Payment_cc_no,char* Pick_Payment_Flag)
{	
	
	char Payment_Amount[20];
	int Payment_rval=0;
	int PTP_key;	
	
	printf("Payment_cc_no:%s",Payment_cc_no);
	
Enter_Amount:
	lk_dispclr();
	lk_disptext(0,1,"     HDFC Bank CC",0);
	lk_disptext(1,4,"   Details",0);
	lk_disphlight(1);
	lk_disptext(3,0," Enter Amount",0);
	memset(Payment_Amount,0,sizeof(Payment_Amount));
	if((Payment_rval=lk_getnumeric(5,1,Payment_Amount,9,strlen(Payment_Amount),0))<0) Payment(Payment_cc_no,Pick_Payment_Flag);
	Payment_Amount[Payment_rval]='\0';
	printf("\nAmount:%s",Payment_Amount);
	if((strlen(Payment_Amount)>0)&&(Check_point(Payment_Amount)==1)&&(atol(Payment_Amount)>0)&&(9999999.99>=atof(Payment_Amount)))
	{		
		Executive_Check();
		Cash_Chq_DD_Menu(Payment_cc_no,Payment_Amount,Pick_Payment_Flag);	
	}	
	else
	{
Payment_Amount_return:
	lk_dispclr();
	lk_disptext(2,1,"    Please Enter",0);
	lk_disptext(3,1,"       Amount",0);
	lk_disptext(5,0," Press Enter to Cont.",0);
	PTP_key = lk_getkey();
	if(PTP_key==ENTER)
	{	
	goto Enter_Amount;
	}
	else goto Payment_Amount_return;	
	}	

}


/*******************************************************************/
int Cash_Chq_DD_Menu(char* Cash_Chq_DD_CC_no,char* Cash_Chq_DD_Amount,char* Cash_Chq_PickPayment_Flag)
{

	MENU_T menu;
	int opt=0,ret=0,Due_Low_key_balance=0,Contact_key=0,Contact_Branch_key=0;
	int selItem  = 0;
	int acceptKbdEvents;
	int maxEntries = 0;
	short scroll = 0;
	int payee_type_retn=0;
	resultset Server_Wallet_Amount;

	memset(&Server_Wallet_Amount,0,sizeof(Server_Wallet_Amount));


	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Server_Wallet_Amount=get_result("SELECT WalletBalance,ExecutiveStatus FROM operationalstatus;");
	printf("\nWalletBalance:%s",Server_Wallet_Amount.recordset[0][0]);
	if(strcmp(Server_Wallet_Amount.recordset[0][1],"0")==0)	Dra_Menu();	
	if((Server_Wallet_Amount.rows>0)&&((strcmp(Server_Wallet_Amount.recordset[0][1],"0")==0)||(atoi(Server_Wallet_Amount.recordset[0][1])==0)))  Dra_Menu();
	

	lk_dispclr(); 
	menu.start                      =       0;
	menu.maxEntries                 =       3;
	maxEntries                      =      menu.maxEntries;

	strcpy(menu.title,"Payment Menu");  
	strcpy(menu.menu[0],"1.Cash");//
	strcpy(menu.menu[1],"2.Cheque");
	strcpy(menu.menu[2],"3.DD");


	while(1)
	{    
        opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

        switch(opt)
        {
	case CANCEL:	Payment_mode(Cash_Chq_DD_CC_no,Cash_Chq_PickPayment_Flag);
			break;

	case ENTER:
		switch (selItem+1 )
            	{
		case 1:
			if((Server_Wallet_Amount.rows>0)&&(strcmp(Server_Wallet_Amount.recordset[0][1],"0")==0)) Dra_Menu();  
			else if(atol(Server_Wallet_Amount.recordset[0][0])<atol(Cash_Chq_DD_Amount))
			{
Due_to_Low_check:		
				lk_dispclr();
				lk_disptext(2,2,"Due to Low Balance",0);
				lk_disptext(3,2," Can't proceed for",0);
				lk_disptext(4,2," This Cash Amount",0);
				lk_disptext(5,0,"Press Enter to Cont.",0);
				Due_Low_key_balance = lk_getkey();
				if(Due_Low_key_balance==ENTER)	
				{			
				Cash_Chq_DD_Menu(Cash_Chq_DD_CC_no,Cash_Chq_DD_Amount,Cash_Chq_PickPayment_Flag);
				}
				else goto Due_to_Low_check;
			}
			else Cash_menu(Cash_Chq_DD_CC_no,Cash_Chq_DD_Amount,"1",Cash_Chq_PickPayment_Flag);
			break;

		case 2:
#if PRINTF
			printf("Cash_Chq_DD_CC_no:%s",Cash_Chq_DD_CC_no);
			printf("Cash_Chq_DD_Amount:%s",Cash_Chq_DD_Amount);
#endif
			if((Server_Wallet_Amount.rows>0)&&(strcmp(Server_Wallet_Amount.recordset[0][1],"0")==0))
			{
Contact_Branch:
				lk_dispclr();
				lk_disptext(2,2," Your Transactions",0);
				lk_disptext(3,2,"   Are Blocked",0);
				lk_disptext(4,2,"Pls Contact Branch",0);
				lk_disptext(5,0,"Press Enter to Cont.",0);
				Contact_key = lk_getkey();
				if(Contact_key==ENTER)
				{
				Dra_Menu();
				}
				else goto Contact_Branch;
			}
			else Cheque_DD_menu(Cash_Chq_DD_CC_no,Cash_Chq_DD_Amount,"2",Cash_Chq_PickPayment_Flag);
			break;

		case 3:
			if((Server_Wallet_Amount.rows>0)&&(strcmp(Server_Wallet_Amount.recordset[0][1],"0")==0))
			{
Blocked_Branck:
				lk_dispclr();
				lk_disptext(2,2," Your Transactions",0);
				lk_disptext(3,2,"   Are Blocked",0);
				lk_disptext(4,2,"Pls Contact Branch",0);
				lk_disptext(5,0,"Press Enter to Cont.",0);
				Contact_Branch_key = lk_getkey();
				if(Contact_Branch_key==ENTER)
				{
				Dra_Menu();
				}
				else goto Blocked_Branck;
			} 
			Cheque_DD_menu(Cash_Chq_DD_CC_no,Cash_Chq_DD_Amount,"3",Cash_Chq_PickPayment_Flag);
			break;
		}
		default : break;
	}
	}


}


int Cash_menu(char* Cash_CC_no,char* Cash_Confirm_Amount,char *Cash_mode,char* Cash_PickPayment_Flag)
{
	char Entered_PAN_Number[20],Payee_Mobile_Number[20];
	int PAN_Number_rval=0,Payee_Mobile_Number_rval=0,PAN_Verify_key=0;	
	unsigned int PAN_Flag=0;
	resultset Pan_no_Get;

	memset(&Pan_no_Get,0,sizeof(Pan_no_Get));
#if PRINTF			
	printf("\nCash_CC_no:%s",Cash_CC_no);
	printf("\nCash_mode:%s",Cash_mode);
	printf("\nPayment_Amount:%s",Cash_Confirm_Amount);
#endif	
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccc.db");	
	Pan_no_Get = get_result("SELECT PAN_NO FROM ALLOCATION_DETAILS WHERE CARD_NUMBER='%s';",Cash_CC_no);	

	if(atol(Cash_Confirm_Amount)>=50000&&strcmp(Pan_no_Get.recordset[0][0],"(null)")==0)
	{

	lk_dispclr();
	lk_disptext(0,1,"     HDFC Bank CC",0);
	lk_disptext(1,2,"   Payment",0);
	lk_disphlight(1); 
	lk_disptext(2,0,"Enter PAN Number",0);
	memset(Entered_PAN_Number,0,sizeof(Entered_PAN_Number));
	PAN_Number_rval=0;
        if((PAN_Number_rval=lk_getalpha(5,1,Entered_PAN_Number,10,strlen(Entered_PAN_Number),0))<0) Cash_Chq_DD_Menu(Cash_CC_no,Cash_Confirm_Amount,Cash_PickPayment_Flag);
		
	Entered_PAN_Number[PAN_Number_rval]='\0';
	//printf("\nPan:%d\n",check_PAN(PAN_Number));
	if((check_PAN(Entered_PAN_Number)==1)&&(strlen(Entered_PAN_Number)==10)&&(Check_Alpha_Numeric(Entered_PAN_Number)==1))
	{	
		Pay_Type(Cash_CC_no,Cash_Confirm_Amount,Cash_mode,Entered_PAN_Number,"0","0","0","0",Cash_PickPayment_Flag);
	}
	else if(strlen(Entered_PAN_Number)==0)
	{
		Pay_Type(Cash_CC_no,Cash_Confirm_Amount,Cash_mode,"0","0","0","0","0",Cash_PickPayment_Flag);
	}
	else
	{
PAN_Valid:
	lk_dispclr();
	lk_disptext(1,1,"Please Enter Valid",0);
	lk_disptext(2,2,"  PAN Number",0);
	lk_disptext(5,0,"Press Enter to Cont.",0);
	PAN_Verify_key = lk_getkey();
	if(PAN_Verify_key==ENTER)
	{
	Cash_menu(Cash_CC_no,Cash_Confirm_Amount,Cash_mode,Cash_PickPayment_Flag);
	}
	else goto PAN_Valid;
	}
	}
	else
	{
		if(strcmp(Cash_PickPayment_Flag,"D")==0)
		{		
		Pay_Type(Cash_CC_no,Cash_Confirm_Amount,Cash_mode,Pan_no_Get.recordset[0][0],"0","0","0","0",Cash_PickPayment_Flag);
		}
		else
		{		
		Pay_Type(Cash_CC_no,Cash_Confirm_Amount,Cash_mode,"0","0","0","0","0",Cash_PickPayment_Flag);
		}
	}

}

/*******************************************************************************************************/


int Cheque_DD_menu(char* Cheque_DD_contact_no,char* Cheque_DD_Confirm_Amount,char* Cheque_DD_mode,char* Cheque_DD_PickPayment_Flag)
{
	char Chq_DD_Date[8],Chq_DD_Number[12],Chq_DD_Bank_Name[50],Chq_DD_Bank[50],Payee_Mobile_Number[10],MICR_Number[9];
	int Chq_DD_Date_rval=0,Chq_DD_rval=0,Chq_DD_Bank_Name_rval=0;
	int Valid_key,Valid_Cheque_key,ReEnter_Cheque_key,DD_Cheque_key,ReEnter_DD_key,MICR_rval=0;
#if PRINTF		
	printf("\nCheque_DD_Confirm_Amount:%s",Cheque_DD_contact_no);
	printf("\nCheque_DD_Confirm_Amount:%s",Cheque_DD_Confirm_Amount);
	printf("\nCheque_DD_mode:%s",Cheque_DD_mode);		
	printf("\nCheque_DD_PickPayment_Flag:%s",Cheque_DD_PickPayment_Flag);
#endif
Chq_DD_Lable:
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank CC",0);
		lk_disptext(1,2,"   Payment",0);
		lk_disphlight(1); 
		lk_disptext(2,1,"Enter Chq/DD Number",0);
		memset(Chq_DD_Number,0,sizeof(Chq_DD_Number));
		Chq_DD_rval=0;
		if((Chq_DD_rval=lk_getnumeric(5,1,Chq_DD_Number,6,strlen(Chq_DD_Number),0))<0) Cash_Chq_DD_Menu(Cheque_DD_contact_no,Cheque_DD_Confirm_Amount,Cheque_DD_PickPayment_Flag);
		Chq_DD_Number[Chq_DD_rval]='\0';
		if((strlen(Chq_DD_Number)>=6)&&(strlen(Chq_DD_Number)<=6)&&(atol(Chq_DD_Number)>0)&&(Check_Numeric(Chq_DD_Number)==1))
		{
MICR_Lable:
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank CC",0);
		lk_disptext(1,2,"   Payment",0);
		lk_disphlight(1); 
		lk_disptext(2,1,"Enter MICR Number",0);
		memset(MICR_Number,0,sizeof(MICR_Number));
		MICR_rval=0;
		if((MICR_rval=lk_getnumeric(5,1,MICR_Number,9,strlen(MICR_Number),0))<0) goto Chq_DD_Lable;		
		MICR_Number[MICR_rval]='\0';
		if((strlen(MICR_Number)>=9)&&(strlen(MICR_Number)<=9)&&(Check_Numeric(MICR_Number)==1))
		{

Chq_DD_Date_Lable:
		if(strcmp(Cheque_DD_mode,"2")==0)
		{
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank CC",0);
		lk_disptext(1,2,"   Payment",0);
		lk_disphlight(1); 
		lk_disptext(2,1,"Enter Chq/DD Date",0);
		lk_disptext(3,1,"(DD-MM-YY)",0);
		memset(Chq_DD_Date,0,sizeof(Chq_DD_Date));
		Chq_DD_Date_rval=0;
		if((Chq_DD_Date_rval=lk_getnumeric(5,1,Chq_DD_Date,6,strlen(Chq_DD_Date),0))<0) goto Chq_DD_Lable;
		Chq_DD_Date[Chq_DD_Date_rval]='\0';
		if((strlen(Chq_DD_Date)==6)&&(chk_date(Chq_DD_Date)>0)&&(Check_Numeric(Chq_DD_Date)==1)&&(Check_Date_Chq(Chq_DD_Date)==1))
		{
Bank_Name_Lable:
		lk_dispclr();
		lk_disptext(0,1,"     HDFC Bank CC",0);
		lk_disptext(1,2,"   Payment",0);
		lk_disphlight(1);
		lk_disptext(2,1,"Enter Bank Name",0);
		lk_disptext(3,1,"(Don't Enter Number)",0);
		memset(Chq_DD_Bank_Name,0,sizeof(Chq_DD_Bank_Name));
		Chq_DD_Bank_Name_rval=0;
		if((Chq_DD_Bank_Name_rval=lk_getalpha(5,1,Chq_DD_Bank_Name,20,strlen(Chq_DD_Bank_Name),0))<0) goto Chq_DD_Date_Lable;		
#if PRINTF		
		printf("\nChq_Bank:%s\n",Chq_DD_Bank_Name);
#endif	
		if((strlen(Chq_DD_Bank_Name)>1)&&(Check_Alpha_With_Space(Chq_DD_Bank_Name)==1))		
		{					
                	Pay_Type(Cheque_DD_contact_no,Cheque_DD_Confirm_Amount,Cheque_DD_mode,"0",Chq_DD_Number,Chq_DD_Date,Chq_DD_Bank_Name,MICR_Number,Cheque_DD_PickPayment_Flag);
		}

		else
		{
Valid_Cheqe_DD:
			lk_dispclr();
			lk_disptext(2,0,"   Please ReEnter",0);
			lk_disptext(3,0,"  Valid Cheque/DD",0);
			lk_disptext(4,0,"    Bank Name",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			Valid_Cheque_key = lk_getkey();
			if(Valid_Cheque_key==ENTER)
			{
		 	goto Bank_Name_Lable;  //Bank_Name
			}
			else goto Valid_Cheqe_DD;
		}
		}
		else
		{
Cheque_DD_Date:
			lk_dispclr();
			lk_disptext(2,0,"   Please ReEnter",0);
			lk_disptext(3,0,"      Valid",0);
			lk_disptext(4,0,"  Cheque/DD Date",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			ReEnter_Cheque_key = lk_getkey();
			if(ReEnter_Cheque_key==ENTER)
			{
		 	goto Chq_DD_Date_Lable; //chq date
			}
			else goto Cheque_DD_Date;
		}
		}		
		else 
		{
DD_Date_Payment:
			lk_dispclr();
			lk_disptext(0,1,"     HDFC Bank CC",0);
			lk_disptext(1,2,"   Payment",0);
			lk_disphlight(1); 
			lk_disptext(2,1,"Enter Chq/DD Date",0);
			lk_disptext(3,1,"(DD-MM-YY)",0);
			memset(Chq_DD_Date,0,sizeof(Chq_DD_Date));
			Chq_DD_Date_rval=0;
			if((Chq_DD_Date_rval=lk_getnumeric(5,1,Chq_DD_Date,6,strlen(Chq_DD_Date),0))<0) goto Chq_DD_Lable;
			Chq_DD_Date[Chq_DD_Date_rval]='\0';
			if((strlen(Chq_DD_Date)==6)&&(chk_date(Chq_DD_Date)>0)&&(Check_Numeric(Chq_DD_Date)==1)&&(Check_Date_DD(Chq_DD_Date)==1))
			{
				goto Bank_Name_Lable;
			}
			else
			{
DD_Date:
			lk_dispclr();
			lk_disptext(2,0,"   Please ReEnter",0);
			lk_disptext(3,0,"      Valid",0);
			lk_disptext(4,0,"  Cheque/DD Date",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			ReEnter_DD_key = lk_getkey();
			if(ReEnter_DD_key==ENTER)
			{
				goto DD_Date_Payment;
			}
			else goto DD_Date;
			}
		}
		}
			else
			{
MICR_Number:
			lk_dispclr();
			lk_disptext(2,0,"   Please ReEnter",0);
			lk_disptext(3,0,"   Valid 9 Digit",0);
			lk_disptext(4,0,"  MICR Number",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			ReEnter_Cheque_key = lk_getkey();
			if(ReEnter_Cheque_key==ENTER)
			{
			goto MICR_Lable;	//Chq Number
			}
			else goto MICR_Number;		
			}
		}
		else 
		{
Digit_Number:
			lk_dispclr();
			lk_disptext(2,0,"   Please ReEnter",0);
			lk_disptext(3,0,"   Valid 6 Digit",0);
			lk_disptext(4,0,"  Cheque/DD Number",0);
			lk_disptext(5,0,"Press Enter to Cont.",0);
			ReEnter_Cheque_key = lk_getkey();
			if(ReEnter_Cheque_key==ENTER)
			{
			goto Chq_DD_Lable;	//Chq Number
			}
			else goto Digit_Number;	
		}
		
}


int Pay_Type(char* Pay_contract_no,char* Pay_Confirm_Amount,char* Pay_mode,char* Pay_PAN_Number,char* Pay_Chq_DD_number,char* Pay_date,char* Pay_bank_name,char* Pay_Micr_number,char* Paytype_PickPayment_Flag)
{	
#if PRINTF
	printf("\nPay_contract_no:%s",Pay_contract_no);
	printf("\nPay_Confirm_Amount:%s",Pay_Confirm_Amount);
	printf("\nPay_mode:%s", Pay_mode);
	printf("\nPay_PAN_Number:%s",Pay_PAN_Number);
	printf("\nPay_Chq_DD_number:%s",Pay_Chq_DD_number);
	printf("\nPay_date:%s", Pay_date);
	printf("\nPay_bank_name:%s\n",Pay_bank_name);	
#endif

	int opt=0;
        MENU_T menu;
        int selItem  = 0;
        int acceptKbdEvents = TRUE;
        int maxEntries = 0;
        short scroll = 0;
	

	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       3;
        maxEntries                      =       menu.maxEntries;

	strcpy(menu.title,"Payee Type"); 
	strcpy(menu.menu[0],"1.Customer");//                   
        strcpy(menu.menu[1],"2.Relative");//
	strcpy(menu.menu[2],"3.Others");//
	

	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);


                switch(opt)
                {
		
		case CANCEL:	
				if(strcmp(Pay_mode,"1")==0)
				{					
					Cash_Chq_DD_Menu(Pay_contract_no,Pay_Confirm_Amount,Paytype_PickPayment_Flag);
				}
				else
				{
					Cash_Chq_DD_Menu(Pay_contract_no,Pay_Confirm_Amount,Paytype_PickPayment_Flag);
				}
				break;
		case ENTER:
			switch (selItem+1)
                    	{
			case 1: Place_of_Contact(Pay_contract_no,Pay_Confirm_Amount,Pay_mode,Pay_PAN_Number,Pay_Chq_DD_number,Pay_date,Pay_bank_name,"1",Pay_Micr_number,Paytype_PickPayment_Flag);
				break;
			case 2: Place_of_Contact(Pay_contract_no,Pay_Confirm_Amount,Pay_mode,Pay_PAN_Number,Pay_Chq_DD_number,Pay_date,Pay_bank_name,"2",Pay_Micr_number,Paytype_PickPayment_Flag);
				break;
			case 3:Place_of_Contact(Pay_contract_no,Pay_Confirm_Amount,Pay_mode,Pay_PAN_Number,Pay_Chq_DD_number,Pay_date,Pay_bank_name,"3",Pay_Micr_number,Paytype_PickPayment_Flag);
				break;	
			default : break;
			}		
		default : break;
		}//switch
	}//while

}




int Place_of_Contact(char* Place_contract_no,char* Place_Confirm_Amount,char* Place_mode,char* Place_PAN_Number,char* Place_Chq_DD_number,char* Place_date,char* Place_bank_name,char *Place_Payee_type,char* Place_Micr_Number,char* Place_PickPayment_Flag)
{

	MENU_T menu;
        int opt=0,ret;
        int selItem  = 0;
        int acceptKbdEvents;
        int maxEntries = 0;
        short scroll = 0;

#if PRINTF
	printf("\nPlace_contract_no:%s",Place_contract_no);
	printf("\nPlace_Confirm_Amount:%s",Place_Confirm_Amount);
	printf("\nPlace_mode:%s", Place_mode);
	printf("\nPlace_PAN_Number:%s",Place_PAN_Number);
	printf("\nPlace_date:%s",Place_date);
	printf("\nPlace_bank_name:%s", Place_bank_name);
	printf("\nPlace_Payee_type:%s",Place_Payee_type);
#endif
	
	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       3;
        maxEntries                      =      menu.maxEntries;

	strcpy(menu.title,"Place Contacted");  
      	strcpy(menu.menu[0],"1.Residence");//
	strcpy(menu.menu[1],"2.Office");
	strcpy(menu.menu[2],"3.Others");
		
	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case CANCEL:Pay_Type(Place_contract_no,Place_Confirm_Amount,Place_mode,Place_PAN_Number,Place_Chq_DD_number,Place_date,Place_bank_name,Place_Micr_Number,Place_PickPayment_Flag);
				break;

		case ENTER:
			switch (selItem+1)
                    	{				
			case 1: Mobile_Number(Place_contract_no,Place_Confirm_Amount,Place_mode,Place_PAN_Number,Place_Chq_DD_number,Place_date, Place_bank_name,Place_Payee_type,"1",Place_Micr_Number,Place_PickPayment_Flag);
				break;

			case 2: Mobile_Number(Place_contract_no,Place_Confirm_Amount,Place_mode,Place_PAN_Number,Place_Chq_DD_number,Place_date,Place_bank_name,Place_Payee_type,"2",Place_Micr_Number,Place_PickPayment_Flag);
				break;

			case 3:Mobile_Number(Place_contract_no,Place_Confirm_Amount,Place_mode,Place_PAN_Number,Place_Chq_DD_number,Place_date,Place_bank_name,Place_Payee_type,"3",Place_Micr_Number,Place_PickPayment_Flag);
				break;

			default : break;
			}			
		}
	}
}


/*******************************************************************/
int Mobile_Number(char* Mobile_contract_no,char* Mobile_Confirm_Amount,char* Mobile_mode,char* Mobile_PAN_Number,char* Mobile_Chq_DD_number,char* Mobile_date,char* Mobile_bank_name,char* Mobile_Payee_type,char* Mobile_contacted,char* Mobile_Micr_Number,char* Mobile_PickPayment_Flag)
{	

	char Payee_Mobile_Number_CC[20];
	int Payee_Mobile_Number_rval_cc=0,ReEnter_Mob_key=0;	
#if PRINTF	
	printf("\nMobile_contract_no:%s",Mobile_contract_no);
	printf("\nMobile_Confirm_Amount:%s",Mobile_Confirm_Amount);
	printf("\nMobile_mode:%s", Mobile_mode);
	printf("\nMobile_PAN_Number:%s",Mobile_PAN_Number);
	printf("\nMobile_Chq_DD_number:%s",Mobile_Chq_DD_number);
	printf("\nMobile_date:%s", Mobile_date);
	printf("\nMobile_bank_name:%s",Mobile_bank_name);
	printf("\nMobile_Payee_type:%s",Mobile_Payee_type);
	printf("\nMobile_contacted:%s",Mobile_contacted);
	printf("\nMobile_PickPayment_Flag:%s",Mobile_PickPayment_Flag);	
#endif		
Mobile_Number_Show:			
	lk_dispclr();
	lk_disptext(0,1,"     HDFC Bank CC",0);
	lk_disptext(1,2,"   Payment",0);
	lk_disphlight(1); 
	lk_disptext(2,1,"Enter Payee Mobile",0);
	lk_disptext(3,1,"Number",0);
	memset(Payee_Mobile_Number_CC,0,sizeof(Payee_Mobile_Number_CC));	
	Payee_Mobile_Number_rval_cc=0;			
	if((Payee_Mobile_Number_rval_cc=lk_getnumeric(5,1,Payee_Mobile_Number_CC,10,strlen(Payee_Mobile_Number_CC),0))<0) Place_of_Contact(Mobile_contract_no,Mobile_Confirm_Amount,Mobile_mode,Mobile_PAN_Number,Mobile_Chq_DD_number,Mobile_date,Mobile_bank_name,Mobile_Payee_type,Mobile_Micr_Number,Mobile_PickPayment_Flag);  
	Payee_Mobile_Number_CC[Payee_Mobile_Number_rval_cc]='\0';
	//printf("\nMobile_Number:%d",strlen(Payee_Mobile_Number_CC));
	//printf("\nMobile_Number:%s",Payee_Mobile_Number_CC);
	if((strlen(Payee_Mobile_Number_CC)==10)&&(atof(Payee_Mobile_Number_CC)>7000000000ULL))
	{
	if(strcmp(Mobile_mode,"1")==0)
	{
	Display_Payment_Details("Cash",Mobile_contract_no,Mobile_Confirm_Amount,Mobile_mode,Mobile_PAN_Number,Mobile_Chq_DD_number,Mobile_date,Mobile_bank_name,Mobile_Payee_type,Mobile_contacted,Payee_Mobile_Number_CC,Mobile_Micr_Number,Mobile_PickPayment_Flag);
	}
	else if(strcmp(Mobile_mode,"2")==0)
	{
	Display_Payment_Details("Cheque",Mobile_contract_no,Mobile_Confirm_Amount,Mobile_mode,Mobile_PAN_Number,Mobile_Chq_DD_number,Mobile_date,Mobile_bank_name,Mobile_Payee_type,Mobile_contacted,Payee_Mobile_Number_CC,Mobile_Micr_Number,Mobile_PickPayment_Flag);
	}
	else
	{
	Display_Payment_Details("DD",Mobile_contract_no,Mobile_Confirm_Amount,Mobile_mode,Mobile_PAN_Number,Mobile_Chq_DD_number,Mobile_date,Mobile_bank_name,Mobile_Payee_type,Mobile_contacted,Payee_Mobile_Number_CC,Mobile_Micr_Number,Mobile_PickPayment_Flag);
	}
	}
	else
	{
Ten_Digit:
	lk_dispclr();
	lk_disptext(2,0,"   Please ReEnter",0);
	lk_disptext(3,0,"   Valid 10 Digit",0);
	lk_disptext(4,0,"  Mobile Number",0);
	lk_disptext(5,0,"Press Enter to Cont.",0);
	ReEnter_Mob_key = lk_getkey();
	if(ReEnter_Mob_key==ENTER)
	{
	goto Mobile_Number_Show;	
	}	
	else goto Ten_Digit;	
	}
}

/********************************************************************************************************/
int Display_Payment_Details(char* Instrument_Display,char* CC_no_Display,char* Confirm_Amount_Display,char* Mode_Display,char* PAN_Number_Display,char* Chq_DD_number_Display,char* Date_Display,char* Bank_name_Display,char* Payee_type_Display,char* Contacted_Display,char* Mobile_number_Display,char* Micr_Number_Display,char* Display_PickPayment_Flag)
{
	int i,j=0;
	int Enter_Valid_key=0,Enter_key=0,retval=0,Failed_key=0,PIN_CHECK_F;
	char Collection_Details[50],Payment_Total_Details[50],Payee_Mobil_Details[50],contract_no_Details[50],Chq_DD_Details[50],Payee_Type_Details[10],PAN_Details[50],Chq_DD_Date_Details[50],Chq_DD_Bank_Name_Details[50],Balance_overdue[50],Pending_charges[50],Remarks_Display[50];
	char PAN_Display_show[10],Chq_DD_Display_show[25],Chq_DD_Date_Display_show[25],Chq_DD_Bank_Name_Display_show[25],decrypt_pin[25],Chq_DD_Date_Display_formate[25],Exec_password[20],Micr_Display_show[20],Micr_Display_Details[20];
	char updatepin[20],CC_Number[20],contract_Name_Details[25];
	resultset Exec_PIN,Noofpinallow,Get_contract_no,Get_pickup_contract_no;

	memset(&Exec_PIN,0,sizeof(Exec_PIN));
	memset(&Noofpinallow,0,sizeof(Noofpinallow));
	memset(&Get_contract_no,0,sizeof(Get_contract_no));
	memset(&Get_pickup_contract_no,0,sizeof(Get_pickup_contract_no));
	
/*********************************************************************************************************/
	memset(CC_Number,0,sizeof(CC_Number));
		for(i=3;i<20;i++)
		{
			CC_Number[j] = CC_no_Display[i];
			j++;
		}
		printf("\nCC_Number:%s",CC_Number);
/**********************************************************************************************************/
		

#if PRINTF  
	printf("\nInstrument_Display:%s",Instrument_Display);
	printf("\nConfirm_Amount_Display:%s",Confirm_Amount_Display);
	printf("\nMode_Display:%s",Mode_Display);
	printf("\nPAN_Number_Display:%s", PAN_Number_Display);
	printf("\nChq_DD_number_Display:%s",Chq_DD_number_Display);
	printf("\nDate_Display:%s",Date_Display);
	printf("\nBank_name_Display:%s", Bank_name_Display);
	printf("\nPayee_type_Display:%s",Payee_type_Display);
	printf("\nContacted_Display:%s",Contacted_Display);
	printf("\nMobile_number_Display:%s",Mobile_number_Display);
	printf("\nMicr_Number_Display:%s",Micr_Number_Display);
#endif

	memset(PAN_Display_show,0,sizeof(PAN_Display_show));
	if(strcmp(PAN_Number_Display,"0")==0) strcpy(PAN_Display_show,"N/A");
	else if(strcmp(PAN_Number_Display,"(null)")==0) strcpy(PAN_Display_show,"N/A");
	else strcpy(PAN_Display_show,PAN_Number_Display); 	
	memset(Chq_DD_Display_show,0,sizeof(Chq_DD_Display_show));
	if(strcmp(Chq_DD_number_Display,"0")==0) strcpy(Chq_DD_Display_show,"N/A");
	else strcpy(Chq_DD_Display_show,Chq_DD_number_Display);	
	memset(Micr_Display_show,0,sizeof(Micr_Display_show));
	if(strcmp(Micr_Number_Display,"0")==0) strcpy(Micr_Display_show,"N/A");
	else strcpy(Micr_Display_show,Micr_Number_Display); 	
	memset(Chq_DD_Date_Display_show,0,sizeof(Chq_DD_Date_Display_show));
	memset(Chq_DD_Date_Display_formate,0,sizeof(Chq_DD_number_Display));
	if(strcmp(Date_Display,"0")==0) strcpy(Chq_DD_Date_Display_show,"N/A");
	else
	{
	sprintf(Chq_DD_Date_Display_formate,"%c%c-%c%c-%c%c",Date_Display[0],Date_Display[1],Date_Display[2],Date_Display[3],Date_Display[4],Date_Display[5]);
	strcpy(Chq_DD_Date_Display_show,Chq_DD_Date_Display_formate);
	}	
	memset(Chq_DD_Bank_Name_Display_show,0,sizeof(Chq_DD_Bank_Name_Display_show));
	if(strcmp(Bank_name_Display,"0")==0) strcpy(Chq_DD_Bank_Name_Display_show,"N/A");
	else strcpy(Chq_DD_Bank_Name_Display_show,Bank_name_Display);	
	memset(Collection_Details,0,sizeof(Collection_Details));
	sprintf(Collection_Details,"Instrument:%s",Instrument_Display);
	memset(Payment_Total_Details,0,sizeof(Payment_Total_Details));
	sprintf(Payment_Total_Details,"Amt:Rs.%s/-",Confirm_Amount_Display);
	memset(Payee_Mobil_Details,0,sizeof(Payee_Mobil_Details));	
	sprintf(Payee_Mobil_Details,"Mob no.:%s",Mobile_number_Display);
	memset(contract_no_Details,0,sizeof(contract_no_Details));
	sprintf(contract_no_Details,"CCno:%s",CC_Number);	
	memset(Payee_Type_Details,0,sizeof(Payee_Type_Details));	
	sprintf(Payee_Type_Details,"Payee_Type.:%s",Payee_type_Display);	
	memset(PAN_Details,0,sizeof(PAN_Details));
	sprintf(PAN_Details,"PAN :%s",PAN_Display_show);	
	memset(Chq_DD_Details,0,sizeof(Chq_DD_Details));	
	sprintf(Chq_DD_Details,"Chq/DD No.:%s",Chq_DD_Display_show);
	memset(Chq_DD_Date_Details,0,sizeof(Chq_DD_Date_Details));	
	sprintf(Chq_DD_Date_Details,"Chq/DD Date:%s",Chq_DD_Date_Display_show);
	memset(Chq_DD_Bank_Name_Details,0,sizeof(Chq_DD_Bank_Name_Details));		
	sprintf(Chq_DD_Bank_Name_Details,"BankName:%s",Chq_DD_Bank_Name_Display_show);
	memset(Micr_Display_Details,0,sizeof(Micr_Display_Details));		
	sprintf(Micr_Display_Details,"Micr No.:%s",Micr_Display_show);


	if(strcmp(Display_PickPayment_Flag,"D")==0)
	{
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccc.db");	
	Get_contract_no= get_result("SELECT CUST_ID,BRANCH_CODE,ADDRESS_P_LMARK,BRANCH_CODE,AGENCE_CODE,AGENCE_NAME from ALLOCATION_DETAILS WHERE CARD_NUMBER='%s';",CC_no_Display);
	memset(contract_Name_Details,0,sizeof(contract_Name_Details));		
	snprintf(contract_Name_Details,21,"Name:%s",Get_contract_no.recordset[0][0]);	
	}
	else
	{
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfccc.db");	
	Get_pickup_contract_no= get_result("SELECT Filler1,AGENCE_NAME,AGENCE_CODE,Filler2 from PICKUP_DETAILS WHERE CARD_NUMBER='%s';",CC_no_Display);
	memset(contract_Name_Details,0,sizeof(contract_Name_Details));		
	snprintf(contract_Name_Details,21,"Name:%s",Get_pickup_contract_no.recordset[0][0]);	
	}

#if PRINTF
	printf("\ncontract_Name_Details:%s",contract_Name_Details);
#endif	


	MENU_T menu;
        int opt=0,ret;
        int selItem  = 0;
        int acceptKbdEvents;
        int maxEntries = 0;
        short scroll = 0;


	lk_dispclr(); 
       	menu.start                      =       0;
        menu.maxEntries                 =       11;
        maxEntries                      =      menu.maxEntries;

		
	strcpy(menu.title,"Payment Details");  
	strcpy(menu.menu[0],contract_no_Details);       
	strcpy(menu.menu[1],contract_Name_Details);            
       	strcpy(menu.menu[2],Collection_Details);
	strcpy(menu.menu[3],Payment_Total_Details);
	strcpy(menu.menu[4],Payee_Mobil_Details);
	strcpy(menu.menu[5],PAN_Details);
	strcpy(menu.menu[6],Chq_DD_Details);
	strcpy(menu.menu[7],Chq_DD_Date_Details);
	strcpy(menu.menu[8],Chq_DD_Bank_Name_Details);
	strcpy(menu.menu[9],Micr_Display_Details);		
	strcpy(menu.menu[10],"Print");

	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
	
		case CANCEL:
			     Mobile_Number(CC_no_Display,Confirm_Amount_Display,Mode_Display,PAN_Number_Display,Chq_DD_number_Display,Date_Display,Bank_name_Display,Payee_type_Display,Contacted_Display,Micr_Number_Display,Display_PickPayment_Flag);

		case ENTER:
			switch (selItem+1)
                    	{			
			case 11:
PIN:
			retval=0;
			lk_dispclr();
			lk_disptext(0,1,"     HDFC Bank CC",0);
			lk_disptext(1,2,"   Payment PIN",0);
			lk_disphlight(1); 		
			lk_disptext(2,3,"Enter PIN",0);
			memset(Exec_password,0,sizeof(Exec_password));
			retval=lk_getpassword(Exec_password,5,6);
			printf("retval=%d\n",retval);		
			Exec_password[retval]='\0';
			if((strlen(Exec_password)>0)&&(Check_Numeric(Exec_password)==1))
			{
				close_sqlite();
				open_sqlite("/mnt/jffs2/hdfcccoperational.db");
				Exec_PIN=get_result("SELECT Executive_id,Executive_Password FROM operationalstatus;");
				Noofpinallow = get_result("SELECT noofpin_allow FROM operationalstatus");
				memset(decrypt_pin,0,sizeof(decrypt_pin));
				sprintf(decrypt_pin,"%s",Decrypted_sqlite_DB(Exec_PIN.recordset[0][1]));		
				if((strcmp(decrypt_pin,Exec_password)==0))
				{
				memset(updatepin,0,sizeof(updatepin));
				sprintf(updatepin,"%d",(atoi(Noofpinallow.recordset[0][0])));			
				}
				else 
				{	
					PIN_CHECK_F=Wrong_Pin_Authentication();
					if(PIN_CHECK_F==0) goto PIN;
					else if(PIN_CHECK_F==1) Dra_Menu();
				}			
#if PRINTF
				printf("\nInstrument_Display:%s",Instrument_Display);
				printf("\nConfirm_Amount_Display:%s",Confirm_Amount_Display);
				printf("\nMode_Display:%s",Mode_Display);
				printf("\nPAN_Number_Display:%s", PAN_Number_Display);
				printf("\nChq_DD_number_Display:%s",Chq_DD_number_Display);
				printf("\nDate_Display:%s",Date_Display);
				printf("\nBank_name_Display:%s", Bank_name_Display);
				printf("\nPayee_type_Display:%s",Payee_type_Display);
				printf("\nContacted_Display:%s",Contacted_Display);
				printf("\nMobile_number_Display:%s",Mobile_number_Display);
#endif

Print_Display(Instrument_Display,Confirm_Amount_Display,Mode_Display,Mobile_number_Display,CC_no_Display,PAN_Number_Display,Chq_DD_number_Display,Date_Display,Bank_name_Display,Payee_type_Display,Contacted_Display,Micr_Number_Display,Display_PickPayment_Flag);
			}
			else if(retval==-1) Display_Payment_Details(Instrument_Display,CC_no_Display,Confirm_Amount_Display,Mode_Display,PAN_Number_Display,Chq_DD_number_Display,Date_Display,Bank_name_Display,Payee_type_Display,Contacted_Display,Mobile_number_Display,Micr_Number_Display,Display_PickPayment_Flag);
			else goto PIN;				
			default : break;			
			}			
		}
	}		
}
