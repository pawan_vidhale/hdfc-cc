/*******************************************************************
			   HDFC Bank CC
		            main.c
/*******************************************************************/
/* 
 * @author ftlit003 
**/
/******************************************************************/
#include <header.h>
#include "sqlite3.h"
#include <X6x8.h>
#include "logo.h"
FILE * pf;
/******************************************************************/
/******************************************************************/
int create_operationaldatabase()
{
	int ret;	
	
	ret = open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	/*****************************************/
	if(ret == 0)
	{
		printf("ERROR ...... Opening Operational Databasefile");
		return -1;
	}
	ret =  execute("SELECT * FROM operationalstatus");
	if(ret == 0)
    	{		
		ret = execute("CREATE TABLE if not exists operationalstatus(hwid VARCHAR(20) NOT NULL,		loginstatus VARCHAR(10) NOT NULL,simstatus VARCHAR(10) NOT NULL,Executive_Name VARCHAR(50) NOT NULL,Executive_id VARCHAR(20) NOT NULL,Executive_Password VARCHAR(20) NOT NULL,	ExecutiveStatus VARCHAR(10) NOT NULL,	DueDays VARCHAR(10) NOT NULL,DueHours VARCHAR(20) NOT NULL,WalletBalance VARCHAR(20) NOT NULL,	Dialno VARCHAR(20) NOT NULL,	Domain VARCHAR(20) NOT NULL,	SIM_Slot VARCHAR(2) NOT NULL,	GPRS_Setting_Flag VARCHAR(5) NOT NULL,	httpip VARCHAR(50) NOT NULL,	localhttpip VARCHAR(50) NOT NULL, ppp_options_usr VARCHAR(10) NOT NULL, ppp_options_pass VARCHAR(10) NOT NULL,Previous_date VARCHAR(20) NOT NULL, password_status  VARCHAR(20) NOT NULL,password_status_count VARCHAR(20) NOT NULL,logout VARCHAR(50) NOT NULL,	adminpassword VARCHAR(10) NOT NULL,	last_transaction_no VARCHAR(10) NOT NULL,Current_Software_Ver VARCHAR(50) NOT NULL,	New_Software_Ver VARCHAR(50) NOT NULL,	SIM_ICCID VARCHAR(25) NOT NULL,	SIM_ICCID_Server VARCHAR(25) NOT NULL,ServerDate VARCHAR(10) NOT NULL,ServerTime VARCHAR(10) NOT NULL,ChangePwdDay VARCHAR(5) NOT NULL,ReprintPerDay VARCHAR(10) NOT NULL,NoOfPin_Allow VARCHAR(10) NOT NULL,NoOfCancellationOTP VARCHAR(10) NOT NULL,log_date VARCHAR(10) NOT NULL,Update_transaction_no VARCHAR(20) NOT NULL);");
		if(ret == 0)
		{
			printf("ERROR ...... creating operational status table");
 			return -1;
		}
#if URL_NETMAGIC
		

//		ret = execute("INSERT INTO operationalstatus(hwid,loginstatus,simstatus,Executive_Name,Executive_id,Executive_Password,ExecutiveStatus,DueDays,DueHours,WalletBalance,Dialno,Domain,SIM_Slot,GPRS_Setting_Flag,httpip,localhttpip,ppp_options_usr,ppp_options_pass,Previous_date,password_status,password_status_count,logout,adminpassword,last_transaction_no,Current_Software_Ver,New_Software_Ver,SIM_ICCID,SIM_ICCID_Server,ServerDate,ServerTime,ChangePwdDay,ReprintPerDay,NoOfPin_Allow,NoOfCancellationOTP,log_date,Update_transaction_no) VALUES('12345678','0','1','Executive Name','HDFC@1234','123123','1','1','0','100000','*99***1#','internet','2','0','http://124.153.86.129:84','http://124.153.86.129:84','1234','1234','2013-11-30','1','3','0','40639295','1','1_0','1_0','0','0','0','0','0','0','5','0','2013-11-30','0');");

	ret = execute("INSERT INTO operationalstatus(hwid,loginstatus,simstatus,Executive_Name,Executive_id,Executive_Password,ExecutiveStatus,DueDays,DueHours,WalletBalance,Dialno,Domain,SIM_Slot,GPRS_Setting_Flag,httpip,localhttpip,ppp_options_usr,ppp_options_pass,Previous_date,password_status,password_status_count,logout,adminpassword,last_transaction_no,Current_Software_Ver,New_Software_Ver,SIM_ICCID,SIM_ICCID_Server,ServerDate,ServerTime,ChangePwdDay,ReprintPerDay,NoOfPin_Allow,NoOfCancellationOTP,log_date,Update_transaction_no) VALUES('12345678','0','1','Executive Name','HDFC@1234','123123','1','1','0','100000','*99***1#','gprsnac.com','2','0','https://192.168.250.10:4001','https://192.168.250.10:4001','void','void','2013-11-30','1','3','0','40639295','1','1_1','1_1','0','0','0','0','0','0','5','0','2013-11-30','0');");

//	ret = execute("INSERT INTO operationalstatus(hwid,loginstatus,simstatus,Executive_Name,Executive_id,Executive_Password,ExecutiveStatus,DueDays,DueHours,WalletBalance,Dialno,Domain,SIM_Slot,GPRS_Setting_Flag,httpip,localhttpip,ppp_options_usr,ppp_options_pass,Previous_date,password_status,password_status_count,logout,adminpassword,last_transaction_no,Current_Software_Ver,New_Software_Ver,SIM_ICCID,SIM_ICCID_Server,ServerDate,ServerTime,ChangePwdDay,ReprintPerDay,NoOfPin_Allow,NoOfCancellationOTP,log_date,Update_transaction_no) VALUES('12345678','0','1','Executive Name','HDFC@1234','123123','1','1','0','100000','*99***1#','gprsnac.com','2','0','https://192.168.250.10:4002','https://192.168.250.10:4002','void','void','2013-11-30','1','3','0','40639295','1','1_0','1_0','0','0','0','0','0','0','5','0','2013-11-30','0');");



		//ret = execute("INSERT INTO operationalstatus(hwid,loginstatus,simstatus,Executive_Name,Executive_id,Executive_Password,ExecutiveStatus,DueDays,DueHours,WalletBalance,Dialno,Domain,SIM_Slot,GPRS_Setting_Flag,httpip,localhttpip,ppp_options_usr,ppp_options_pass,Previous_date,password_status,password_status_count,logout,adminpassword,last_transaction_no,Current_Software_Ver,New_Software_Ver,SIM_ICCID,SIM_ICCID_Server,ServerDate,ServerTime,ChangePwdDay,ReprintPerDay,NoOfPin_Allow,NoOfCancellationOTP,log_date,Update_transaction_no) VALUES('12345678','0','1','Executive Name','PAWA180982','123123','1','1','0','100000','*99***1#','gprsnac.com','2','0','https://192.168.250.10:4001','https://192.168.250.10:4001','1234','1234','2013-11-30','1','3','0','40639295','1','1.0','1.0','0','0','0','0','0','0','5','0','2013-11-30','0');");

#endif
		if(ret == 0)
		{
		    	printf("ERROR ...... Inserting operational status table");
		    	return -1;
		}
	}
	/*****************************************/
	ret =  execute("SELECT * FROM CancellationOTPs;");
	if(ret == 0)
    	{
		ret = execute("CREATE TABLE CancellationOTPs(OTP_count varchar(10) NOT NULL,OTP varchar(20) NOT NULL);");
		if(ret == 0)
		{
			printf("ERROR ...... creating CancellationOTPs table");
 			return -1;
		} 
	}
	/*****************************************/	
	if(ret == 0)
	{
		printf("ERROR ...... Opening Operational Databasefile");
		return -1;
	}
	ret =  execute("SELECT * FROM Action_Status");
	if(ret == 0)
    	{		
		ret = execute("CREATE TABLE if not exists Action_Status(Action_Code VARCHAR(20) NOT NULL,		Action_Disc VARCHAR(20) NOT NULL,Action_Number VARCHAR(4) NOT NULL);");
		if(ret == 0)
		{
			printf("ERROR ...... creating operational status table");
 			return -1;
		}

	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('APT','Appointment','1');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('BKS','BrokenSettlement','2');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('CAN','Cancellation','3');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('CIH','Cash In Hand','4');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('CLBK','Call Back','5');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('DEC','Deceased Customer','6');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('DISP','Dispute','7');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('FVT','Field Visit','8');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('ICC','Incoming Call','9');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('MSG','Left Message','10');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('NCID','New Contact Number Identified','11');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('PTP','Promise To Pay','12');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('RNR','Ringing No Response','13');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('RTP','Refuse To Pay','14');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('SKIP','Non Contactable Customer','15');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('SKTR','Skip Traced','16');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('STD','Fully Settled','17');");
	ret = execute("INSERT INTO Action_Status(Action_Code,Action_Disc,Action_Number) VALUES('TEL','Telecall','18');");
		if(ret == 0)
		{
		    	printf("ERROR ...... Inserting operational status table");
		    	return -1;
		}
	}
	close_sqlite();
	return 1;
}


/******************************************************************/
int create_customerdatabase()
{
	int ret;
	resultset pay_type_details;	
	
	ret = open_sqlite("/mnt/jffs2/hdfccc.db");
	if(ret == 0)
	{
		printf("ERROR ...... Opening Customer info Databasefile");
		return -1;
	}
	ret =  execute("SELECT * FROM ALLOCATION_DETAILS;");
	if(ret == 0)
    	{
		ret = execute("CREATE TABLE ALLOCATION_DETAILS( BRANCH_CODE varchar(19),CARD_NUMBER varchar(19) NOT NULL,	ALT_ACCOUNT_NO varchar(19) NOT NULL,	BILL_CYCLE varchar(2) NOT NULL,	COUNT_PAY_CURRCYC varchar(4) NOT NULL,	DELINQENCY_STRING varchar(24) NOT NULL,	CUST_ID varchar(40) NOT NULL,	ADDRESS_M1 varchar(40) NOT NULL,	ADDRESS_M2 varchar(40) NOT NULL,	ADDRESS_M3 varchar(40) NOT NULL,	ADDRESS_M4 varchar(40) NOT NULL,	M_ADDRESS_CITY varchar(22) NOT NULL,	M_ADDRESS_STATE varchar(3) NOT NULL,	M_ADDRESS_ZIP varchar(10) NOT NULL,	M_ADDRESS_LM varchar(30) NOT NULL,	HOME_PHONE varchar(15) NOT NULL,	MOBILE_NO varchar(15) NOT NULL,	GENDER varchar(1) NOT NULL,	SS_NAME varchar(15) NOT NULL,	EMP_CODE varchar(15) NOT NULL,	PRS_EMAIL_ID varchar(55) NOT NULL,	OFF_EMAIL_ID varchar(55) NOT NULL,	EMP_NAME varchar(30) NOT NULL,	DESIGNATION varchar(30) NOT NULL,	BAND varchar(4) NOT NULL,	WORK_PHONE varchar(15) NOT NULL,	WORK_PHONE_E varchar(9) NOT NULL,	CURRENT_BL varchar(13) NOT NULL,	MEMO_BL varchar(13) NOT NULL,	TOTAL_PAST_DUE varchar(13) NOT NULL,	TOTAL_CURR_DUE varchar(13) NOT NULL,	TOTAL_AMT_DUE varchar(13) NOT NULL,	OVERLIMIT_AMT varchar(13) NOT NULL,	DATE_L_DELINQUENCY datetime(10) NOT NULL,	DATE_L_R_CHEQUE datetime(10) NOT NULL,	NO_OF_AUTOP_RET varchar(4) NOT NULL,	L_SMT_BNG_BAL varchar(13) NOT NULL,	L_SMT_ENG_BAL varchar(13) NOT NULL,	L_SMT_PAST_DUE varchar(13) NOT NULL,	L_SMT_TMT_PAY_DUE varchar(15) NOT NULL,	NO_OF_AUTO_RET_LSTAT varchar(4) NOT NULL,	AT_RET_AMT_LSTAT varchar(13) NOT NULL,	LSTAT_CURR_DUE varchar(15) NOT NULL,	DATE_OF_LPAY datetime(10) NOT NULL,	AMT_OF_LPAY varchar(13),	DATE_OF_LPUR datetime(10) NOT NULL,	AMT_LPUR varchar(13) NOT NULL,	DATE_L_CYCLE datetime(10) NOT NULL,	LTD_RETAIL_PURAMT varchar(15) NOT NULL,	LTD_RETAIL_RETAMT varchar(15) NOT NULL,	LTD_CASH_TRS_AMT varchar(15) NOT NULL,	ADDRESS_L1 varchar(40) NOT NULL,	ADDRESS_L2 varchar(40) NOT NULL, ADDRESS_L3 varchar(40) NOT NULL, ADDRESS_L4 varchar(40) NOT NULL, ADDRESS_ALT_CITY varchar(30) NOT NULL, ADDRESS_ALT_STATE varchar(3) NOT NULL, ADDRESS_ALT_ZIP varchar(9) NOT NULL, PAN_NO varchar(10) NOT NULL,  M_ADDRESS_TIME_CC varchar(120) NOT NULL,	ADDRESS_P1 varchar(40) NOT NULL,	ADDRESS_P2 varchar(40) NOT NULL,	ADDRESS_P3 varchar(40) NOT NULL,	ADDRESS_P_CITY varchar(30) NOT NULL,	ADDRESS_P_ZIP varchar(10) NOT NULL,	ADDRESS_P_LMARK varchar(30) NOT NULL,	ADDRESS_P_TELPNO varchar(40) NOT NULL, CPY_CAT_CODE varchar(3) NOT NULL,	OCC_TYPE varchar(2) NOT NULL, REGION_CODE varchar(3) NOT NULL, LOAN_BAL varchar(13) NOT NULL, DATE_OF_ADDRESSCHANGE datetime(10) NOT NULL, DATE_OF_LSTAT datetime(10) NOT NULL,  DATE_OF_NSTAT datetime(10) NOT NULL, LOADED_BAL varchar(17) NOT NULL, BUCKET varchar(1) NOT NULL, BUCKET_DEST varchar(10) NOT NULL, EMP_ADDRESS1 varchar(40) NOT NULL, EMP_ADDRESS2 varchar(40) NOT NULL, EMP_ADDRESS3 varchar(40) NOT NULL, EMP_ADDRESS4 varchar(40) NOT NULL, EMP_ADDRESS_PIN varchar(10) NOT NULL, EMP_ADDRESS_LM varchar(30) NOT NULL, EMP_ADDRESS_CITY varchar(23) NOT NULL, EMP_TELE_NO varchar(10) NOT NULL, ADDON_CARD1 varchar(26) NOT NULL,  ADDON_CARD2 varchar(26) NOT NULL, ADDON_CARD3 varchar(30) NOT NULL, ADDON_CARD4 varchar(30) NOT NULL, DATE_ACC_CHARGED_OFF datetime(10) NOT NULL, EXTRA1 NOT NULL,AGENCE_NAME varchar(30) NOT NULL,AGENCE_CODE varchar(30) NOT NULL,filler1 varchar(30) NOT NULL,filler2 varchar(30) NOT NULL,filler3 varchar(30) NOT NULL,filler4 varchar(30) NOT NULL);");
		if(ret == 0)
		{
			printf("ERROR ...... creating ALLOCATION_DETAILS table");
 			return -1;
		} 
	}
	ret =  execute("SELECT * FROM PICKUP_DETAILS;");
	if(ret == 0)
    	{
		ret = execute("CREATE TABLE PICKUP_DETAILS(CARD_NUMBER varchar(50),PICKUP_ID varchar(20) NOT NULL,PICKUP_AMOUNT varchar(20) NOT NULL,PICKUP_DATE varchar(20) NOT NULL,PICKUP_TIME varchar(150) NOT NULL,PICKUP_ADDRESS varchar(1000) NOT NULL,PICKUP_MODE varchar(40) NOT NULL,PICKUP_AGENCY_CODE varchar(40) NOT NULL,	CUSTOMER_CONTACT_NO varchar(40) NOT NULL,PLACE_CONTACTED varchar(40) NOT NULL,PERSON_CONTACTED varchar(40) NOT NULL,CONTACT_MODE varchar(22) NOT NULL,	CONTACTED_BY varchar(50) NOT NULL,CONTACT_DATE varchar(20) NOT NULL,	ACTION_DATE varchar(30) NOT NULL,ACTION_TIME varchar(15) NOT NULL,ACTION_CODE varchar(100) NOT NULL,REMARKS varchar(510) NOT NULL,USER_ID varchar(50) NOT NULL,MAKE_DATE varchar(15) NOT NULL,AGENCE_CODE  varchar(100) NOT NULL,AGENCE_NAME varchar(100) NOT NULL,HUB varchar(55) NOT NULL,BRANCHID varchar(55) NOT NULL,CLMCODE varchar(30) NOT NULL,CLMID varchar(60) NOT NULL,Slab varchar(5) NOT NULL,Filler1 varchar(100) NOT NULL,Filler2 varchar(100) NOT NULL,Filler3 varchar(100) NOT NULL,Filler4 varchar(100) NOT NULL);");
		if(ret == 0)
		{
			printf("ERROR ...... creating ALLOCATION_DETAILS table");
 			return -1;
		} 
	}
	close_sqlite();
	return 1;
}


/****************************************************************/
int create_transactiondatabase()
{
	int ret;

	ret = open_sqlite("/mnt/jffs2/hdfccctrans.db");	
	if(ret == 0)
	{
		printf("ERROR ...... Opening transactiondetails Databasefile");
		return -1;
	}
	ret =  execute("SELECT * FROM transactiondetails;");
	if(ret == 0)
    	{
	ret = execute("CREATE TABLE transactiondetails(Executive_Name varchar(20) NOT NULL,Executive_Code varchar(10) NOT NULL,Loan_No varchar(50) NOT NULL,Transaction_ID varchar(25) NOT NULL,Total_Amt varchar(50) NOT NULL,Payee_Type varchar(1) NOT NULL,Place_Contacted varchar(1) NOT NULL,Instument_Type varchar(10) NOT NULL,Bank_Name varchar(20) NOT NULL,chq_DD_No varchar(12) NOT NULL,	chq_DD_Date varchar(10) NOT NULL,PAN_No varchar(10) NOT NULL,Payee_Mobile_No varchar(10) NOT NULL,Transaction_Date varchar(10) NOT NULL,Transaction_Time varchar(8) NOT NULL,Micr_number varchar(10) NOT NULL,Trn_cancel varchar(7) NOT NULL,Reprint_count varchar(5) NOT NULL,Customer_no varchar(10) NOT NULL,Concatenate_no varchar(50) NOT NULL);");
	if(ret == 0)
		{
			printf("ERROR ...... creating transactiondetails table");
 			return -1;
		} 
	}
	/*****************************************/
	ret =  execute("SELECT * FROM unuploadedtransactiondetails;");
	if(ret == 0)
    	{
		ret = execute("CREATE TABLE unuploadedtransactiondetails(Trans_Counter integer PRIMARY KEY autoincrement NOT NULL,Transaction_ID varchar(25) NOT NULL,Transaction_Details VARCHAR(2000) NOT NULL,Status VARCHAR(5) NOT NULL)");
		if(ret == 0)
		{
			printf("ERROR ...... creating unuploadedtransactiondetails table");
 			return -1;
		} 
	}
	/*****************************************/
	ret =  execute("SELECT * FROM unuploadedfollowupdetails;");
	if(ret == 0)
    	{
		ret = execute("CREATE TABLE unuploadedfollowupdetails(Counter integer PRIMARY KEY autoincrement NOT NULL,followupdetails varchar(2000) NOT NULL,FollowUp_Status varchar(5) NOT NULL);");
		if(ret == 0)
		{
			printf("ERROR ...... creating unuploadedfollowupdetails table");
 			return -1;
		} 
	}
	/*****************************************/
	ret =  execute("SELECT * FROM unuploaded_BOD_EOD_details;");
	if(ret == 0)
    	{
	ret = execute("CREATE TABLE unuploaded_BOD_EOD_details(Counter integer PRIMARY KEY autoincrement NOT NULL,BOD_EOD_details varchar(1000) NOT NULL,BOD_EOD_Status varchar(5) NOT NULL);");
	if(ret == 0)
		{
			printf("ERROR ...... creating unuploaded_BOD_EOD_details table");
 			return -1;
		} 
	}
	/*****************************************/
	ret =  execute("SELECT * FROM lockpassword_details;");
	if(ret == 0)
    	{
	ret = execute("CREATE TABLE lockpassword_details(Counter integer PRIMARY KEY autoincrement NOT NULL,lockpassword_details varchar(1000) NOT NULL,lockpassword_Status varchar(5) NOT NULL);");
	if(ret == 0)
		{
			printf("ERROR ...... creating lockpassword_details table");
 			return -1;
		} 
	}
	/*****************************************/
	ret =  execute("SELECT * FROM lasttransactiondetails;");
	if(ret == 0)
    	{
	ret = execute("CREATE TABLE lasttransactiondetails(Executive_Name varchar(20) NOT NULL,Executive_Code varchar(10) NOT NULL,Loan_No varchar(50) NOT NULL,Transaction_ID varchar(25) NOT NULL,Total_Amt varchar(50) NOT NULL,Payee_Type varchar(1) NOT NULL,Place_Contacted varchar(1) NOT NULL,Instument_Type varchar(10) NOT NULL,Bank_Name varchar(20) NOT NULL,chq_DD_No varchar(12) NOT NULL,	chq_DD_Date varchar(10) NOT NULL,PAN_No varchar(10) NOT NULL,Payee_Mobile_No varchar(10) NOT NULL,Micr_number varchar(10) NOT NULL,Transaction_Date varchar(10) NOT NULL,Transaction_Time varchar(8) NOT NULL,Trn_cancel varchar(7) NOT NULL,Reprint_count varchar(5) NOT NULL);");
	if(ret == 0)
		{
			printf("ERROR ...... creating transactiondetails table");
 			return -1;
		} 
	}
	close_sqlite();
	return 1;

}

/****************************************************************/
int Day_transactiondatabase()
{
	int ret_day;

	ret_day = open_sqlite("/mnt/jffs2/hdfccctransday.db");	
	if(ret_day == 0)
	{
		printf("ERROR ...... Opening transactiondetails Databasefile");
		return -1;
	}
	ret_day =  execute("SELECT * FROM transactionday;");
	if(ret_day == 0)
    	{
	ret_day = execute("CREATE TABLE transactionday(Executive_Name varchar(20) NOT NULL,Executive_Code varchar(10) NOT NULL,Loan_No varchar(50) NOT NULL,Transaction_ID varchar(25) NOT NULL);");
	if(ret_day == 0)
		{
			printf("ERROR ...... creating transactiondetails table");
 			return -1;
		} 
	}
}


/****************************************************************/
int hwid()
{
     int key=0;
     char hwid[20]="";

     key=lk_gethwid(hwid);
     hwid[8]='\0';
     if(key==-1)   
     {
        lk_dispclr();
        lk_disptext(2,8,"HWID",0);
        lk_disptext(3,4,"NOT AVAILABLE",0);
	sleep(5);
	close_sqlite();
	lk_close();
	system("cd");
	system("poweroff"); 	
	sleep(10);        
    }
    lk_dispclr();
    lk_disptext(1,8,"HWID",1);
    lk_disptext(3,6,hwid,1);
    lk_disptext(5,1,"  Please Wait...",0); 
    close_sqlite();			
    open_sqlite("/mnt/jffs2/hdfcccoperational.db");
    execute("UPDATE operationalstatus SET hwid='%s';",hwid);
    close_sqlite();
    sleep(2);		
    return 0;
}


/******************************************************************/
int main()
{
	int gprs_return=0,certif_return=0,upgrade_key;
	unsigned int dir;
	char dash = '_';
	char New_Version[20],Change_Version[20];
	resultset SIM_Card_table,Version_Diff,unuploaded_trans,unuploaded_followUp,unuploaded_BOD_EOD;
	FILE *pf;
	
	memset(&SIM_Card_table,0,sizeof(SIM_Card_table));
	memset(&Version_Diff,0,sizeof(Version_Diff));
	memset(&unuploaded_trans,0,sizeof(unuploaded_trans));
	memset(&unuploaded_followUp,0,sizeof(unuploaded_followUp));
	memset(&unuploaded_BOD_EOD,0,sizeof(unuploaded_BOD_EOD));	

	lk_open();
        lk_dispfont(&(X6x8_bits[0]),6);

	dir=chdir("/mnt/jffs2/");
	printf("\ndir:%d",dir);
	if(dir==0) printf("jffs2 is mounted\n");
	else 
	{
		printf("jffs2 is not mounted\n");
		system("cd");
		system("pwd");
		system("cd /mnt/");
		system("pwd");
		system("mkdir jffs2");
		system("mount -t jffs2 /dev/mtdblock5 /mnt/jffs2");
	}	
	printf("\nVersion Is HDFC_CC PRODUCTION 1.0\n");
	if(stat("/mnt/jffs2/hdfcccoperational.db",&st)==0) hwid();		
	else
	{
	close_sqlite();
	create_operationaldatabase();
	create_customerdatabase();
	create_transactiondatabase();		
	hwid();		
	}
	
	if(stat("/var/log/apn_trans.txt",&st)==0) Read_file_tran();
	if(stat("/var/log/trans_date.txt",&st)==0) Read_file_date_tran();		
	//rtc_ckeck();	
	//Dra_Menu();
	chk_rtc();
	gprsconnection=0;	
#if PRINTF 
	printf("\nDate:%d/%d/%d",(curt.tm_mday),(curt.tm_mon+1),(curt.tm_year+1900));
#endif
	lk_dispclr();
	lk_disptext(0,1,"     HDFC Bank",1);
	lk_disptext(2,1,"   Credit Card(CC)",1);
   	lk_disptext(4,1,"      Loading",0);
	lk_disptext(5,1,"  Please Wait...",0); 

gprs_start:
	close_sqlite();	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	SIM_Card_table=get_result("SELECT SIM_Slot,GPRS_Setting_Flag from operationalstatus;");
	if(atoi(SIM_Card_table.recordset[0][0])==2&&atoi(SIM_Card_table.recordset[0][1])==0)
	{
		gprs_return=gsm_init();										
		printf("\ngprs_return: %d",gprs_return);	
		if(gprs_return==0)
		system("rm /mnt/jffs2/public.cer");
		certif_return=Download_Certificate();
		printf("\ncertif_return: %d",certif_return);
		if(certif_return==0)
		{
		Version_SIMNo_Request();
		After_main();
		}
		else goto gprs_start;
		After_main();
	}
	else
	{
	gprs_return=gsm_init();		
	if(gprs_return==0) 	
	{		
		certif_return=Download_Certificate();
		printf("\ncertif_return: %d",certif_return);
		if(certif_return==0)
		{
		Unuploaded_Tr_Fl();
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfccctrans.db");
		unuploaded_trans=get_result("SELECT * FROM unuploadedtransactiondetails;");
		unuploaded_followUp=get_result("SELECT * FROM unuploadedfollowupdetails;");
		unuploaded_BOD_EOD=get_result("SELECT * FROM unuploaded_BOD_EOD_details;");
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		Version_Diff=get_result("Select httpip,loginstatus,hwid,New_Software_Ver,Current_Software_Ver From operationalstatus;");

		memset(New_Version,0,sizeof(New_Version));
		sprintf(Change_Version,"%s",Version_Diff.recordset[0][3]);
		sprintf(New_Version,"%c%c%c",Change_Version[0],dash,Change_Version[2]);
		printf("\nNew version%s",New_Version);
	#if PRINTF
		printf("New Version at software update%s",Version_Diff.recordset[0][3]);
	#endif		
		
		if((unuploaded_trans.rows==0)&&(unuploaded_followUp.rows==0)&&(unuploaded_BOD_EOD.rows==0)&&(Version_Diff.rows>0)&&(gprsconnection==1)&&strcmp(New_Version,Version_Diff.recordset[0][4])!=0&&(atoi(Version_Diff.recordset[0][1])==0))
		{
			if(Version_Diff.rows>0)
			{
Version_upgrade:
			lk_dispclr();
			lk_disptext(0,0,"   A New Version is",0);
			lk_disptext(1,0,"      Available",0);
			lk_disptext(2,8,Version_Diff.recordset[0][3],1);
			lk_disptext(4,0,"Do u wish to Upgrade?",0);
			lk_disptext(5,0,"   1.Yes    2.No",0);
			upgrade_key = lk_getkey();
			if(upgrade_key==1)
			{
			Soft_Update();		
			}
			else if(upgrade_key==2)
			{
			Download_Certificate();	
			After_main();		
			}
			else goto Version_upgrade;
			}
			else After_main();
		}
		else After_main();
		}
		else After_main();
	}
	else
	{
	Download_Certificate();
	After_main();
	}			
	}

}

/****************************************************************/
int After_main()
{
	int result;
	resultset Disp_appl;
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Disp_appl=get_result("SELECT localhttpip From operationalstatus;");
	lk_buff_clear();				
	chk_rtc();
	rtc_ckeck_year();	
	lk_dispclr();  
	lk_bkl_timeout(20); 	
	lk_disptext(0,1,"     HDFC Bank",1);
	lk_disptext(2,1,"   Credit Card(CC)",0);
	if(strcmp(Disp_appl.recordset[0][0],"NDRA")==0)	
	{
	lk_disptext(3,1,"       NON-DRA_vR1.0",0);
	}
	else
	{	
	lk_disptext(3,1,"           DRA_vR1.0",0);
	}
	lk_disptext(4,1,"1-Login   2-Admin",0);
	lk_disptext(5,1,"3-Sync    4-Shutdown",0);
				    
	while(1)
    	{
		result = lk_getkey();
		if((result==1)||(result==2)||(result==3)||(result==4)) break;
	}
	if(result==1)
	{			
	Dra_login();
	}
	else if(result==2)
	{
	admin_pass();
	}
	else if(result==3)
	{	
		lk_dispclr(); 
		lk_disptext(2,1,"  Synchronizing...",1);  
		Synchronize();
		After_main();
	}
	else if(result==4)
	{
		close_sqlite();	
		lk_dispclr();
		lk_disptext(3,2,"Shutdown...",1);
		ppp_close();
		lk_close();   
		system("poweroff");
		sleep(13);
	}
	else After_main();
}


/****************************************************************/
int Unuploaded_Tr_Fl()
{
	resultset unuploaded_trans,unuploaded_followUp,unuploaded_BOD_EOD,unuploaded_lock_password,Login_Diff;
	resultset unuploaded_trans1,unuploaded_followUp1,unuploaded_BOD_EOD1;	

	close_sqlite();	
	create_transactiondatabase();
	if(gprsconnection==1)
	{
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Login_Diff=get_result("Select loginstatus From operationalstatus;");
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfccctrans.db");
	unuploaded_trans=get_result("SELECT * FROM unuploadedtransactiondetails;");
	unuploaded_followUp=get_result("SELECT * FROM unuploadedfollowupdetails;");
	unuploaded_BOD_EOD=get_result("SELECT * FROM unuploaded_BOD_EOD_details;");
	unuploaded_lock_password=get_result("SELECT * FROM lockpassword_details;");
	if((unuploaded_trans.rows>0)||(unuploaded_followUp.rows>0)||(unuploaded_BOD_EOD.rows>0)||(unuploaded_lock_password.rows>0))
	{
		Upload_Transaction_Request_For_Thrd();
		Upload_FollowUp_Request_For_Thrd();
		Upload_BOD_EOD_Request_Trid();
		//lock_password_Request_Trid();
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfccctrans.db"); 
		execute("Delete FROM unuploadedtransactiondetails WHERE Status='1';");
		execute("Delete FROM unuploadedfollowupdetails WHERE FollowUp_Status='1';");		
		unuploaded_trans1=get_result("SELECT * FROM unuploadedtransactiondetails;");
		unuploaded_followUp1=get_result("SELECT * FROM unuploadedfollowupdetails;");
		unuploaded_BOD_EOD1=get_result("SELECT * FROM unuploaded_BOD_EOD_details;");				
		if((unuploaded_trans1.rows==0)&&(unuploaded_followUp1.rows==0)&&(unuploaded_BOD_EOD1.rows==0)&&atoi(Login_Diff.recordset[0][0])==0)
		{
			close_sqlite();			
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			execute("UPDATE operationalstatus SET loginstatus='0';");
		}
		else
		{
			close_sqlite();			
			open_sqlite("/mnt/jffs2/hdfcccoperational.db");
			execute("UPDATE operationalstatus SET loginstatus='1';");
		}
	}	
	else
	{
		close_sqlite();			
		open_sqlite("/mnt/jffs2/hdfcccoperational.db");
		execute("UPDATE operationalstatus SET loginstatus='0';");
	}
	}
	else
	{
		Download_Certificate();	
		After_main();
	}

return SUCCESS;
}


