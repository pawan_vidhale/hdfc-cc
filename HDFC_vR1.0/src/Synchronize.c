/*******************************************************************/
//		      HDFC Bank CC
//		      Synchronize.c
/*******************************************************************/
#include <header.h>
#include <curl.h>
/*******************************************************************/

int Synchronize()
{
	char dec_executive_syn[25];
	resultset Exec_ID_Sync;

	memset(&Exec_ID_Sync,0,sizeof(Exec_ID_Sync));
	Png_Status_Reconnect();
	Upload_Transaction_Request_For_Thrd();	
	Upload_FollowUp_Request_For_Thrd();
	//lock_password_Request_Trid();
		
	Executive_Check();
	close_sqlite();			
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	Exec_ID_Sync=get_result("Select Executive_id from operationalstatus;");
	memset(dec_executive_syn,0,sizeof(dec_executive_syn));
	sprintf(dec_executive_syn,"%s",Decrypted_sqlite_DB(Exec_ID_Sync.recordset[0][0]));
	Customer_Info_Sync(dec_executive_syn);	
	return 0;
}
