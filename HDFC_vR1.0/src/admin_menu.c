/*******************************************************************
			   HDFC Bank CC
		         admin_menu.c
/*******************************************************************/
#include <header.h>

struct tm curt;
struct tm intim;
struct stat st;	
/*******************************************************************/
int admin_pass()
{
	MENU_T menu;
        int opt=0,ret;
        int selItem  = 0;
        int acceptKbdEvents;
        int maxEntries = 0;
        short scroll = 0;

	char HWID_Show[20];
	resultset hwid_res;

	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	hwid_res=get_result("SELECT hwid from operationalstatus;");
	memset(HWID_Show,0,sizeof(HWID_Show));
	sprintf(HWID_Show,"HWID:%s",hwid_res.recordset[0][0]);

	lk_dispclr(); 
        menu.start                      =       0;
        menu.maxEntries                 =       2;
        maxEntries                      =      menu.maxEntries;

	strcpy(menu.title," Admin Details"); 
	strcpy(menu.menu[0],HWID_Show);                  
        strcpy(menu.menu[1],"Admin menu");
		
	while(1)
        {    
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);

                switch(opt)
                {
		case CANCEL:	After_main();
				break;

		case ENTER:
			switch (selItem+1 )
                    	{
			case 1: admin_pass();
				break;
			case 2: admin_cc_pass();
				break;			
			}
			default : break;
		}
	}
}


/*******************************************************************/
int admin_cc_pass()
{

	resultset table;
	char passwordlog[25],decrypt_pass[20];
	int retval,Wrong_key;
	    
	memset(passwordlog,0,sizeof(passwordlog));
	close_sqlite();
	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	table=get_result("SELECT adminpassword FROM  operationalstatus");
	close_sqlite();

	lk_dispclr();
	lk_disptext(0,1,"   HDFC Bank CC",0);
	lk_disptext(1,0,"Admin Password",0);
	lk_disphlight(1);
	memset(passwordlog,0,sizeof(passwordlog));
	lk_disptext(3,0,"Enter Admin Password",0);
	retval=lk_getpassword(passwordlog,4,10);
        printf("retval=%d\n",retval);		
	passwordlog[retval]='\0';
	printf("\npass:%s",passwordlog);
	memset(decrypt_pass,0,sizeof(decrypt_pass));
	sprintf(decrypt_pass,"%s",table.recordset[0][0]);
	if(strcmp(passwordlog,decrypt_pass)==0)
	{
		admin_menu();	
	}
	else if(retval==-1)
	{
		After_main();
	}
	else
	{
Wrong_Password:
		lk_dispclr();
		lk_disptext(2,2,"Wrong Password",1);	    	
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Wrong_key = lk_getkey();
		if(Wrong_key==ENTER)
		{
		admin_pass();
		}
		else goto Wrong_Password;
	}
}

/*******************************************************************/
int admin_menu()
{
	MENU_T menu;
        int opt=0,ret;
        int selItem  = 0;
        int acceptKbdEvents;
        int maxEntries = 0;
        short scroll = 0;
	int Rate_Details_Return;

	while(1)
	{
	lk_dispclr(); 
	menu.start                      =       0;
        menu.maxEntries                 =       3;
        maxEntries                      =      menu.maxEntries;
        strcpy(menu.title, "ADMIN MENU");        
	
	strcpy(menu.menu[0],"Set Date & Time"); 	
	strcpy(menu.menu[1],"Printer Test");
	strcpy(menu.menu[2],"Comm Setting");	
	while(1)
        {	
              opt = scroll_menu(&menu,&selItem,acceptKbdEvents);
        	    
                if(opt==CANCEL) admin_pass();

		switch(opt)
                {
                    case CANCEL: After_main();
                    case ENTER:
                    switch (selItem+1 )
                    {	       		
			case 1: admin_set_date_time();   	break;
           		case 2: Printer_test();   		break;
			case 3:	communication_setting();	break;
		    }
		    default:   break;
		}
	 }
	}
}

/*******************************************************************/
int communication_setting()
{
	int retval=0,Wrong_key_Comm=0,retval_comm=0;
	char http_ip[100];
	char http_ip_dest1[100],passwordlog_comm[25];
	resultset gprs_setting;

	memset(&gprs_setting,0,sizeof(gprs_setting));
	
	lk_dispclr();
	lk_disptext(0,1,"   HDFC Bank CC",0);
	lk_disptext(1,0,"Admin Password",0);
	lk_disphlight(1);
	memset(passwordlog_comm,0,sizeof(passwordlog_comm));
	lk_disptext(3,0,"Enter Password",0);
	retval_comm=lk_getpassword(passwordlog_comm,4,10);
	if(strcmp(passwordlog_comm,"4063106")==0)
	{
	close_sqlite();
    	open_sqlite("/mnt/jffs2/hdfcccoperational.db");
	gprs_setting=get_result("SELECT httpip FROM operationalstatus;");
	memset(http_ip_dest1,0,sizeof(http_ip_dest1));
	sprintf(http_ip_dest1,"%s",gprs_setting.recordset[0][0]);		
	lk_dispclr();	
	lk_disptext(2,0,"Enter http Addrs",0);//
	if((retval=lk_getalpha(5,1,http_ip_dest1,50,strlen(http_ip_dest1),0)<0))
	http_ip_dest1[retval]='\0';
        if(strlen(http_ip_dest1)>0)
	{
	printf("set2");	
	execute("UPDATE operationalstatus SET httpip='%s';",http_ip_dest1);
	admin_menu();
	}
	else
	{
	admin_menu();
	}
	}
	else if(retval_comm==-1)
	{
		After_main();
	}
	else
	{
Wrong_Password_Comm:
		lk_dispclr();
		lk_disptext(2,2,"Wrong Password",1);	    	
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Wrong_key_Comm = lk_getkey();
		if(Wrong_key_Comm==ENTER)
		{
		admin_pass();
		}
		else goto Wrong_Password_Comm;
	}
		
}



/*******************************************************************/
int chk_date (char *dt)
{
        char str[3];
	int epos_date=0,epos_month=0,epos_year=0; 

	printf("\nDate str:%s\n",dt);
	memset(str,0,3);
        str[0]=dt[0];
        str[1]=dt[1];
        str[2]=0x00;
	epos_date = atoi(str);

	memset(str,0,3);
	str[0]=dt[2];
        str[1]=dt[3];
        str[2]=0x00;
	epos_month = atoi(str);

	memset(str,0,3);
	str[0]=dt[4];
	str[1]=dt[5] ;
	str[2]=0x00;
	epos_year = 2000 + atoi(str);

	//printf("\nDate:%d/%d/%d",(curt.tm_mday),(curt.tm_mon),(curt.tm_year));	

	printf("\n1:%d/%d/%d\n",epos_date,epos_month,epos_year);
	if ( epos_month == 0 || epos_date == 0 || epos_year < 2008 ) return ERROR ;
	else if(epos_month == 1 || epos_month == 3 || epos_month == 5 || epos_month == 7 || epos_month == 8 ||epos_month ==10 ||epos_month == 12)
	{
		printf("\n2:%d/%d/%d\n",epos_date,epos_month,epos_year);
		if (epos_date > 31) return ERROR;
		else return 1;
	}	
	else  if (epos_month == 4 || epos_month == 6 || epos_month == 9 || epos_month == 11)
	{
		printf("\n3:%d/%d/%d\n",epos_date,epos_month,epos_year);
		if (epos_date > 30) return ERROR;
		else return 1;
	}
	else if  (epos_month == 2 )
	{
		printf("\n4:%d/%d/%d\n",epos_date,epos_month,epos_year);
		 if ( !(epos_year%400) || (epos_year%100 != 0 && epos_year%4==0 ) )
		 { 
			if (epos_date > 29 ) return ERROR;
			else return 1;
		 }
		 else
		 {  
			if( epos_date > 28 ) return ERROR;
			else return 1;
		 }				
	}	
	else return 0;
}
/*******************************************************************************/
//Server time Date update
/******************************************************************************/
int HHT_Date_setting(struct tm *curt,char* HHT_str)
{
	printf("\nServer Date");
	if(HHT_str[0]!=0&&HHT_str[1]!=0&&HHT_str[2]!=0&&HHT_str[3]!=0&&HHT_str[4]!=0&&HHT_str[5]!=0)
	{
	 printf("\nUpdate server date\n");
         curt->tm_mday = (HHT_str[0]-0x30)*10+(HHT_str[1]-0x30);
         curt->tm_mon  = (HHT_str[2]-0x30)*10+(HHT_str[3]-0x30)-1;
         curt->tm_year = (HHT_str[4]-0x30)*10+(HHT_str[5]-0x30)+100;
	}
         return 0;
}

/*******************************************************************/
int HHT_time_setting(struct tm *curt,char* HHT_str)
{
	printf("\nServer Time");
        curt->tm_hour=(HHT_str[0]-0x30)*10+(HHT_str[1]-0x30);
        curt->tm_min=(HHT_str[2]-0x30)*10+(HHT_str[3]-0x30);
        curt->tm_sec=(HHT_str[4]-0x30)*10+(HHT_str[5]-0x30);
	return 0;
}	


/*******************************************************************/
int admin_set_date_time()
{
        int ret_dt,ret=0,ret1=0;
        unsigned char ptr[10];
	char passwordlog2[9],passvalid[9]="40639534";
	int retval,Invalid_key;
	printf("\npassvalid:%d\n",strlen(passvalid));
	memset(passwordlog2,0,sizeof(passwordlog2));
	lk_dispclr();
	lk_disptext(2,0,"Enter Password",0);
	retval=lk_getpassword(passwordlog2,4,10);
        printf("retval=%d\n",retval);		
	passwordlog2[retval]='\0';
	printf("\npass:%s",passwordlog2);
	printf("\npasswordlog2:%d\n",strlen(passwordlog2));
	printf("\npassvalid:%s",passvalid);
	if(strcmp(passvalid,passwordlog2)==0)
	{
		lk_getrtc(&curt);
                time_setting(&curt);
                lk_setrtc(&curt);
                        
                lk_getrtc(&curt);
                Date_setting(&curt);
                lk_setrtc(&curt);
		printf("\nSet Date:%d/%d/%d",(intim.tm_mday),(intim.tm_mon+1),(intim.tm_year-100));

	}
	else
	{
Invalid_password:
		lk_dispclr();
               	lk_disptext(2,0,"Invalid Password",1);
                lk_disptext(4,0,"Enter Valid password",0);		
		lk_disptext(5,0,"Press Enter to Cont.",0);
		Invalid_key = lk_getkey();
		if(Invalid_key==ENTER)
		{
		admin_menu();
		}
		else goto Invalid_password;
	}
admin_menu();
}

/*******************************************************************/	
int time_setting(struct tm *curt)
{
	char str[10];
	int rval;
	while(1)
         {
		memset(str,0x00,10 );
                lk_dispclr(); 
        	lk_disptext(2,0,"Enter Time (HHMMSS) ",0);
                lk_disptext(5,0,"Enter in 24HRS Format",0); 
                rval=lk_getnumeric(4,1,str,6,strlen(str),0);
        	if(rval < 0)
                return 0;
	
        	if(chk_time(str)==0 && rval==6 )                                                            
               	break;

                else 
                {
                 lk_dispclr();
                 lk_disptext(3,4,"TRY AGAIN",1);
                 printf("ENTER CORRECT DATA\n");  
	         if (CANCEL==lk_getkey())
	         return -1;
                }
   
         }
        curt->tm_hour=(str[0]-0x30)*10+(str[1]-0x30);
        curt->tm_min=(str[2]-0x30)*10+(str[3]-0x30);
        curt->tm_sec=(str[4]-0x30)*10+(str[5]-0x30);

        return 0;
}


/*******************************************************************/
int Date_setting(struct tm *curt)
{
        char str[10];
        int rval;

        while(1)
         {
           memset(str,0,10);
           lk_dispclr();
           lk_disptext(2,0,"Enter Date (DDMMYYYY)",0);
        rval = lk_getnumeric (4, 1,str, 8,strlen(str),0);
           if(rval<0)  return 0;  

           if(chk_date_admin(str) == 0  && rval == 8 )  break;
	   else 	
             {
              lk_dispclr();
              lk_disptext(3,4,"TRY AGAIN",1);
              printf("ENTER CORRECT DATA\n");
              if (CANCEL==lk_getkey())
              return 0;
             }
      	}
         curt->tm_mday = ( str[0]-0x30)*10+(str[1]-0x30);
	 curt->tm_mon  = ( str[2]-0x30)*10+(str[3]-0x30) -1; 
	 curt->tm_year = (( str[4]-0x30)*1000 + (str[5]-0x30)*100 + (str[6]-0x30)*10 + (str[7]-0x30)) -1900 ;
	 return 0;
}
/*******************************************************************/
                                                                                                
int chk_time (char *str)
{
	int HH,MM,SS;
	HH=(str[0]-0x30)*10+(str[1]-0x30);
        MM=(str[2]-0x30)*10+(str[3]-0x30);
        SS=(str[4]-0x30)*10+(str[5]-0x30);

	if ( HH < 0 || HH > 23 || MM < 0 || MM > 59 || SS < 0 || SS > 59 )
	return ERROR;

	return 0;
}

/*************************************************************/
int chk_date_admin (char *str)
{
	int epos_date=0,epos_month=0,epos_year=0; 

         epos_date  = ( str[0]-0x30)*10+(str[1]-0x30);
         epos_month = ( str[2]-0x30)*10+(str[3]-0x30);
         epos_year  = ( str[4]-0x30)*1000+ (str[5]-0x30)*100 + (str[6]-0x30)*10 + (str[7]-0x30);


	if ( epos_month < 1 || epos_date < 1 || epos_date > 31 || epos_month > 12  ||  epos_year < 2008 ) return ERROR ;

	else if(epos_month == 1 || epos_month == 3 || epos_month == 5 || epos_month == 7 || 										epos_month == 8 || epos_month ==10 ||epos_month == 12)
		{

			if (epos_date > 31)
			return ERROR;
		}	

	 else  if (epos_month == 4 || epos_month == 6 || epos_month == 9 || epos_month == 11)
		{

		if (epos_date > 30)
			return ERROR;
		}

	else if  (epos_month == 2 )

		{
		 if ( !(epos_year%400) || (epos_year%100 != 0 && epos_year%4==0 ) )
			 { if (epos_date > 29 ) return ERROR;}

			else  if( epos_date > 28 ) return ERROR;				
		}	
				
			
	return 0;
}

/************************************************************/
int lk_getrtc_new(struct tm *curt)
{
        int ret;
        struct tm tc,td,*tt;
        static int cnt=1;
        struct timeval tv;

        tt=&td;
        memset(&tc,0,sizeof(struct tm));
        //time is taken from Linux
        if( gettimeofday (&tv,NULL) ==-1)
        {
                fprintf(stdout, "Error1\n");    
                return -1;      
        }
        tt=gmtime (&tv.tv_sec);
  
	if(cnt++==1 )
        {
                ret=lk_getrtc(&tc); 
                if(ret==-1)
                {
                        fprintf(stdout, "Error2\n");
                        goto ERRORS;
                }
                if((tc.tm_sec>=0&&tc.tm_sec<=59)&&(tc.tm_min>=0&&tc.tm_min<=59)&&(tc.tm_hour>=0&&tc.tm_hour<=23))
                {
                 if((tc.tm_mday>=1&&tc.tm_mday<=31)&&(tc.tm_mon>=0&&tc.tm_mon<=11)&&(tc.tm_year>=100&&tc.tm_year<=137))
                        {

                        }
                        else 
                        {
                                fprintf(stdout, "Error3\n");
                                 goto ERRORS;
                        }
                }
                else 
                {
                        fprintf(stdout, "Error4\n");
                         goto ERRORS;
                }
                if( (tt->tm_sec>=0 && tt->tm_sec<=59)   &&  (tt->tm_min>=0 && tt->tm_min<=59)  &&  (tt->tm_hour>=0 && tt->tm_hour<=23))
                {
                        if( (tt->tm_mday>=1  && tt->tm_mday<=31)   &&  (tt->tm_mon>=0 && tt->tm_mon<=11)  &&  (tt->tm_year>=110 && tt->tm_year<=137))
                        {

                        }
                        else
                        {
                                fprintf(stdout, "Error5\n");
                                 goto ERRORS;
                        }
                }
                else
                {
                        fprintf(stdout, "Error6\n");
                         goto ERRORS;
                }
        }
	curt->tm_hour = tt->tm_hour;
        curt->tm_min  = tt->tm_min;
        curt->tm_sec  = tt->tm_sec;
        curt->tm_year = tt->tm_year;
        curt->tm_mon  = tt->tm_mon;
        curt->tm_mday = tt->tm_mday;
        curt->tm_wday = tt->tm_wday;
        curt->tm_yday = tt->tm_yday;
        curt->tm_isdst = tt->tm_isdst;
        return 0;
ERRORS:  return -1;
}


/******************************************************************/
int chk_rtc()
{
	int ret,chk_rtc_index,rtc_flag=0;
	

	for (chk_rtc_index=0; chk_rtc_index<3;chk_rtc_index++) {
		memset(&curt,0,sizeof(struct tm));
		ret=lk_getrtc_new(&curt);
		if (ret == 0){rtc_flag=1;break;}
	}
	if(rtc_flag!=1)
	{
	        printf("\nRTC Read Error\n");
		lk_dispclr();
		lk_disptext(1,7,"  HDFC",0);
		lk_disptext(3,0,"   RTC Not Working",0);
		lk_disptext(4,0,"Pls Restart Terminal",0);
		lk_disptext(5,0,"    And Check ",0);
		lk_getkey();
		lk_dispclr();
   		lk_disptext(3,1,"  Please Wait...",1);
		close_sqlite();
		lk_close();
		system("poweroff"); 	
		sleep(20);
		system("shutdown");
		sleep(10);
	}
return 1;
}


/******************************************************************************/
int rtc_ckeck_year()
{
	long int Current_year=0;

	Current_year=((curt.tm_year+1900));
	printf("\nCurrent_year:%ld",Current_year);
	
	if(Current_year>2050) 
	{
		printf("\nRTC Read Error\n");
		lk_dispclr();
		lk_disptext(2,0,"  RTC ",0);
		lk_disptext(3,0,"Pls Restart Terminal",0);
		lk_disptext(4,0,"    And Check ",0);
		lk_getkey();
		close_sqlite();
		lk_dispclr();
		lk_disptext(3,1,"Shutdown..",1);
		ppp_close();
		lk_close();   
		system("poweroff");
		sleep(13);
	}
	else if(Current_year>=2015) return 0;
	else
	{
		printf("\nRTC Read Error\n");
		lk_dispclr();
		lk_disptext(2,0,"  RTC ",0);
		lk_disptext(3,0,"Pls Restart Terminal",0);
		lk_disptext(4,0,"    And Check ",0);
		lk_getkey();
		close_sqlite();
		lk_dispclr();
		lk_disptext(3,1,"Shutdown..",1);
		ppp_close();
		lk_close();   
		system("poweroff");
		sleep(13);
	}
return 0;
}	


/******************************************************************/
void lk_buff_clear (void)
{
	int i=1000;
	while(i--)
	lk_getkey_wait();
}

