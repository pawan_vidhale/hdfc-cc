/*******************************************************************
			   HDFC Bank CC
		            header.h
/*******************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/ioctl.h>
#include <string.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <time.h>
#include <net/if.h>
#include <dirent.h>
#include <regex.h>
#include <0202lcd.h>
#define SUCCESS 0
#define ERROR -1

#define F1 17
#define F2 18
#define F3 19
#define F4 20
#define F5 21
#define F6 22

#define AVAILABLE		1
#define UNAVAILABLE		0
#define Rate_File		6
#define ENTER   15             
#define CANCEL  12

#define RES32 5                 // No.of menu items to be displayed at once
#define UprOffset 0             // Title row to be displayed
#define MAX_MENU_ENTRIES 40
#define MAX_ENTRY_LENGTH 21
#define MAX_MENU_ENTRIES_L 3000
#define MAX_ENTRY_LENGTH_L 21
#define FALSE 0
#define TRUE  1

#define PRINTF			0
#define PRINTF_IN_DB		0
#define PRINTF_SQL_Error	0
#define PRINTF_UPLOAD		1
/******************************************/
#define URL_NETMAGIC		1
#define URL_TEST		0
/*******************************************/

#define PUBLIC_CERTIFICATE "/mnt/jffs2/public.cer"


extern struct tm curt;
extern struct stat st;

/******************************************************************************/

extern int downloadstatus;
extern int filestatus;
extern int Flag_Logout;
/******************************************************************************/
typedef struct result_set {
                                  int rows;
                                  int cols;
                                  char ***recordset;
                          } resultset;
typedef struct
{
    short start;
    short maxEntries;
    char selectedIndex;
    char title[20];
    char menu[MAX_MENU_ENTRIES][MAX_ENTRY_LENGTH];

}MENU_T;

/*******************************************************************************/
/*******************************************************************************/

typedef struct
{
    short start;
    short maxEntries;
    char selectedIndex;
    char title[20];
    char menu[MAX_MENU_ENTRIES_L][MAX_ENTRY_LENGTH_L];

}MENU_L;



typedef struct {
                 char phnum[32];
                 char apnnum[32];
                 char username[32];
                 char password[32];
                 char timeout[8];
                 char command[32];
               } GPRS_SET;

 GPRS_SET gprs_settings;

extern int gprsconnection;

void prn_header_footer(unsigned int);
/******************************************************************************/

/******************************************************************************/
resultset get_result(const char* fmt, ...);
                                                                                                                             
int execute(const char *fmt, ...);

void free_result(resultset resultset_table);
                                                                                                                             
int err_printf(const char *fmt, ...);
                                                                                                                             
char *m_fgets(char *s, int n, FILE *f);

int open_sqlite(char *);
                                                                                                                             
int close_sqlite();

/******************************************************************************/

/******************************************************************************/

